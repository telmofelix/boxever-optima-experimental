[TOC]


# Release Notes

## Unreleased

* BOX-26645 - Enable events_checkout for Ryanair

## Release 1.2.0 (13-11-19)

* BOX-26277 - Add Export Lifecycle Rule
* BOX-25337 - Add flows lookup table in Optima/Looker
* BOX-26274 - Provision the pipeline in Dev
* BOX-25541 - Add Decision and Decision Variants lookup tables in Optima/Looker
* BOX-25942 - Add headers to offers table
* BOX-26202 - Add event_add to pipeline (exploded product in Trusted)... see PR >>>BREAKING CHANGE<<<
* BOX-26432 - Scheduled Qatar pipeline
* BOX-26530 - Mapping fix for events_tracking_execution offer_ref... see PR >>>BREAKING CHANGE<<<

## Release 1.1.4 (22-10-19)

* BOX-25855 - Removed day names from export folders
* BOX-26239 - Support flatten array of strings (e.g. address, phone numbers, etc)
* BOX-26178 - Events Stats table not working for Ryanair export

## Release 1.1.3 (04-10-19)

* BOX-26067 - Enable AIB Offers
* LABS-1553 - Updated to use a new version of the onelogin-aws-cli.jar which was released with updated command line parameters
* BOX-25969 - Updated AIB Production configuration file for the initial deployment
* BOX-26018 - Disabled AIB UAT
* BOX-26028 - Increase delay to decrease chances of duplicate pipeline runs


## Release 1.1.2 (17-09-19)

* BOX-25545 - Create new derived table order_item_flight_view
* BOX-25331 - Add segment membership to Optima
* LABS-865 - Optima - Provision Qatar Pipeline
* BOX-25332 - Add offers lookup table in Optima/ Looker
* BOX-25857 - Exclude trusted data sets from Optima Export
* BOX-25854 - Notify Customer when export is complete


## Release 1.1.1 (10-09-19)

* BOX-25761 - Refactor Optima Export bucket policy
* BOX-25799 - Disable Cloudwatch Ryanair
* BOX-25797 - Remove Add Partitions from Data Extensions and UTMs
* BOX-25812 - Trusted Events missing from Export Ryanair


## Release 1.1.0 (04-09-19)

* BOX-25650 - Optima - Implement Export in Pipeline
* BOX-25696 - Partitions not added in events
* BOX-25708 - Update Lambda function to execute only once for S3 trigger
* BOX-25748 - Add ARN to quarantine functions
* BOX-25747 - Adding order_ref to events checkout



## Release 1.0.6 (23-08-19)

* BOX-25611 - Data Quality LookML should run independently of the main LookML
* BOX-25626 - Add env variable to run cloudwatch function to run optima from s3 trigger
* BOX-25599 - Optima - Remove Event De Duplication Ryanair
* BOX-25635 - Improve aggregations in LookML Generation
* BOX-25644 - Unschedule Emirates Explore
* BOX-25645 - Adjust Ryanair DPUs


## Release 1.0.5 (20-08-19)

* BOX-25454 - Add missing modifiedAt in order_items for Banking Trusted phase
* BOX-25101 - Optima - Increase Pipeline execution by running tasks in parallel
* BOX-25473 - Optima - Prototype missing in Data Quality
* BOX-25474 - Optima - LookerML Error in Emirates Duplicate entries
* BOX-25483 - Optima - Data Quality Report Error with the new Schema Service
* BOX-25254 - Add filter on category to event_tracking_execution, event_tracking_interaction tables
* BOX-25492 - Optima - Timeout long executions after X number of minutes
* BOX-25508 - Fixing typo on modified_at for order_item_flights
* BOX-25100 - Optima - Add partitions instead of table repair for events
* BOX-25498 - Enable language field in sessions
* BOX-25226 - Enable status field in order_items
* BOX-25549 - Optima - Duplicate guest_ref in schema service
* BOX-25546 - Optima - Step Functions output size Exception


## Release 1.0.4 (08-08-19)

* BOX-25371 - Optima - Add configuration to only push views and not the model for LookML
* BOX-25372 - Optima - Make LookerML model configurable per industry/phase
* BOX-25419 - Optima - Enable or Disable PII for Data Quality and LookML
* BOX-25440 - Source field is actually cmpid, not cmp_id
* BOX-25338 - Add modified_at for other entities
* BOX-25394 - Optima - Retry long running executions
* BOX-25452 - Optima - Enable Emirates in Labs
* BOX-25453 - Optima - Error Running Session UTMs


## Release 1.0.3 (06-08-19)

* LABS-1280 - Added minimum, maximum checks on ints and min and max length checks on string
* LABS-1396 - Pipeline should run if there is no data write empty result
* BOX-25174 - Optima - Create new project for data quality reports
* BOX-25185 - Add session_ref field for orders
* BOX-25220 - Add reference_id field for orders
* BOX-25230 - Write quarantine summary for quarantine alert
* BOX-25236 - Missing enum for type field validation on events
* BOX-25239 - Optima -  Enable and test Events and Sessions Data Quality
* BOX-25249 - Optima - Create master branch after creating repo data quality
* BOX-25255 - Add cmp_id field for events view
* BOX-25285 - Optima - Add Summary Report to Data Quality
* BOX-25284 - Optima - Split Data quality function in two
* BOX-25199 - Optima - Make data quality customizable per customer
* BOX-25187 - Optima - Add Data Quality for Data Extensions
* BOX-25188 - Optima - Automatically merge to master dashboard reports
* BOX-25319 - Optima - Reduce number of queries data quality
* BOX-25159 - Optima - Add Data Quality Dashboards to pipeline
* BOX-25130 - Add CI for Python aws_lambda project
* BOX-25229 - Optima - remove optima-data-pipeline-cloud-watch-rule-input.json
* BOX-25320 - Optima - Split Historical and current year dashboard
* BOX-25336 - Optima - Enable Orders Ryanair
* BOX-25111 - Optima - Automate Looker setup
* BOX-25379 - Optima - Duplicate data Volaris Explore
* BOX-25098 - Optima - Events should automatically pick up the right number of days based on previous run
* BOX-25160 - Optima - Add support for trigger in S3 to start the pipeline as soon as the Snapshot job completes


## Release 1.0.2 (17-07-19)

* LABS-865 - Optima - Provision Qatar Pipeline
* LABS-1475 - Enabling modified_at field in sessions
* LABS-1474 - Documented using Temporary AWS Credentials to connect to Athena via DBeaver
* LABS-1461 - Optima - Pipeline not reporting as failure
* LABS-1447 - Optima - Check Export Success after run
* LABS-1477 - Optima - Fix Prototype/Phase1 specs
* BOX-25158 - Optima - Add Sessions, events and orders to data quality dashboard



## Release 1.0.1 (15-07-2019)

* LABS-1408 - Optima - Check Pipeline status fails to check the output
* LABS-1407 - Optima - Export Fails when new root folders are added
* LABS-1347 - Optima - Adding event_tracking_execution, event_tracking_interaction tables
* LABS-1348 - Optima - Retail Banking Specs
* LABS-1411 - Optima - Fix status and DB names
* LABS-1413 - Optima - Run Ryanair pipeline 2 hours earlier
* LABS-1412 - Optima - Use full prefix for Export
* LABS-1397 - Optima - Add pipeline PENDING/SKIPPED/etc defaults to default/main.yml, enable overrides in customer specific env files
* LABS-1386 - Optima - Split Input files into smaller files
* LABS-1443 - Optima - Lambda vars should be set in runOptima and passed as parameters
* LABS-1458 - Optima - Enable Event search for volaris and emirates
* LABS-1460 - Optima - Search Event Failure
* LABS-1452 - Optima - add bx_batch_id partition for quarantine events
* LABS-1451 - Optima - Deploy new tracking events to Ryanair
* LABS-1445 - Optima - Contact Request and Calculator events are missing from aib-uat
* LABS-1453 - Optima - Fix renaming of status fields in event tracking execution/interaction
* LABS-1450 - Optima - Remove lifecycle policy for trusted events?
* LABS-1426 - Optima - Stats should have its own input
* LABS-1461 - Optima - Pipeline not reporting as failure
* LABS-1466 - Optima - Add ansible var for export stack


### 1.0.0 (26-06-2019)

* LABS-1395 - Adding Identity Event to pipeline
* LABS-1398 - Enable Search Events, Sessions on Ryanair
* LABS-1400 - Optima - Increase retries and timeout lambda
* LABS-1399 - Optima - Setup Cloudwatch for Ryanair
* LABS-1401 - Optima - Increase Guest DPUs Ryanair


### 1.0.0-rc11 (25-06-2019)

* LABS-1391 Event Pipeline Error

### 1.0.0-rc10 (24-06-2019)

* LABS-1350 - Optima - Tech Debt - Refactor event delete step
* LABS-1368 - Optima - Deactivate Job per customer
* LABS-1387 - Fix pipeline state output read
* LABS-1389 - Add CloudWatch support to Pager Duty customer event transformer
* LABS-1388 - Optima - Set Stats requirements and do not run job if one of the dependencies is skipped

### 1.0.0-rc9 (19-06-2019)

* LABS-1340 - Fixed Optima data catalog ansible module generating transformation params incorrectly
* LABS-1329 - Adjusted job DPUs for Ryanair production
* LABS-1333 - Finalizing spec for Explore Event Tracking
* LABS-1343 - Optima - Implement export script. In /export folder
* LABS-1322 - Duplicate Events Rerun Fix and new batch id
* LABS-1336 Create AWS Lambda to notify to Pager Duty
* LABS-1337 Change "- failed" Pass tasks to Task type, and call Lambda

### 1.0.0-rc8 (12-06-2019)

* LABS-1332 - Added missing category enums (EXECUTED and FAILED) for tracking events
* LABS-1330 - Fixed issue where source types in mapping were not being used.

### 1.0.0-rc7 (11-06-2019)

* LABS-1321 - Fixed duplicates in sessions and session_stats tables
* LABS-1320 - Fixed typos in created_at and modified_at
* LABS-1319 - Change alert statistic
* LABS-1318 - Enable SNS alert subscription
* LABS-1316 - Added events_campaign_tracking
* LABS-1263 - Enabling session stats searches/views
* LABS-1246 - Added in Airline phase 1 specs, ddls and views

### 1.0.0-rc6 (05-06-2019)

* LABS-1312 - Fix missing iam role for step functions
* LABS-1303 - Remove where clause and process always 7 days old events
* LABS-1246 - Added support for different industries and phases

### 1.0.0-rc5 (04-06-2019)

* LABS-1296 - Updated Guest Data Extensions, Sessions UTMs and Stats with prefix
* LABS-1296 - Updated Guest Data Extensions, Sessions UTMs and Stats data location to root of the S3 bucket
* LABS-1296 - Updated Athena Results data location to root of the S3 bucket
* LABS-1292 - Added lifecycle rules to delete data after 6 days
* LABS-1281 - Fixed Guest Data Extension, Session UTM View and Stats Views
* LABS-1236 - Renamed optima-explore-specs to optima-specs
* LABS-1208 - Add support for database views and rollback capability based on a 7 day sliding window

### 1.0.0-rc4 (04-06-2019)

* LABS-1287 - Pipeline cron is now MON-SUN
* LABS-1282 - Added Ryanair UAT pipeline config

### 1.0.0-rc3 (31-05-2019)

* LABS-1274 - Updated Ansible data-catalog to support full-date
* LABS-1274 - Updated Ansible data-catalog to support not support long and date-time
* LABS-1241 - Increased Emirates DPUs to 240 due to Spark writer frame issue
* LABS-1239 - Allow customer overrides of optima explore specs
* LABS-1237 - Disabled s3 bucket versioning
* LABS-1235 - Renamed optima-explore-ddls to optima-ddls
* LABS-1234 - Changed data dir from data/explore dir to data/optima
* LABS-1228 - Fixed issue with Guest Data Extension schema and data generation location being incorrect
* LABS-1228 - Updated docs with AWS Glue Security Configuration manual steps
* LABS-1225 - Pipeline create bucket should be first env provisions
* LABS-1224 - Shortened lambda function names less than 65 characters
* LABS-1223 - Provisioned Emirates Pipeline in Explore Production
* LABS-1188 - Process Events in a delta manner (appending to history based on modifiedAt from previous day)

### 1.0.0-rc2 (24-05-2019)

* LABS-911 - Added Max Records Per File and Where support to Optima Specification v2
* LABS-911 - Added Event Raw Json 2 Events Raw Parquet Job
* LABS-910 - Added Downloadable Glue Assembly Jar
* LABS-909 - Added Parquet Tools Quick Start Guide
* LABS-908 - Optima Specification v2 updated to be more inline with v3
* LABS-907 - Added support to specify multiple source locations in specifications
* LABS-906 - Added better handling of reading specifications using classpath://
* LABS-897 - Removed Generation 1 of OptimaETL
* LABS-893 - Create Ansible Module for Step Functions

### 1.0.0-rc1 (28-01-2019)

* LABS-896 - Added Release Notes
* LABS-892 - Allow string replacement in Optima specification
* LABS-890 - Create Glue Lambda Functions & IAM Roles
* LABS-875 - Analytics - Prototype Optima ETL Script
* LABS-877 - Determine whether to use int64 or int96 to represent timestamps
* LABS-876 - Determine approach for partitioning and potential case sensitivity issues
* LABS-878 - Analytics - Rename optimo to optima
* LABS-888 - Hive and Presto Quickstart