import boto3

EXECUTION = 'arn:aws:states:eu-west-1:228626570070:execution:boxever-optima-emirates-labs-data-pipeline-guests:318134f5-3468-4868-887e-a681c7cce5e9'

TARGET = "SQL" # Use ETL, Stats or SQL

client = boto3.client('stepfunctions')

token = '-'
start = None
end = None
while token:

    if token != '-':
        response = client.get_execution_history(
            executionArn=EXECUTION,
            maxResults=1000,
            nextToken=token
        )
    else:
        response = client.get_execution_history(
            executionArn=EXECUTION,
            maxResults=1000
        )

    if 'nextToken' in response:
        token = response['nextToken']
        #print("Next Token: " + token)
    else:
        token = None

    for event in response['events']:

        if isinstance(event.values()[3], dict) and 'name' in event.values()[3]:

            step = event.values()[3]['name']

            timestamp = event.values()[0]
            if TARGET in step:

                if '-' not in step and '?' not in step:
                    start = timestamp
                elif 'Success' in step:
                    end = timestamp
                    minutes_diff = (end - start).total_seconds() / 60.0
                    name = step.split('-')[0].strip()
                    print(name + " started: " + str(start) + " ended: " + str(end) + " TOTAL: " + "{0:.2f}".format(minutes_diff)  + "min")
