#!/bin/bash
# https://docs.aws.amazon.com/glue/latest/dg/dev-endpoint-tutorial-local-notebook.html
usage() {
    echo "./zeppelin.sh {COMMAND}";
    echo "Example: ./zeppelin.sh start"
    echo "Example: ./zeppelin.sh stop"
    exit 1;
}

if [ $# -ne 1 ]; then
    usage
fi

if [ ! -d "zeppelin-0.7.3-bin-all" ]; then
    echo Downloading Zeppelin
    if [ ! -f zeppelin-0.7.3-bin-all.tgz ]; then
        curl http://archive.apache.org/dist/zeppelin/zeppelin-0.7.3/zeppelin-0.7.3-bin-all.tgz -o zeppelin-0.7.3-bin-all.tgz
    fi
    tar -xvzf zeppelin-0.7.3-bin-all.tgz
fi

COMMAND=$1

if [ "${COMMAND}" == "start" ]; then
    echo Starting Zeppelin
    zeppelin-0.7.3-bin-all/bin/zeppelin-daemon.sh start
fi
if [ "${COMMAND}" == "stop" ]; then
    echo Stopping Zeppelin
    zeppelin-0.7.3-bin-all/bin/zeppelin-daemon.sh stop
fi