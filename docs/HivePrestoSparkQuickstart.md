[TOC]

# Overview
Below is a quickstart guide of connecting to an Explore instance which is running Hive, Spark and Presto via EMR.

## SSH
The following is an example .ssh/config for connecting to Explore Spinair on Labs.

Note: A prerequisite is to ask Labs to include you ssh public key in the Explore Spinair settings.


    Host explore
        User ubuntu
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
        LogLevel ERROR
        Hostname explore-spinair-bastion.boxever.io 
    
    Host emr
        User hadoop
        Hostname explore-emr.boxever.io
        StrictHostKeyChecking no
        UserKnownHostsFile /dev/null
        LogLevel ERROR
        ProxyCommand ssh explore nc %h %p
        
Connect to Explores EMR server


    ssh emr
    
Once connected you should see something like the following

    Last login: Wed Jan 16 15:06:04 2019
    
           __|  __|_  )
           _|  (     /   Amazon Linux AMI
          ___|\___|___|
    
    https://aws.amazon.com/amazon-linux-ami/2018.03-release-notes/
    8 package(s) needed for security, out of 12 available
    Run "sudo yum update" to apply all updates.
    
    EEEEEEEEEEEEEEEEEEEE MMMMMMMM           MMMMMMMM RRRRRRRRRRRRRRR
    E::::::::::::::::::E M:::::::M         M:::::::M R::::::::::::::R
    EE:::::EEEEEEEEE:::E M::::::::M       M::::::::M R:::::RRRRRR:::::R
      E::::E       EEEEE M:::::::::M     M:::::::::M RR::::R      R::::R
      E::::E             M::::::M:::M   M:::M::::::M   R:::R      R::::R
      E:::::EEEEEEEEEE   M:::::M M:::M M:::M M:::::M   R:::RRRRRR:::::R
      E::::::::::::::E   M:::::M  M:::M:::M  M:::::M   R:::::::::::RR
      E:::::EEEEEEEEEE   M:::::M   M:::::M   M:::::M   R:::RRRRRR::::R
      E::::E             M:::::M    M:::M    M:::::M   R:::R      R::::R
      E::::E       EEEEE M:::::M     MMM     M:::::M   R:::R      R::::R
    EE:::::EEEEEEEE::::E M:::::M             M:::::M   R:::R      R::::R
    E::::::::::::::::::E M:::::M             M:::::M RR::::R      R::::R
    EEEEEEEEEEEEEEEEEEEE MMMMMMM             MMMMMMM RRRRRRR      RRRRRR
    
    [hadoop@ip-10-0-0-162 ~]$    
    
    
    
    
# Hive Quickstart
After connecting to Explore via ssh you need to open the Hive CLI.

## Open Hive CLI

    $ hive
    
## Show Schemas available in Hive

### Show Schemas SQL

    hive>show schemas;    
    
### Show Schemas SQL Result

    OK
    default
    oneview
    workspace
    Time taken: 0.668 seconds, Fetched: 3 row(s)
    
as you can see Hive has three schemas, default, oneview and workspace;

## Show Tables available in Hive oneview schema

### Show Tables SQL

    hive>show tables from oneview;
    
### Show Tables SQL Result

    OK
    events
    guestdataextensions
    guestidentifiers
    guests
    guestsubscriptions
    orderconsumerorderitems
    orderconsumers
    orderdataextensions
    orderitemflightsegments
    orderitems
    orders
    sessiondataextensions
    sessions
    Time taken: 0.039 seconds, Fetched: 13 row(s)
    
As you can see there are thirteen tables in the oneview schema
    
## Show Create Table in Hive oneview.guests

### Show Create Table SQL

    hive>use oneview;
    hive>show create table guests;
    
### Show Tables SQL Result

    OK
    CREATE EXTERNAL TABLE `guests`(
      `city` string,
      `clientkey` string,
      `country` string,
      `createdat` bigint,
      `dataextensions` array<struct<createdAt:bigint,key:string,modifiedAt:bigint,name:string,ref:string,values:struct<GUID:string,Name:string,channel:string,created:string,currency:string,facebook_connect_likes:array<struct<category:string,category_list:array<struct<id:string,name:string>>,created_time:string,id:string,name:string>>,firstname:string,foo:string,language:string,lastname:string,mail:string,memberId:string,memberProgram:string,page:string,paging:struct<cursors:struct<after:string,before:string>,next:string>,points:bigint,pos:string,preference_html_email:string,product:struct<consumer:array<struct<dob:string,ffn:string,firstname:string,lastname:string,nationality:string,passport_expire_date:string,passport_no:string,pax_type:string,title:string>>,currency:string,item_id:string,name:string,price:string,sku:string,type:string>,provider:string,receive_advertise_email:string,status:string,type:string>>>,
      `dateofbirth` bigint,
      `email` string,
      `firstname` string,
      `firstseen` bigint,
      `gender` string,
      `guesttype` string,
      `identifiers` array<struct<expiryDate:bigint,id:string,provider:string>>,
      `language` string,
      `lastname` string,
      `lastseen` bigint,
      `modifiedat` bigint,
      `nationality` string,
      `passportnumber` string,
      `phonenumbers` array<string>,
      `postcode` string,
      `ref` string,
      `state` string,
      `street` array<string>,
      `subscriptions` array<struct<channel:string,effectiveDate:bigint,name:string,pointOfSale:string,status:string>>,
      `title` string)
    ROW FORMAT SERDE
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
    STORED AS INPUTFORMAT
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
    OUTPUTFORMAT
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/oneview/2017/02/13/wjtc2eog1lvueo72kts3mn1ean0nentz/guests.parquet'
    TBLPROPERTIES (
      'transient_lastDdlTime'='1520014578')
    Time taken: 0.397 seconds, Fetched: 36 row(s)


Important things to note

* The table is listed as EXTERNAL. This means that if you drop the table the data will still exist.
* The ddl is hive specific therefore city is a string not a varchar like traditional databases.
* The row format is Parquet as per "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"
* The location of the data is in S3 as per the LOCATION field.  


## Select Some Data Table in Hive oneview.guests

### Select Some Data Table SQL

    hive>use oneview;
    hive>select ref from guests limit 5;
    
### Select Some Data Table SQL Result  

    OK
    SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
    SLF4J: Defaulting to no-operation (NOP) logger implementation
    SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
    1b5c2a34-f3a8-444d-b38e-8558e8de1bf7
    504892bd-f6ab-4edc-8959-2e2ed5971ced
    0ece3dd3-af47-4040-91f5-df2df3680d92
    cc2e7567-3ce4-4152-892c-f920ef82d4fc
    6d1df211-79b5-4445-a3d7-160d7c145009
    Time taken: 1.745 seconds, Fetched: 5 row(s)
    
## Exit Hive CLI

    hive>exit;
    
    
    

# Presto Quickstart
After connecting to Explore via ssh you need to open the Presto CLI.

## Open Presto CLI

    $ presto-cli
    
## Show Catalogs available in Presto

### Show Catalogs SQL

    presto> show catalogs;

### Show Catalogs SQL Result

      Catalog
    ------------
     hive
     mysql
     postgresql
     system
    (4 rows)
    
    Query 20190116_153048_02964_rtzxh, FINISHED, 2 nodes
    Splits: 36 total, 36 done (100.00%)
    0:00 [0 rows, 0B] [0 rows/s, 0B/s]

## Show Schemas available in hive

### Show Schemas SQL

    presto>show schemas from hive;    
    
### Show Schemas SQL Result

           Schema
    --------------------
     default
     information_schema
     oneview
     workspace
    (4 rows)
    
    Query 20190116_153150_02973_rtzxh, FINISHED, 3 nodes
    Splits: 36 total, 36 done (100.00%)
    0:00 [4 rows, 61B] [23 rows/s, 364B/s]
    
as you can see the Catalog Hive has three schemas, default, oneview and workspace. This is consistent with Hive Quickstart 
above apart from information_schema which is not displayed via the hive cli.

## Show Tables available in hive oneview schema

### Show Tables SQL

    presto>show tables from hive.oneview;
    
### Show Tables SQL Result

              Table
    -------------------------
     events
     guestdataextensions
     guestidentifiers
     guests
     guestsubscriptions
     orderconsumerorderitems
     orderconsumers
     orderdataextensions
     orderitemflightsegments
     orderitems
     orders
     sessiondataextensions
     sessions
    (13 rows)
    
    Query 20190116_153306_02983_rtzxh, FINISHED, 3 nodes
    Splits: 36 total, 36 done (100.00%)
    0:00 [13 rows, 410B] [34 rows/s, 1.06KB/s]
    
As you can see there are thirteen tables in the oneview schema. Also note that you must specify the full catalog.schema notation. 
Alternatively you can use the sql "use" to drop into the oneview schema allowing subsequent sql commands not have to 
include the full catalog.schema notation as we will see below.
    
## Show Create Table in hive hive.oneview.guests

### Show Create Table SQL

    presto>use hive.oneview;
    presto:oneview>show create table guests;
    
### Show Tables SQL Result

    ------------------------------------------------------------------------------------------------------------------------------------------------------------
     CREATE TABLE hive.oneview.guests (
        city varchar,
        clientkey varchar,
        country varchar,
        createdat bigint,
        dataextensions array(row(createdat bigint, key varchar, modifiedat bigint, name varchar, ref varchar, values row(guid varchar, name varchar, channel var
        dateofbirth bigint,
        email varchar,
        firstname varchar,
        firstseen bigint,
        gender varchar,
        guesttype varchar,
        identifiers array(row(expirydate bigint, id varchar, provider varchar)),
        language varchar,
        lastname varchar,
        lastseen bigint,
        modifiedat bigint,
        nationality varchar,
        passportnumber varchar,
        phonenumbers array(varchar),
        postcode varchar,
        ref varchar,
        state varchar,
        street array(varchar),
        subscriptions array(row(channel varchar, effectivedate bigint, name varchar, pointofsale varchar, status varchar)),
        title varchar
     )
     WITH (
        external_location = 's3://boxever-explore-spinair-labs-eu-west-1/oneview/2017/02/13/wjtc2eog1lvueo72kts3mn1ean0nentz/guests.parquet',
        format = 'PARQUET'
     )
    (1 row)


Important things to note

* The ddl is more standard ANSI SQL specific therefore city is a varchar not a string like hive.
* The format is Parquet as per the WITH options.
* The location of the data is in S3 as per the external_location field. external location is similar to hives CREATE EXTERNAL TABLE command. This means that if you drop the table the data will still exist.    

## Select Some Data Table in Hive hive.oneview.guests

### Select Some Data Table SQL

    presto>use hive.oneview;
    presto:oneview>select ref from guests limit 5;
    
### Select Some Data Table SQL Result  

                     ref
    --------------------------------------
     30123dc5-27ea-4d7a-bf7a-b44c8f18be19
     4677712c-079b-4c56-87c4-4a6087638b1d
     574a115a-2144-457e-811c-d2d1dc0e4f10
     f3013464-9bd7-44dd-b868-de86b77190d9
     f283d637-9980-4c3a-bda2-1e76f73ed36a
    (5 rows)
    
    Query 20190116_154206_03056_rtzxh, FINISHED, 2 nodes
    Splits: 21 total, 18 done (85.71%)
    0:01 [1.02K rows, 1.52MB] [1.33K rows/s, 1.98MB/s]
    
## Exit Presto CLI

    presto>exit;
    
    
   
    
# Spark Shell Quickstart
After connecting to Explore via ssh you need to open the Spark Shell CLI.

## Fix Missing Spark Shell Files
Spark shell will try to write to /stdout and /stderr, however on EMR due to permissions these files are missing and therefore results in the following error on shell start

    java.io.FileNotFoundException: /stdout (Permission denied)
    java.io.FileNotFoundException: /stderr (Permission denied)

To fix the error execute the following commands once

    $ sudo touch /stdout
    $ sudo touch /stderr
    $ sudo chown hadoop:hadoop /stdout
    $ sudo chown hadoop:hadoop /stderr
    
## Open Spark Shell CLI

    $ spark-shell
   
Yuo should see something like the following
 
    Welcome to
          ____              __
         / __/__  ___ _____/ /__
        _\ \/ _ \/ _ `/ __/  '_/
       /___/ .__/\_,_/_/ /_/\_\   version 2.4.0
          /_/
    
    Using Scala version 2.11.12 (OpenJDK 64-Bit Server VM, Java 1.8.0_191)
    Type in expressions to have them evaluated.
    Type :help for more information.
    
    scala>
    
    
## Load Parquet Data from S3

    scala>val sampleDF = spark.read.parquet("s3://boxever-explore-spinair-labs-eu-west-1/oneview/2017/02/13/wjtc2eog1lvueo72kts3mn1ean0nentz/guests.parquet/")

### Show Schema Table SQL

    scala>sampleDF.printSchema
   
### Show Schema SQL Result
   
    root
     |-- city: string (nullable = true)
     |-- clientKey: string (nullable = true)
     |-- country: string (nullable = true)
     |-- createdAt: long (nullable = true)
     |-- dataExtensions: array (nullable = true)
     |    |-- element: struct (containsNull = true)
     |    |    |-- createdAt: long (nullable = true)
     |    |    |-- key: string (nullable = true)
     |    |    |-- modifiedAt: long (nullable = true)
     |    |    |-- name: string (nullable = true)
     |    |    |-- ref: string (nullable = true)
     |    |    |-- values: struct (nullable = true)
     |    |    |    |-- GUID: string (nullable = true)
     |    |    |    |-- Name: string (nullable = true)
     |    |    |    |-- channel: string (nullable = true)
     |    |    |    |-- created: string (nullable = true)
     |    |    |    |-- currency: string (nullable = true)
     |    |    |    |-- facebook_connect_likes: array (nullable = true)
     |    |    |    |    |-- element: struct (containsNull = true)
     |    |    |    |    |    |-- category: string (nullable = true)
     |    |    |    |    |    |-- category_list: array (nullable = true)
     |    |    |    |    |    |    |-- element: struct (containsNull = true)
     |    |    |    |    |    |    |    |-- id: string (nullable = true)
     |    |    |    |    |    |    |    |-- name: string (nullable = true)
     |    |    |    |    |    |-- created_time: string (nullable = true)
     |    |    |    |    |    |-- id: string (nullable = true)
     |    |    |    |    |    |-- name: string (nullable = true)
     |    |    |    |-- firstname: string (nullable = true)
     |    |    |    |-- foo: string (nullable = true)
     |    |    |    |-- language: string (nullable = true)
     |    |    |    |-- lastname: string (nullable = true)
     |    |    |    |-- mail: string (nullable = true)
     |    |    |    |-- memberId: string (nullable = true)
     |    |    |    |-- memberProgram: string (nullable = true)
     |    |    |    |-- page: string (nullable = true)
     |    |    |    |-- paging: struct (nullable = true)
     |    |    |    |    |-- cursors: struct (nullable = true)
     |    |    |    |    |    |-- after: string (nullable = true)
     |    |    |    |    |    |-- before: string (nullable = true)
     |    |    |    |    |-- next: string (nullable = true)
     |    |    |    |-- points: long (nullable = true)
     |    |    |    |-- pos: string (nullable = true)
     |    |    |    |-- preference_html_email: string (nullable = true)
     |    |    |    |-- product: struct (nullable = true)
     |    |    |    |    |-- consumer: array (nullable = true)
     |    |    |    |    |    |-- element: struct (containsNull = true)
     |    |    |    |    |    |    |-- dob: string (nullable = true)
     |    |    |    |    |    |    |-- ffn: string (nullable = true)
     |    |    |    |    |    |    |-- firstname: string (nullable = true)
     |    |    |    |    |    |    |-- lastname: string (nullable = true)
     |    |    |    |    |    |    |-- nationality: string (nullable = true)
     |    |    |    |    |    |    |-- passport_expire_date: string (nullable = true)
     |    |    |    |    |    |    |-- passport_no: string (nullable = true)
     |    |    |    |    |    |    |-- pax_type: string (nullable = true)
     |    |    |    |    |    |    |-- title: string (nullable = true)
     |    |    |    |    |-- currency: string (nullable = true)
     |    |    |    |    |-- item_id: string (nullable = true)
     |    |    |    |    |-- name: string (nullable = true)
     |    |    |    |    |-- price: string (nullable = true)
     |    |    |    |    |-- sku: string (nullable = true)
     |    |    |    |    |-- type: string (nullable = true)
     |    |    |    |-- provider: string (nullable = true)
     |    |    |    |-- receive_advertise_email: string (nullable = true)
     |    |    |    |-- status: string (nullable = true)
     |    |    |    |-- type: string (nullable = true)
     |-- dateOfBirth: long (nullable = true)
     |-- email: string (nullable = true)
     |-- firstName: string (nullable = true)
     |-- firstSeen: long (nullable = true)
     |-- gender: string (nullable = true)
     |-- guestType: string (nullable = true)
     |-- identifiers: array (nullable = true)
     |    |-- element: struct (containsNull = true)
     |    |    |-- expiryDate: long (nullable = true)
     |    |    |-- id: string (nullable = true)
     |    |    |-- provider: string (nullable = true)
     |-- language: string (nullable = true)
     |-- lastName: string (nullable = true)
     |-- lastSeen: long (nullable = true)
     |-- modifiedAt: long (nullable = true)
     |-- nationality: string (nullable = true)
     |-- passportNumber: string (nullable = true)
     |-- phoneNumbers: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- postCode: string (nullable = true)
     |-- ref: string (nullable = true)
     |-- state: string (nullable = true)
     |-- street: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- subscriptions: array (nullable = true)
     |    |-- element: struct (containsNull = true)
     |    |    |-- channel: string (nullable = true)
     |    |    |-- effectiveDate: long (nullable = true)
     |    |    |-- name: string (nullable = true)
     |    |    |-- pointOfSale: string (nullable = true)
     |    |    |-- status: string (nullable = true)
     |-- title: string (nullable = true)

   
## Select Some Data Table in S3 guests

### Select Some Data Spark SQL

    scala>sampleDF.select("ref").show(5)
    
### Select Some Data Spark SQL Result  

    +--------------------+
    |                 ref|
    +--------------------+
    |30123dc5-27ea-4d7...|
    |4677712c-079b-4c5...|
    |574a115a-2144-457...|
    |f3013464-9bd7-44d...|
    |f283d637-9980-4c3...|
    +--------------------+
    only showing top 5 rows
 
## Exit Spark Shell CLI

    scala>:quit