[TOC]


# Overview
The following will show you how to use Temporary AWS Credentials to connect to Athena via DBeaver.

## What will be covered?
The following will guide you through

1) Setting up One Login CLI as per [Accessing AWS through OneLogin](https://boxever.atlassian.net/wiki/spaces/OPS/pages/528482305/Accessing+AWS+through+OneLogin)
for generating Temporary AWS Credentials. 
2) Setting up an AWS Athena Connection in DBeaver to use these credentials to access Optima. 

Note: When the credentials expire the One Login CLI must be run to regenerate new Temporary AWS Credentials.

## One Login

### Download CLI
Download the OneLogin Jar by running the following commands

    mkdir -p ~/.onelogin
    curl https://s3-eu-west-1.amazonaws.com/boxever-labs-binaries/services/one-login/onelogin-aws-cli.jar -o ~/.onelogin/onelogin-aws-cli.jar

Verify the installation

    java -jar ~/.onelogin/onelogin-aws-cli.jar --help

Expected output

    OneLogin AWS Assume Role Tool
    
    usage: onelogin-aws-cli.jar [options]
     -a,--appid <arg>            Set AWS App ID.
        --aws-account-id <arg>   AWS Account ID.
        --aws-role-name <arg>    AWS Role Name.
     -d,--subdomain <arg>        OneLogin Instance Sub Domain.
     -f,--file <arg>             Set a custom path to save the AWS
                                 credentials. (if not used, default AWS path
                                 is used)
     -h,--help                   Show the help guide
     -l,--loop <arg>             Number of iterations
     -p,--profile <arg>          Save temporary AWS credentials using that
                                 profile name
        --password <arg>         OneLogin password.
     -r,--region <arg>           Set the AWS region.
     -t,--time <arg>             Sleep time between iterations, in minutes
                                 [15-60 min]
     -u,--username <arg>         OneLogin username.
     -z,--duration <arg>         Desired AWS Credential Duration
     

### Generate onelogin.sdk.properties
Generate the `~/.onelogin/onelogin.sdk.properties` as per [Accessing AWS through OneLogin](https://boxever.atlassian.net/wiki/spaces/OPS/pages/528482305/Accessing+AWS+through+OneLogin)
by running the following command

```
echo 'onelogin.sdk.client_id=208b6ae1cc9fb9e5cab78e799ec98f32c1f049a2c9f3424993c468ffbdc5b5e0
onelogin.sdk.client_secret=e581bc7f2ab146b2ffb8378267864ac59ea2fb05164eb7777f65205033b2c147
onelogin.sdk.region=eu
onelogin.sdk.ip=' > ~/.onelogin/onelogin.sdk.properties
```    
    
Verify the settings

    cat ~/.onelogin/onelogin.sdk.properties
    
Expected output
      
    onelogin.sdk.client_id=208b6ae1cc9fb9e5cab78e799ec98f32c1f049a2c9f3424993c468ffbdc5b5e0
    onelogin.sdk.client_secret=e581bc7f2ab146b2ffb8378267864ac59ea2fb05164eb7777f65205033b2c147
    onelogin.sdk.region=eu
    onelogin.sdk.ip=

### Create AWS Credentials File

    mkdir -p ~/.aws/
    touch ~/.aws/credentials

### Generate AWS Credentials
Generate AWS credentials by running the following command replacing the email with your email and aws profile following 
the prompts to fill in your username, password, OTP Token for Google Authenticator and selecting the desired role to generate 
the credentials for. The credentials will be saved into ~/.aws/credentials under the aws profile you specify using the `---profile`.

Note: It is good practice to use specific aws profiles to describe them rather than overriding `default`. e.g. `--profile optima_spinair` or `--profile optima_emirates`.

    java -jar ~/.onelogin/onelogin-aws-cli.jar --appid 358863 --subdomain boxever --username jack.smith@boxever.com --region eu-west-1 --profile optima_one_login_test
     
Verify the generate Temporary AWS Credentials

    vi ~/.aws/credentials
    
Look for a aws profile section starting with `[optima_one_login_test]` that contains properties `aws_access_key_id`, `aws_session_token` amd `aws_secret_access_key` and `region` as shown below.

    [optima_one_login_test]
    aws_access_key_id=ASIAXMOD7OO2DHQL3IHK
    aws_secret_access_key=8h2eGWHLu8bmzH4njV1bRSwjCjI7Cj7QGRkZP/N2
    aws_session_token=FQoGZXIvYXdzEIH//////////wEaDGU8y/D2v3dRkmq+MSKsAnbvziwa6jNhR9weBuNCq/Jft5rutDY1NOmtTdFa8fXTxPUEJA5nuecF2xMYzu/R+96f0fsvJzb6JlAMKjkIPJI5/ANXwaeJPJMluXA9tSvfbfJc00hYLkSgiTvPhMaFeXj2jAaRh4llKd/KhossqU/m0WErOsCI6ETNPmZXQuezTWN3AfJf46jqhmmV0q4kXL7TtYBTgR+Zlmr39LA2dy8s8mOVt1guM3L2b9/KPh6ELIREePg6ikerw7I3U9IqF8oebTbsjX9LXNAPsfbLFB2g2UdlPhgow8FD05jg3biYBvD0wEpCFN0osk525XboagOPOI/Wq1j1YDzkNoVmGzE8v9lKIVSHczMuxdpM4kegyVU/iDRxs0Zf8hZdSyhabWLnRhlkAib1DYSuMii0z9zpBQ==
    region=eu-west-1

Note: These AWS Credentials will expire after an hour therefore you will need to generate new AWS Credentials hourly.


## DBeaver Setup

### Athena Driver

    ClassName: com.simba.athena.jdbc.Driver
    URL Template: jdbc:athena://AWSRegion=region;

#### Libraries

1) https://s3.amazonaws.com/athena-downloads/drivers/JDBC/SimbaAthenaJDBC_2.0.7/AthenaJDBC42_2.0.7.jar
2) https://s3.amazonaws.com/athena-blog-jdbc-custom-credentials-provider/athena-jdbc-custom-credentials-provider-0.0.1-SNAPSHOT.jar

Note: DBeaver may provide the option to download the Athena Driver but the `athena-jdbc-custom-credentials-provider-0.0.1-SNAPSHOT.jar` 
needs to be manually downloaded and added to the Libraries for the connection. It is recommended to use the `AthenaJDBC42_2.0.7.jar` or above driver and
not the one that DBeaver uses.

### Connection
Replace `{ region }` with the AWS region e.g. `eu-west-1` and `{ profile }` with the AWS Profile Section e.g. `dev`, `production`, `explore`, `labs` or `optima_{customer_name}` e.g optima_spinair.
Replace `{ customer_name }` with the customer name e.g. `Spinair`

    Name: Boxever Optima Athena - ({ customer_name } Temp Profile Credentials - { profile })
    Region: { region }
    
    AWS Access Key: test
    AWS Secret Key: test

Note: The AWS Access Key and AWS Secret Key are actually not used and overwritten by the ProfileCredentialsProvider below.

#### Driver Properties
The `{ profile }` and `{ region }` is values specified to the `onelogin-aws-cli.jar` as per the One Login/Generate Credentials section above.
The `{ environment }` is the environment that the Optima Pipeline is running in e.g. `dev`, `production` or `labs`.

    AwsCredentialsProviderArguments: { profile }
    AwsCredentialsProviderClass: com.amazonaws.auth.profile.ProfileCredentialsProvider
    
    S3OutputEncOption: SSE_S3
    S3OutputLocation: s3://boxever-optima-{ customer_name }-{ environment }-{ region }/data/athena/results

e.g.

    AwsCredentialsProviderArguments: optima_qatar
    AwsCredentialsProviderClass: com.amazonaws.auth.profile.ProfileCredentialsProvider
    
    S3OutputEncOption: SSE_S3
    S3OutputLocation: s3://boxever-optima-qatar-production-eu-west-1/data/athena/results
 

#### General/Filter/Schemas
Replace `{ customer_name }` with the customer name e.g. `spinair`

Include:
    
    { customer_name }*



# References
https://github.com/onelogin/onelogin-aws-cli-assume-role
https://boxever.atlassian.net/wiki/spaces/OPS/pages/528482305/Accessing+AWS+through+OneLogin

https://aws.amazon.com/blogs/big-data/connect-to-amazon-athena-with-federated-identities-using-temporary-credentials/
https://github.com/aws-samples/aws-blog-athena-custom-jdbc-credentials
https://github.com/aws-samples/aws-blog-athena-custom-jdbc-credentials/blob/master/pom.xml
https://docs.aws.amazon.com/athena/latest/ug/connect-with-jdbc.html