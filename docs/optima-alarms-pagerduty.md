# Optima alerting to PagerDuty

## How an Pager Duty incident is triggered

1. *Step Function steps invoke Lambda* functions (start another step function, start glue job, check job status, etc).
2. If the Lambda  function fails will trigger an alarm. The alarm action is _send notification to SNS topic_.
3. The SNS topic has a subscriptor that sends a HTTP request to the pager duty entrypoint.

![Optima alerting overview schema](./images/optima-alerting-overview-diagram.png)

[Article for AWS CloudWatch Pager Duty integration](https://support.pagerduty.com/docs/aws-cloudwatch-integration-guide).

## Environment configuration

### Pager Duty

The pager duty service must have an **Amazon CloudWatch integration**. The **integration URL** is the one we'll use to send from AWS CloudWatch.

### Creating the AWS artifacts

The AWS artifacts are created from Optima Ansible role.

The Pager Duty service entrypoint is configured in `ansible/roles/boxever-optima/defaults/main.yml`.

Fox example, the test service is:

```yaml
# Pager duty notification hook endpoint
pager_duty_notification_endpoint: "https://events.pagerduty.com/integration/0dec390c8e3249f080102d2ef3520bb4/enqueue"
```

If a customer needs itsown entrypoint, it will be configured in its customer variables file in `ansible/roles/boxever-optima/` folder.

The SNS topic is created in `ansible/roles/boxever-optima/tasks/aws_sns.yml`. This tasks are (and must be) executed 
before create the alarms.

The Lambda function alamrs are configured in `ansible/roles/boxever-optima/tasks/aws_lambda_functions.yml`

For example, the alarm for *Data Quality Data Looker Dashboards* Lambda function is:

```yaml
- name:                                 Create AWS Lambda Data Quality Data Looker Dashboards Function Alarm
  ec2_metric_alarm:
    name:                               "{{ lambda_data_quality_looker_name }}-alarm"
    description:                        "Error alarm for {{ lambda_data_quality_looker_name }} lambda function"
    state:                              present
    region:                             "{{ aws_region }}"
    metric:                             "Errors"
    namespace:                          "AWS/Lambda"
    statistic:                          SampleCount
    comparison:                         ">="
    threshold:                          1.0
    period:                             300
    evaluation_periods:                 1
    unit:                               Count
    dimensions:
      FunctionName:                    "{{ lambda_data_quality_looker_name }}"
      Resource:                        "{{ lambda_data_quality_looker_name }}"
    alarm_actions:                     [ "{{ pager_duty_topic_arn }}" ]

```

## Log processing alerts

Some functions ends with no errors, but from pipeline point of view the status is failure. In this case the function will 
log this fact and finish without error and writting the log.

To receive incidents in PD for this situations, we need to:

1. Create a **Log Metric Filter** based on the lambda function **Log Group**
2. With this *Log Metric Filter* create an **Alarm**.

### Create Log Metric Filter 

1. In **CloudWatch** / **Logs** > select the **Log Group** and click **Create Metric Filter**.

![Create Log Metric Filter](./images/optima-alerting-overview-create-metrics-filter-1.png)

2. In **Define Logs Metric Filter** Write the filter pattern and click **Assign Metric**. The pattern usually will be the error
code we want to monitor.

![Define Logs Metric Filter](./images/optima-alerting-overview-create-metrics-filter-2.png)

3.  In **Create Metric Filter and Assign Filter** set a **Metric Name** and click **Create Filter**. The filter is created.

![Create Metric Filter and Asign filter](./images/optima-alerting-overview-create-metrics-filter-3.png)


### Create Metric Filter Alarm

1. With thew *Log Metric Filter* click in **Create Alarm**
   
![Create Metric Filter result](./images/optima-alerting-overview-create-alarm-1.png)

2. **Edit the Alarm Details**:

![Configure Alarm Details](./images/optima-alerting-overview-create-alarm-2.png)

3. In **Actions** select **Send notification to:** `boxever-optima-{customer}-{env}-pager-duty-notification` when **State is ALARM**

![Set Alarm Action](./images/optima-alerting-overview-create-alarm-3.png)

4. Click **Create Alarm** button
