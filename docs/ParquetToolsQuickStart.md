[TOC]

# Overview
Below is a quickstart guide of installing and using parquet tools.


# Requirements

* Mac OS X or Linux
* [HomeBrew](https://brew.sh/)
* Java 8
* Mercurial 4.8.2 or higher
* Parquet Tools - See Parquet Tools Installation Guide Installation Guide below


# Using Parquet Tools
Below is a quick start of Parquet tools that can be used to inspect and verify the schema and contents of a parquet files.

## Sample Data
Sample data can be found in the directory `docs/samples/parquet-data`.

    guests1.gz.parquet
    
## Viewing Schema

### Command
Execute the following

    parquet-tools schema parquet-tools head docs/samples/parquet-data/guests1.gz.parquet 

### Result

    message spark_schema {
      optional binary ref (UTF8);
      optional int96 createdAt;
      optional binary firstName (UTF8);
      optional binary gender (UTF8);
      optional binary guestType (UTF8);
      optional binary lastName (UTF8);
      optional int96 lastSeen;
      optional int96 modifiedAt;
      optional binary guest_type (UTF8);
      optional binary last_seen_year_month (UTF8);
      optional boolean containsValidationErrors;
      optional binary validationErrors (UTF8);
    }
    
## Viewing Contents

### Command
Execute the following

    parquet-tools head docs/samples/parquet-data/guests1.gz.parquet 

### Result
Each block below is a row.

Note: in some of the examples below the data such as createdAt is a timestamp encoded as int96 and therefore is not readable.

    ref = 68ae46d0-3916-4d93-8b91-21425a351d5a
    createdAt = gJ77XJdGAABxgyUA
    firstName = Jack
    gender = male
    guestType = customer
    lastName = Smith
    lastSeen = gJ77XJdGAABxgyUA
    modifiedAt = gJ77XJdGAABxgyUA
    guest_type = customer
    last_seen_year_month = 2018-12
    containsValidationErrors = false
    validationErrors = 
    
    ref = 02169f57-2dcb-4bb0-8d85-afba0c6ab1c0
    createdAt = AAAAAAAAAACMPSUA
    firstName = Peter
    gender = male
    guestType = unknown
    lastName = Smith
    lastSeen = gJ77XJdGAABxgyUA
    modifiedAt = AAAAAAAAAACMPSUA
    guest_type = unknown
    last_seen_year_month = 2018-12
    containsValidationErrors = false
    validationErrors = 
    
    ref = 2e182b0f-ac3c-4ac5-8e92-61ba697c78e5
    createdAt = gJ77XJdGAABxgyUA
    firstName = Paul
    gender = unknown
    guestType = unknown
    lastName = Smith
    lastSeen = gJ77XJdGAABxgyUA
    modifiedAt = AAAAAAAAAACMPSUA
    guest_type = unknown
    last_seen_year_month = 2018-12
    containsValidationErrors = false
    validationErrors = 

## Viewing Meta

### Command
Execute the following

    parquet-tools meta docs/samples/parquet-data/guests1.gz.parquet 

### Result
Each block below is a row.

Note: in some of the examples below the data such as createdAt is a timestamp encoded as int96 and therefore is not readable.

    file:                     file:/Users/jacksmith/boxever-labs-optima/docs/samples/parquet-data/guests1.gz.parquet 
    creator:                  parquet-mr version 1.10.0 (build 031a6654009e3b82020012a18434c582bd74c73a) 
    extra:                    org.apache.spark.sql.parquet.row.metadata = {"type":"struct","fields":[{"name":"ref","type":"string","nullable":true,"metadata":{}},{"name":"createdAt","type":"timestamp","nullable":true,"metadata":{}},{"name":"firstName","type":"string","nullable":true,"metadata":{}},{"name":"gender","type":"string","nullable":true,"metadata":{}},{"name":"guestType","type":"string","nullable":true,"metadata":{}},{"name":"lastName","type":"string","nullable":true,"metadata":{}},{"name":"lastSeen","type":"timestamp","nullable":true,"metadata":{}},{"name":"modifiedAt","type":"timestamp","nullable":true,"metadata":{}},{"name":"guest_type","type":"string","nullable":true,"metadata":{}},{"name":"last_seen_year_month","type":"string","nullable":true,"metadata":{}},{"name":"containsValidationErrors","type":"boolean","nullable":true,"metadata":{}},{"name":"validationErrors","type":"string","nullable":true,"metadata":{}}]} 
    
    file schema:              spark_schema 
    --------------------------------------------------------------------------------
    ref:                      OPTIONAL BINARY O:UTF8 R:0 D:1
    createdAt:                OPTIONAL INT96 R:0 D:1
    firstName:                OPTIONAL BINARY O:UTF8 R:0 D:1
    gender:                   OPTIONAL BINARY O:UTF8 R:0 D:1
    guestType:                OPTIONAL BINARY O:UTF8 R:0 D:1
    lastName:                 OPTIONAL BINARY O:UTF8 R:0 D:1
    lastSeen:                 OPTIONAL INT96 R:0 D:1
    modifiedAt:               OPTIONAL INT96 R:0 D:1
    guest_type:               OPTIONAL BINARY O:UTF8 R:0 D:1
    last_seen_year_month:     OPTIONAL BINARY O:UTF8 R:0 D:1
    containsValidationErrors: OPTIONAL BOOLEAN R:0 D:1
    validationErrors:         OPTIONAL BINARY O:UTF8 R:0 D:1
    
    row group 1:              RC:3 TS:1043 OFFSET:4 
    --------------------------------------------------------------------------------
    ref:                       BINARY GZIP DO:0 FPO:4 SZ:211/225/1.07 VC:3 ENC:PLAIN,BIT_PACKED,RLE ST:[min: 02169f57-2dcb-4bb0-8d85-afba0c6ab1c0, max: 68ae46d0-3916-4d93-8b91-21425a351d5a, num_nulls: 0]
    createdAt:                 INT96 GZIP DO:0 FPO:215 SZ:101/67/0.66 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[no stats for this column]
    firstName:                 BINARY GZIP DO:0 FPO:316 SZ:80/65/0.81 VC:3 ENC:PLAIN,BIT_PACKED,RLE ST:[min: Jack, max: Peter, num_nulls: 0]
    gender:                    BINARY GZIP DO:0 FPO:396 SZ:115/77/0.67 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: male, max: unknown, num_nulls: 0]
    guestType:                 BINARY GZIP DO:0 FPO:511 SZ:123/85/0.69 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: customer, max: unknown, num_nulls: 0]
    lastName:                  BINARY GZIP DO:0 FPO:634 SZ:119/79/0.66 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: Smith, max: Smith, num_nulls: 0]
    lastSeen:                  INT96 GZIP DO:0 FPO:753 SZ:150/110/0.73 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: 0x809EFB5C9746000071832500, max: 0x809EFB5C9746000071832500, num_nulls: 0]
    modifiedAt:                INT96 GZIP DO:0 FPO:903 SZ:101/67/0.66 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[no stats for this column]
    guest_type:                BINARY GZIP DO:0 FPO:1004 SZ:123/85/0.69 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: customer, max: unknown, num_nulls: 0]
    last_seen_year_month:      BINARY GZIP DO:0 FPO:1127 SZ:129/89/0.69 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: 2018-12, max: 2018-12, num_nulls: 0]
    containsValidationErrors:  BOOLEAN GZIP DO:0 FPO:1256 SZ:60/40/0.67 VC:3 ENC:PLAIN,BIT_PACKED,RLE ST:[min: false, max: false, num_nulls: 0]
    validationErrors:          BINARY GZIP DO:0 FPO:1316 SZ:94/54/0.57 VC:3 ENC:BIT_PACKED,PLAIN_DICTIONARY,RLE ST:[min: , max: , num_nulls: 0]




# Parquet Tools Installation Guide
On Mac OS X run the following commands to install Parquet tools.

    brew install parquet-tools
