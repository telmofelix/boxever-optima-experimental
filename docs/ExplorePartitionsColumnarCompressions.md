[TOC]

# Partitioning Data

When you partition your data, you can restrict the amount of data that Explore scans by filtering on the partition key. 
You can partition your data by any key but its best to pick low cardinality keys. A common practice is to partition the 
data based on time. For example, you might choose to partition by year, month, date, and hour. If you have data coming 
from multiple sources, you might partition by a data source identifier and date.

## Example

    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-02
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-02

## Example Query 1

    select count(*) from guests where guest_type = 'visitor' and created_at_year_month = '2018-01';

If each folder above based on partitioning is 100GB, then the sum of the guests folder is 400GB. The execution of the 
query would mean that the total data scanned is only 100GB due to partitioning. This means at $5/TB it would cost 
$5/1000*100=$0.50 instead of $2.00. Secondly the query response time would be 4x faster.

## Example Query 2

    select count(*) from guests where guest_type = 'visitor';

If each folder above based on partitioning is 100GB, then the sum of the guests folder is 400GB. The execution of the 
query would mean that the total data scanned is now 200GB due to partitioning. This means at $5/TB it would cost 
$5 / 1000GB * 200GB = $1.00 instead of $2.00. Lastly the query response time would be 2x faster due to the decrease in 
data that has to beto be scanned.

## Benefits
* Decreases cost especially for the likes of Athena, BigQuery or Redshift Spectrum who charge $5/TB scanned
* It increases performance as the cluster does not have to scan data that is actually irrelevant to the result

## Recommendation
Use partitioning for commonly used predicates. When querying especially when exploring the data or testing queries 
include partition keys where possible to reduce the data scanned. Following this recommendation will lead to be benefits 
described above. More info: [Athena Partitions](https://docs.aws.amazon.com/athena/latest/ug/partitions.html)


# Columnar Data Formats

Converting your data to columnar formats allows Explore to selectively read only required columns to process the data. 
Explore supports Apache ORC and Apache Parquet.

## Example

    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-02
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-02

# Example Query 1

    select count(ref) from guests where guest_type = 'visitor' and created_at_year_month = '2018-01';

If each folder above based on partitioning is 100GB, then the sum of the guests folder is 400GB. The total number of 
guests in each bucket is 100m. As each ref is a UUID and is stored as a String each ref equates to 36 bytes. 100m * 36 
bytes =  3.6 GB

Then execution of the query would mean that the total data scanned is only 3.6GB due to partitioning and selecting only 
required data. This means at $5/TB it would cost $5 / 1000GB * 3.6 = $0.018 instead of $0.50 if you had used count(*). 
Secondly the query response time would be greater than 4x faster.

## Benefits
* Decreases cost especially for the likes of Athena, BigQuery or Redshift Spectrum who charge $5/TB scanned
* It increases performance as the cluster does not have to scan data that is actually irrelevant to the result.

## Recommendation
Only select the data you need for the query and avoide * in selects unless actually required. Following this 
recommendation will lead to be benefits described above.


# Compression

Files written can be compressed using a variety of compression algorithms. 

* GZIP compression uses more CPU resources than Snappy or LZO, but provides a higher compression ratio. GZip is often a 
good choice for cold data, which is accessed infrequently. 
* Snappy or LZO are a better choice for hot data, which is accessed frequently. 
* BZip2 can also produce more compression than GZip for some types of files, at the cost of some speed when compressing 
and decompressing. HBase does not support BZip2 compression. Snappy often performs better than LZO. It is worth running 
tests to see if you detect a significant difference. 
* For MapReduce, if you need your compressed data to be splittable, BZip2, LZO, and Snappy formats are splittable, but
GZip is not. Splittability is not relevant to HBase data. 
* For MapReduce, you can compress either the intermediate data, the output, or both. Adjust the parameters you provide 
for the MapReduce job accordingly. The following examples compress both the intermediate data and the output. MR2 is 
shown first, followed by MR1.

## Example

    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=customer/created_at_year_month=2018-02
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-01
    s3://boxever-data-lake/explore/guests/guest_type=visitor/created_at_year_month=2018-02

## Example Query 1

    select count(ref) from guests where guest_type = 'visitor' and created_at_year_month = '2018-01';

If each folder above based on partitioning is 33GB (3:1 compression on 100GB), then the sum of the guests folder 
is 133GB (3:1 compression on 400GB). The total number of guests in each bucket is 100m. As each ref is a UUID and is 
stored as a String each ref equates to 36 bytes. 100m * 36 bytes =  3.6 GB, this actually would be 1.03GB with a 3:1 
compression.

Then execution of the query would mean that the total data scanned is only 1.03GB due to partitioning, selecting only 
required data and compression. This means at $5/TB it would cost $5 / 1000GB * 1.03GB = $0.00515 instead of $0.018 if 
you had used count(ref).

Benefits
* Decreases cost especially for the likes of Athena, BigQuery or Redshift Spectrum who charge $5/TB scanned
* It increases performance as the cluster does not have to scan data that is actually irrelevant to the result.

## Recommendation
Compress using GZIP for infrequently access data (events) and compress using SNAPPY for frequently access data (guests, 
orders and aggregate tables). Following this recommendation will lead to be benefits described above.