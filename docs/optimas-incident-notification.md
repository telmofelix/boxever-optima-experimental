# Optima incident notification

[TOC]

## Architecture

When a step function goes to failure state the `boxever-optima-{custome}-{env}-notify-pager-duty` Lambda function is called. This Lambda get the message and send it to `boxever-optima-{customer}-{env}-pager-duty-notification` SNS topic.  This topic have an HTTPS Subscription to Pager Duty webhook. The webhhok is bound to a Custom Event Transformer script, that build and publish the Pager Duty incident.

![Incident notification architeture](./images/optima_incident-notification_architecture.png)

## Notify Pager Duty Lambda Function

Notify Pager Duty Lambda Function is a [python script](../aws_lambda_functions/notifyPagerDuty.py) that gets the input event and publish to `boxever-optima-{ customer }-{ env }-pager-duty-notification` SNS topic.

## `boxever-optima-{ customer }-{ env }-pager-duty-notification` SNS topic

This topic receives the alerts triggered from pipeline and publish to subscribers. There is a HTTPS subscriber to Optima Pipeline Alert Pager Duty service integration webhook.

## Optima Cloudwatch Transformer

[**Optima Cloudwatch Transformer**](https://boxever.pagerduty.com/services/PLFRSBT/integrations/PJKU0JE) is the integration defined in Pager Duty [**Optima Pipeline Alert**](https://boxever.pagerduty.com/services/PLFRSBT/integrations) service to receive messages from Lambda. Its type is [Custom Event Transformer](https://v2.developer.pagerduty.com/docs/cet).

The [**Custom Event Transformer** javascript script](../pager_duty/optima-failure-pagerduty-cet.js) unwrap the message and creates a *pager duty normalized event*. At this moment is handling four message types:

- step function failures
- job failures
- alarms
- unknown messages

The script analyzes the input message type, extract information and creates a message with links failure in AWS Console.
