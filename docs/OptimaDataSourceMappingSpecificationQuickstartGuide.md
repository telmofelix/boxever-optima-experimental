[TOC]

# Data Source Mapping Specification

## Overview

The ETL jobs relies on it logic based on a *data source mapping specification*. This specification defines the process 
of mapping from a source data source to a destination data source. This documents explains how to create this specifications.

## Mapping specification 

In this context, a mapping is the ETL (extract, transform and load) process of filtering, transforming and validating data 
from one **source data source**, and moving it to a **destination data source**. The specification is composed of:

- a `source` section, to define the source data source.
- a `destination`section, to define the destination data source.
- a `quarantine` section, to define que *quarantine* space, to store the data that does not fit the filtering and validation.
- a `mapping` section, that will contains all mappings to be performed. 

### Mapping specification format

A mapping specification should cover the goals:

- It should be **a document**. Not a script, code in a programming language, etc.
- Must be an **executable document**. That means that a engine (the ETL job in our case) will be able to execute it.
- Must be **human readable** and **human friendly**.

In this document we will use `YAML` format to ease human understanding, but other more verbose languages could be used, like `JSON` or `XML`.

``` yaml
# source datasource
source:
  paths: 
    - boxever-optima/src/main/resources/optima/sample-data/guests.json
  format: json 

# destination datasource
destination:
  path: build/etl/guests.parquet
  format: parquet                                                   # optional. Default based on src extension
  partitions:
    - guest_type
  repartition:
    number: 100                                                     # repartition final data frame to 100
    mappings:                                                        # the mappings to reparition by
      - guest_type
  saveMode: overwrite                                              # default: overwrite

# quarantine destination datasource
quarantine:
  path: build/etl/quarantine/guests.parquet
  format: parquet
  partitions:
    - guest_type
  repartition:
    number: 100
    mappings:
      - guest_type
  saveMode: overwrite

mappings:
  # the the list of mappings
```

### How to define a mapping

A mapping specification describes the ETL (extract, transform and load) process which includes filtering, transforming 
(renaming, type conversion, format conversion, data manipulation such as string trim, uppercase etc) and validating data 
such as age is greater than 0 etc from one **source data source** and moving it to a **destination data source**. 

A mapping needs:

- a `source` section, to define the field to map, its type and cardinality.
- a `destination` section, to define the destination field name, type or mappings (for complex types) to apply.
- the `transformations` to apply.
- the `validations` to apply.
- error handling configuration.

There are four different type of mappings, based on kind of type and cardinality:

- for **simple types**
- for **collection** of *simple types*
- for **complex types**
- for **collection** of *complex types*

#### Simple type mapping

This kind of mapping is used when source data is of simple type (`string`, `int`, `long`,...).

```yaml
  source:
    name: ref
    type: string                                    # int, string, long,...
  destination:
    name: ref
    type: string                                    # int, string, long,...
  transformations: []                               # list of transformation functions
  validations: []                                   # list of validation functions
  throwValidationError: true
  quarantineOnError: true
```

#### Collection of simple type mapping

```yaml
source:
  name: emails
  type: string
  collection: true                                  # flag to indicate that is a collection
destination:
  name: emails
  type: string
transformations: []
validations: []
throwValidationError: true
quarantineOnError: true
```

#### Complex type mapping

```yaml
  source:
    name: twitterAccount
    type: complex                                   # flag to indicate that is a complex type
  destination:
    name: twitter_account
    mappings:                                       # list of complex type mappings
    -
      source:                                       # each simple type of complex type will have its mapping
        name: handle                                # the name of the source field under the complex type sourceName
        type: string
      destination:
        name: handle                                # the name of the destination field under the destination complex type destinationName
        type: string
      transformations: []
      validations: []
      throwValidationError: true
      quarantineOnError: true
```

#### Collection of complex type mapping

```yaml
    source:
      name: identifiers
      type: complex                                 # flag to indicate that is a complex type
      collection: true                              # flag to indicate that is a collection
    destination:
      name: identifiers
      mappings:
        -
          source:                                   # each item of the complex type collection will have its mapping
            name: provider                          # the name of the source field under the source complex type collection destinationName
            type: string
          destination:
            name: provider                          # the name of the destination field under the destination complex type collection destinationName
            type: string
          transformations:
            - toLower
          validations: []
          throwValidationError: true
          quarantineOnError: true
```

## Examples

### Simple type mapping example

Let's map a `string` field **firstName** to a field named **first_name**, applying some transformation functions (trim, toLower).

The source:

```json
{ "firstName": " John   WINSTON  ", "lastName": " Ono   lenon     " }
```

The target:

```json
{ "first-name": "john winston", "last_name": "ono lenon" }
```

The mapping:

```yaml
  source:
    name: firstName
    type: string
  destination:
    name: first_name
    type: string
  transformations:
    - fullTrim
    - toLower
  validations: []
  throwValidationError: true
  quarantineOnError: true
```

The mapping reduced (removing optionals, etc):

```yaml
  source:
    name: firstName
    type: string
  destination:
    name: first_name
  transformations:
    - fullTrim
    - toLower
```

## Full mappings spec commented example

Here the full commented example, about how to map a raw entity to be trusted:

The entity, with three simple fields (`ref`, `fullName` and `guestType`), a collection of simple types (`emails`), a complex type (`twitterAccount`) and a collection of complex type (`identifiers`).

[`samples/data/simple-guests.json`](samples/data/simple-guest.json)

```json
{
  "ref": "ce29f8bf-6c60-421d-8035-29cadecee40c",
  "fullName": " John   WINSTON Ono   lenon ",
  "guest_type": "customer",
  "emails": [
    "    john.lennon@thebeatles.co.uk",
    "dwarf.mcdougal@annonymous.com    ",
    "   lennie@johnandyoko.com  "
  ],
  "twitterAccount": {
    "handle": "@john",
    "registeredAt":1546032815914
  },
  "identifiers": [
    {
      "provider": "Google",
      "id": "ff10158a-0b47-41f9-ab64-17113b0fdaa9",
      "expiryDate": 1547457315112
    },
    {
      "provider": "Globe",
      "id": "ad7c72cd-0e7e-4e65-b7a8-2f12cc5ffb3c",
      "expiryDate": 1547485436859
    }
  ]
}
```

[`samples/spec/guests-spec.yaml`](samples/spec/guests-spec.yaml)

```yaml
source:
  paths: 
    - /optima/samples/data/guests.json
  format: json
destination:
  path: build/optima/output/trusted/guests.parquet
  format: parquet
  partitions:
    - guest_type
  saveMode: overwrite
quarantine:
  path: build/optima/output/quarantine/guests.parquet
  format: parquet
  partitions:
    - guest_type
  saveMode: overwrite
mappings:
  # simple type mapping
  -
    source:
      name: ref
      type: string
    destination:
      name: ref
      type: string
    transformations:
    validations:
      - isUUID
    throwValidationError: true
    quarantineOnError: true
  -
    source:
      name: fullName
      type: string
    destination:
      name: full_name
      type: string
    transformations:
      - fullTrim
      - toLower
    validations:
    throwValidationError: true
    quarantineOnError: true
  -
    source:
      name: guestType
      type: string
    destination:
      name: guest_type
    transformations:
      - fullTrim
      - toLower
    validations:
    throwValidationError: true
    quarantineOnError: true
  # collection of simple type mapping
  -
    source:
      name: emails
      type: string
      collection: true
    destination:
      name: emails
      type: string
    transformations:
      - trim
      - toLower
      - removeDuplicates
    validations:
    throwValidationError: true
    quarantineOnError: true
  # complex type mapping
  -
    source:
      name: twitterAccount
      type: complex
    destination:
      name: twitter_account
      mappings:
        -
          source:
            name: handler
            type: string
          destination:
            name: handler
            type: string
          transformations:
          validations:
          throwValidationError: true
          quarantineOnError: true
        -
          source:
            name: registeredAt
            type: long
          destination:
            name: registered_at
            type: timestampInt96
          transformations:
          validations:
          throwValidationError: true
          quarantineOnError: true
  -
    source:
      name: identifiers
      type: complex
      collection: true
    destination:
      name: identifiers
      mappings:
        -
          source:
            name: provider
            type: string
          destination:
            name: provider
            type: string
          transformations:
          validations:
          throwValidationError: true
          quarantineOnError: true
        -
          source:
            name: id
            type: string
          destination:
            name: id
            type: string
          transformations:
          validations:
          throwValidationError: true
          quarantineOnError: true
        -
          source:
            name: expiryDate
            type: long
          destination:
            name: expiry_date
            type: timestampInt96
          transformations:
          validations:
          throwValidationError: true
          quarantineOnError: true
```

[`samples/spec/guests-spec-simplied.yaml`](samples/spec/guests-spec-simplied.yaml)

```yaml
source:
  paths: 
    - /optima/samples/data/guests.json
destination:
  path: build/optima/output/trusted/guests.parquet
  partitions:
    - guest_type
quarantine:
  path: build/optima/output/quarantine/guests.parquet
  partitions:
    - guest_type
mappings:
  # simple type mapping
  -
    source:
      name: ref
      type: string
    validations:
      - isUUID
  -
    source:
      name: fullName
      type: string
    destination:
      name: full_name
    transformations:
      - fullTrim
      - toLower
  -
    source:
      name: guestType
      type: string
    destination:
      name: guest_type
    transformations:
      - fullTrim
      - toLower
  # collection of simple type mapping
  -
    source:
      name: emails
      type: string
      collection: true
    transformations:
      - trim
      - toLower
  # complex type mapping
  -
    source:
      name: twitterAccount
      type: complex
    destination:
      name: twitter_account
      mappings:
        -
          source:
            name: handler
            type: string
        -
          source:
            name: registeredAt
            type: long
          destination:
            name: registered_at
            type: timestampInt96
          transformations:
            - toTimestampInt96
  -
    source:
      name: identifiers
      type: complex
      collection: true
    destination:
      name: identifiers
      mappings:
        -
          source:
            name: provider
            type: string
        -
          source:
            name: id
            type: string
        -
          source:
            name: expiryDate
            type: long
          destination:
            name: expiry_date
            type: timestampInt96
          transformations:
            - toTimestampInt96
```

## To be defined

- The simple types.
- Values or restrictions for `[from|to|quarantine].format`? json, parquet, csv,...?
- Values or restrictions for `[from|to|quarantine].saveMode`?
- **Optional** fields and **defaults**.
- The predicates list for field validations.
- The functions list for field transformations.
- Function such as *sum* of orderItems.price * orderItems.quantity to order.price
- Joining multiple data sources

### Flattening
The current OptimaETL support flattening. This is where a complex types or collection of complex types is flattened 
into the parent object. This can result in an explosion of rows depending on whether the complex types is a collection 
or not. To prevent name conflicts with the parent renaming should be used and config validation should prevent human error.

#### Flattening a complex type

The source:

```json
{ "ref": "5ef53faf-beb1-4b8a-b23d-7e38f795f703", "twitter": { "handle": "@jack", "registeredAt": 1548368710262 } }
{ "ref": "03c6ab86-cb62-41d9-bf8f-61a7c2435e37", "twitter": { "handle": "@paul", "registeredAt": 1548368710262 } }
```

The target:

```json
{ "guest_ref": "5ef53faf-beb1-4b8a-b23d-7e38f795f703", "handle": "@jack", "registeredAt": 1548368710262 }
{ "guest_ref": "03c6ab86-cb62-41d9-bf8f-61a7c2435e37", "provider": "@paul", "registeredAt": 1548368710262 }
```

Proposed Mapping

```yaml
mappings:
  -
    source:
      name: ref
      type: string
    destination:
      name: guest_ref
      type: string
  -
    source:
      name: twitter
      type: complex     # flag to indicate that is a complex type
    destination:
      name: twitter
      flatten: true     # flag to indicate that this should be flattened/merged into the parent
      mappings:
        -
          source:
            name: handle
            type: string
          destination:
            name: handle
            type: string
        -
          source:
            name: registeredAt
            type: long
          destination:
            name: long
            type: registeredAt
```

#### Flattening a Collection of complex types

The source:

```json
{ "ref": "5ef53faf-beb1-4b8a-b23d-7e38f795f703", "identifiers": [ { "provider": "Google", "gclid": "63034cad-a93f-46f8-8e57-81e337fadeba" } ] }
{ "ref": "03c6ab86-cb62-41d9-bf8f-61a7c2435e37", "identifiers": [ { "provider": "Google", "gclid": "92559274-1ccb-4e3c-87bf-426e5cd2b81e" },{ "provider":  "Facebook", "id":  "ef913777-2a71-43ab-988f-c4ab7788fdba" } ] }
```

The target:

```json
{ "guest_ref": "5ef53faf-beb1-4b8a-b23d-7e38f795f703", "provider": "Google", "gclid": "63034cad-a93f-46f8-8e57-81e337fadeba" }
{ "guest_ref": "03c6ab86-cb62-41d9-bf8f-61a7c2435e37", "provider": "Google", "gclid": "92559274-1ccb-4e3c-87bf-426e5cd2b81e" }
{ "guest_ref": "03c6ab86-cb62-41d9-bf8f-61a7c2435e37", "provider": "Facebook", "gclid": "ef913777-2a71-43ab-988f-c4ab7788fdba" }

```

Proposed Mapping

```yaml
mappings:
  -
    source:
      name: ref
      type: string
    destination:
      name: guest_ref
      type: string
  -
    source:
      name: identifiers
      type: complex     # flag to indicate that is a complex type
      collection: true  # flag to indicate that is a collection
    destination:
      name: identifiers
      flatten: true     # flag to indicate that this should be flattened/merged into the parent
      mappings:
        -
          source:
            name: provider
            type: string
          destination:
            name: provider
            type: string
        -
          source:
            name: id
            type: string
          destination:
            name: id
            type: string
```

#### Limitations

Does not flatten more than one level. Therefore it cannot flatten Orders/OrderItems/FlightSegments. 
Perhaps a different parameter like

* flattenLevels: 0 (default)
* flattenLevels = 1 (Orders/OrderItems to Order)
* flattenLevels = 1 (Orders/OrderItems/FlightSegments to Order)
