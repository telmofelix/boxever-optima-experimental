Optima Research
===============

Created by 

[Javier Ramos](https://boxever.atlassian.net/people/5ca1e0f423ef8d0370f4578d?ref=confluence&src=profilecard)

Last updated [Apr 18, 2019](https://boxever.atlassian.net/wiki/pages/diffpagesbyversion.action?pageId=1076723721&selectedPageVersions=21&selectedPageVersions=22)

-   [](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-3square)
-   [Introduction](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-Introduction)
-   [Current Proposed Solution](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-CurrentProposedSolution)
    -   [Metrics](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-Metrics)
    -   [Pros and Cons](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-ProsandCons)
        -   [References](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-References)
-   [Alternative Solutions](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-AlternativeSolutions)
    -   [Spark on EMR](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-SparkonEMR)
        -   [Cost Estimation](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-CostEstimation)
        -   [Pros and Cons](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-ProsandCons.1)
    -   [Apache Flink](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-ApacheFlink)
        -   [Cost Estimation](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-CostEstimation.1)
        -   [Pros and Cons](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-ProsandCons.2)
    -   [Kafka Streams](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-KafkaStreams)
        -   [Cost Estimation](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-CostEstimation.2)
        -   [Pros and Cons](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-ProsandCons.3)
-   [Comparison](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-Comparison)
-   [Kubernetes](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-Kubernetes)
-   [Conclusion](https://boxever.atlassian.net/wiki/spaces/LABS/pages/1076723721/Optima+Research#OptimaResearch-Conclusion)


Introduction
============

TODO

Current Proposed Solution
=========================

Currently the data lake is processed using Spark running on EMR. We are currently running on an old EMR Version of 5.1.0 and Spark 2.0.1 and upgrading has been difficult and the maintenance overhead can be high. Our current cluster run using EMR and SPOT instances, this is quite lucrative as we can save up to 70% of the costs of running EMR. The challenge is it may not work as we scale as we need to be able to run parallel clusters and this could lead to challenges where both clusters are bidding against each other. THe solution to this is to go on-demand at an increased cost or to potentially utilise a Serverless architecture.

One such architecture is [AWS Glue](https://aws.amazon.com/glue/), which is a fully managed, pay as you go Spark infrastructure for running ETL Jobs. The cost of AWS Glue is above the on-demand cost of EMR but there is less management and maintenance overhead.

The **proposed** **solution** uses AWS Serverless Big data solutions:

-   AWS Glue - for running ETL Jobs
-   AWS Step Functions and AWS Lambda - for Orchestration
-   AWS Glue Data Crawlers & AWS Glue Catalog - to generate a Data Catalog from data in S3
-   AWS S3 - for data storage
-   AWS Athena (Based on Presto) - for running ad hoc SQL queries against the data defined in the AWS Glue Catalog
-   AWS Cloudwatch - for monitoring

**Step Functions** are used to orchestrate Glue jobs based on Spark. They control the level of parallelism, error handling, and logging and monitoring. This is an example of a simple state machine:

![Simple](https://bytebucket.org/boxever/boxever-labs-optima/raw/36721eeb9d5e4aedc7224acb77ca3ee967f13a45/docs/Optima-Data-Pipeline-Completed.jpg?token=7a562775f5bd2d80093452f26f5f2a28dac0116a)

Metrics
-------

This is the general timeline for a job, in this case Emirates:

![](https://boxever.atlassian.net/wiki/download/attachments/1076723721/image2019-4-17_17-36-35.png?version=1&modificationDate=1555518997676&cacheVersion=1&api=v2)

#### The following table has all the details about Emirates and VIVA pipelines


#### <https://docs.google.com/spreadsheets/d/18dDowae_KWmmsl46oUJJ-LTlZd4FbdQAkXEW2rEgiSM/edit#gid=0>

#### Initial Findings

As shown on the table above, there are some optimisations we can do in the pipelines to be more cost effective.

Trusted jobs are well divided and DPUs can be increased so they run faster, specially the guest and sessions. Sessions is the most expensive. We also need to be careful with increasing the DPUs since we have issues writing to S3.

Another consideration is Parallel jobs (in the state machine) vs single spark parallel job. For example, many explore jobs are small and they run under 10min, so we are getting billed for 10min without using it because the jobs are too small, we have several options:

-   Merge some jobs into a single job so the state machine has less parallel jobs and the single spark jobs has more DPUs, figures show that we can increase from 20 to 70, so we could still achieve same performance at a cheaper rate.
-   Another option is to use Python shell, this is recommended by AWS for small jobs:
    -   <https://aws.amazon.com/about-aws/whats-new/2019/01/introducing-python-shell-jobs-in-aws-glue/>
    -   You also get billed fo 1 min intervals not 10min but it may be too slow for the current data size.

The figures show that there is a relation between data size and optimal DPUs, Viva required less DPUs because the data is smaller but it is not a linear relation as the dataset gets bigger adding DPUs will not improve performance. It also depends on the data itself, for example trusted orders is 94GB and the optimal DPU is 45 whereas Guests is just 164GB but the optimal DPU is 185, so a lot more tasks can run in parallel. 

For each type of item we can document an initial DPU values based on the size and type(guest, sessions, events...) but it will be hard to get the right value an after the first run it will need to be tweaked. 

There is also a difference between runs, this may happen because of the underlying infrastructure.

Pros and Cons
-------------


| Pros        | Cons           | 
| ------------- |-------------| 
| Very Easy to Setup     | Lack of Control: cannot set executor or driver memory or other fine grain settings | 
| Serverless, it runs on demand     | Expensive      |  
| Good integration with other AWS services, specially QuickSight and Athena | Not Portable, hard to migrate     |   
| Crawlers, triggers and classifiers are very powerful    | Has limitations: cannot create tables as select or views      |  
| Automatic code generation for ETL jobs and diagram generation| Constrains based on glue limits     |   



From the Initial findings it looks like Glue is a great option for Fully Serverless Data Analysis using QuickSight and Athena and the power of Crawlers. To run just ETL jobs it may not be worth the costs.

### **References**

#### Articles

-   [Martin Fowler - Data Lake](https://martinfowler.com/bliki/DataLake.html)

-   [The different type of Spark functions (custom transformations, column functions, UDFs)](https://medium.com/@mrpowers/the-different-type-of-spark-functions-custom-transformations-column-functions-udfs-bf556c9d0ce7)

-   [Chaining Custom DataFrame Transformations in Spark](https://medium.com/@mrpowers/chaining-custom-dataframe-transformations-in-spark-a39e315f903c)

-   [Spark User Defined Functions (UDFs)](https://medium.com/@mrpowers/spark-user-defined-functions-udfs-6c849e39443b)

-   [Split 1 column into 3 columns in Spark Scala](https://stackoverflow.com/questions/39255973/split-1-column-into-3-columns-in-spark-scala)

-   [Data Cleansing and Exploration for the New Era with Optimus](https://medium.com/@faviovazquez/data-cleansing-and-exploration-for-the-new-era-with-optimus-7a2ea2d996f8)

-   [Spark + Parquet In Depth](https://databricks.com/session/spark-parquet-in-depth)

-   [Resume AWS Step Functions from Any State](https://aws.amazon.com/blogs/compute/resume-aws-step-functions-from-any-state/)

#### Case Studies

-   [Delivering High Quality Analytics at Netflix](https://www.youtube.com/watch?v=nMyuCdqzpZc)

#### Tooling

-   [Optimus](https://hioptimus.com/) - Agile Data Science Workflows made easy with Python and Spark.

-   [Quick Start With Apache Livy](https://dzone.com/articles/quick-start-with-apache-livy)

Alternative Solutions
=====================

Spark on EMR
------------

Use EMR but still use State Machines to run tasks in parallel. This is the Do It Yourself version of the current solution using Glue, more complicated but more cost effective.

[**See this**](https://github.com/aws-samples/aws-etl-orchestrator) for more information about using step functions to orchestrate ETL.

This is the simplified workflow:

![](https://boxever.atlassian.net/wiki/download/attachments/1076723721/image2019-4-16_16-39-50.png?version=1&modificationDate=1555429192225&cacheVersion=1&api=v2)

-   A Cloud Watch Event will trigger the pipeline and call a Lambda Function
-   The Lambda Function will invoke the **Step Function** state machine.

    -   A lambda function will use [cloud formation](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cloudformation.html) to create a [EMR cluster.](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-emr-cluster.html)
    -   Once is in READY state, the current Scala pipeline will run using the Spark Context instead of the Glue Context
    -   Lambda will use [Apache Livy](https://livy.incubator.apache.org/docs/latest/rest-api.html) to query the job status so lambda can orchestrate the different jobs and perform retries and parallel execution. Apache Livy will be used also to collect metrics.
    -   [See this](https://aws.amazon.com/blogs/big-data/orchestrate-apache-spark-applications-using-aws-step-functions-and-apache-livy/) for more information.
    -   One issue is that we still want to use the Glue Data Catalog to run Athena and Looker and other services in the future such as QuickSight or Redshift Spectrum. To overcome this, we must upgrade to EMR version 5.8.0 or higher which supports writing to the Glue Data catalog as the Metastore for Spark SQL. See [**this**](https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-spark-glue.html) for more info.
    -   Destroy the cluster

### Cost Estimation

TODO

We need a POC

### Pros and Cons

| Pros        | Cons           | 
| ------------- |-------------| 
| Cheap, we can use spot instances | Harder to setup as compared to Glue |
| We can still use Glue Data Catalog without paying high amounts for ETL jobs | We need to upgrade EMR |
| More control over the machines, the cpu, memory, etc. We set our own limits |
 
| More Portable than a Serverless Offering |
 |
| Still same advantages as the current solution |
 |

Apache Flink
------------

Use [Apache Flink](https://flink.apache.org/) to run the data pipelines. Flink is the 4th generation big data streaming engine for real time and batch data. It is faster than Spark and also stateful, it can handle out of order events which may solve some of our issues.

**Flink** was created in 2009 and now has a great community and it used in production by many large organizations. It is an **open source stream processing framework** for **distributed**, **high-performing** and **always available** streaming applications. Check the [runtime documentation ](https://ci.apache.org/projects/flink/flink-docs-release-1.7/concepts/runtime.html)for more details. A key concept is that Flink is **stateful**, this means that you can recall past events to aggregate data. This is very important for many use cases where stateless processing it is not enough. It also has a **SQL** like interface to process streams and batch data. Flink has lots of [**use cases**](https://flink.apache.org/usecases.html), just like [Kafka](https://kafka.apache.org/). You can use Flink for [event driven microservices](https://microservices.io/patterns/data/event-driven-architecture.html), **data analytics** or data pipelines for [**ETL**](https://en.wikipedia.org/wiki/Extract,_transform,_load); among many others. Flink supports **Iterative processing** to train AI models and has a machine learning API that developers can easily integrate with the streaming apps. It also has a graph API. 

**Flink has also a batch processing API. **Batch is just bounded data (has a limit) and streams are unbounded data. 

Flink is very **fast and has very low latency**, much better than Spark. It also has a robust **fault tolerance**, apps can restart exactly from the same point where they fail with **exactly once deliver semantics**. Flink** can handle events out of order**, this is very important because in real life scenarios there are delay and failures in the network that we need to take into consideration.

Flink can scale while the app is running, it is more flexible than Kafka which you need to manually set new partitions and wait for rebalance.

**Flink can run as stand alone cluster or on top of Hadoop, Kubernetes or Mesos**. So it is not coupled to any platform making it very portable.

It also has many **integrations** and connectors to integrate with Twitter, Kafka, cloud providers and much more.

For us, the stateful nature, higher performance and handling out of order events are the biggest selling points. 

Flink [is **supported in EMR**](https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-flink.html). So, the way to orchestrate the pipeline will be similar to the previous one.

The main **problem** with Flink is that has a very limited integration with Hive to use it as a Metastore. The [feature](https://issues.apache.org/jira/browse/FLINK-10556) is still in progress. More info [here](https://www.slideshare.net/BowenLi9/integrating-flink-with-hive-xuefu-zhang-and-bowen-li-seattle-flink-meetup-feb-2019).

The main advantage is that Flink could be also used in RT simplifying and unifying the tech stack. We can also create Stream tables to have real time views which are stateful.

### Cost Estimation

TODO

We need a POC

### Pros and Cons

|Pros|Cons|
| --- | --- |
| Faster than Spark and Glue | No integration with Hive |
| Real Time data streaming with [SQL](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/sql.html) support | Less popular and less community support |
| Stateful, it handles out of order events | More code changes |
| Can be used for RT and Batch | Quite different approach, focused more on real time |
| Good support for Kubernetes |
 |
| Better fault tolerance when failing at shuffle stage |
 |

In general, Flink may not be the best choice if we just want to replace the ETL jobs, Flink will be a better choice if we decide to move from batch to real time processing (3rd to 4th generation)

Kafka Streams
-------------

Confluent has recently introduced Kafka Streams which allow you to run ETL jobs as fast as Flink and it is much easier to setup but it doesn't support Batch processing, only real time. We will use stream applications to perform real time ETL.

Flink also supports the same idea of having a virtual table on an unbounded real time data stream. Kafka uses [KSQL](https://www.confluent.io/product/ksql/). Again, this is the 4th generation of big data which fundamentally changes the ETL process. [See this video](https://www.youtube.com/watch?v=I32hmY4diFY) for more info.

Kafka can use Stream Apps that run on a distribute matter based on the number of partitions which can run ETL in real time. Then Kafka tables can [aggregate data](https://www.confluent.io/stream-processing-cookbook/ksql-recipes/aggregating-data) creating real time views using KSQL. The [REST interface](https://docs.confluent.io/current/ksql/docs/developer-guide/api.html) can be used to query the data and tools such as [Arcadia Data](https://www.arcadiadata.com/) can be used for visualization.

More info.:

-   ETL: 
    -   <https://www.infoq.com/articles/batch-etl-streams-kafka> 
    -   <https://www.confluent.io/blog/changing-face-etl>
    -   <https://blog.codecentric.de/en/2018/03/etl-kafka/>
    -   <https://www.confluent.io/blog/how-to-build-a-scalable-etl-pipeline-with-kafka-connect/>
-   KSQL:
    -   <https://www.confluent.io/blog/ksql-streaming-sql-for-apache-kafka/>
    -   <https://www.confluent.io/blog/data-wrangling-apache-kafka-ksql>
    -   <https://www.confluent.io/stream-processing-cookbook/ksql-recipes/lookups>
    -   <https://www.confluent.io/stream-processing-cookbook/ksql-recipes/inline-streaming-aggregation>
    -   <https://www.confluent.io/stream-processing-cookbook/ksql-recipes/streaming-insurance-events>
    -   <https://www.confluent.io/stream-processing-cookbook/ksql-recipes/real-time-inventory-management>

Data streams can be moved to[ Elastic Search](https://www.elastic.co/) and[ InfluxDB](https://www.influxdata.com/) to create live dashboards which are extremely valuable for customers. [See this](https://medium.com/@raghavrastogi594/analyzing-time-series-data-using-ksql-influxdb-and-grafana-636c19393cf6) for more info.

The [Schema registry ](https://docs.confluent.io/current/schema-registry/index.html)provide a great way to manage changes on the schema without affecting clients.

### Cost Estimation

TODO

We need a POC

### Pros and Cons

|Pros|Cons|
| --- | --- |
| Very fast | Could be a bit complex to get the best performance |
| Relative easy to setup | It needs a lot of code changes and a huge shift on the way we do things |
| We already use Kafka | It will not provide the same level of BI and data analysis as a regular data lake |
| Unified tech for RT and Explore |
 |
| Streams are great to apply AI models |
 |
| Good integration with Kubernetes |
 |

Kafka by its own may not cover all the use cases but it will be a good addition for the Explore functionality adding real time dashboards which can do stateful aggregations or events, filter, anomaly detection, alerting, etc. This can also be used with Flink.

Comparison
==========

|Feature|AWS Glue|Spark EMR|Flink EMR|Kafka||Winner|Looser |Comments|
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Easy to setup |
| Migration |
| Stream Processing | NO | YES (micro) | YES | YES |
| Batch Processing | YES | YES | YES | NO |
| Deployment |
| Stateful |
| Out of order events |
| Failover |
| Machine Learning |
| Languages |
| Integrations | Other AWS Service Only: Athena, QucikSight | Hadoop Ecosystem, Kafka | Kafka, HDFS, Elastic Search, S3 | Huge Ecosystem |
| Portability |

Kubernetes
==========

Spark, Flink and Kafka can run on Kubernetes. Flink has a great support for Kubernetes.

Regardless of the solution Kubernetes could be a a really cost effective solution to run pipelines since the same machines can be reused for multiple purposes and we have full control on the cluster resources. 

Kubernetes is a broad topic and we should actively engage with the broader team regarding Kubernetes.