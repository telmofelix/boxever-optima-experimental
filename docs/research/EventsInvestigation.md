
Events Investigation
====================

Created by 

[Javier Ramos](https://boxever.atlassian.net/people/5ca1e0f423ef8d0370f4578d?ref=confluence&src=profilecard)

Last updated [about 6 hours ago](https://boxever.atlassian.net/wiki/pages/diffpagesbyversion.action?pageId=1090453555&selectedPageVersions=6&selectedPageVersions=7)

Events are the largest item in terms of volume. The Real Time system has a single topic with all the data. Several consumers process the data.

This diagram shows the main structure of the data:

![](https://documents.lucidchart.com/documents/590741fe-3cd4-48ff-9d18-5e6be7faa0b3/pages/0_0?a=596&x=51&y=-30&w=1518&h=471&store=1&accept=image%2F*&auth=LCA%20c2d4c733a3f5c8ad0e6aee1fbbc19f9c3ccb744d-ts%3D1556019199)

-   A Kafka Consumer reads data from the EMD queue which has all the items (guests, sessions, events...) in a single topic. It processes the data every hour of 100MB and writes it in S3.
    -   At this point the S3 data has duplicates
-   A nightly EMD Spark Job will process the raw S3 data, it will flatten the data and remove duplicates by taken the item with the newest time stamp. 
    -   It will create the snapshots partition by date that we use in Optima
    -   It will also retrofit events that arrive late. 
    -   Sessions, orders and guest should not change after being added to the snapshot. 
    -   Snapshots have all the data except for events.
    -   Events work a bit different:
        -   Snapshot folder only has 7 days of data and a event may change, so it is  mutable
        -   After 7 days it is move to the immutable folder and an event will not change. However, events may arrive late, up to 60 days late through the batch interface. These will get retrofitted to the immutable events folder.
        -   All events are a combination of immutable + snapshot
        -   Snapshot are partition by date and immutable by quarter
        -   Delta just has the last 2 days and this is used for other jobs to feed into OneView

Note Immutable is does not mean that the data will not change, it means that a single item will not change but new ones can be added.

This is the folder structure for events:

![](https://boxever.atlassian.net/wiki/download/thumbnails/1090453555/image2019-4-23_12-32-34.png?version=1&modificationDate=1556019156478&cacheVersion=1&api=v2&width=197&height=400)

2019 is the Snapshot.

Events Delta Solution
---------------------

The goal is to minimize the data consumption so Optima only processes the delta and not all the events which would be massive (immutable + snapshot data sets).

For events, we have two options:

-   Re process the last 60 days to account for late events
    -   Simple solution but 60 days of events is still a lot. 
-   Re process just the 7 days daily and run the whole data weekly. 

### Research Needed

-   Find out how late the events can be (60? 50? days). We can check the modified date for updates but if we get new events which are late how do we know by looking at the immutable data set?
    -   It seems that the maximum date difference in the immutable data set is 1 day. Which means that if events can arrive up to 60 days late the modifiedAt field it is not updated
-   Find out how the percentage of total events vs events later that 1 day or 7 days. We need to get the distribution. We also need to decide on the data quality for events. For example, if it is okay to be 98% accurate then if 99% of events arrive in 2 days we can just do the delta for 2 days and once a week the whole data set to catch up

### Research

The results are inconclusive. 

Looking at the immutable or snapshot data set:

![](https://boxever.atlassian.net/wiki/download/attachments/1090453555/image2019-4-24_17-52-52.png?version=1&modificationDate=1556124775040&cacheVersion=1&api=v2)

It looks like there are no late events, but looking at the batch import the modified date it is [changed](https://bitbucket.org/boxever/boxever-batch-import-job/src/87537fc9dc5d6405f3b3ab479adf16496462ec6e/src/main/java/com/boxever/bulk/job/etl/tracking/TrackingUtil.java#lines-117:118):

 event.setCreatedAt(trackingEventEvent.getOccurredAt());

 event.setModifiedAt(new Date());

So, either there is a bug in the code or a problem with the data analysis. Running the events pipeline so we can query in Athena should help.

### Solution 1: Reprocess X number of events with 99.9 accuracy

-   We need to a new partition for the immutable data set since year/quarter is too broad. We need to partition by CHANNEL/TYPE/YYYY-MM-DD
-   We need to add the data from the Snapshot dataset (7 days)  and the immutable data set to get the last X(60) days of data

### Solution 2: Reprocess subset of events with less accuracy

-   In this case we can process just 1 to 7 days. We assume that late events are rate
-   Once a week we run the whole pipeline.

Kafka Topic Consumer
--------------------

In order to get a better understanding of the data we could write a consumer that runs every night and gets data from the EMD topic. There are many variables to consider. The object type (session, event..), channel, customer... For example, we could decide that we need to go back 60 days for events but maybe customer X doesn't have late event. In order to be more efficient we need to understand better the data. 

We could build a consumer app that just writes to S3 a small file each day containing the customer, object type, channel in this format:

|

Customer

 |

Object Type

 |

Channel

 |

Created Date

 |

Modified Date

 |

Late by

 |
| --- | --- | --- | --- | --- | --- |
| Emirates | Event | WEB | .. | ... | 0 days |
| ... |\
 |\
 |\
 |\
 |\
 |

With this data we can optimize better the pipeline and answer some of the questions.

Questions
---------

Add here any questions