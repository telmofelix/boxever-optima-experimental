[TOC]

# Overview
Slowness has been observed when running MSCK REPAIR TABLE on large tables with many partition columns.
This affects order tables in the Explore (Optima) dataset in particular.
Various approaches to tackling this are outlined as follows.
      
 
## Differing Approaches

https://stackoverflow.com/questions/53067709/aws-glue-crawlers-and-large-tables-stored-in-s3

### Glue Crawler

AWS Glue crawlers can be set up to run on a schedule or on demand

https://docs.aws.amazon.com/athena/latest/ug/glue-best-practices.html

### Alter Table Add Partition

https://stackoverflow.com/questions/47546670/how-to-make-msck-repair-table-execute-automatically-in-aws-athena

https://docs.aws.amazon.com/athena/latest/ug/alter-table-add-partition.html

### Configuration Tuning

https://www.cloudera.com/documentation/enterprise/5-11-x/topics/admin_hive_on_s3_tuning.html

https://stackoverflow.com/questions/53667639/what-does-msck-repair-table-do-behind-the-scenes-and-why-its-so-slow

### Upgrade Hive

Some issues wrt MSCK REPAIR TABLE may be resolved in later versions of Hive

https://issues.apache.org/jira/browse/HIVE-21040