Step Functions and Lambda
-------------------------

-   Always set a reasonable timeout in the step functions
-   Do not pass large payloads between functions, max is 32KB but keep it smaller, use S3 to store large data
-   Do not create long pipelines, there is a maximum 25,000 entries in the execution history, you can [start new execution](https://docs.aws.amazon.com/step-functions/latest/dg/tutorial-continue-new.html) and break the state machine in smaller ones.
-   Handle [errors](https://docs.aws.amazon.com/step-functions/latest/dg/concepts-error-handling.html#error-handling-retrying-after-an-error) and perform [retries](https://docs.aws.amazon.com/step-functions/latest/dg/bp-lambda-serviceexception.html).
-   Use [logging](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-functions.html) in Cloudwatch. Enable X-ray for advanced monitoring.
-   Collect [metrics](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue.html#Glue.Client.get_job_run) while running the job and send them to cloud watch.
-   Use [**waiters**](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/clients.html#waiters) when [possible](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue.html#Glue.Client.get_waiter) instead of pooling while waiting for an ETL job.
-   Avoid [Latency](https://docs.aws.amazon.com/step-functions/latest/dg/bp-activity-pollers.html) When Polling for Activity Tasks.
-   Select multiple AZs for Lambda functions
-   Set the lowest memory possible. Note that the CPU power increases also with memory, for simple pooling functions set the minimum.
-   Keep any init logic or expensive logic away from the handler function in Lambda. Keep in the handler any code that must execute per request. Check [cold starts](https://hackernoon.com/cold-starts-in-aws-lambda-f9e3432adbf0) in Lambda. On first invoke, a container will be created, the whole file is executed and then the handler, after that all other calls will just call the handler.
-   Use one IAM role per function
-   Use [SAM](https://github.com/awslabs/serverless-application-model) or [Serverless](https://serverless.com/) Framework for testing and development. Visual Studio code has good support for them. Serverless framework support step functions.
-   Use Environment Variables (and Parameter Store) to separate code from configuration. Use KMS for sensitive data.

AWS Glue, Spark, Athena
-----------------------

-   Set the [right](https://medium.com/parrot-prediction/partitioning-in-apache-spark-8134ad840b0) partition strategy: In general, *more numerous* partitions allow work to be distributed among more workers, but *fewer partitions* allow work to be done in larger chunks (and often quicker). Check [this](https://techmagie.wordpress.com/2015/12/19/understanding-spark-partitioning/) and [that](http://stackoverflow.com/documentation/apache-spark/5822/partitions#t=201609112000500779093) for guidance.
    -   Files should not be too small and partitions should not be too high (> 100000)
    -   Keep in mind the purpose of the job and the end data set.  For example, partition by a nested yyyy/mm/dd is faster than a single yyyy-mm-dd but it is [harder](https://community.hortonworks.com/questions/29031/best-pratices-for-hive-partitioning-especially-by.html) to query.
    -   It depends on the queries if you query by date most of the time, use date as partition, but this will make other queries slower, so keep that in mind
-   Use [AWS Bookmarks](https://docs.aws.amazon.com/glue/latest/dg/monitor-continuations.html) when possible, your ETL jobs will just. This allows to keep the source and sink dataset in sync and only process the data that has changed. Note that Parquet and ORC formats are currently not supported. 
-   Optimize files. Not too large or to small (<128MB). if your files are too small (generally less than 128 MB), the execution engine might be spending additional time with the overhead of opening Amazon S3 files, listing directories, getting object metadata, setting up data transfer, reading file headers, reading compression dictionaries, and so on. On the other hand, if your file is not splittable and the files are too large, the query processing waits until a single reader has completed reading the entire file. That can reduce parallelism. One remedy to solve your small file problem is to use the [S3DistCP](http://docs.aws.amazon.com/emr/latest/ReleaseGuide/UsingEMR_s3distcp.html) utility on Amazon EMR.
-   Compress and split files

-   Bucket your data. With bucketing, you can specify one or more columns containing rows that you want to group together, and put those rows into multiple buckets. This allows you to query only the bucket that you need to read when the bucketed columns value is specified, which can dramatically reduce the number of rows of data to read.

-   Optimize columnar data store generation. One parameter that could be tuned is the *block *size which represent the maximum number rows that can fit into one block in terms of size in bytes. The larger the block size, the more rows can be stored in each block.  By default, the Parquet block size is 128 MB. We recommend a larger block size if you have tables with many columns, to ensure that each column block remains at a size that allows for efficient sequential I/O. Another parameter that could be tuned is the compression algorithm on data blocks. 

-   Athena uses Presto underneath the covers. Use Presto [best practices](https://support.treasuredata.com/hc/en-us/articles/360001450908-Presto-Performance-Tuning) to optimize the queries.
    -   If you are using the ORDER BY clause to look at the top or bottom *N* values, then use a LIMIT clause to reduce the cost of the sort significantly by pushing the sorting and limiting to individual workers, rather than the sorting being done in a single worker.
    -   When you join two tables, specify the larger table on the left side of join and the smaller table on the right side of the join. Presto distributes the table on the right to worker nodes, and then streams the table on the left to do the join. If the table on the right is smaller, then there is less memory used and the query runs faster.
    -   When using GROUP BY in your query, order the columns by the cardinality by the highest cardinality (that is, most number of unique values, distributed evenly) to the lowest.
    -   When you are filtering for multiple values on a string column, it is generally better to use regular expressions instead of using the LIKE clause multiple times. This is particularly useful when you are comparing a long list of values.
    -   Use approximate functions: When an exact number may not be required--for instance, if you are looking for which webpages to deep dive into, consider using approx_distinct().

    -   Only include the columns that you need!

-   **See tips [here](https://aws.amazon.com/blogs/big-data/top-10-performance-tuning-tips-for-amazon-athena/).**