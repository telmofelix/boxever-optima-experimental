# Sessions modified time after what expected

## Context

Sessions, including WEB, are modified (`session.modified_at` - `session.created_at`) time after what expected, some of them more than 45 days (even some are modified more than 1300 days after creation). If this is correct, sessions are mutable and this may impact Optima and the number of sessions that have to handle.

## Research

There is a session migration process. When a `IDENTITY` event is triggered, the session `guest_ref`field will be configured to all anonymous sessions (sessions without `guest_ref`) with same `browser_ref` than the session that fires the event.

### Where this update is done

This is done by [`batch-import-job`](https://bitbucket.org/boxever/boxever-batch-import-job)/[`batch-import-service`](https://bitbucket.org/boxever/boxever-batch-service). The only fields changed in migration process are guest_ref and modified_at. The rest of fields are copied. The code is in `boxever-batch-import-job` project, method `com.boxever.bulk.job.etl.migration.DefaultMigrationProcessor#migrateSession(com.boxever.models.v2.Session, com.boxever.models.v2.Guest, java.util.Date)`.

```java
private void migrateSession(Session session, Guest newGuest, Date now) {
    Session originalSession = copy(session, Session.class);
    session.setGuestRef(newGuest.getRef());
    session.setModifiedAt(now);
    sessionsStore.update(UpdateSessionQuery.replace(session, originalSession));
    sessionsQueue.publish(session, originalSession);
}
```

## Approach: Process sessions modified yesterday  

Sessions are mutable because of *session migration process*. So, the trusted job can load the sessions modified yesterday and insert or update the sessions trusted store.

Before execute the *raw to trusted* job in the pipeline we prepare a dataset with the sessions updated yesterday. Then the rest of the jobs in the pipeline downstream will process this sessions, adding the result to the output dataset of each job (trusted, explore, ...).

Based on the test, session updated daily are about 0.3% and 1% to total of sessions snapshot, so, processiong time is reduced from 40 minutes to 7 in the first test.

### Number of sessions to be processed

To calculate the **total number of sessions**, the **sessions** that have been **modified yesterday** and the **percentage** we can execute a query like that:

```sql
select all_sessions.total                                                   as total_sessions,
       yesterday.total                                                      as yesterday_sessions,
       cast(yesterday.total as double) / cast(all_sessions.total as double) as percentage
 from (
    select count(*) as total
      from sessions session
      where session.channel = 'WEB'
        and DATE_DIFF('day', session.modified_at, NOW()) between 1 and 2
    ) yesterday,
   (
    select count(ref) as total
      from emirates.sessions
     where channel = 'WEB'
    ) all_sessions
```

At this moment the query execution result is:

```text
total_sessions  yesterday_sessions  percentage
423287010       1462034             0.0034540015768497125
```

So, less than a 0.4% of sessions (1.46 millons of sessions over more 423 millions) have been modified yesterday.

## About quarantined sessions

When we process an entity, sessions in this case, some validations validations are performed, and if validation fails the session is is stored in quarantine database.

If we are reprocessing sessions, because of mutability, two different things can happens:

- if an **existing session** arrives:
  - if **validation pass**:
    - if **is trusted** session, the session must be **updated in trusted** database.
    - if **is quarantined** session, the session must be **inserted in trusted** database and **removed from quarantine** database.
  - if **validation fails**:
    - if **is trusted** session, the session must be **removed from trusted** database and *inserted in quarantine* database.
    - if **is quarantined** session, the session must be **updated in quarantine** database.
- if a session **arrives for the first time**:
  - if **validation pass** then will be **stored in trusted** database
  - if **validation fails**, will be **stored in quarantine**database.
