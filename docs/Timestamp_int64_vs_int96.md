[TOC]

# Overview

Currently Spark writes Timestamps as int96 by default. If you specify a spark session conf variable of the following Spark will write Timestamps as in64.

    spark.conf.set("spark.sql.parquet.int64AsTimestampMillis", "true")
 
There is currently an open question of whether we should use the default int96 in Spark that is compatible with 
Impala, Presto, Athena or to use int64.

Below is some analysis of testing using Spark, int96, int64 on Hive, Presto and AWS Athena and its compatibility.

## Executive Summary

* After analysis int96 does not have any compatibility issues when using Athena, Hive or Presto natively.
* After analysis int64 does only has compatibility issues when using Hive natively. Athena and Presto natively are ok.

The recommendation is to use the Spark's default of int96 for Timestamps to ensure compatibility across Spark, Athena, 
Hive and Presto.

# Scenario JSON

## JSON Input

    {
    	"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a",
    	"createdAt": 1546032815914
    }
    
Each directory contains many files and each file is constructed with a flattened json document per line like the following

    {"ref":"68ae46d0-3916-4d93-8b91-21425a351d5a","createdAt":1546032815914}
    {"ref":"ce29f8bf-6c60-421d-8035-29cadecee40c","createdAt":1546032815914}
 
 
# Timestamp int96
We take the above an perform a conversion on field createdAt to a timestamp in int96. We do not need to explicitly set 
the conf value "spark.sql.parquet.int96AsTimestampMillis" to true as Spark will default to it

    val spark = createSession
    
    val guestsDF = spark.read.json(inputDir)
    
    val trustedGuestsDF = guestsDF.withColumn("createdAt", milliToTimestamp($"createdAt"))
    
    trustedGuestsDF.write.mode(SaveMode.Overwrite).parquet(outputDir)

More Info: TimestampInt96FormatExample.scala

## Parquet Schema as Interpreted by Spark
Below is the schemas as interpreted by Spark.

### Guests
The guests file is stored in parquet format

    spark.read.parquet(outputDir).printSchema

    root
     |-- createdAt: timestamp (nullable = true)
     |-- ref: string (nullable = true) 
     

## Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli.
 
### Guests

    parquet-tools schema build/etl/TimestampInt96FormatExample/

    message spark_schema {
      optional int96 createdAt;
      optional binary ref (UTF8);
    }

## Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field

    org.apache.spark.sql.parquet.row.metadata
    
### Guests

    parquet-tools meta build/etl/TimestampInt96FormatExample/

    {
        "type": "struct",
        "fields": [{
            "name": "createdAt",
            "type": "timestamp",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "ref",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }]
    }
 
    
## Athena Testing

### Athena Create Table
 
    CREATE EXTERNAL TABLE `guests_int96`(
      `createdat` timestamp, 
      `ref` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/TimestampInt96FormatExample/'
    TBLPROPERTIES (
      'classification'='parquet');
      
### SQL Query

    select * from guests_int96;
    
### SQL Result

    +-----------------------+-------------------------------------+
    |           createdat   |              ref                    |
    +-----------------------+-------------------------------------+
    |2018-12-28 21:33:35.914|68ae46d0-3916-4d93-8b91-21425a351d5a |
    |2018-12-28 21:33:35.914|ce29f8bf-6c60-421d-8035-29cadecee40c |
    +-----------------------+-------------------------------------+


## Hive Testing

### Hive Create Table
 
    CREATE EXTERNAL TABLE `guests_int96`(
      `createdat` timestamp, 
      `ref` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/TimestampInt96FormatExample/'
    TBLPROPERTIES (
      'classification'='parquet');
      
### Hive SQL Query

    select * from guests_int96;
    
### Hive SQL Result

    +-----------------------+-------------------------------------+
    |           createdat   |              ref                    |
    +-----------------------+-------------------------------------+
    |2018-12-28 21:33:35.914|68ae46d0-3916-4d93-8b91-21425a351d5a |
    |2018-12-28 21:33:35.914|ce29f8bf-6c60-421d-8035-29cadecee40c |
    +-----------------------+-------------------------------------+
   
## Presto Testing 
We currently replied on the table being created via Hive.

### Presto SQL Query

    select * from guests_int96;
    
### Presto SQL Result

            createdat        |                 ref
    -------------------------+--------------------------------------
     2018-12-28 21:33:35.914 | 68ae46d0-3916-4d93-8b91-21425a351d5a
     2018-12-28 21:33:35.914 | ce29f8bf-6c60-421d-8035-29cadecee40c
    
### Presto Create Table From SQL Result

    create table guests_int96_copy as select * from guests_int96;
  
### Presto SQL Result

    CREATE TABLE: 2 rows
  
### Presto SQL Query

    select * from guests_int96_copy;
    
### Presto SQL Result

            createdat        |                 ref
    -------------------------+--------------------------------------
     2018-12-28 21:33:35.914 | 68ae46d0-3916-4d93-8b91-21425a351d5a
     2018-12-28 21:33:35.914 | ce29f8bf-6c60-421d-8035-29cadecee40c
     
### Presto Show Create Query Table

    show create table guests_int96_copy;

### Presto Show Create Table Query Result

     CREATE TABLE hive.workspace.guests_int96_copy (
        createdat timestamp,
        ref varchar
     )
     WITH (
        format = 'PARQUET'
     )
  
## Parquet Tools testing Presto files 
We downloaded the presto generated files and tested them via Parquet Tool cli.

### Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli.

    parquet-tools schema /guests_int64_copy/

    message hive_schema {
      optional int96 createdat;
      optional binary ref (UTF8);
    }
    
### Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field below but is missing

    org.apache.spark.sql.parquet.row.metadata

## Summary

After analysis int96 does not have any compatibility issues when using Athena, Hive or Presto natively.
  
    
# Timestamp int64
We take the above an perform a conversion on field createdAt to a timestamp in int64. We need to explicitly set the conf
value of "spark.sql.parquet.int64AsTimestampMillis" to true otherwise Spark will default to "spark.sql.parquet.int96AsTimestampMillis"

    val spark = createSession
    spark.conf.set("spark.sql.parquet.int64AsTimestampMillis", "true")
    
    val guestsDF = spark.read.json(inputDir)
    
    val trustedGuestsDF = guestsDF.withColumn("createdAt", milliToTimestamp($"createdAt"))
    
    trustedGuestsDF.write.mode(SaveMode.Overwrite).parquet(outputDir)

More Info: TimestampInt64FormatExample.scala

## Parquet Schema as Interpreted by Spark
Below is the schemas as interpreted by Spark.

### Guests
The guests file is stored in parquet format

    spark.read.parquet(outputDir).printSchema

    root
     |-- createdAt: timestamp (nullable = true)
     |-- ref: string (nullable = true) 
     

## Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli.
 
### Guests

    parquet-tools schema build/etl/TimestampInt64FormatExample/

    message spark_schema {
      optional int64 createdAt (TIMESTAMP_MILLIS);
      optional binary ref (UTF8);
    }

## Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field

    org.apache.spark.sql.parquet.row.metadata
    
### Guests

    parquet-tools meta build/etl/TimestampInt64FormatExample/

    {
        "type": "struct",
        "fields": [{
            "name": "createdAt",
            "type": "timestamp",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "ref",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }]
    }
    
## Athena Testing

### Athena Create Table
 
    CREATE EXTERNAL TABLE `guests_int64`(
      `createdat` timestamp, 
      `ref` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/TimestampInt64FormatExample/'
    TBLPROPERTIES (
      'classification'='parquet');
      
### SQL Query

    select * from guests_int64;
    
### SQL Result

    +-----------------------+-------------------------------------+
    |           createdat   |              ref                    |
    +-----------------------+-------------------------------------+
    |2018-12-28 21:33:35.914|68ae46d0-3916-4d93-8b91-21425a351d5a |
    |2018-12-28 21:33:35.914|ce29f8bf-6c60-421d-8035-29cadecee40c |
    +-----------------------+-------------------------------------+
    
## Hive Testing
   
### Hive Create Table
 
    CREATE EXTERNAL TABLE `guests_int64`(
      `createdat` timestamp, 
      `ref` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/TimestampInt64FormatExample/'
    TBLPROPERTIES (
      'classification'='parquet');
      
### Hive SQL Query

    select * from guests_int64;
    
### Hive SQL Result

    Failed with exception java.io.IOException:org.apache.hadoop.hive.ql.metadata.HiveException: java.lang.ClassCastException: org.apache.hadoop.io.LongWritable cannot be cast to org.apache.hadoop.hive.serde2.io.TimestampWritable
    

## Presto Testing
We currently replied on the table being created via Hive.

### Presto SQL Query

    select * from guests_int64;
    
### Presto SQL Result

            createdat        |                 ref
    -------------------------+--------------------------------------
     2018-12-28 21:33:35.914 | 68ae46d0-3916-4d93-8b91-21425a351d5a
     2018-12-28 21:33:35.914 | ce29f8bf-6c60-421d-8035-29cadecee40c
  
### Presto Create Table From SQL Result

    create table guests_int64_copy as select * from guests_int64;
  
### Presto SQL Result

    CREATE TABLE: 2 rows
  
### Presto SQL Query

    select * from guests_int64_copy;
    
### Presto SQL Result

            createdat        |                 ref
    -------------------------+--------------------------------------
     2018-12-28 21:33:35.914 | 68ae46d0-3916-4d93-8b91-21425a351d5a
     2018-12-28 21:33:35.914 | ce29f8bf-6c60-421d-8035-29cadecee40c
     
### Presto Show Create Query Table

    show create table guests_int64_copy;

### Presto Show Create Table Query Result

     CREATE TABLE hive.workspace.guests_int64_copy (
        createdat timestamp,
        ref varchar
     )
     WITH (
        format = 'PARQUET'
     )

## Parquet Tools testing Presto files 
We downloaded the presto generated files and tested them via Parquet Tool cli.

### Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli.

    parquet-tools schema /guests_int64_copy/

    message hive_schema {
      optional int96 createdat;
      optional binary ref (UTF8);
    }
    
### Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field below but is missing

    org.apache.spark.sql.parquet.row.metadata
     
## Summary

After analysis int64 does only has compatibility issues when using Hive natively. Athena and Presto natively are ok.

# References

* [Parquet Logical Types](https://github.com/apache/parquet-format/blob/master/LogicalTypes.md)
* [Spark's int96 time type](https://stackoverflow.com/questions/42628287/sparks-int96-time-type)
* [INT96 should be marked as deprecated](https://issues.apache.org/jira/browse/PARQUET-323)

