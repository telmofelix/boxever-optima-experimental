[TOC]

# Requirements
The following is required and must be installed before the project can be built or run

* One Login Account
* Boxever AWS Account
* Mac OS X or Linux
* Mercurial 4.8.2 or higher
* Ansible v2.8 - See Ansible v2.8 Installation Guide below
* Java 8
* [HomeBrew](https://brew.sh/) (Mac OS only)



# AWS Glue Assembly Dependent Jars
The AWS glue-assembly.jar is in Boxever Nexus and should be pulled in automatically using gradle.

You can also download and install the AWS glue-assembly.jar dependent jars into you local `maven` repository manually

    curl https://s3-eu-west-1.amazonaws.com/boxever-labs-binaries/services/aws-glue/glue-assembly.jar -o glue-assembly.jar
    mvn install:install-file -Dfile=glue-assembly.jar -DgroupId=com.amazonaws -DartifactId=aws-glue-assembly -Dversion=20190202 -Dpackaging=jar

Note: 
* The glue-assembly.jar was originally sourced from a AWS Glue Dev Endpoint Server at location `/usr/share/aws/glue/etl/jars/glue-assembly.jar`
* However it must be noted that glue-assembly.jar dependencies are unknown and the jar may be an uber jar.
* https://github.com/awsdocs/aws-glue-developer-guide/blob/master/doc_source/dev-endpoint-tutorial-repl.md
* https://boxever.atlassian.net/browse/LABS-910


# Building
Ensure you run the following to build the Optima jar before Provisioning

    ./gradle clean build
    


# Provisioning
The ideal way to provision Boxever Optima is to create a new AWS account. Alternatively you can use an existing AWS 
account however this is not recommended as there could be conflicts in naming of resources so ensure you do due diligence 
if proceeding with an existing stack. To reduce errors the majority of provisioning of Boxever Optima is automated 
via Ansible however there is still some manual steps.

## Ansible Provisioning
The following will be automatically provisioned using Ansible.

* Cloudwatch Rules
* SNS Topic 
* Step Functions & IAM Roles
* Lambda Functions & IAM Roles
* Glue ETL Jobs & IAM Roles
* S3 Bucket

## Manual Provisioning

* Production Bucket Policy Permissions in [boxever-aws-security/bucket_policies](https://bitbucket.org/boxever/boxever-aws-security/src/default/roles/bucket_policies/files/)
* DNS Names in [boxever-aws-security/dev_route_53_records](https://bitbucket.org/boxever/boxever-aws-security/src/default/roles/dev_route_53_records/tasks/main.yml)
* AWS Glue Security Configuration in [AWS Glue Console](https://eu-west-1.console.aws.amazon.com/glue/home?region=eu-west-1#securityConfigurations:) 
* Lambda Functions [Log Metrics Alert](./optima-alarms-pagerduty.md) 

### AWS Glue Security Configuration
A default security configuration needs to be setup in the [AWS Glue Console](https://eu-west-1.console.aws.amazon.com/glue/home?region=eu-west-1#securityConfigurations:)

    Name:                                   default
    S3 encryption mode:                     SSE-S3
    CloudWatch logs encryption mode:        DISABLED
    Job bookmark encryption mode:           DISABLED
    
Note: We may need to create a policy per customer with different settings. Currently this is set as SSE-S3 as is the Explore EMR clusters. Note `boxever-emr-local-disk-encryption-key` is 
only for local disk encryption and not S3.


# Create Ansible Stack Provisioning File
Create a stack file in the `ansible` directory in the following format boxever-optima-{customer-label}-{optima-environment}-vars.yaml

Example

    boxever-optima-spinair-labs-vars.yaml

### Create Ansible Stack Provisioning File
Create a stack file in the `ansible` directory in the following format `boxever-optima-{customer-label}-{optima-environment}-vars.yaml` 
    
Example

    boxever-optima-spinair-labs-vars.yaml

### Alerting and Pager Duty integration 

The document [Optima alerting to PagerDuty](./optima-alarms-pagerduty.md) explains how to configure 
Optima pipelines - Pager Duty integration.

# Ansible v2.8 Installation Guide
On Mac OS X run the following commands to install Ansible.

    brew install ansible
    pip install boto
    pip install boto3
    pip install botocore
    pip install pyyaml

    
## Ansible Modules
There are a few custom or patched modules used in this project. To active execute the following

    rm ~/.ansible/plugins/modules/boxever (if already existing)
    mkdir -p ~/.ansible/plugins/modules/
    ln -s $(pwd)/ansible_modules/boxever ~/.ansible/plugins/modules/boxever
    
Then verify installation

    ansible-doc -t module bx_aws_glue_job
    ansible-doc -t module bx_aws_step_functions_state_machine

For more information see [https://docs.ansible.com/ansible/latest/dev_guide/developing_locally.html](https://docs.ansible.com/ansible/latest/dev_guide/developing_locally.html)



# Creating and Updating Optima Environment

## Run Provisioning Script

Command

    ./updateOptima.sh {optima-environment} {customer-label}
    
Example

    ./updateOptima.sh labs emirates


