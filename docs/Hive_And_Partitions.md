[TOC]

# Overview
To have better read performance, it is recommended to partition your data. In Spark when writing out a data frame you 
can specify the columns to partition by using the following syntax to partition by columns *guest_type* and *create_at_year_month* 

    df.write
      .partitionBy("guest_type", "create_at_year_month") 
      
Hive ignores case and this results in all columns as being lower cased, therefore when writing partitions the columns by 
specified in partitionBy must be lower cased otherwise Hive cannot read them. This poses problems if we want to maintain 
camelCase in the Trusted v2 Data set.
 
## Example Problem

### Spark Write Data Frame

    df.write
      .partitionBy("guestType", "createdAtYearMonth")
      .parquet(outputDir + "/v2Trusted/guests/")
    
### Spark Print Schema

    root
     |-- createdAt: timestamp (nullable = true)
     |-- emails: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- firstName: string (nullable = true)
     |-- gender: string (nullable = true)
     |-- lastName: string (nullable = true)
     |-- lastSeen: timestamp (nullable = true)
     |-- ref: string (nullable = true)
     |-- guestType: string (nullable = true)
     |-- createdAtYearMonth: string (nullable = true)
    
### Spark Show

    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |           createdAt|              emails|firstName|gender|lastName|            lastSeen|                 ref|guestType|createdAtYearMonth|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |2018-12-28 21:33:...|[pete.smith@boxev...|     Jack|  male|   Smith|2018-12-28 21:33:...|68ae46d0-3916-4d9...| customer|           2018-12|
    |2018-12-28 21:33:...|[paul.smith@boxev...|     Paul|  male|   Smith|2018-12-28 21:33:...|ce29f8bf-6c60-421...| customer|           2018-12|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+

### S3 Folder Structure

    guests/guesType=customer/createdAtYearMonth=2019-01/part-00000-ed9d8a55-a296-4179-9cc6-a1fb40fc7dfc.c000.snappy.parquet

### Athena/Hive Create Table

    CREATE EXTERNAL TABLE `guests`(
      `createdat` timestamp, 
      `emails` array<string>, 
      `firstname` string, 
      `gender` string, 
      `lastname` string, 
      `lastseen` timestamp)
    PARTITIONED BY ( 
      `guestType` string, 
      `createdAtYearMonth` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/v2Trusted/guests/'
    TBLPROPERTIES (
      'classification'='parquet');
      
or with

    PARTITIONED BY ( 
      `guesttype` string, 
      `createdatyearmonth` string)
   
### Athena/Hive Load Partitions Query

    MSCK REPAIR TABLE guests;
  
### Athena/Hive Load Partitions Result 

    Partitions not in metastore:	guests:guestType=customer/createdAtYearMonth=2018-12

### SQL Query

    select * from guests;
    
### SQL Result

    Zero records returned.

#### Root Cause

Hive always lower cases column names including partition by columns, however the physical data structure on disk or S3 
is camel cased meaning Hive will never locate cased partitions.  



## Example Solution
To resolve the issue ensure Spark lower cases the partition columns.

### Spark Write Data Frame

    df.write
      .partitionBy("guesttype", "createdatyearmonth")
      .parquet(outputDir + "/v2Trusted/guests/")

### Spark Print Schema

    root
     |-- createdAt: timestamp (nullable = true)
     |-- emails: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- firstName: string (nullable = true)
     |-- gender: string (nullable = true)
     |-- lastName: string (nullable = true)
     |-- lastSeen: timestamp (nullable = true)
     |-- ref: string (nullable = true)
     |-- guesttype: string (nullable = true)
     |-- createdatyearmonth: string (nullable = true)
    
### Spark Show

    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |           createdAt|              emails|firstName|gender|lastName|            lastSeen|                 ref|guesttype|createdatyearmonth|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |2018-12-28 21:33:...|[pete.smith@boxev...|     Jack|  male|   Smith|2018-12-28 21:33:...|68ae46d0-3916-4d9...| customer|           2018-12|
    |2018-12-28 21:33:...|[paul.smith@boxev...|     Paul|  male|   Smith|2018-12-28 21:33:...|ce29f8bf-6c60-421...| customer|           2018-12|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+

## S3 Structure

    guests/guestype=customer/createdatyearmonth=2019-01/part-00000-ed9d8a55-a296-4179-9cc6-a1fb40fc7dfc.c000.snappy.parquet 
 
### Athena/Hive Create Table
 
    CREATE EXTERNAL TABLE `guests`(
      `createdat` timestamp, 
      `emails` array<string>, 
      `firstname` string, 
      `gender` string, 
      `lastname` string, 
      `lastseen` timestamp)
    PARTITIONED BY ( 
      `guesttype` string, 
      `createdatyearmonth` string)
    ROW FORMAT SERDE 
      'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe' 
    STORED AS INPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat' 
    OUTPUTFORMAT 
      'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
    LOCATION
      's3://boxever-explore-spinair-labs-eu-west-1/sample-data/v2Trusted/guests/'
    TBLPROPERTIES (
      'classification'='parquet');
      
### Athena/Hive Load Partitions Query

    MSCK REPAIR TABLE guests;
  
### Athena/Hive Load Partitions Result 

    Partitions not in metastore:	guests:guesttype=customer/createdatyearmonth=2018-12
    Repair: Added partition to metastore guests:guesttype=customer/createdatyearmonth=2018-12
    
### SQL Query

    select * from guests;
    
### SQL Result

    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |           createdAt|              emails|firstName|gender|lastName|            lastSeen|                 ref|guesttype|createdatyearmonth|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
    |2018-12-28 21:33:...|[pete.smith@boxev...|     Jack|  male|   Smith|2018-12-28 21:33:...|68ae46d0-3916-4d9...| customer|           2018-12|
    |2018-12-28 21:33:...|[paul.smith@boxev...|     Paul|  male|   Smith|2018-12-28 21:33:...|ce29f8bf-6c60-421...| customer|           2018-12|
    +--------------------+--------------------+---------+------+--------+--------------------+--------------------+---------+------------------+
