[TOC]

# Overview
For context as a prerequisite to reading this document you read and understand [Analytics - Optima Objectives](https://docs.google.com/document/d/1zfyffpjQ7lCKd0tBhr5dHkJ1DhhOzWN90iXwbkUt_ZM/edit#) first.

We want to produce a Base Data Mart, partitioned correctly in parquet format so that we can connect Explore (Presto via EMR) or Explore (Athena via AWS Glue Catalog & Data Crawler) to the new data set.

To do this we require a Data Pipeline that executes two ETL Jobs: 

* ETL Job 1 - This transforms the Data lake v2 Physical Model into a Data lake v2 Trusted Physical Model and applies data cleaning.
* ETL Job 2 - This transforms the Data lake v2 Trusted Physical Model into a Explore Physical Model and explodes nested objects into separate data sets.

To understand the ETL process, type conversion, internal storage and schema the following document details a step by 
step overview of the ETL job to convert the Data lake v2 (JSON format) to Data lake v2 Trusted (Parquet format) and then
onto the Explore (Parquet format).

See [GuestsDataTransformationExample.scala](../boxever-optima-examples/src/main/scala/com/boxever/labs/optima/examples/OrdersDataTransformationExample.scala) for a working prototype example of the conversions

# Data lake v2 JSON

## JSON Input

    {
    	"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a",
    	"createdAt": 1546032815914,
    	"guestType": "CUSTOMER",
    	"firstName": "Jack",
    	"lastName": "Smith",
    	"gender": "male",
    	"lastSeen": 1546032815914,
    	"emails": ["jack.smith@boxever.com   ", "JACK@boxever.com"]
    }
    
Each directory contains many files and each file is constructed with a flattened json document per line like the following

    {"ref":"68ae46d0-3916-4d93-8b91-21425a351d5a","createdAt":1546032815914,"guestType":"CUSTOMER","firstName":"Jack","lastName":"Smith","gender":"male","lastSeen":1546032815914,"emails":["jack.smith@boxever.com   ", "JACK@boxever.com"]}
    {"ref":"ce29f8bf-6c60-421d-8035-29cadecee40c","createdAt":1546032815914,"guestType":"customer","firstName":"Paul","lastName":"Smith","gender":"male","lastSeen":1546032815914,"emails":["   paul.smith@boxever.com"]}
 
  
## Data lake v2 Schema
Below is the following schema as interpreted by Spark.

    spark.read.json("/samples/guests/").printSchema

    root
     |-- createdAt: long (nullable = true)
     |-- emails: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- firstName: string (nullable = true)
     |-- gender: string (nullable = true)
     |-- guestType: string (nullable = true)
     |-- lastName: string (nullable = true)
     |-- lastSeen: long (nullable = true)
     |-- ref: string (nullable = true)
  
## Querying Data Via Spark

    spark.read.parquet("/samples/v2/guests/").show
    
| createdAt                 | emails                                        | firstName     | gender        | guestType     | lastName      | lastSeen                  | ref                                   |
| ------------------------- | --------------------------------------------- | ------------- | ------------- | ------------- | ------------- | ------------------------- | ------------------------------------- |
| 1546032815914             | [jack.smith@boxever.com   ,JACK@boxever.com]  | Jack          | male          | CUSTOMER      | Smith         | 1546032815914             | 68ae46d0-3916-4d93-8b91-21425a351d5a  |
| 1546032815914             | [   paul.smith@boxever.com]                   | Paul          | male          | customer      | Smith         | 1546032815914             | 68ae46d0-3916-4d93-8b91-21425a351d5a  |
    
  

# Data lake v2 Trusted Schema
After conversion of the Data lake v2 to Data lake v2 Trusted Schema data set.

**Important Things to Note:**
* the guests.parquet files still contain a nested object of emails
* createdAt and lastSeen which are actually dates but were defined as longs in the Data Lake v2 but now are timestamps
* some data has been cleansed e.g email is trimmed of white space and guestType of CUSTOMER is normalized to customer

## Parquet Schema as Interpreted by Spark
Below is the schemas as interpreted by Spark.

### Guests
The guests file is stored in parquet format

    spark.read.parquet("/samples/v2Trusted/guests.parquet/").printSchema

    root
     |-- createdAt: timestamp (nullable = true)
     |-- emails: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- firstName: string (nullable = true)
     |-- gender: string (nullable = true)
     |-- guestType: string (nullable = true)
     |-- lastName: string (nullable = true)
     |-- lastSeen: timestamp (nullable = true)
     |-- ref: string (nullable = true) 
   
## Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli.
 
### Guests

    parquet-tools schema /samples/v2Trusted/guests.parquet/

    message spark_schema {
      optional int96 createdAt;
      optional group emails (LIST) {
        repeated group list {
          optional binary element (UTF8);
        }
      }
      optional binary firstName (UTF8);
      optional binary gender (UTF8);
      optional binary guestType (UTF8);
      optional binary lastName (UTF8);
      optional int96 lastSeen;
      optional binary ref (UTF8);
    }
    
## Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field

    org.apache.spark.sql.parquet.row.metadata
       
### Guests
    
    parquet-tools meta /samples/v2Trusted/guests.parquet/
    
    {
        "type": "struct",
        "fields": [{
            "name": "createdAt",
            "type": "timestamp",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "emails",
            "type": {
                "type": "array",
                "elementType": "string",
                "containsNull": true
            },
            "nullable": true,
            "metadata": {}
        }, {
            "name": "firstName",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "gender",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "guestType",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "lastName",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "lastSeen",
            "type": "timestamp",
            "nullable": true,
            "metadata": {}
        }, {
            "name": "ref",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }]
    }

## Querying Data Via Spark

### Guests

    spark.read.parquet("/samples/v2Trusted/guests.parquet/").show
    
| createdAt                 | emails                                    | firstName     | gender        | guestType     | lastName      | lastSeen                  | ref                                   |
| ------------------------- | ----------------------------------------- | ------------- | ------------- | ------------- | ------------- | ------------------------- | ------------------------------------- |
| 2018-10-18 20:37:20.668   | [jack.smith@boxever.com,jack@boxever.com] | Jack          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   | 68ae46d0-3916-4d93-8b91-21425a351d5a  |
| 2018-10-18 20:37:20.668   | [paul.smith@boxever.com]                  | Paul          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   | 68ae46d0-3916-4d93-8b91-21425a351d5a  |
    
        
# Explore Schema
After conversion of the Data lake v2 Trusted to Explore.

**Important Things to Note:**

* the emails array is exploded out into a separate data set called guest_emails.
* field names are snake_case rather than camelCase 

## Parquet Schema as Interpreted by Spark
Below is the schemas as interpreted by Spark

### Guests
The guests file is stored in parquet format

    spark.read.parquet("/samples/explore/guests.parquet/")

    root
     |-- created_at: timestamp (nullable = true)
     |-- first_name: string (nullable = true)
     |-- gender: string (nullable = true)
     |-- guest_type: string (nullable = true)
     |-- last_name: string (nullable = true)
     |-- last_seen: timestamp (nullable = true)
     |-- ref: string (nullable = true)  
   
### Guests Emails
The guest emails file is stored in parquet format

    spark.read.parquet("/samples/explore/guest_emails.parquet/")

    root
     |-- email: string (nullable = true)  
     |-- guest_ref: string (nullable = true)
  

## Parquet Schema as Interpreted by Parquet Tools
Below is the schemas as interpreted by Parquet Tools cli
 
## Guests

    parquet-tools schema /samples/explore/guests.parquet/

    message spark_schema {
      optional int96 created_at;
      optional binary first_name (UTF8);
      optional binary gender (UTF8);
      optional binary guest_type (UTF8);
      optional binary last_name (UTF8);
      optional int96 last_seen;
      optional binary ref (UTF8);
    }
    
## Guest Emails

    parquet-tools schema /samples/explore/guest_emails.parquet/

    message spark_schema {
      optional binary guest_ref (UTF8);
      optional binary email (UTF8);
    }
 
## Parquet Meta information as Interpreted by Parquet Tools
Below is the meta information as interpreted by Parquet Tools cli. The main data listed below of relevance is the field

    org.apache.spark.sql.parquet.row.metadata
       
### Guests
    
    parquet-tools meta /samples/explore/guests.parquet/
    
    {
    	"type": "struct",
    	"fields": [{
    		"name": "created_at",
    		"type": "timestamp",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "first_name",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "gender",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "guest_type",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "last_name",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "last_seen",
    		"type": "timestamp",
    		"nullable": true,
    		"metadata": {}
    	}, {
            "name": "ref",
            "type": "string",
            "nullable": true,
            "metadata": {}
        }]
    } 

### Guest Emails
    
    parquet-tools meta /samples/explore/guest_emails.parquet/
    
    {
    	"type": "struct",
    	"fields": [{
    		"name": "guest_ref",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}, {
    		"name": "email",
    		"type": "string",
    		"nullable": true,
    		"metadata": {}
    	}]
    } 
  
 
## Hive Create Tables
Below is the command to create the guests and guest_emails tables in Hive

### Guests

    CREATE TABLE `guests`(
      `ref` string,
      `created_at` timestamp,
      `first_name` string,
      `gender` string,
      `guest_type` string,
      `last_name` string,
      `last_seen` timestamp)
  
### Guest Emails

    CREATE TABLE `guests`(
      `guest_ref` string,
      `email` string)
  
  
## Presto Create Tables
Below is the command to create the guests and guest_emails tables in Presto

### Guests

    CREATE TABLE guests (
        ref varchar,
        created_at timestamp,
        first_name varchar,
        gender varchar,
        guest_type varchar,
        last_name varchar,
        last_seen timestamp)

### Guest Emails

    CREATE TABLE guests (
        guest_ref varchar,
        email varchar)


## Querying Data Via Spark

### Guests

    spark.read.parquet("/samples/explore/guests.parquet/").show
    
| ref                                   | created_at                | first_name    | gender        | guest_type    | last_name     | last_seen                 |
| ------------------------------------- | ------------------------- | ------------- | ------------- | ------------- | ------------- | ------------------------- |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | Jack          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | Paul          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |


### Guest Emails

    spark.read.parquet("/samples/explore/guest_emails.parquet/").show
    
| guest_ref                             | email                     |
| ------------------------------------- | ------------------------- |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | jack.smith@boxever.com    |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | jack@boxever.com          |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | paul.smith@boxever.com    |
  

## Querying Data Via Hive or Presto

## Guests

    select * from guests;

| ref                                   | created_at                | first_name    | gender        | guest_type    | last_name     | last_seen                 |
| ------------------------------------- | ------------------------- | ------------- | ------------- | ------------- | ------------- | ------------------------- |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | Jack          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | Paul          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
 
    
## Guest Emails

    select * from guest_emails;
  
| guest_ref                             | email                     |
| ------------------------------------- | ------------------------- |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | jack.smith@boxever.com    |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | jack@boxever.com          |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | paul.smith@boxever.com    |


## Guests and Guest Emails Join

    select 
        g.ref,
        g.created_at,
        ge.email, 
        g.first_name
        g.gender,
        g.guest_type,
        g.last_name,
        g.last_seen
    from 
        guests g, guest_emails ge
    where
        g.ref = ge.guestRef;
        
        
| ref                                   | created_at                | email                     | first_name    | gender        | guest_type    | last_name     | last_seen                 |
| ------------------------------------- | ------------------------- | ------------------------- | ------------- | ------------- | ------------- | ------------- | ------------------------- |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | jack.smith@boxever.com    | Jack          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | jack@boxever.com          | Jack          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
| 68ae46d0-3916-4d93-8b91-21425a351d5a  | 2018-10-18 20:37:20.668   | paul.smith@boxever.com    | Paul          | male          | customer      | Smith         | 2018-10-18 20:37:20.668   |
     