#Parquet Schema Updates issue

##Summary
By default presto will use column indexes to access data in parquet files. This means that when you add columns to the metadata but not in the underlying parquet files (imagine the common case where you only add them to new files), any of the following can happen, depending on the order and type of columns:

* a hard to interpret error (null pointer exception, or some json message)
* garbage data in the new column (if it's a confusion between compatible type columns)
* wrong data (columns are "offset" if all are string for example).

Specifically this causes issues if we generate parquet data in non alphabetically order than the hive schema resulting in errors and corrupt data being returned.

See PRESTO issue here: https://github.com/prestodb/presto/issues/8911

##Workaround

A way around this is to set `hive.parquet_use_column_names` to true via the session

```
show session;
set session hive.parquet_use_column_names = true
show session;
```

Alternativly, the hive connector as per https://github.com/prestodb/presto/blob/master/presto-hive/src/main/java/com/facebook/presto/hive/HiveClientConfig.java#L690 in `/etc/presto/conf/catalog/hive.properties` can be overriden as

```
hive.parquet.use-column-names = true
```

This can be controlled via EMR Classifications using the Explore Agent service https://bitbucket.org/boxever/boxever-labs-explore/src/default/ansible/roles/boxever-explore-agent/templates/boxever-explore-agent/configuration.yml

Also Athena which is based on Presto sets `hive.parquet.use-column-names` so to handle upgrading and different file with different columns https://docs.aws.amazon.com/athena/latest/ug/handling-schema-updates-chapter.html.

##Conclusion
The only real caveat of this approach is you cannot easily rename columns, but the trade off is worth it and is cleaner and easier to understand.