[TOC]

# Running Data Pipeline

Work in Progress

## Overview

The Optima Data Pipeline is run using [AWS Step Functions](https://aws.amazon.com/step-functions/). Upon first launch
of a Step Function you can pass JSON as input, and this input can be passed to tasks. Each task can generate its own output 
which can be appended to or replace objects in the original Input. In the case of Optima this JSON contains configuration
settings that is used to maintain state for the AWS Step Function.

A ETL Job can contain many tasks to fulfill it desired goal. Tasks can be AWS Services such as a AWS Lambda Function 
that kicks off a Glue Job, checks a Glue Job's status, or checks the results of a task to determine success or failure.

Tasks can be chained together or run in parallel allowing for a very sophisticated pipeline of dependencies and recovery 
options.

Current Data Pipeline Lambda Functions

* [startGlueJob.py](../ansible/roles/boxever-optima/files/scripts/aws-lambda/startGlueJob.py)
* [checkGlueJob.py](../ansible/roles/boxever-optima/files/scripts/aws-lambda/checkGlueJob.py)
* [notifyGChat.py](../ansible/roles/boxever-optima/files/scripts/aws-lambda/notifyGChat.py)

## AWS Step Functions Configuration

The Optima Data Pipeline Step Function configuration logic is located in [optima-data-pipeline.json](../ansible/roles/boxever-optima/templates/aws-step-functions/optima-data-pipeline.json).

Upon launching a Step Function you must specify JSON known as the `Optima Data Pipeline Specification`. This specification 
contains a configuration object for each job in the pipeline. This configuration contains all the data required by that 
particular job and may vary per job depending on what it does.

A ETL Job can contain many tasks to fulfill it desired goal, such as starting a AWS Glue Job, checking the Glue Job's 
status and waiting for its to complete.
 
Each task can use the ETL Job's configuration via `InputPath` or update the ETL Job's configuration using `ResultPath` 
post task completion. This ETL Job's configuration is essentially the Jobs settings and state.
 
The advantage of this approach is that the AWS Step Function logic/pipeline is fairly generic and configurable via the 
`Optima Data Pipeline Specification` with the capability to recover a pipelines state based on the last known state of 
the `Optima Data Pipeline Specification` JSON in the case of failure.

## Data Pipeline Input Specification

THe Data Pipeline specification is a JSON configuration that has a grouping of Jobs. Each Job can have one or more fields.
 
### Single Job Input Specification Example

Below is a single specification for a Job that would be passed as Input to the AWS Step Function. 

```json
    {
        "trustedGuestsEtlJob": {
            "jobName": "boxever-optima-spinair-labs-trusted-guests-etl-job",
            "jobArgs": {
                "--enable-metrics": "",
                "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests.yml",
                "--date": "2019/03/02"
            },
            "waitTime": 60,
            "status": "PENDING"
        }
    }
```

The job is `trustedGuestsEtlJob` and this matches the Step Function job.

### Multi Job Input Specification Example

```json
    {
        "trustedGuestsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-guests-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedOrdersEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-orders-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/orders.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        }
    }
```


### Configuration Settings

#### Job Name

The `jobName` refers to the name of the Glue ETL Job to run.

#### Wait Time

The `waitTime` is the time is seconds to wait before checking the job status

#### Available Status Code

The following `status` codes are available: PENDING, RUNNING, JOB_FAILED, SUCCEEDED, SKIPPED
  
#### Job Args

The `config_file` and `date` are args passed to the Glue ETL App

Example

```bash
    --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests.yml --date 2019/03/02
```

# Optima Data Pipeline (Spinair)

## AWS Step Function (Spinair)

Step Function Name: boxever-optima-spinair-labs-data-pipeline
    
## Optima Data Pipeline Input Specification (Spinair)

```json
    {
        "trustedGuestsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-guests-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedOrdersEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-orders-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/orders.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedSessionsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-sessions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/sessions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedEventsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-trusted-events-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/events.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-guests-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/guests.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestEmailsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-guest-emails-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/guest_emails.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestIdentifiersEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-guest-identifiers-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/guest_identifiers.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestSubscriptionsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-guest-subscriptions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/guest_subscriptions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrdersEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-orders-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/orders.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderConsumerOrderItemsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-consumer-order-item-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_consumer_order_items.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderConsumersEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-consumers-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_consumers.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderContactsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-contacts-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_contacts.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemFlightSegmentsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-items-flight-segments-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_item_flight_segments.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemFlightsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-item-flights-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_item_flights.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-order-items-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/order_items.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreSessionsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-sessions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/sessions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreEventsEtlJob": {
          "jobName": "boxever-optima-spinair-labs-explore-events-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-spinair-labs-eu-west-1/config/optima-specs/events.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        }
    }
```
     
# Optima Data Pipeline (Emirates)

## AWS Step Function (Emirates)
     
Step Function Name: boxever-optima-emirates-labs-data-pipeline

## Optima Data Pipeline Input Specification (Emirates)

```json
    {
        "trustedGuestsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-trusted-guests-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-trusted-specs/guests.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedOrdersEtlJob": {
          "jobName": "boxever-optima-emirates-labs-trusted-orders-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-trusted-specs/orders.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedSessionsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-trusted-sessions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-trusted-specs/sessions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "trustedEventsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-trusted-events-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-trusted-specs/events.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-guests-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/guests.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestEmailsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-guest-emails-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/guest_emails.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestIdentifiersEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-guest-identifiers-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/guest_identifiers.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreGuestSubscriptionsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-guest-subscriptions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/guest_subscriptions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrdersEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-orders-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/orders.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderConsumerOrderItemsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-consumer-order-item-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_consumer_order_items.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderConsumersEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-consumers-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_consumers.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderContactsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-contacts-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_contacts.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemFlightSegmentsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-items-flight-segments-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_item_flight_segments.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemFlightsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-item-flights-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_item_flights.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreOrderItemsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-order-items-etl-job",
          "jobArgs": {
            "--enable-metrics": "",
            "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/order_items.yml",
            "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreSessionsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-sessions-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/sessions.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        },
        "exploreEventsEtlJob": {
          "jobName": "boxever-optima-emirates-labs-explore-events-etl-job",
          "jobArgs": {
              "--enable-metrics": "",
              "--config_file": "s3://boxever-optima-emirates-labs-eu-west-1/config/optima-specs/events.yml",
              "--date": "2019/03/02"
          },
          "waitTime": 60,
          "status": "PENDING"
        }
    }
```
