const now = () => new Date().toISOString()

const isStepFunctionFailure = event => event.status !== "SUCCESS" && event.stateMachineArn !== undefined
const isJobFailure          = event => event.status !== "SUCCESS" && event.jobName !== undefined
const isDatabaseFailure     = event => event.status !== "SUCCESS" && event.database !== undefined
const isExportJobFailure    = event => event.status !== "SUCCESS" && event.type === "Export"
const isAlarm               = event => event.AlarmName !== undefined

const normalizeStepFunctionFailure = event => {
    const stepMachineName = event => event.stateMachineArn ? event.stateMachineArn.split(':')[6] : undefined
    const region          = event => event.stateMachineArn ? event.stateMachineArn.split(':')[3] : undefined
    const executionURL    = event => `https://${ region(event) }.console.aws.amazon.com/states/home?region=${ region(event) }#/executions/details/${ event.executionArn }`

    return {
        status:             event.status,
        start_date:         event.startDate,
        timestamp:          now(),
        step_machine:       stepMachineName(event),
        step_machine_arn:   event.stateMachineArn,
        execution_arn:      event.executionArn,
        step_machine_input: event.stateMachineInput,
        execution:          executionURL(event)
    }
}

const normalizeJobFailure = event => {
    const customer     = event => event.jobName ? event.jobName.split('-')[2] : ""
    const executionURL = event => `https://${ region(event) }.console.aws.amazon.com/glue/home?region=${ region(event) }#jobRun:jobName=${ event.jobName };jobRunId=${ event.jobId }`
    const configURL    = event => `https://s3.console.aws.amazon.com/s3/object/${ event.jobArgs['--config_file'].substring(5) }`
    const region       = event => {
        let temp = event.jobArgs['--config_file'].split('-')
        return temp[4] + '-' + temp[5] + '-' + temp[6].substring(0, temp[6].indexOf('/'))
    }

    return {
        status:      event.status,
        job_name:    event.jobName,
        client:      customer(event),
        timestamp:   now(),
        job_info: {
            id:          event.jobId,
            execution:   executionURL(event),
            config:      configURL(event),
            date:        event.jobArgs['--date'],
            day_of_week: event.jobArgs['--day_of_the_week']
        }
    }
}

const normalizeAlarm = event => {
    const dimension      = (event, name) => event.Trigger.Dimensions.find( dimension => dimension.name == name) ? event.Trigger.Dimensions.find( dimension => dimension.name == name).value : undefined
    const stepMachineARN = event => dimension(event, "StateMachineArn")
    const region         = event => stepMachineARN(event) ? stepMachineARN(event).split(':')[3] : undefined
    const executionURL   = event => `https://${ region(event) }.console.aws.amazon.com/states/home?region=${ region(event) }#/executions/details/${ stepMachineARN(event) }`

    return {
        status:       event.NewStateValue,
        start_date:   event.StateChangeTime,
        timestamp:    now(),
        executionURL: executionURL(event),
        alarm: {
            name:        event.AlarmName,
            description: event.AlarmDescription
        }
    }
}

const normalizeDatabase = event => {
    const customer = event => event.database ? event.database.split('_')[0] : ""
    const table    = event => {
        let ddl = event.script.substring(event.script.lastIndexOf("/") + 1)
        return ddl.substring(0, ddl.lastIndexOf("_table.ddl"))
    }

    return {
        status:      event.status,
        database:    event.database,
        table:       table(event),
        client:      customer(event),
        timestamp:   now(),
        description: "Error running export, log to EMR Cluster and troubleshoot"
    }
}

const normalizeExport = event => {
    return {
        status:      event.status,
        timestamp:   now(),
        description: "Error running export, log to EMR Cluster and troubleshoot"
    }
}

const normalizeUnknownFailure = event => ({
    timestamp: now(),
    raw_body:  event
})

const describe = event => {
    if (isStepFunctionFailure(event))   return `Step Function ${ event.stateMachineArn.split(':')[6] }, status: ${ event.status }`
    else if (isJobFailure(event))       return `Job ${ event.jobName }, status: ${ event.status }`
    else if (isDatabaseFailure(event))  return `Database ${ event.database }, status: ${ event.status }`
    else if (isExportJobFailure(event)) return `Export status: ${ event.status } ${ event.type } ${event.msg}`
    else if (isAlarm(event))            return `${ event.AlarmName } ${event.Trigger.MetricName}`
    else                                return "Unknown Failure"
}

const normalize = event => {
    if (isStepFunctionFailure(event))   return normalizeStepFunctionFailure(event)
    else if (isJobFailure(event))       return normalizeJobFailure(event)
    else if (isDatabaseFailure(event))  return normalizeDatabase(event)
    else if (isExportJobFailure(event)) return normalizeExport(event)
    else if (isAlarm(event))            return normalizeAlarm(event)
    else                                return normalizeUnknownFailure(event)
}

const toPagerDutyEvent = event => ({
    event_type:  PD.Trigger,
    description: describe(event),
    details:     normalize(event)
})

PD.emitGenericEvents( [toPagerDutyEvent(JSON.parse(PD.inputRequest.rawBody))] )
