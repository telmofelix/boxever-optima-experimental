# Pager Duty Optima Service Customer Event Transformer

This folder contains the **Pager Duty Optima Service [Customer Event Transformer (CET)](https://v2.developer.pagerduty.com/docs/cet)** source code.

This document is not a detailed documentation, only the minimal to update the CET code. The code is in the [./optima-failure-pagerduty-cet.js](optima-failure-pagerduty-cet.js) file. 

## How to update the CET

Steps:

1. Login on [Pager Duty](https://boxever.pagerduty.com/) and go to **Configuration** > **Services** and search for 
**Optima** service and click on **Optima Cloudwatch Transformer**.  

![Go to Optima CloudWatch Trasnformer code](../docs/images/optima-pager-duty-cet-1.png)

2. Click on **Edit Integration**

![Edit Optima CloudWatch Trasnformer code](../docs/images/optima-pager-duty-cet-2.png)

3. Change **the code you want to execute** with the content you want and click **Save changes**.

![Change Optima CloudWatch Trasnformer code](../docs/images/optima-pager-duty-cet-3.png)
 