import unittest

#Handle Python 2 vs 3
try:
    from unittest import mock
except ImportError:
    import mock

class TestBXDataCatalog(unittest.TestCase):

    json = {
        "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions",
        "items": [
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/d51dbbf2-d3b1-4e9c-ac0a-0af6ddc2ae68",
                "ref": "d51dbbf2-d3b1-4e9c-ac0a-0af6ddc2ae68",
                "name": "guests",
                "type": "object",
                "uiLabel": "Guests",
                "dbTableName": "guests",
                "entityType": "Guest",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "city",
                        "type": "string",
                        "uiLabel": "City",
                        "dbColumnName": "city"
                    },
                    {
                        "name": "country",
                        "type": "string",
                        "uiLabel": "Country",
                        "dbColumnName": "country"
                    },
                    {
                        "name": "dateOfBirth",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Data Of Birth",
                        "dbColumnName": "date_of_birth"
                    },
                    {
                        "name": "email",
                        "type": "string",
                        "uiLabel": "Email",
                        "dbColumnName": "email"
                    },
                    {
                        "name": "firstName",
                        "type": "string",
                        "uiLabel": "First Name",
                        "dbColumnName": "first_name"
                    },
                    {
                        "name": "firstSeen",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "First Seen",
                        "dbColumnName": "first_seen"
                    },
                    {
                        "name": "gender",
                        "type": "string",
                        "uiLabel": "Gender",
                        "dbColumnName": "gender"
                    },
                    {
                        "name": "guestType",
                        "type": "string",
                        "uiLabel": "Guest Type",
                        "dbColumnName": "guest_type",
                        "enum": [
                            "visitor",
                            "customer",
                            "traveller",
                            "retired"
                        ]
                    },
                    {
                        "name": "language",
                        "type": "string",
                        "uiLabel": "Language",
                        "dbColumnName": "language"
                    },
                    {
                        "name": "lastName",
                        "type": "string",
                        "uiLabel": "Last Name",
                        "dbColumnName": "last_name"
                    },
                    {
                        "name": "lastSeen",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Last Seen",
                        "dbColumnName": "last_seen"
                    },
                    {
                        "name": "nationality",
                        "type": "string",
                        "uiLabel": "Nationality",
                        "dbColumnName": "nationality"
                    },
                    {
                        "name": "passportExpiry",
                        "type": "string",
                        "uiLabel": "Passport Expiry",
                        "dbColumnName": "passport_expiry"
                    },
                    {
                        "name": "passportNumber",
                        "type": "string",
                        "uiLabel": "Passport Number",
                        "dbColumnName": "passport_number"
                    },
                    {
                        "name": "postCode",
                        "type": "string",
                        "uiLabel": "Post Code",
                        "dbColumnName": "post_code"
                    },
                    {
                        "name": "state",
                        "type": "string",
                        "uiLabel": "State",
                        "dbColumnName": "state"
                    },
                    {
                        "name": "street",
                        "type": "array",
                        "uiLabel": "Street",
                        "dbTableName": "street",
                        "dbTableAsView": True,
                        "items": {
                            "type": "string",
                            "uiLabel": "Street",
                            "dbColumnName": "street"
                        }
                    },
                    {
                        "name": "title",
                        "type": "string",
                        "uiLabel": "Title",
                        "dbColumnName": "title"
                    },
                    {
                        "name": "dataExtensions",
                        "type": "array",
                        "uiLabel": "Data Extensions",
                        "items": {
                            "type": "object",
                            "$ref": "#preferences"
                        }
                    },
                    {
                        "name": "identifiers",
                        "type": "array",
                        "uiLabel": "Identifiers",
                        "dbTableName": "identifiers",
                        "items": {
                            "type": "object",
                            "$ref": "#identifiers"
                        }
                    },
                    {
                        "name": "subscriptions",
                        "type": "array",
                        "uiLabel": "Subscriptions",
                        "dbTableName": "subscriptions",
                        "items": {
                            "type": "object",
                            "$ref": "#subscriptions"
                        }
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/45bc00a7-e321-4865-b5cb-0ef9dfd86ae6",
                "ref": "45bc00a7-e321-4865-b5cb-0ef9dfd86ae6",
                "name": "identifiers",
                "type": "object",
                "uiLabel": "Guest Identifiers",
                "dbTableName": "guest_identifiers",
                "entityType": "GuestIdentifier",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "id",
                        "type": "string",
                        "uiLabel": "Id",
                        "dbColumnName": "id",
                        "required": True
                    },
                    {
                        "name": "provider",
                        "type": "string",
                        "uiLabel": "Provider",
                        "dbColumnName": "provider",
                        "required": True
                    },
                    {
                        "name": "expiryDate",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Expiry Date",
                        "dbColumnName": "expiry_date"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/8137a96c-f712-488f-a832-0adfa94a51ff",
                "ref": "8137a96c-f712-488f-a832-0adfa94a51ff",
                "name": "subscriptions",
                "type": "object",
                "uiLabel": "Guest Subscriptions",
                "dbTableName": "guest_subscriptions",
                "entityType": "GuestSubscription",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "channel",
                        "type": "string",
                        "uiLabel": "Channel",
                        "dbColumnName": "channel",
                        "required": True
                    },
                    {
                        "name": "name",
                        "type": "string",
                        "uiLabel": "Name",
                        "dbColumnName": "name",
                        "required": True
                    },
                    {
                        "name": "pointOfSale",
                        "type": "string",
                        "uiLabel": "Point Of Sale",
                        "dbColumnName": "point_of_sale"
                    },
                    {
                        "name": "status",
                        "type": "string",
                        "uiLabel": "Status",
                        "dbColumnName": "status"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/543fb533-b3b3-4567-88d0-a8884c5e77d4",
                "ref": "543fb533-b3b3-4567-88d0-a8884c5e77d4",
                "name": "preferences",
                "key": "interest",
                "type": "object",
                "uiLabel": "Guest Preferences",
                "dbTableName": "guest_preferences",
                "entityType": "GuestDataExtension",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "newspaper",
                        "type": "string",
                        "uiLabel": "Newspaper",
                        "dbColumnName": "newspaper",
                        "default": "N",
                        "minLength": 1,
                        "maxLength": 1
                    },
                    {
                        "name": "soccer",
                        "type": "string",
                        "uiLabel": "Soccer",
                        "dbColumnName": "soccer",
                        "default": "N"
                    },
                    {
                        "name": "horseRacing",
                        "type": "string",
                        "uiLabel": "Horse Racing",
                        "dbColumnName": "horseRacing",
                        "default": "N"
                    },
                    {
                        "name": "ekEhspof",
                        "type": "string",
                        "uiLabel": "EK Ehspof",
                        "dbColumnName": "ek_ehspof",
                        "default": "N"
                    },
                    {
                        "name": "art",
                        "type": "string",
                        "uiLabel": "Art",
                        "dbColumnName": "art",
                        "default": "N"
                    },
                    {
                        "name": "other",
                        "type": "string",
                        "uiLabel": "Other",
                        "dbColumnName": "other",
                        "default": "N"
                    },
                    {
                        "name": "rugby",
                        "type": "string",
                        "uiLabel": "Rugby",
                        "dbColumnName": "rugby",
                        "default": "N"
                    },
                    {
                        "name": "watersports",
                        "type": "string",
                        "uiLabel": "Water Sports",
                        "dbColumnName": "water_sports",
                        "default": "N"
                    },
                    {
                        "name": "selfReportedFlag",
                        "type": "string",
                        "uiLabel": "Self Reported Flag",
                        "dbColumnName": "self_reported_flag",
                        "default": "N"
                    },
                    {
                        "name": "motorsports",
                        "type": "string",
                        "uiLabel": "Motor Sports",
                        "dbColumnName": "motor_sports",
                        "default": "N"
                    },
                    {
                        "name": "tennis",
                        "type": "string",
                        "uiLabel": "Tennis",
                        "dbColumnName": "tennis",
                        "default": "N"
                    },
                    {
                        "name": "theatre",
                        "type": "string",
                        "uiLabel": "Theatre",
                        "dbColumnName": "theatre",
                        "default": "N"
                    },
                    {
                        "name": "basketball",
                        "type": "string",
                        "uiLabel": "Basketball",
                        "dbColumnName": "basketball",
                        "default": "N"
                    },
                    {
                        "name": "golf",
                        "type": "string",
                        "uiLabel": "Golf",
                        "dbColumnName": "golf",
                        "default": "N"
                    },
                    {
                        "name": "music",
                        "type": "string",
                        "uiLabel": "Music",
                        "dbColumnName": "music",
                        "default": "N"
                    },
                    {
                        "name": "cricket",
                        "type": "string",
                        "uiLabel": "Cricket",
                        "dbColumnName": "cricket",
                        "default": "N"
                    },
                    {
                        "name": "americanFootball",
                        "type": "string",
                        "uiLabel": "American Football",
                        "dbColumnName": "american_football",
                        "default": "N"
                    },
                    {
                        "name": "baseball",
                        "type": "string",
                        "uiLabel": "Baseball",
                        "dbColumnName": "baseball",
                        "default": "N"
                    },
                    {
                        "name": "ekEhsoff",
                        "type": "string",
                        "uiLabel": "EK Ehsoff",
                        "dbColumnName": "ek_ehsoff",
                        "default": "N"
                    },
                    {
                        "name": "perLanguage",
                        "type": "string",
                        "uiLabel": "Pre Language",
                        "dbColumnName": "per_language"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/4ee1d5fe-d943-4dba-baba-894f6c9f8970",
                "ref": "4ee1d5fe-d943-4dba-baba-894f6c9f8970",
                "name": "orders",
                "type": "object",
                "uiLabel": "Orders",
                "dbTableName": "orders",
                "entityType": "Order",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "orderedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Ordered At",
                        "dbColumnName": "ordered_at"
                    },
                    {
                        "name": "channel",
                        "type": "string",
                        "uiLabel": "Channel",
                        "dbColumnName": "channel",
                        "enum": [
                            "WEB",
                            "AGENT",
                            "ATM",
                            "SERVICE_ISSUE",
                            "CALL_CENTER",
                            "EMAIL",
                            "WEB",
                            "MOBILE",
                            "MOBILE_WEB",
                            "MOBILE_APP",
                            "AIRPORT_KIOSK",
                            "DATA_SYNC",
                            "OTHER"
                        ]
                    },
                    {
                        "name": "cardType",
                        "type": "string",
                        "uiLabel": "Card Type",
                        "dbColumnName": "card_type"
                    },
                    {
                        "name": "paymentType",
                        "type": "string",
                        "uiLabel": "Payment Type",
                        "dbColumnName": "payment_type"
                    },
                    {
                        "name": "price",
                        "type": "string",
                        "uiLabel": "Payment Type",
                        "dbColumnName": "price"
                    },
                    {
                        "name": "referenceId",
                        "type": "string",
                        "uiLabel": "Reference Id",
                        "dbColumnName": "reference_id"
                    },
                    {
                        "name": "status",
                        "type": "string",
                        "uiLabel": "Status",
                        "dbColumnName": "status"
                    },
                    {
                        "name": "currencyCode",
                        "type": "string",
                        "uiLabel": "Currency Code",
                        "dbColumnName": "currency_code"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/4ee1d5fe-d943-4dba-baba-894f6c9f8970",
                "ref": "4ee1d5fe-d943-4dba-baba-894f6c9f8970",
                "name": "orders",
                "type": "object",
                "uiLabel": "Orders",
                "dbTableName": "orders",
                "entityType": "Order",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "orderedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Ordered At",
                        "dbColumnName": "ordered_at"
                    },
                    {
                        "name": "channel",
                        "type": "string",
                        "uiLabel": "Channel",
                        "dbColumnName": "channel",
                        "enum": [
                            "WEB",
                            "AGENT",
                            "ATM",
                            "SERVICE_ISSUE",
                            "CALL_CENTER",
                            "EMAIL",
                            "WEB",
                            "MOBILE",
                            "MOBILE_WEB",
                            "MOBILE_APP",
                            "AIRPORT_KIOSK",
                            "DATA_SYNC",
                            "OTHER"
                        ]
                    },
                    {
                        "name": "cardType",
                        "type": "string",
                        "uiLabel": "Card Type",
                        "dbColumnName": "card_type"
                    },
                    {
                        "name": "paymentType",
                        "type": "string",
                        "uiLabel": "Payment Type",
                        "dbColumnName": "payment_type"
                    },
                    {
                        "name": "price",
                        "type": "string",
                        "uiLabel": "Payment Type",
                        "dbColumnName": "price"
                    },
                    {
                        "name": "referenceId",
                        "type": "string",
                        "uiLabel": "Reference Id",
                        "dbColumnName": "reference_id"
                    },
                    {
                        "name": "status",
                        "type": "string",
                        "uiLabel": "Status",
                        "dbColumnName": "status"
                    },
                    {
                        "name": "currencyCode",
                        "type": "string",
                        "uiLabel": "Currency Code",
                        "dbColumnName": "currency_code"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/4b7cbf20-b931-420f-8c30-75974de1d4c4",
                "ref": "4b7cbf20-b931-420f-8c30-75974de1d4c4",
                "name": "order_item_flights",
                "type": "object",
                "uiLabel": "Order Item Flights",
                "dbTableName": "order_item_flights",
                "entityType": "OrderItem",
                "subType": "FLIGHT",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/9da1b239-3321-4d44-87e6-c92794266b5d",
                "ref": "9da1b239-3321-4d44-87e6-c92794266b5d",
                "name": "events_view",
                "type": "object",
                "uiLabel": "Events View",
                "dbTableName": "events_view",
                "entityType": "Event",
                "subType": "VIEW",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "browserRef",
                        "type": "string",
                        "uiLabel": "Browser Ref",
                        "dbColumnName": "browser_ref"
                    },
                    {
                        "name": "sessionRef",
                        "type": "string",
                        "uiLabel": "SessionRef",
                        "dbColumnName": "session_ref"
                    },
                    {
                        "name": "type",
                        "type": "string",
                        "uiLabel": "Type",
                        "dbColumnName": "type"
                    },
                    {
                        "name": "channel",
                        "type": "string",
                        "uiLabel": "Channel",
                        "dbColumnName": "channel"
                    },
                    {
                        "name": "currency",
                        "type": "string",
                        "uiLabel": "Currency",
                        "dbColumnName": "currency"
                    },
                    {
                        "name": "language",
                        "type": "string",
                        "uiLabel": "Language",
                        "dbColumnName": "language"
                    },
                    {
                        "name": "pointOfSale",
                        "type": "string",
                        "uiLabel": "Point Of Sale",
                        "dbColumnName": "point_of_sale"
                    },
                    {
                        "name": "page",
                        "type": "string",
                        "uiLabel": "Page",
                        "dbColumnName": "page"
                    }
                ]
            },
            {
                "href": "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions/d27af8da-df0d-4f6f-b258-fc180f40ab0b",
                "ref": "d27af8da-df0d-4f6f-b258-fc180f40ab0b",
                "name": "events_search",
                "type": "object",
                "uiLabel": "Events Search",
                "dbTableName": "events_search",
                "entityType": "Event",
                "subType": "SEARCH",
                "apiOmitNullValuesInResponse": True,
                "properties": [
                    {
                        "name": "createdAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Created At",
                        "dbColumnName": "created_at"
                    },
                    {
                        "name": "modifiedAt",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Modified At",
                        "dbColumnName": "modified_at"
                    },
                    {
                        "name": "ref",
                        "type": "string",
                        "format": "uuid",
                        "uiLabel": "Ref",
                        "dbColumnName": "ref"
                    },
                    {
                        "name": "browserRef",
                        "type": "string",
                        "uiLabel": "Browser Ref",
                        "dbColumnName": "browser_ref"
                    },
                    {
                        "name": "sessionRef",
                        "type": "string",
                        "uiLabel": "SessionRef",
                        "dbColumnName": "session_ref"
                    },
                    {
                        "name": "type",
                        "type": "string",
                        "uiLabel": "Type",
                        "dbColumnName": "type"
                    },
                    {
                        "name": "channel",
                        "type": "string",
                        "uiLabel": "Channel",
                        "dbColumnName": "channel"
                    },
                    {
                        "name": "currency",
                        "type": "string",
                        "uiLabel": "Currency",
                        "dbColumnName": "currency"
                    },
                    {
                        "name": "language",
                        "type": "string",
                        "uiLabel": "Language",
                        "dbColumnName": "language"
                    },
                    {
                        "name": "pointOfSale",
                        "type": "string",
                        "uiLabel": "Point Of Sale",
                        "dbColumnName": "point_of_sale"
                    },
                    {
                        "name": "page",
                        "type": "string",
                        "uiLabel": "Page",
                        "dbColumnName": "page"
                    },
                    {
                        "name": "productType",
                        "type": "string",
                        "uiLabel": "Product Type",
                        "dbColumnName": "product_type"
                    },
                    {
                        "name": "productName",
                        "type": "string",
                        "uiLabel": "Product Name",
                        "dbColumnName": "product_name"
                    },
                    {
                        "name": "flightType",
                        "type": "string",
                        "uiLabel": "Flight Type",
                        "dbColumnName": "flight_type"
                    },
                    {
                        "name": "origin",
                        "type": "string",
                        "uiLabel": "Origin",
                        "dbColumnName": "origin"
                    },
                    {
                        "name": "destination",
                        "type": "string",
                        "uiLabel": "Destination",
                        "dbColumnName": "destination"
                    },
                    {
                        "name": "startDate",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "Start Date",
                        "dbColumnName": "start_date"
                    },
                    {
                        "name": "endDate",
                        "type": "string",
                        "format": "date-time",
                        "uiLabel": "End Date",
                        "dbColumnName": "end_date"
                    },
                    {
                        "name": "fareClass",
                        "type": "string",
                        "uiLabel": "Fare Class",
                        "dbColumnName": "fare_class"
                    },
                    {
                        "name": "fareFamily",
                        "type": "string",
                        "uiLabel": "Fare Family",
                        "dbColumnName": "fare_family"
                    },
                    {
                        "name": "adults",
                        "type": "integer",
                        "uiLabel": "Adults",
                        "dbColumnName": "adults"
                    },
                    {
                        "name": "children",
                        "type": "integer",
                        "uiLabel": "Children",
                        "dbColumnName": "children"
                    },
                    {
                        "name": "infants",
                        "type": "integer",
                        "uiLabel": "Infants",
                        "dbColumnName": "infants"
                    }
                ]
            }
        ]
    }

    def test_process_request(self):
        print("Testing process_request...")
        from ansible_modules.boxever.bx_data_catalog import process_request
        data = process_request(self.json, 'complex')
        self.assertEqual(data[0]['source']['name'], "dataExtensions", "Checking first field mapping name...")
        self.assertEqual(data[0]['source']['type'], "complex", "Checking first field mapping type...")
        self.assertEqual(data[0]['destination']['name'], "dataExtensions")
        self.assertEqual(data[0]['destination']['mappings'][0]['source']['name'], "createdAt")
        self.assertEqual(data[0]['destination']['mappings'][0]['source']['type'], "string")
        self.assertEqual(data[0]['destination']['mappings'][0]['destination']['name'], "createdAt")
        self.assertEqual(data[0]['destination']['mappings'][0]['destination']['type'], "timestampInt64")
        self.assertEqual(data[0]['destination']['mappings'][0]['transformations'][0]['name'], "setLongIfNull")
        self.assertEqual(data[0]['destination']['mappings'][0]['transformations'][0]['params'][0], 0)
        self.assertEqual(data[0]['destination']['mappings'][0]['transformations'][1]['name'], "castLongToTimestampInt64")
        self.assertEqual(data[0]['destination']['mappings'][5]['destination']['mappings'][0]['validations'][0]['name'], "minLength")
        self.assertEqual(data[0]['destination']['mappings'][5]['destination']['mappings'][0]['validations'][0]['params'][0], 1)
        self.assertEqual(data[0]['destination']['mappings'][5]['destination']['mappings'][0]['validations'][1]['name'], "maxLength")
        self.assertEqual(data[0]['destination']['mappings'][5]['destination']['mappings'][0]['validations'][1]['params'][0], 1)

    def test_write_spec(self):
        print("Testing write_spec...")
        from ansible_modules.boxever.bx_data_catalog import process_request
        data = process_request(self.json, 'complex')
        from ansible_modules.boxever.bx_data_catalog import write_spec
        with mock.patch('os.open'):
            with mock.patch('os.remove'):
                with mock.patch('os.path.exists'):
                    write_spec("/tmp/out", data, "  ")

    def test_generate_minimum_validation(self):
        print("Testing generate_minimum_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_minimum_validation
        actual = generate_minimum_validation(5)
        self.assertEqual(actual['name'], "minimum")
        self.assertEqual(actual['params'][0], 5)

    def test_generate_maximum_validation(self):
        print("Testing generate_maximum_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_maximum_validation
        actual = generate_maximum_validation(5)
        self.assertEqual(actual['name'], "maximum")
        self.assertEqual(actual['params'][0], 5)

    def test_generate_min_length_validation(self):
        print("Testing generate_min_length_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_min_length_validation
        actual = generate_min_length_validation(5)
        self.assertEqual(actual['name'], "minLength")
        self.assertEqual(actual['params'][0], 5)

    def test_generate_max_length_validation(self):
        print("Testing generate_max_length_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_max_length_validation
        actual = generate_max_length_validation(5)
        self.assertEqual(actual['name'], "maxLength")
        self.assertEqual(actual['params'][0], 5)

    def test_generate_date_mapping(self):
        print("Testing generate_date_mapping")
        from ansible_modules.boxever.bx_data_catalog import generate_date_mapping
        name = "createdAt"
        db_name = "created_at"
        actual = generate_date_mapping(name, db_name)
        self.assertEqual(actual['source']['name'], "createdAt")
        self.assertEqual(actual['destination']['name'], "created_at")
        self.assertEqual(actual['transformations'][0]['name'], "setLongIfNull")
        self.assertEqual(actual['transformations'][0]['params'][0], 0)
        self.assertEqual(actual['transformations'][1]['name'], "castLongToTimestampInt64")

    def test_generate_date_transformations(self):
        print("Testing generate_date_transformations")
        from ansible_modules.boxever.bx_data_catalog import generate_date_transformations
        actual = generate_date_transformations()
        self.assertEqual(actual[0]['name'], "setLongIfNull")
        self.assertEqual(actual[0]['params'][0], 0)
        self.assertEqual(actual[1]['name'], "castLongToTimestampInt64")

    def test_generate_set_long_if_null_transformation(self):
        print("Testing generate_set_long_if_null_transformation")
        from ansible_modules.boxever.bx_data_catalog import generate_set_long_if_null_transformation
        actual = generate_set_long_if_null_transformation()
        self.assertEqual(actual['name'], "setLongIfNull")
        self.assertEqual(actual['params'][0], 0)

    def test_generate_cast_long_to_timestamp_int_64_transformation(self):
        print("Testing generate_cast_long_to_timestamp_int_64_transformation")
        from ansible_modules.boxever.bx_data_catalog import generate_cast_long_to_timestamp_int_64_transformation
        actual = generate_cast_long_to_timestamp_int_64_transformation()
        self.assertEqual(actual['name'], "castLongToTimestampInt64")

    def test_generate_cast_string_to_timestamp_int_64_transformation(self):
        print("Testing generate_cast_string_to_timestamp_int_64_transformation")
        from ansible_modules.boxever.bx_data_catalog import generate_cast_string_to_timestamp_int_64_transformation
        actual = generate_cast_string_to_timestamp_int_64_transformation()
        self.assertEqual(actual['name'], "castStringToTimestampInt64")

    def test_generate_cast_string_to_date_transformation(self):
        print("Testing generate_cast_string_to_date_transformation")
        from ansible_modules.boxever.bx_data_catalog import generate_cast_string_to_date_transformation
        actual = generate_cast_string_to_date_transformation()
        self.assertEqual(actual['name'], "castStringToDateWithPattern")
        self.assertEqual(actual['params'][0], "yyyy-MM-dd")

    def test_generate_set_string_if_null(self):
        print("Testing generate_set_string_if_null")
        from ansible_modules.boxever.bx_data_catalog import generate_set_string_if_null
        default_value = "0"
        actual = generate_set_string_if_null(default_value)
        self.assertEqual(actual['name'], "setStringIfNull")
        self.assertEqual(actual['params'][0], "0")

    def test_generate_matches_uuid_pattern_validation(self):
        print("Testing generate_matches_uuid_pattern_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_matches_uuid_pattern_validation
        actual = generate_matches_uuid_pattern_validation()
        self.assertEqual(actual['name'], "matchesPattern")
        self.assertEqual(actual['params'][0], "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")

    def test_generate_uuid_validation(self):
        print("Testing generate_uuid_validation")
        from ansible_modules.boxever.bx_data_catalog import generate_uuid_validation
        actual = generate_uuid_validation()
        self.assertEqual(actual['name'], "isUUID")

    def test_get_field_type(self):
        print("Testing get_field_type")
        from ansible_modules.boxever.bx_data_catalog import get_field_type
        field_type = "number"
        field_format = "int32"
        actual = get_field_type(field_type, field_format)
        self.assertEqual(actual, "integer")

        field_format = "int64"
        actual = get_field_type(field_type, field_format)
        self.assertEqual(actual, "long")

        field_format = "float"
        actual = get_field_type(field_type, field_format)
        self.assertEqual(actual, "float")

        field_format = "double"
        actual = get_field_type(field_type, field_format)
        self.assertEqual(actual, "double")

        field_type = "string"
        field_format = None
        actual = get_field_type(field_type, field_format)
        self.assertEqual(actual, "string")

    def test_basic_test(self):
        print("Testing...")
        self.assertEqual(1, 1, "1 equals 1")


if __name__ == '__main__':
    unittest.main()
