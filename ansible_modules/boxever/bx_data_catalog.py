#!/usr/bin/python

# Copyright: (c) 2019, Alan Giles (alan.giles@boxever.com)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: bx_data_catalog
short_description: Retrieve and generate optima spec mapping from data catalog service
description:
    -  Retrieve and generate optima spec mapping from data catalog service
version_added: "2.7"
requirements: [ boto3 ]
author: "Alan Giles (alan.giles@boxever.com)"
options:
  name:
    description:
      - The name of the Boxever tenant.
    required: true
  client_key:
    description:
      - The client_key of the Boxever tenant.
    required: true
  url:
    description:
      - The url of the Data Catalog API.
    required: true
  entity_type:
    description:
      - The entity_type of model in the Data Catalog API.
    required: true
  dest:
    description:
      - The destination location of optima mappings.
    required: true
  padding:
    description:
      - The padding to add to each line of the dest.
  mapper:
    description: 
      -The mapper that will be used.

'''

EXAMPLES = '''
# Note: These examples do not set authentication details, see the AWS Guide for details.

# Generate Data Extension Mappings from Data Catalog Service
- bx_data_catalog:
    name: spinair
    client_key: wjtc2eog1lvueo72kts3mn1ean0nentz
    url:  "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions"
    entity_type: "GuestDataExtension"
    dest: "tmp-templates/wjtc2eog1lvueo72kts3mn1ean0nentz/optima-data-catalog/data_extensions.yml"
    padding: "  "
    mapper: "complex"

'''

RETURN = '''
name:
    description: The name assigned to this job definition.
    returned: when state is present
    type: string
    sample: spinair
client_key:
    description: The client_key of the Boxever tenant.
    type: string
    sample: wjtc2eog1lvueo72kts3mn1ean0nentz
entity_type:
    description: The entity_type of model in the Data Catalog API.
    type: string
    sample: GuestDataExtension
dest:
    description: The destination location of optima mappings.
    type: string
    sample: tmp-templates/wjtc2eog1lvueo72kts3mn1ean0nentz/optima-data-catalog/data_extensions.yml
padding:
    description: The padding to add to each line of the dest.
    type: string
    sample: "  "
mapper:
    description: The mapper that will be used.
    type: string
    sample: "complex"
'''

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url, url_argument_spec

# Non-ansible imports
import os
import json
import yaml
from yaml import dump, load, safe_dump


def represent_none(self, _):
    """
    This is needed to prevent PyYAML outputting empty string ''
    """
    return self.represent_scalar('tag:yaml.org,2002:null', '')


yaml.add_representer(type(None), represent_none)


def generate_date_mapping(name, db_name):

    set_long_if_null_transformation = dict()
    set_long_if_null_transformation['name'] = 'setLongIfNull'
    set_long_if_null_transformation['params'] = [0]

    set_cast_long_to_timestamp_int_64 = dict()
    set_cast_long_to_timestamp_int_64['name'] = 'castLongToTimestampInt64'

    date_mapping = dict()
    date_mapping['source'] = dict()
    date_mapping['source']['name'] = name
    date_mapping['source']['type'] = 'long'

    date_mapping['destination'] = dict()
    date_mapping['destination']['name'] = db_name
    date_mapping['destination']['type'] = 'timestampInt64'

    date_mapping['transformations'] = list()
    date_mapping['transformations'].append(set_long_if_null_transformation)
    date_mapping['transformations'].append(set_cast_long_to_timestamp_int_64)

    return date_mapping


def generate_date_transformations():

    date_transformations = []

    set_long_if_null_transformation = dict()
    set_long_if_null_transformation['name'] = 'setLongIfNull'
    set_long_if_null_transformation['params'] = [0]

    date_transformations.append(set_long_if_null_transformation)

    set_cast_long_to_timestamp_int_64 = dict()
    set_cast_long_to_timestamp_int_64['name'] = 'castLongToTimestampInt64'

    date_transformations.append(set_cast_long_to_timestamp_int_64)

    return date_transformations


def generate_set_long_if_null_transformation():
    transformation = dict()
    transformation['name'] = 'setLongIfNull'
    transformation['params'] = [0]

    return transformation


def generate_cast_long_to_timestamp_int_64_transformation():
    transformation = dict()
    transformation['name'] = 'castLongToTimestampInt64'

    return transformation


def generate_cast_string_to_timestamp_int_64_transformation():
    transformation = dict()
    transformation['name'] = 'castStringToTimestampInt64'

    return transformation


def generate_cast_string_to_date_transformation():
    transformation = dict()
    transformation['name'] = 'castStringToDateWithPattern'
    transformation['params'] = ['yyyy-MM-dd']

    return transformation


def generate_set_string_if_null(default_value):
    transformation = dict()
    transformation['name'] = 'setStringIfNull'
    transformation['params'] = [default_value]

    return transformation


def generate_matches_uuid_pattern_validation():
    validation = dict()
    validation['name'] = "matchesPattern"
    validation['params'] = ["^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"]

    return validation


def generate_uuid_validation():
    validation = dict()
    validation['name'] = "isUUID"

    return validation


def generate_minimum_validation(minimum):
    validation = dict()
    validation['name'] = "minimum"
    validation['params'] = [minimum]

    return validation


def generate_maximum_validation(maximum):
    validation = dict()
    validation['name'] = "maximum"
    validation['params'] = [maximum]

    return validation


def generate_min_length_validation(min_length):
    validation = dict()
    validation['name'] = "minLength"
    validation['params'] = [min_length]

    return validation


def generate_max_length_validation(max_length):
    validation = dict()
    validation['name'] = "maxLength"
    validation['params'] = [max_length]

    return validation


def get_field_type(field_type, field_format):
    if field_type == 'number':
        if field_format is None or field_format == 'int32':
            return 'integer'
        elif field_format == 'int64':
            return 'long'
        elif field_format == 'float':
            return 'float'
        elif field_format == 'double':
            return 'double'
        else:
            return 'integer'
    else:
        return field_type


def process_request(data_catalog_response, mapper):
    """
    Process json from request service and generate yaml mapping for
    fields/values defined in schema service

    Parameters:
    var (rest response object): response object from schema service

    Returns:
    data : dictionary of yaml mappings

    Note:
        key and name are static and do not come from the Schema API and they should NOT be listed in the Schema API as they are really technical storage details.
        createdAt, modifiedAt, ref are static but must be specified in the schema API for use by down stream services such as LookML, Engage UI, Segmentation UI etc

    Example Output:
      - source:
          type: complex
          name: dataExtensions
          collection: true
        destination:
          name: dataExtensions
          mappings:
          - source:
              type: string
              name: createdAt
            destination:
              type: timestamp
              name: createdAt
              transformations:
              - params:
                - 0
                name: setLongIfNull
              - name: castLongToTimestampInt64
          - source:
              type: string
              name: key
            destination:
              type: string
              name: key
          - source:
              type: string
              name: modifiedAt
            destination:
              type: timestamp
              name: modifiedAt
              transformations:
              - params:
                - 0
                name: setLongIfNull
              - name: castLongToTimestampInt64
          - source:
              type: string
              name: name
            destination:
              type: string
              name: name
          - source:
              type: string
              name: ref
            destination:
              validations:
              - params:
                - ^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$
                name: matchesPattern
              - name: isUUID
              type: string
              name: ref
          - source:
              type: complex
              name: values
            destination:
              name: values
              mappings:
              - source:
                  type: string
                  name: newspaper
                destination:
                  type: string
                  name: newspaper
    """

    """
    Empty collection for yaml elements
    """
    root = dict()
    root['source'] = dict()
    root['source']['name'] = 'dataExtensions'
    root['source']['type'] = 'complex'
    root['source']['collection'] = True

    root['destination'] = dict()
    root['destination']['name'] = 'dataExtensions'
    root['destination']['mappings'] = []

    # Created At
    created_at_mapping = dict()
    created_at_mapping['source'] = dict()
    created_at_mapping['source']['name'] = 'createdAt'
    created_at_mapping['source']['type'] = 'string'

    created_at_mapping['destination'] = dict()
    created_at_mapping['destination']['name'] = 'createdAt'
    created_at_mapping['destination']['type'] = 'timestampInt64'

    created_at_mapping['transformations'] = []
    created_at_mapping['transformations'].append(generate_set_long_if_null_transformation())
    created_at_mapping['transformations'].append(generate_cast_long_to_timestamp_int_64_transformation())

    root['destination']['mappings'].append(created_at_mapping)

    # Key
    key_mapping = dict()
    key_mapping['source'] = dict()
    key_mapping['source']['name'] = 'key'
    key_mapping['source']['type'] = 'string'

    key_mapping['destination'] = dict()
    key_mapping['destination']['name'] = 'key'
    key_mapping['destination']['type'] = 'string'

    root['destination']['mappings'].append(key_mapping)

    # Modified At
    modified_at_mapping = dict()
    modified_at_mapping['source'] = dict()
    modified_at_mapping['source']['name'] = 'modifiedAt'
    modified_at_mapping['source']['type'] = 'string'

    modified_at_mapping['destination'] = dict()
    modified_at_mapping['destination']['name'] = 'modifiedAt'
    modified_at_mapping['destination']['type'] = 'timestampInt64'

    modified_at_mapping['transformations'] = []
    modified_at_mapping['transformations'].append(generate_set_long_if_null_transformation())
    modified_at_mapping['transformations'].append(generate_cast_long_to_timestamp_int_64_transformation())

    root['destination']['mappings'].append(modified_at_mapping)

    # Name
    name_mapping = dict()
    name_mapping['source'] = dict()
    name_mapping['source']['name'] = 'name'
    name_mapping['source']['type'] = 'string'

    name_mapping['destination'] = dict()
    name_mapping['destination']['name'] = 'name'
    name_mapping['destination']['type'] = 'string'

    root['destination']['mappings'].append(name_mapping)

    # Ref
    ref_mapping = dict()
    ref_mapping['source'] = dict()
    ref_mapping['source']['name'] = 'ref'
    ref_mapping['source']['type'] = 'string'

    ref_mapping['destination'] = dict()
    ref_mapping['destination']['name'] = 'ref'
    ref_mapping['destination']['type'] = 'string'

    ref_mapping['validations'] = []
    ref_mapping['validations'].append(generate_matches_uuid_pattern_validation())
    ref_mapping['validations'].append(generate_uuid_validation())

    root['destination']['mappings'].append(ref_mapping)

    # Values
    values = dict()
    values['source'] = dict()
    values['source']['name'] = 'values'
    values['source']['type'] = 'complex'

    values['destination'] = dict()
    values['destination']['name'] = 'values'

    values['destination']['mappings'] = []

    root['destination']['mappings'].append(values)

    for item in data_catalog_response['items']:
        if item['entityType'] == "GuestDataExtension":
            for n in item['properties']:
                field_name = None
                field_type = None
                field_default_value = None
                field_format = None

                if 'name' in n:
                    field_name = n['name']
                    if field_name == 'createdAt' or field_name == 'modifiedAt' or field_name == 'ref':
                        continue
                if 'type' in n:
                    field_type = n['type']
                if 'format' in n:
                    field_format = n['format']
                if 'default' in n:
                    field_default_value = n['default']

                if 'dbColumnName' in n:
                    field_db_column_name = n['dbColumnName']

                if 'minimum' in n:
                    field_minimum = n['minimum']
                if 'maximum' in n:
                    field_maximum = n['maximum']
                if 'minLength' in n:
                    field_min_length = n['minLength']
                if 'maxLength' in n:
                    field_max_length = n['maxLength']

                exists = False
                for existing_mapping in values['destination']['mappings']:
                    if existing_mapping['source']['name'] == field_name:
                        exists = True
                        break
                if exists:
                    continue

                mapping = dict()

                # Source
                mapping['source'] = dict()
                mapping['source']['name'] = field_name
                mapping['source']['type'] = get_field_type(field_type, field_format)

                # Destination
                mapping['destination'] = dict()
                if mapper == "complex":
                    mapping['destination']['name'] = field_name
                else:
                    mapping['destination']['name'] = field_db_column_name

                mapping['destination']['type'] = get_field_type(field_type, field_format)

                # Transformation
                if field_default_value is not None and mapping['destination']['type'] == 'string':
                    if 'transformations' not in mapping:
                        mapping['transformations'] = []
                    mapping['transformations'].append(generate_set_string_if_null(field_default_value))

                # Transformation & Validations
                # As defined by date-time - RFC3339 https://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14
                if field_type == 'string' and field_format == 'date-time':
                    if 'transformations' not in mapping:
                        mapping['transformations'] = []
                    mapping['destination']['type'] = 'timestampInt64'
                    mapping['transformations'].append(generate_cast_string_to_timestamp_int_64_transformation())
                # As defined by full-date - RFC3339 https://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14
                if field_type == 'string' and field_format == 'date':
                    if 'transformations' not in mapping:
                        mapping['transformations'] = []
                    mapping['destination']['type'] = 'date'
                    mapping['transformations'].append(generate_cast_string_to_date_transformation())

                if field_format == 'uuid':
                    if 'validations' not in mapping:
                        mapping['validations'] = []
                    mapping['validations'].append(generate_matches_uuid_pattern_validation())
                    mapping['validations'].append(generate_uuid_validation())

                if 'minimum' in n:
                    if 'validations' not in mapping:
                        mapping['validations'] = []
                    mapping['validations'].append(generate_minimum_validation(field_minimum))
                if 'maximum' in n:
                    if 'validations' not in mapping:
                        mapping['validations'] = []
                    mapping['validations'].append(generate_maximum_validation(field_maximum))
                if 'minLength' in n:
                    if 'validations' not in mapping:
                        mapping['validations'] = []
                    mapping['validations'].append(generate_min_length_validation(field_min_length))
                if 'maxLength' in n:
                    if 'validations' not in mapping:
                        mapping['validations'] = []
                    mapping['validations'].append(generate_max_length_validation(field_max_length))

                values['destination']['mappings'].append(mapping)

    if len(values['destination']['mappings']) > 0:
        return [root]
    else:
        return None


def write_spec(dest, data, padding):
    if not os.path.exists(os.path.abspath(os.path.join(dest, '..'))):
        os.makedirs(os.path.abspath(os.path.join(dest, '..')))

    if os.path.exists(dest + ".tmp"):
        os.remove(dest + ".tmp")

    if os.path.exists(dest):
        os.remove(dest)

    with open(dest + ".tmp", 'w') as tempFile:
        if data is not None:
            safe_dump(data, tempFile, default_flow_style=False, sort_keys=False)

    # Do padding on the yaml
    with open(dest + ".tmp", 'r') as tempFile2:
        lines = tempFile2.readlines()
        with open(dest, 'w') as outfile:
            for line in lines:
                outfile.write(padding + line)

            outfile.write("")
            outfile.write("")
    os.remove(dest + ".tmp")


def main():

    argument_spec = (
        dict(
            name=dict(required=True, type='str'),
            client_key=dict(required=True, type='str'),
            url=dict(required=True, type='str'),
            entity_type=dict(required=True, type='str'),
            dest=dict(required=True, type='str'),
            padding=dict(default='', type='str'),
            mapper=dict(default='complex', choices=['complex', 'simple'], type='str')
        )
    )

    module = AnsibleModule(argument_spec=argument_spec)

    url = module.params.get("url")
    method = "GET"
    headers = {
        'ClientKey': module.params.get("client_key")
    }
    data = None
    dest = module.params.get("dest")
    padding = module.params.get("padding")
    mapper = module.params.get("mapper")

    response, info = fetch_url(module,
                               url=url,
                               method=method,
                               headers=headers,
                               data=data)

    if int(info['status']) != 200:
        module.fail_json(msg="Failed to execute the API request:"
                             " {0}".format(info['msg']),
                         url=url,
                         method=method,
                         headers=headers)

    if response is not None:
        body = json.loads(response.read())

        data = process_request(body, mapper)

        write_spec(dest, data, padding)

    result = dict(
        name=module.params.get("name"),
        client_key=module.params.get("client_key"),
        url=module.params.get("url"),
        entity_type=module.params.get("entity_type"),
        dest=module.params.get("dest"),
        mapper=module.params.get("mapper"),
        padding=module.params.get("padding")
    )

    module.exit_json(changed=True, **result)


if __name__ == '__main__':
    main()
