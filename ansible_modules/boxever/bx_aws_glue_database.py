#!/usr/bin/python

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: bx_aws_glue_database
short_description: Manage an AWS Glue Database. Creates and deletes databases
description:
    - Manage an AWS Glue data catalog. See U(https://aws.amazon.com/glue/) for details.
    - API: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue.html#Glue.Client.create_database
version_added: "2.6"
requirements: [ boto3 ]
author: "Javier Ramos"
options:
  name:
    description:
      - Database name
    required: true
  description:
    description:
      - Database description
    required: false
  location:
    description:
      - Database location on S3
    required: false
'''

EXAMPLES = '''
# Note: These examples do not set authentication details, see the AWS Guide for details.

# Create a database
- bx_aws_glue_database:
    name: myDB
    description: test
    state: present

- bx_aws_glue_database:
    name: myDB
    description: test
    location: s3://my-unique-bucket
    state: present
            
# Delete a database
- bx_aws_glue_database:
    name: myDB
    state: absent

'''

RETURN = '''
'''

from ansible.module_utils.aws.core import AnsibleAWSModule
from ansible.module_utils.ec2 import camel_dict_to_snake_dict

# Non-ansible imports
import copy
try:
    from botocore.exceptions import BotoCoreError, ClientError
except ImportError:
    pass

def xstr(s):
    return ' ' if s is None else str(s)

def create_or_update_glue_database(connection, module, name, description, location):
    """
    Create or update a new DB in the data catalog

    :param connection: AWS boto3 glue connection
    :param module: Ansible module
    :param name: DB name
    :param description: DB description
    :param location: DB location
    :return: boto3 Glue job dict or None if not found
    """

    changed = False

    if name:
        try:

            if not is_glue_database_created(connection, name):

                connection.create_database(DatabaseInput={
                    'Name': name,
                    'Description': xstr(description) ,
                    'LocationUri': xstr(location)
                })
                changed = True
        except (BotoCoreError, ClientError) as e:
            module.fail_json_aws(e)

    module.exit_json(changed=changed)

def is_glue_database_created(connection, name):
    try:
        connection.get_database(Name=name)
        return True

    except:
        return False

def delete_glue_database(connection, module, name):
    """
    Delete an AWS Glue database

    :param connection: AWS boto3 glue connection
    :param module: Ansible module
    :param name: Name of the Data Catalog Database
    :return:
    """

    changed = False

    if name:
        try:
            if is_glue_database_created(connection, name):
                connection.delete_database(Name=name)
                changed = True
        except (BotoCoreError, ClientError) as e:
            module.fail_json_aws(e)

    module.exit_json(changed=changed)


def main():

    argument_spec = (
        dict(
            name=dict(required=True, type='str'),
            description=dict(required=False, type='str'),
            location=dict(required=False, type='str'),
            state=dict(required=True, choices=['present', 'absent'], type='str'),
        )
    )

    module = AnsibleAWSModule(argument_spec=argument_spec)

    connection = module.client('glue')

    state = module.params.get("state")
    name = module.params.get("name")
    description = module.params.get("description")
    location = module.params.get("location")

    if state == 'present':
        create_or_update_glue_database(connection, module, name, description, location)
    else:
        delete_glue_database(connection, module, name)


if __name__ == '__main__':
    main()
