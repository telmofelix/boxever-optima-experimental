#!/usr/bin/python

# Copyright: (c) 2019, Alan Giles (alan.giles@boxever.com)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: bx_aws_step_functions_state_machine
short_description: Manage an AWS Step Function state machine
description:
    -  Manage an AWS Step Function. See U(https://aws.amazon.com/step-functions/) for details.
version_added: "2.7"
requirements: [ boto3 ]
author: "Alan Giles (alan.giles@boxever.com)"
options:
  name:
    description:
      - The name you assign to this state machine definition. It must be unique in your account.
    required: true
  role:
    description:
      - The name or ARN of the IAM role associated with this state machine.
    required: true
  definition_document:
    description:
      - The path to the properly json formatted policy file (mutually exclusive with definition_json)
  definition_json:
    description:
      - A properly json formatted policy as string (mutually exclusive with definition_document, see https://github.com/ansible/ansible/issues/7005#issuecomment-42894813 on how to use it properly)
      
extends_documentation_fragment:
    - aws
    - ec2
'''

EXAMPLES = '''
# Note: These examples do not set authentication details, see the AWS Guide for details.

# Create an AWS Step Function state machine
- bx_aws_step_functions_state_machine:
    name: my-state-machine
    role: my-iam-role
    definition_document: my-state-machine.json
    state: present
    
# Create an AWS Step Function state machine
- bx_aws_step_functions_state_machine:
    name: my-state-machine
    role: my-iam-role
    definition_json: "{{ lookup('template', 'my-state-machine.json') }}"
    state: present

# Delete an AWS Step Function state machine
- bx_aws_step_functions_state_machine:
    name: my-state-machine
    state: absent

'''

RETURN = '''
name:
    description: The name assigned to this job definition.
    returned: when state is present
    type: string
    sample: my-state-machine
role:
    description: The name or ARN of the IAM role associated with this state machine.
    returned: when state is present
    type: string
    sample: my-iam-role
'''

from ansible.module_utils.aws.core import AnsibleAWSModule
from ansible.module_utils.ec2 import camel_dict_to_snake_dict
from ansible.module_utils.six import string_types
from collections import OrderedDict

# Non-ansible imports
import json
try:
    from botocore.exceptions import BotoCoreError, ClientError
except ImportError:
    pass


def _get_state_machine(connection, module, step_functions_state_machine_name):
    """
    Get an AWS Step Functions state machine based on name. If not found, return None.

    :param connection: AWS boto3 Step Function state machine connection
    :param module: Ansible module
    :param step_functions_state_machine_name: Name of Step Functions state machine to get
    :return: boto3 Step Function state machine dict or None if not found
    """

    try:
        list_state_machines_response = connection.list_state_machines();

        for state_machine in list_state_machines_response['stateMachines']:
            if state_machine['name'] == step_functions_state_machine_name:
                return connection.describe_state_machine(stateMachineArn=state_machine['stateMachineArn'])
        return None
    except (BotoCoreError, ClientError) as e:
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            return None
        else:
            module.fail_json_aws(e)

def get_iam_role(connection, module, name):
    """
    Get an AWS IAM role based on name. If not found, return None.

    :param connection: AWS boto3 Step Function state machine connection
    :param module: Ansible module
    :param name: Name of IAM Role to get
    :return: boto3 IAM role dict or None if not found
    """

    try:
        return connection.get_role(RoleName=name)['Role']
    except (BotoCoreError, ClientError) as e:
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            return None
        else:
            module.fail_json_aws(e)

def _compare_step_functions_state_machine_params(user_params, current_params):
    """
    Compare Step Function state machine params. If there is a difference, return True immediately else return False

    :param user_params: the Step Function state machine parameters passed by the user
    :param current_params: the Step Function state machine parameters currently configured
    :return: True if any parameter is mismatched else False
    """

    if 'roleArn' in user_params and user_params['roleArn'] != current_params['roleArn']:
        return True
    if 'definition' in user_params and user_params['definition'] != current_params['definition']:
        return True

    return False


def create_or_update_step_functions_state_machine(connection, module, step_functions_state_machine, definition, iam_role):
    """
    Create or update an AWS Step Functions state machine

    :param connection: AWS boto3 Step Functions connection
    :param module: Ansible module
    :param step_functions_state_machine: a dict of AWS Step function state machine parameters or None
    :param definition: the state machine definition as a json document
    :return:
    """

    changed = False
    params = dict()
    params['name'] = module.params.get("name")
    params['roleArn'] = iam_role['Arn']
    params['definition'] = definition
    # params['definition'] = '{"StartAt":"HelloWorld","States":{"HelloWorld":{"Type":"Pass","Result":"Hello World!","End":true}}}'
    if step_functions_state_machine:
        params['stateMachineArn'] = step_functions_state_machine['stateMachineArn']

    # If step_functions_state_machine is not None then check if it needs to be modified, else create it
    if step_functions_state_machine:
        if _compare_step_functions_state_machine_params(params, step_functions_state_machine):
            try:
                connection.update_state_machine(stateMachineArn=params['stateMachineArn'],
                                                definition=params['definition'],
                                                roleArn=params['roleArn'])
                step_functions_state_machine = connection.describe_state_machine(stateMachineArn=params['stateMachineArn'])
                changed = True
            except (BotoCoreError, ClientError) as e:
                module.fail_json_aws(e)
    else:
        try:
            create_result = connection.create_state_machine(**params)
            new_arn = create_result['stateMachineArn'] # set ARN of the new state machine
            step_functions_state_machine = connection.describe_state_machine(stateMachineArn=new_arn)
            changed = True
        except (BotoCoreError, ClientError) as e:
            module.fail_json_aws(e)

    module.exit_json(changed=changed, **camel_dict_to_snake_dict(step_functions_state_machine))


def delete_step_functions_state_machine(connection, module, step_functions_state_machine):
    """
    Delete an AWS Step Function state machine

    :param connection: AWS boto3 step functions connection
    :param module: Ansible module
    :param step_functions_state_machine: a dict of AWS Step function state machine parameters or None
    :return:
    """

    changed = False

    if step_functions_state_machine:
        try:
            connection.delete_state_machine(stateMachineArn=step_functions_state_machine['stateMachineArn'])
            changed = True
        except (BotoCoreError, ClientError) as e:
            module.fail_json_aws(e)

    module.exit_json(changed=changed)

def main():

    argument_spec = (
        dict(
            name=dict(required=True, type='str'),
            role=dict(type='str'),
            definition_document=dict(type='str'),
            definition_json=dict(type='str'),
            state=dict(required=True, choices=['present', 'absent'], type='str')
        )
    )

    module = AnsibleAWSModule(argument_spec=argument_spec,
                              required_if=[
                                  ('state', 'present', ['role'])
                              ]
                              )

    definition_document = module.params.get('definition_document')
    if definition_document is not None and module.params.get('definition_json') is not None:
        module.fail_json(msg='Only one of "definition_document" or "definition_json" may be set')

    if definition_document is not None:
        try:
            with open(definition_document, 'r') as json_data:
                ddoc = json.dumps(json.load(json_data), indent=4, separators=(',', ': '))
                json_data.close()
        except IOError as e:
            if e.errno == 2:
                module.fail_json(
                    msg='definition_document {0:!r} does not exist'.format(definition_document))
            else:
                raise
    elif module.params.get('definition_json') is not None:
        ddoc = module.params.get('definition_json')
    else:
        ddoc = None

    connection = module.client('stepfunctions')
    iam_connection = module.client('iam')

    state = module.params.get("state")

    step_functions_state_machine = _get_state_machine(connection, module, module.params.get("name"))

    if state == 'present':
        iam_role = get_iam_role(iam_connection, module, module.params.get("role"))
        create_or_update_step_functions_state_machine(connection, module, step_functions_state_machine, ddoc, iam_role)
    else:
        delete_step_functions_state_machine(connection, module, step_functions_state_machine)

if __name__ == '__main__':
    main()
