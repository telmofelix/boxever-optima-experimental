#!/usr/bin/python

# Copyright: (c) 2019, Alan Giles (alan.giles@boxever.com)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: bx_data_catalog_pipeline_input
short_description: Retrieve and generate optima data pipeline input from data catalog service
description:
    -  Retrieve and generate optima data pipeline input from data catalog service
version_added: "2.7"
requirements: [ boto3 ]
author: "Alan Giles (alan.giles@boxever.com)"
options:
  name:
    description:
      - The name of the Boxever tenant.
    required: true
  client_key:
    description:
      - The client_key of the Boxever tenant.
    required: true
  url:
    description:
      - The url of the Data Catalog API.
    required: true
  entity_type:
    description:
      - The entity_type of model in the Data Catalog API.
    required: true
  padding:
    description:
      - The padding to add to each line of the dest.
  source_schema:
    description: 
      - The name of the source schema.
  target_schema:
    description: 
      - The name of the target schema.
  s3_bucket_schemas_path:
    description: 
      - The S3 destination path for the ddls.
  s3_bucket_data_path:
    description: 
      - The S3 destination path for the data.
  s3_bucket_data_path_prefix:
    description: 
      - The prefix to add to the destination table.
  dest:
    description:
      - The destination location of optima pipeline input.
    required: true
    
extends_documentation_fragment:
    - aws
    - ec2
'''

EXAMPLES = '''
# Note: These examples do not set authentication details, see the AWS Guide for details.

# Generate Data Extension Pipeline Input from Data Catalog Service
- bx_data_catalog_pipeline_input:
    name: spinair
    client_key: wjtc2eog1lvueo72kts3mn1ean0nentz
    url:  "https://api.boxever.io/v3/data-catalog/v2/schemaDefinitions"
    entity_type: "GuestDataExtension"
    padding: "  "
    source_schema: "spinair_trusted"
    target_schema: "spinair"
    s3_bucket_schemas_path: "s3://my_bucket/schemas/"
    s3_bucket_data_path: "s3://my_bucket/data/"
    s3_bucket_data_path_prefix: "${data_of_week}_"
    dest: "tmp-templates/wjtc2eog1lvueo72kts3mn1ean0nentz/optima-data-catalog/guest-data-extensions.json"

'''

RETURN = '''
name:
    description: The name assigned to this job definition.
    returned: when state is present
    type: string
    sample: spinair
client_key:
    description: The client_key of the Boxever tenant.
    type: string
    sample: wjtc2eog1lvueo72kts3mn1ean0nentz
entity_type:
    description: The entity_type of model in the Data Catalog API.
    type: string
    sample: GuestDataExtension
padding:
    description: The padding to add to each line of the dest.
    type: string
    sample: "  "
source_schema:
    description: The name of the source schema.
    type: string
    sample: "spinair_trusted"
target_schema:
    description: The name of the target schema.
    type: string
    sample: "spinair"
s3_bucket_schemas_path:
    description: The S3 destination path for the ddls.
    type: string
    sample: s3://my_bucket/schemas
s3_bucket_data_path:
    description: The S3 destination path for the data.
    type: string
    sample: s3://my_bucket/data/
s3_bucket_data_path_prefix:
    description: The prefix to add to the destination table.
    type: string
    sample: "${day_of_week}_"

dest:
    description: The destination location of optima pipeline input.
    type: string
    sample: tmp-templates/wjtc2eog1lvueo72kts3mn1ean0nentz/optima-data-catalog/guest-data-extensions.json
'''

from ansible.module_utils.aws.core import AnsibleAWSModule
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url, url_argument_spec

# Non-ansible imports
import os
import json
try:
    from botocore.exceptions import BotoCoreError, ClientError
except ImportError:
    pass

def get_s3_bucket_name(url):
    return url[len('s3://'):url.index('/', len('s3://'))]


def get_s3_key(url):
    return url[url.index('/', len('s3://')) + 1:len(url)]

def generate_and_upload_ddl(connection, s3_data_bucket_path, source_schema, target_schema, item, s3_ddl_dest_path):

    table = item['dbTableName']

    tmp_file = "/tmp/" + target_schema + "-" + table + ".tmp"

    with open(tmp_file, 'w') as outfile:

        attributes = []

        outfile.write("DROP TABLE IF EXISTS " + target_schema + "." + table + ";\n")
        outfile.write("\n")
        outfile.write("CREATE TABLE " + target_schema + "." + table + "\n")
        outfile.write("WITH (\n")
        outfile.write("    format = 'PARQUET',\n")
        outfile.write("    parquet_compression = 'GZIP',\n")
        outfile.write("    external_location = '" + s3_data_bucket_path +"'\n")
        outfile.write(")\n")
        outfile.write("AS\n")
        outfile.write("SELECT\n")
        guest_ref = 'guest_ref' # required
        attributes.append(guest_ref)
        outfile.write("    g.ref as " + guest_ref + " ,\n")

        added = False

        for n in item['properties']:
            field_name = n['name']
            db_column_name = n['dbColumnName']
            if db_column_name not in attributes:
                attributes.append(db_column_name)
                if added:
                    outfile.write(",\n")
                else:
                    added = True

                if field_name == 'createdAt' or field_name == 'modifiedAt' or field_name == 'ref':
                    outfile.write("    de." + field_name + " as " + db_column_name)
                else:
                    outfile.write("    de.\"values\"." + field_name + " as " + db_column_name)

        outfile.write("\n")
        outfile.write("FROM\n")
        outfile.write("    " + source_schema + ".guests g\n")
        outfile.write("CROSS JOIN\n")
        outfile.write("    UNNEST(g.dataExtensions) AS de(de)\n")
        outfile.write("WHERE\n")
        outfile.write("    lower(de.name)=lower('" + item['name'] + "')\n") # lower case to prevent typos in Data Extension entry
        outfile.write("AND\n")
        outfile.write("    lower(de.key)=lower('" + item['key'] + "')\n") # lower case to prevent typos in Data Extension entry
        outfile.write(";\n")

    bucket = get_s3_bucket_name(s3_ddl_dest_path)
    key = get_s3_key(s3_ddl_dest_path)

    connection.upload_file(tmp_file, bucket, key)

    os.remove(tmp_file)

def generate_and_upload_view_ddl(connection, source_schema, target_schema, item, s3_ddl_dest_path):

    table = item['dbTableName']

    tmp_file = "/tmp/" + target_schema + "-" + table + ".tmp"

    with open(tmp_file, 'w') as outfile:
        outfile.write("CREATE OR REPLACE VIEW " + target_schema + "." + table + " AS select * from " + source_schema + "." + table + ";\n")

    bucket = get_s3_bucket_name(s3_ddl_dest_path)
    key = get_s3_key(s3_ddl_dest_path)

    connection.upload_file(tmp_file, bucket, key)

    os.remove(tmp_file)

def process_data_catalog_response(data_catalog_response, connection, s3_bucket_schemas_path, s3_bucket_data_path, source_schema, target_schema, s3_bucket_data_path_prefix):
    
    tasks = []
    
    for item in data_catalog_response['items']:
        if item['entityType'] == "GuestDataExtension":
            ddl_s3_path=s3_bucket_schemas_path + "/" + item['dbTableName'] + ".ddl"

            s3_data_bucket_path = s3_bucket_data_path + "/" + s3_bucket_data_path_prefix + item['dbTableName']

            task = dict()
            task['s3']=s3_data_bucket_path
            task['script']=ddl_s3_path
            task_name = 'DEX' + "_" + item['dbTableName']
            if len(task_name) > 70:
                task_name = task_name[0:70]

            task['taskName']=task_name
            task['table']=item['dbTableName']
            task['folder']=s3_data_bucket_path.replace("s3://","")

            tasks.append(task)

            # Generate DDL and upload into bucket using s3 boto client
            generate_and_upload_ddl(connection, s3_data_bucket_path, source_schema, target_schema, item, ddl_s3_path)

    return tasks

def process_data_catalog_response_view(data_catalog_response, connection, s3_bucket_schemas_path, source_schema, target_schema):

    tasks = []

    for item in data_catalog_response['items']:
        if item['entityType'] == "GuestDataExtension":
            ddl_s3_path=s3_bucket_schemas_path + "/" + item['dbTableName'] + "_view.ddl"

            task = dict()
            task['script']=ddl_s3_path

            tasks.append(task)

            # Generate view DDL and upload into bucket using s3 boto client
            generate_and_upload_view_ddl(connection, source_schema, target_schema, item, ddl_s3_path)

    return tasks

def save_json(tasks, dest, padding):
    if not os.path.exists(os.path.abspath(os.path.join(dest, '..'))):
        os.makedirs(os.path.abspath(os.path.join(dest, '..')))

    if os.path.exists(dest + ".tmp"):
        os.remove(dest + ".tmp")

    if os.path.exists(dest):
        os.remove(dest)

    with open(dest + ".tmp", 'w') as tempFile:
        if tasks is not None:
            json.dump(tasks, tempFile, sort_keys=False, indent=2)

    # Do padding on the json
    with open(dest + ".tmp", 'r') as tempFile2:
        lines = tempFile2.readlines()
        with open(dest, 'w') as outfile:
            for line in lines:
                outfile.write(padding + line)

            outfile.write("")
            outfile.write("")
    os.remove(dest + ".tmp")

def main():

    argument_spec = (
        dict(
            name=dict(required=True, type='str'),
            client_key=dict(required=True, type='str'),
            url=dict(required=True, type='str'),
            entity_type=dict(required=True, type='str'),
            padding=dict(default='', type='str'),

            source_schema=dict(required=True, type='str'),
            target_schema=dict(required=True, type='str'),
            view_schema=dict(required=True, type='str'),

            s3_bucket_schemas_path=dict(required=True, type='str'),
            s3_bucket_data_path=dict(required=True, type='str'),
            s3_bucket_data_path_prefix=dict(required=True, type='str'),

            dest=dict(required=False, type='str', default=None),
            view_dest=dict(required=False, type='str', default=None)
        )
    )

    module = AnsibleAWSModule(argument_spec=argument_spec)

    amodule = AnsibleModule(argument_spec=argument_spec)

    s3_connection = module.client('s3')

    url = module.params.get("url")
    method="GET"
    headers={
        'ClientKey': module.params.get("client_key")
    }
    data=None
    client_key=module.params.get("client_key")
    padding=module.params.get("padding")
    s3_bucket_schemas_path=module.params.get("s3_bucket_schemas_path")
    s3_bucket_data_path=module.params.get("s3_bucket_data_path")
    s3_bucket_data_path_prefix=module.params.get("s3_bucket_data_path_prefix")
    source_schema=module.params.get("source_schema")
    target_schema=module.params.get("target_schema")
    view_schema=module.params.get("view_schema")
    dest = module.params.get("dest")
    view_dest = module.params.get("view_dest")

    response, info = fetch_url(amodule,
                               url=url,
                               method=method,
                               headers=headers,
                               data=data)

    if int(info['status']) != 200:
        module.fail_json(msg="Failed to execute the API request:"
                             " {0}".format(info['msg']),
                         url=url,
                         method=method,
                         headers=headers)

    if response is not None:
        body = json.loads(response.read())

        tasks = process_data_catalog_response(body, s3_connection, s3_bucket_schemas_path, s3_bucket_data_path, source_schema, target_schema, s3_bucket_data_path_prefix)

        if dest is not None:
            save_json(tasks, dest, padding)

        view_tasks = process_data_catalog_response_view(body, s3_connection, s3_bucket_schemas_path, target_schema, view_schema)

        if view_dest is not None:
            save_json(view_tasks, view_dest, padding)

    result = dict(
        name=module.params.get("name"),
        client_key=module.params.get("client_key"),
        url=module.params.get("url"),
        entity_type=module.params.get("entity_type"),
        padding=module.params.get("padding"),
        source_schema=module.params.get("source_schema"),
        target_scheama=module.params.get("target_schema"),
        view_schema=module.params.get("target_schema"),
        s3_bucket_schemas_path=module.params.get("s3_bucket_schemas_path"),
        s3_bucket_data_path=module.params.get("s3_bucket_data_path"),
        s3_bucket_data_path_prefix=module.params.get("s3_bucket_data_path_prefix"),
        dest=module.params.get("dest"),
        view_dest=module.params.get("view_dest")
    )

    module.exit_json(changed=True, **result)

if __name__ == '__main__':
    main()
