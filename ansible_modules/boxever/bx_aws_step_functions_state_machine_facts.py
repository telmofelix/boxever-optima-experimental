#!/usr/bin/python

# Copyright: (c) 2019, Alan Giles (alan.giles@boxever.com)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: bx_aws_step_functions_state_machine_facts
short_description: Gather Facts about an AWS Step Function state machine
description:
    -  Gather Facts about an AWS Step Function. See U(https://aws.amazon.com/step-functions/) for details.
version_added: "2.7"
requirements: [ boto3 ]
author: "Alan Giles (alan.giles@boxever.com)"
options:
  name:
    description:
      - The name you assign to this state machine definition.
    required: true

extends_documentation_fragment:
    - aws
    - ec2
'''

EXAMPLES = '''
# Note: These examples do not set authentication details, see the AWS Guide for details.

# Create an AWS Step Function state machine
- bx_aws_step_functions_state_machine_facts:
    name: my-state-machine


'''

RETURN = '''
name:
    description: The name assigned to this job definition.
    returned: when state is present
    type: string
    sample: my-state-machine
role:
    description: The name or ARN of the IAM role associated with this state machine.
    returned: when state is present
    type: string
    sample: my-iam-role
arn:
    description: The name or ARN of the IAM role associated with this state machine.
    returned: when state is present
    type: string
    sample: my-state_machine-arn
'''

from ansible.module_utils.aws.core import AnsibleAWSModule
from ansible.module_utils.ec2 import camel_dict_to_snake_dict
from ansible.module_utils.six import string_types
from collections import OrderedDict

# Non-ansible imports
import json
try:
    from botocore.exceptions import BotoCoreError, ClientError
except ImportError:
    pass


def _get_state_machine(connection, module, step_functions_state_machine_name):
    """
    Get an AWS Step Functions state machine based on name. If not found, return None.

    :param connection: AWS boto3 Step Function state machine connection
    :param module: Ansible module
    :param step_functions_state_machine_name: Name of Step Functions state machine to get
    :return: boto3 Step Function state machine dict or None if not found
    """

    try:
        list_state_machines_response = connection.list_state_machines();

        for state_machine in list_state_machines_response['stateMachines']:
            if state_machine['name'] == step_functions_state_machine_name:
                return connection.describe_state_machine(stateMachineArn=state_machine['stateMachineArn'])
        return None
    except (BotoCoreError, ClientError) as e:
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            return None
        else:
            module.fail_json_aws(e)

def main():

    argument_spec = (
        dict(
            name=dict(required=True, type='str')
        )
    )

    module = AnsibleAWSModule(argument_spec=argument_spec)

    connection = module.client('stepfunctions')

    step_functions_state_machine = _get_state_machine(connection, module, module.params.get("name"))

    module.exit_json(changed=True, **camel_dict_to_snake_dict(step_functions_state_machine))

if __name__ == '__main__':
    main()
