import boto3
import logging
import datetime

glue = boto3.client('glue')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class CannotStartGlueJob(Exception): pass

# Error codes
START_GLUE_JOB_ERROR  = "OPTERR-SGLU-0001"
INITIAL_STATUS_FAILED = "OPTERR-SGLU-0002"

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue.html#Glue.Client.start_job_run
def handler(event, context):
    logger.info("start glue job. event: " + str(event))
    if event['status'] == 'FAILED':
        logger.error(INITIAL_STATUS_FAILED + ". Initial status is FAILED")
        return event

    job_name = event['jobName']
    job_args = event['jobArgs']
    try:
        response = glue.start_job_run(
            JobName   = job_name,
            Arguments = job_args
        )
        event['jobId']  = response['JobRunId']
        event['status'] = 'SUBMITTED'
        event['start_time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        logger.info("Execution started at " + str(event['start_time']))
        logger.info("Glue job SUBMITTED. Job id: " + event['jobId'])
        return event
    except Exception as e:
        logger.error("Error stating glue job")
        logger.error(str(e))
        raise CannotStartGlueJob(e, "Error starting glue job", START_GLUE_JOB_ERROR)
