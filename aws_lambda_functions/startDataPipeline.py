import boto3
import uuid
import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

step_functions = boto3.client('stepfunctions')

def replace_item(obj, key, replace_value):
    for k, v in obj.items():
        if isinstance(v, dict):
            obj[k] = replace_item(v, key, replace_value)
    if key in obj:
        obj[key] = replace_value
    return obj

def get_item(obj, key):
    for k, v in obj.items():
        if isinstance(v, dict):
            obj[k] = get_item(v, key)
    if key in obj:
        return obj[key]


def replace_value(obj, key, value, replacement):
    for k, v in obj.items():
        if isinstance(v, dict):
            obj[k] = replace_value(v, key, value,replacement)
    if key in obj:
        if value in obj[key]:
            obj[key] = obj[key].replace(value,replacement)
    return obj


# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.get_object
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/stepfunctions.html#SFN.Client.start_execution
# https://docs.python.org/2/library/uuid.html
def handler(event, context):
    logger.info("starting data pipeline")
    if event['status'] == 'FAILED':
        logger.info("starting data pipeline return FAILED, because status event['STATUS']=='FAILED'")
        return event

    state_machine_arn = event['stateMachineArn']
    state_machine_execution_name = str(uuid.uuid4())
    state_machine_input = event['stateMachineInput']
    logger.info("starting data pipeline. ARN: {0}, name: {1}, input: {2}", )

    day_of_the_week = event['optima_vars']['day_of_the_week']
    batchid = event['optima_vars']['batchid']
    modified_at_date_since = event['optima_vars']['modified_at_date_since']
    current_date = event['optima_vars']['current_date']

    replace_item(state_machine_input,'--day_of_the_week', day_of_the_week)
    replace_item(state_machine_input,'--date', current_date)
    replace_item(state_machine_input,'--modified_at_date_since', modified_at_date_since)
    replace_item(state_machine_input,'--bx_batch_id', batchid)
    replace_value(state_machine_input, 'database', '${day_of_the_week}', day_of_the_week)

    try:
        response = step_functions.start_execution(
            stateMachineArn = state_machine_arn,
            name            = state_machine_execution_name,
            input           = json.dumps(state_machine_input)
        )
        event['executionArn'] = response['executionArn']
        event['startDate']    = str(response['startDate'])
        event['status']    = 'SUBMITTED'
    except:
        event['status']='FAILED'

    return event