import boto3
import json
import logging

stepFunctions = boto3.client('stepfunctions')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class ErrorCheckingPipeline(Exception): pass

# Error codes
CHECK_PIPELINE_ERROR  = "OPTERR-CHKP-0001"

def handler(event, context):
    if event['status'] != 'FAILED':
        try:
            execution_arn = event['executionArn']

            response = stepFunctions.describe_execution(
                executionArn=execution_arn
            )

            event['status'] = response['status']
            logger.info("response STATUS: " + str(response['status']))


            if event['status'] != 'FAILED':
                if 'output' in response:
                    output = json.loads(response['output'])

                    if output is not None and isinstance(output, dict):
                        for key, value in output.items():
                            if "Status" in key or isinstance(value, list): # Nested output
                                for item in value:
                                    logger.debug("MAIN: " + str(item))
                                    if 'status' in item:
                                        logger.debug("item: " + str(item))
                                        status = item['status']
                                        if status == 'FAILED':
                                            event['status'] = 'FAILED'
                                            logger.error("item " + str(item) + " FAILED")
                                            return event
                                    elif isinstance(item, dict): # Sub pipeline
                                        event = check_sub_pipeline(item, event)
                                        if event['status'] == 'FAILED':
                                            return event
                                    elif isinstance(item, list):
                                        for subitem in item:
                                            logger.info("subitem: " + str(subitem))
                                            status = subitem['status']
                                            if status == 'FAILED':
                                                event['status'] = 'FAILED'
                                                logger.error("item " + str(subitem) + " FAILED")
                                                return event
                    output_len = len(str(output).encode('utf-8'))
                    logger.debug("output_len: " + str(output_len))
                    if output_len < 27000: # avoid issue where payload is too big
                        event['output'] = output
                    else:
                        logger.info("skipping output...")
        except Exception as e:
            logger.error("Error checking pipeline. " + str(e))
            raise ErrorCheckingPipeline(e, "Error checking pipeline", CHECK_PIPELINE_ERROR)
    logger.info("Pipeline checked. ARN: " + event['executionArn'] + ". Status: " + event['status'])
    return event

def check_sub_pipeline(item, event):
    for subkey, subval in item.items():
        if "Status" in subkey: # Nested output
            for subitem in subval:
                logger.info("subitemsubpipeline: " + str(subitem))
                status = subitem['status']
                if status == 'FAILED':
                    logger.error("item " + str(subitem) + " FAILED")
                    event['status'] = 'FAILED'
                    return event
    return event
