"""

Function that copies S3 keys from the source bucket to the destination, appending and overriding files but not deleting them.

It uses multiple threads for high performance. Please set THREAD_PARALLELISM env var to the desired value according to
the memory and timeout settings of the lambda.

The threads will retry 3 times in case the API returns an error, after that a Exception is thrown and the execution halts.

"""

# Imports
import logging
import boto3
from threading import Thread
from queue import Queue, Empty
import os
import json
import time

# Constants
THREAD_PARALLELISM = int(os.environ['THREAD_PARALLELISM'])
METADATA_KEYS = [
    'CacheControl',
    'ContentDisposition',
    'ContentEncoding',
    'ContentLanguage',
    'ContentType',
    'Expires',
    'Metadata'
]

# Exceptions
class CopyExportError(Exception): pass
class ThreadCopyExportError(Exception): pass

# Error codes
ERROR_COPY_EXPORT                 = "OPTERR-CPEXP-0001"
INITIAL_STATUS_FAILED             = "OPTERR-CPEXP-0002"

# Globals
threadErrors = [] # store any exception from the threads
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Utility functions

"""
# Retrieve metadata from the HTTP response
"""
def collect_metadata(response):
    metadata = {}
    for key in METADATA_KEYS:
        if key in response:
            metadata[key] = response[key]
    metadata_json = json.dumps(metadata, sort_keys=True, default=str)
    return metadata_json


# Classes

"""
# Main Class which syncs the data from source to destination.

It represent a single thread that copies a single key at one time.

"""
class KeySynchronizer(Thread):
    ## Constructor takes the main queue, the source, destination buckets and the region
    def __init__(self, job_queue=None, source=None, destination=None, region=None):
        super(KeySynchronizer, self).__init__()
        self.job_queue = job_queue # Main Queue
        self.source = source
        self.destination = destination
        self.s3 = boto3.client('s3', region_name=region)

    """
    # Performs key copy using boto3 API
    """
    def do_copy(self, source_key, dest_key):
        logger.debug("target_key: " + source_key)
        self.s3.copy(
            CopySource={
                'Bucket': self.source,
                'Key': source_key
            },
            Bucket=self.destination,
            Key=dest_key)

    """
    # Main Copy method, it includes retries
    """
    def copy_object(self, source_key, dest_key, retries=0):
        global threadErrors
        logger.debug(
            'Copying key: ' + source_key + ' from bucket: ' + self.source +
            ' to destination bucket: ' + self.destination + ' and destination folder: ' + dest_key)
        try:
            self.do_copy(source_key,dest_key)
        except Exception as e:
            time.sleep(1)
            retries = retries + 1
            logger.error("Error copy_object. Key: " + str(key) +" Exception: " + str(e))
            logger.info("retries: " + str(retries))
            if retries >= 3:
                threadErrors.append(ERROR_COPY_EXPORT)
                logger.info("threadErrors: " + str(threadErrors))
                raise ThreadCopyExportError(e, "Error copy_object. Key: " + str(key) +" Exception: " + str(e), ERROR_COPY_EXPORT)
            else:
                self.copy_object(source_key, dest_key, retries) # recursive call

    """
    # Main Thread Method
    """
    def run(self):
        global threadErrors
        while not self.job_queue.empty(): # check the queue
            try:
                source_key,dest_key = self.job_queue.get(True, 1) # get next pair of keys
            except Empty:
                return

            self.copy_object(source_key,dest_key)


# Functions

"""
# Main Method to sync the keys. 
It creates the thread queue, initializes the classes and starts the threads.
"""
def sync_keys(source=None, destination=None, region=None, keys=None):
    job_queue = Queue()
    worker_threads = []

    for i in range(THREAD_PARALLELISM): # Init Classes
        worker_threads.append(KeySynchronizer(
            job_queue=job_queue,
            source=source,
            destination=destination,
            region=region
        ))

    for key in keys: # insert elements in the queue
        logger.debug('Queuing: ' + key + ' for synchronization.')
        job_queue.put((key,keys[key]))

    logger.info(
        'Starting ' + str(THREAD_PARALLELISM) + ' key synchronization processes for buckets: ' + source +
        ' and ' + destination + '.'
    )

    # Kickoff the threads
    for t in worker_threads:
        t.start()

    # wait for the threads to complete
    for t in worker_threads:
        t.join()

"""
# Main Lambda Handler
"""
def handler(event, context):
    global threadErrors

    if event and 'status' in event and event['status'] == 'FAILED':
        logger.error(INITIAL_STATUS_FAILED + ". Initial status is FAILED")
        return event

    if event['status'] == 'SKIPPED':
        return event

    source = event['source_bucket']
    destination = event['dest_bucket']
    folders = event['folders']

    if len(folders) <= 0: # if the folder is empty we skip the copy
        logger.info("No Folders Passed in Event!")
        return {
            "status" : "SUCCESS",
            "folders": {},
            "source" : source,
            "destination" : destination
        }

    try:

        function_region = context.invoked_function_arn.split(':')[3]
        region = event.get('sourceRegion', function_region)
        s3 = boto3.client('s3', region_name=region)

        keys = {} # store all the keys and destinations
        for source_folder in folders:
            logger.info("Bucket: " + str(source) + " Prefix: " + str(source_folder))
            # We need to use the paginator, otherwise we only get 1K of data and not all files
            paginator = s3.get_paginator("list_objects").paginate(Bucket=source, Prefix=source_folder)
            for page in paginator:
                folder_keys_only = [k['Key'] for k in page.get('Contents', [])]
                folder_keys = {k:folders[source_folder]+'/'+"/".join(k.split('/')[1:]) for k in folder_keys_only}
                logger.info('Got ' + str(len(folder_keys)) + ' result keys.')
                keys.update(folder_keys)

        logger.info('Copying ' + str(len(keys)) + ' keys from bucket: ' + source + ' to bucket: ' + destination)

        # Sync all keys
        sync_keys(source=source, destination=destination, keys=keys, region=region)

        if len(threadErrors) > 0: #check if there are any errors
            raise CopyExportError(None, "Error Copying Export Data in Thread " + str(threadErrors[0]), ERROR_COPY_EXPORT)

        logger.info("Sync Completed!")

        return {
            "status" : "SUCCESS",
            "folders" : folders,
            "source" : source,
            "destination" : destination,
            "region" : region,
            "files_copied" : len(keys)
        }

    except Exception as e:
        logger.error("Error Copying Export Data in Handler. Exception: " + str(e))
        raise CopyExportError(e, "Error Copying Export Data in Handler " + str(e), ERROR_COPY_EXPORT)