from aws_lambda_functions.checkGlueJob import handler, ErrorCheckingGlueJob
import datetime
import os

#Handle Python 2 vs 3
try:
    from unittest import mock
except ImportError:
    import mock

import unittest


class TestCheckGlueJob(unittest.TestCase):

    def setUp(self):
        os.environ['AWS_PROFILE'] = "labs"
        os.environ['AWS_DEFAULT_REGION'] = "eu-west-1"

    def test_check_glue_job(self):
        print("Testing check_glue_job")

        event = dict()
        event['status'] = "SUCCEEDED"
        event['jobName'] = "Emirates Trusted Guests"
        event['jobId'] = "99999"
        event['start_time'] = str(datetime.datetime.strptime(str(datetime.datetime.now()
                                                                 .isoformat(' ', 'seconds')), "%Y-%m-%d %H:%M:%S"))
        event['timeout'] = '60'
        context = None

        mock_glue = dict(dict())
        mock_glue['JobRun'] = ('JobRunState', 'RUNNING')

        with mock.patch('boto3.client') as mock_glue:
            actual = handler(event, context)
            mock_glue.assert_called_once_with('glue')
            self.assertEqual(actual['jobName'], "Emirates Trusted Guests")
            self.assertEqual(actual['jobId'], "99999")

    def test_check_glue_job_timeout(self):
        print("Testing check_glue_job_timeout")

        event = dict()
        event['status'] = "SUCCEEDED"
        event['jobName'] = "Emirates Trusted Guests"
        event['jobId'] = "99999"
        event['start_time'] = str(datetime.datetime.strptime(str((datetime.datetime.now() - datetime.timedelta(days=1))
                                                                 .isoformat(' ', 'seconds')), "%Y-%m-%d %H:%M:%S"))
        event['timeout'] = '60'
        context = None

        mock_glue = dict(dict())
        mock_glue['JobRun'] = ('JobRunState', 'RUNNING')

        with mock.patch('boto3.client') as mock_glue:
            actual = handler(event, context)
            mock_glue.assert_called_once_with('glue')
            self.assertEqual(actual['jobName'], "Emirates Trusted Guests")
            self.assertEqual(actual['jobId'], "99999")
            self.assertEqual(actual['status'], "TIMEOUT")

    def test_check_glue_job_exception(self):
        print("Testing check_glue_job_exception")

        event = dict()
        event['status'] = "SUCCEEDED"
        event['jobName'] = "Emirates Trusted Guests"
        event['jobId'] = "99999"
        event['start_time'] = str(datetime.datetime.strptime(str(datetime.datetime.now()
                                                                 .isoformat(' ', 'seconds')), "%Y-%m-%d %H:%M:%S"))
        event['timeout'] = '60'
        context = None

        mock_glue = dict(dict())
        mock_glue['JobRun'] = ('JobRunState', 'RUNNING')

        with mock.patch('boto3.client') as mock_glue:
            mock_glue.side_effect = Exception

            with self.assertRaises(ErrorCheckingGlueJob):
                handler(event, context)

    def tearDown(self):
        print("Tear Down")


if __name__ == '__main__':
    unittest.main()
