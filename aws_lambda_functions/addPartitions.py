"""
Function that scans the S3 folder and generates the list of partitions and adds then using Athena API.

- It scans the data folder and looks for partitions: partition=value
- It generates the DDL statements to generate all the partitions
- It executes the query

"""
import boto3
import logging
import datetime
import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)

athena = boto3.client('athena')
s3     = boto3.client('s3')

# Exceptions
class ErrorAddingPartitions(Exception): pass
class ErrorRunningQuery(Exception): pass

ERROR_ADD_PARTITIONS= "OPTERR-ADDPT-0001"
ERROR_EXEC_QUERY= "OPTERR-ADDPT-0002"

PARTITION_START = "ALTER TABLE {0} ADD IF NOT EXISTS "
PARTITION_FORMAT = " PARTITION ({0}) LOCATION '{1}' "
MAX_PARTITIONS_PER_SQL = 500

"""
# For the given bucket and folder (prefix) it generate the list of partition DDL statements
"""
def get_partitions(bucket_name, prefix):
    logger.info("get_partitions. bucket_name: " + bucket_name + " prefix: " + prefix)
    ret_val = []
    # Read bucket contents
    logger.info("prefix: " + str(prefix))
    paginator = s3.get_paginator("list_objects").paginate(Bucket=bucket_name, Prefix=prefix)
    folder_list = []
    num_partitions = 0
    for folders in paginator:
        logger.debug("folders: " + str(folders))
        if 'Contents' in folders:
            # This loop looks for the longest partition to populate num_partitions
            for obj in folders['Contents']:
                key = obj['Key']
                if "$folder" in key:
                    logger.debug("folder: " + str(key))
                    key_split = key.replace(prefix + "/","").split("/")
                    logger.debug("key_split: " + str(key_split))
                    if len(key_split) > 0 and len(key_split) > num_partitions:
                        num_partitions = len(key_split)
                    folder_list.append(key)
            logger.info("num_partitions: " + str(num_partitions) + " folder_list len: " + str(len(folder_list)))
        else:
            logger.error("Folder " + str(prefix) + " does not exist!")
            logger.error("This may be because the table is empty")
    logger.info("FINAL: num_partitions: " + str(num_partitions) + " folder_list len: " + str(len(folder_list)))
    # This loop generates the DDL partition statements
    if len(folder_list) > 0:
        partitions_list = []
        for key in folder_list:
            path = key.replace(prefix + "/","").replace("_$folder$","")
            key_split = path.split("/")
            logger.debug("key_split: " + str(key_split))
            # Location of the partition
            s3_folder = "s3://" + bucket_name + "/" + prefix + "/" + path + "/"
            logger.debug("s3_folder " + str(s3_folder))
            new_partition = ""
            index = 0
            for partition in key_split:
                if "=" in partition: # Look for the partition
                    partition_split = partition.split("=")
                    new_partition =  new_partition + partition_split[0] + "='" + partition_split[1] + "'"
                    index += 1
                    if index < len(key_split):
                        new_partition = new_partition + ", "
            # Make sure that the folder is indeed partitioned
            if len(new_partition) > 2 and len(new_partition.split(',')) == num_partitions:
                partition = PARTITION_FORMAT.format(new_partition, s3_folder)
                logger.debug("partition: " + str(partition))
                partitions_list.append(partition) # ADD PARTITION statement
                if len(partitions_list) >= MAX_PARTITIONS_PER_SQL:
                    logger.info("adding len: " + str(len(partitions_list)))
                    ret_val.append(partitions_list)
                    partitions_list = []
        ret_val.append(partitions_list)
    logger.info("ret_val len: " + str(len(ret_val)))
    return ret_val


"""
# Function that generates the DDL SQL commands
"""
def append_add_partitions(event):
    partition_folder = event['partition_folder']
    day_of_the_week = event['optima_vars']['day_of_the_week']
    partition_folder = partition_folder.replace('${day_of_the_week}', day_of_the_week)
    bucket_name = partition_folder[:partition_folder.index('/')]
    folder     = partition_folder[partition_folder.index('/') + 1:]

    try:
        partitions = get_partitions(bucket_name, folder)
        commands = []
        if len(partitions) > 0: # only generate it if the data is partitioned
            logger.info("partitions: " + str(partitions))
            table_name = event['database'] + "." + event["table"]

            for partition in partitions:
                if partition and len(partition) > 0:
                    command = PARTITION_START.format(table_name) + ' '.join(partition) + ";"
                    logger.info("command: " + str(command))
                    commands.append(command)
            return commands
    except Exception as e:
        logger.error("Error Adding partitions statement " + str(e))
        raise ErrorAddingPartitions(e, "Error Adding partitions statement", ErrorAddingPartitions)

"""
# Lambda Handler
"""
def handler(event, context):
    if event['status'] == 'FAILED':
        return event

    executions = []
    event['executions']=executions
    database = event['database']
    output_location = event['outputLocation']

    # Get the SQL DDL
    sql_commands = append_add_partitions(event)

    if sql_commands and len(sql_commands) > 0:

        try:

            for sql_command in sql_commands:
                logger.info("Running Command: " + str(sql_command)[:70] + " of " + str(len(sql_commands)))
                response = athena.start_query_execution(
                    QueryString=sql_command,
                    QueryExecutionContext={
                        'Database': database
                    },
                    ResultConfiguration={
                        'OutputLocation': output_location,
                        'EncryptionConfiguration': {
                            'EncryptionOption': 'SSE_S3'
                        }
                    }
                )

                execution = {'status': 'SUBMITTED', 'id': response['QueryExecutionId'], 'query' : 'Add Partitions',
                             'start_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
                logger.info("Execution started at " + str(execution['start_time']))
                executions.append(execution)
                time.sleep(0.4)
        except Exception as e:
            logger.error("Error Starting DDL Query " + str(e))
            handle_failure(event, e)
    else:
        day_of_the_week = event['optima_vars']['day_of_the_week']
        folder = event['partition_folder'].replace('${day_of_the_week}', day_of_the_week)
        logger.info("No data found in " + folder)
        event['status'] = 'SKIPPED'
        event['skipped_reason'] = "No folder or partitions in " + folder

    event['executions'] = executions # Set the executions for the check athena query lambda function
    return event

def handle_failure(event, error):
    num_retries = event['num_retries'] if 'num_retries' in event else 3
    current_retries = event['current_retries'] if 'current_retries' in event else 0
    logger.error("handle_failure: " + str(error))
    logger.error("num_retries: " + str(num_retries))
    logger.error("current_retries: " + str(current_retries))

    if current_retries >= num_retries:
        event['status'] = 'FAILED'
        event['current_retries'] = 0
    else:
        event['status'] = 'FAILED_RETRY'
        event['current_retries'] = current_retries + 1
    return event