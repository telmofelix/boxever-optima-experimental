"""
This functions reads the looker spec file from S3 and calls the schema service.

This function is used to generate the optima model and data quality dashboards for Looker using LookerML

It parses the entities and setup an JSON object that is passed to the next functions.

It also saves the schema on S3

"""

from __future__ import print_function
import boto3
import logging
import os
from botocore.vendored import requests
import json

# LOGGER
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#EXCEPTIONS
class ReadConfigException(Exception): pass

class ParseConfigException(Exception): pass

class RestConnectionException(Exception): pass

class ParseFieldsException(Exception): pass

class SaveSchemaException(Exception): pass


# ERRORS
READ_CONFIG_ERROR  = "OPTERR-LKMLRD-P1-0001"
PARSE_CONFIG_ERROR = "OPTERR-LKMLRD-P1-0002"
REST_CALL_ERROR    = "OPTERR-LKMLRD-P1-0003"
PARSE_FIELDS_ERROR = "OPTERR-LKMLRD-P1-0004"
SAVE_CACHE_ERROR   = "OPTERR-LKMLRD-P3-0005"

s3 = boto3.resource('s3')
CONFIG_FILE_BUCKET = os.environ['CONFIG_FILE_BUCKET']
CONFIG_FILE_KEY = os.environ['CONFIG_FILE_KEY']

def handler(event, context):

    logger.info("data quality read, event: " + str(event))
    if 'status' not in event or event['status'] != 'FAILED':
        event['status'] = 'RUNNING'

        config_file = read_config_file()
        logger.info("config file: " + str(config_file))

        config_data = parse_config_file(config_file)
        logger.debug("config data: " + str(config_data))

        schema = call_schema_service(config_data)
        logger.debug("schema: " + str(schema))

        fields = parse_data_quality_fields(schema, config_data, event)
        logger.info("fields: " + str(fields))

        save_schema_file(schema, config_data)
        logger.info("data quality read completed")

        event['status'] = 'SUCCESS'

    return event


def read_config_file():
    try:
        logger.debug("Reading config file " + CONFIG_FILE_KEY)
        obj = s3.Object(CONFIG_FILE_BUCKET, CONFIG_FILE_KEY)
        return obj.get()['Body'].read().decode('utf-8')
    except BaseException as e:
        logger.error("Error Getting Config File from S3")
        logger.error(str(e))
        raise ReadConfigException(e, "Error Getting Config File from S3", str(READ_CONFIG_ERROR) +  "\n" + str(e))

def parse_config_file(body):
    ret_val = {}
    try:
        logger.debug("parsing yaml config...")
        logger.debug(body)
        for line in body.splitlines():
            logger.debug(line)
            if '#' not in line: # skip comments
                keyVal = line.split(":", 1)
                ret_val[keyVal[0]] = keyVal[1].strip().replace('\"','')
        logger.debug(ret_val)
        return ret_val
    except Exception as e:
        logger.error("Error Parsing Config File from S3")
        logger.error(str(e))
        raise ParseConfigException(e, "Error Parsing Config File from S3", PARSE_CONFIG_ERROR +  "\n" + str(e))

""" 

Calls the schema service, if nto available it falls back to the previously saved schema on S3.

"""
def call_schema_service(conf):
    headers={
        'ClientKey': conf['looker_client_key']
    }

    try:
        logger.info("making call to " + conf['looker_url'])
        response = requests.get(conf['looker_url'], headers=headers)
        if response:
            return response.json()
        else:
            logger.warn("Fall back to S3 copy...")
            cache_path = conf['looker_schema_path']
            schema = s3.Object(CONFIG_FILE_BUCKET, cache_path)
            return  json.loads(schema)

    except Exception as e:
        logger.error("Error Calling Schema Service")
        logger.error(str(e))
        raise RestConnectionException(e, "Error Making REST Call to Schema Service", REST_CALL_ERROR +  "\n" + str(e))

""" Gets the main partitions or filters for the entities. For guest the filter will be guest type, for events channel"""
def get_filter_field(filters, entity):
    logger.info("get_filter_field: " + str(filters) + " " + str(entity))
    filter_fields = filters.split(",")
    logger.info("filter_fields: " + str(filter_fields))
    for filter_field in filter_fields:
        filter_field = filter_field.split(":")
        if filter_field[0] == entity and len(filter_field) > 1:
            logger.info("filter_field: " + str(filter_field))
            return filter_field[1]

""" Generate a simple input for the quality dashboards """
def parse_data_quality_fields(schema, conf, event):
    data_quality_schema = {}
    types = conf['data_quality_entity_types'].split(",")
    logger.info("Parsing Schema, types: " + str(types))
    try:
        logger.debug("parse_fields. schema: " + str(schema))

        for item in schema['items']:
            if item['entityType'] == 'GuestDataExtension':
                process_field(item, conf, None, data_quality_schema)
            else:
                for type in types:
                    if item['dbTableName'] == type:
                        process_field(item, conf, type, data_quality_schema)
                        break

        logger.debug("parse result: " + str(data_quality_schema))
        event['data_quality'] = data_quality_schema
        event['looker_conf'] = conf
    except BaseException as e:
        logger.error("Error parsing schema: " + str(schema))
        logger.error(str(e))
        raise ParseFieldsException(e, "Error parsing schema", PARSE_FIELDS_ERROR +  "\n" + str(e))

def process_field(item, conf, type, data_quality_schema):
    table_name = item['dbTableName']
    data_quality_schema[table_name] = {}
    logger.debug("Parsing " + str(item['dbTableName']))
    filter_field = get_filter_field(conf['data_quality_filters'], type)
    columns = []
    pii = conf['looker_pii_fields']
    logger.info("pii: " + str(pii))
    for prop in item['properties']:
        if 'dbColumnName' in prop and (not pii or prop['dbColumnName'] not in pii):
            columns.append(prop['dbColumnName'])
            if filter_field and prop['name'] == filter_field and 'enum' in prop: # save the filer
                data_quality_schema[table_name]['filter'] = prop['dbColumnName']
                logger.info("filter_field found: " + str(filter_field) + " enum: " + str(prop['enum']))
                data_quality_schema[table_name]['filter_fields'] = prop['enum']
    logger.debug(columns)
    data_quality_schema[table_name]['columns'] = columns

def save_schema_file(schema, conf):
    try:
        logger.debug("Saving schema file...")
        object = s3.Object(CONFIG_FILE_BUCKET, conf['looker_schema_path'])
        object.put(Body=bytes(json.dumps(schema).encode('UTF-8')))
    except BaseException as e:
        logger.error("Error saving schema file: " + str(schema))
        logger.error(str(e))
        raise SaveSchemaException(e, "Error saving schema file to S3", SAVE_CACHE_ERROR +  "\n" + str(e))