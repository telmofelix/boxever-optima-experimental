"""

Function that creates a new table in the customer DB based on the decisions CSV file.

"""
import boto3
import logging
import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class S3Exception(Exception): pass
class SQLException(Exception): pass

# ERRORS
S3_ERROR = "OPTERR-RTLKDT-P1-0001"
SQL_ERROR = "OPTERR-RTLKDT-P1-0002"

BATCH_ID = 'bx_batch_id='

s3_client = boto3.client('s3')
athena = boto3.client('athena')

"""
Get DDL file with the queries to run.
"""
def get_ddl_from_s3(script):
    logger.info("Getting S3 object: " + script)
    try:

        bucket = script[len('s3://'):script.index('/', len('s3://'))]
        key = script[script.index('/', len('s3://')) + 1:len(script)]

        logger.info("Bucket: " + bucket + ", Key: " + key)

        s3_response = s3_client.get_object(Bucket=bucket, Key=key)

        logger.debug(s3_response)

        return s3_response
    except Exception as e:
        logger.error("Error Getting DDL file from S3 " + str(e))
        raise S3Exception(e, "Error Getting DDL file from S3", S3_ERROR)

"""
Function that executes a single query.
"""
def run_query(sql_command, database, output_location):
    logger.info("Executing: " + sql_command)
    response = athena.start_query_execution(
        QueryString=sql_command,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': output_location,
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_S3'
            }
        }
    )

    id = response['QueryExecutionId']

    logger.info("Execution id " + str(id))

    status = 'SUBMITTED'

    while status == 'SUBMITTED' or status == 'QUEUED' or status == 'RUNNING':
        time.sleep(6)
        response = athena.get_query_execution(
            QueryExecutionId=id
        )
        status = response['QueryExecution']['Status']['State']

        logger.info("Query Result: " + str(response['QueryExecution']))

        if status == 'FAILED' or status == 'CANCELLED':
            raise SQLException(None, "Error Checking Query ", str(SQL_ERROR))

"""
Main Handler
"""
def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    if 'status' in event and event['status'] == 'SKIPPED':
        return event

    decisions_location = event['decisions_location']

    decisions_data = event['decisionsCreateTable']

    logger.debug("decisions_data: " + str(decisions_data))

    output_location = decisions_data['outputLocation']

    script = decisions_data['script']
    database = decisions_data['database']

    s3_response = get_ddl_from_s3(script)

    day_of_the_week = decisions_data['optima_vars']['day_of_the_week']

    try:

        # Get all commands
        sql_commands = s3_response['Body'].read().decode('utf-8').split(';')

        # Drop table command
        drop_command = sql_commands[0].replace('${day_of_the_week}', day_of_the_week)
        logger.info("drop_command: " + str(drop_command))

        # Drop the table
        run_query(drop_command, database, output_location)

        sql_command = sql_commands[1]

        # Create table
        sql_command = sql_command.replace('${day_of_the_week}', day_of_the_week).replace('${decisions_location}', decisions_location)
        logger.info("sql_command: " + str(sql_command))
        run_query(sql_command, database, output_location)

    except Exception as e:
        logger.error("ERROR: " + str(e))
        event['status']='FAILED'
        raise SQLException(e, "Error Running DDL ", str(SQL_ERROR) + str(e))

    logger.debug("EVENT: " + str(event))
    return event
