"""
Functions that retrieve flows to be used in Optima.
- It queries the Flows Service and performs a search to get all the flows
- It retrieves the flows in JSON and generates a CSV file

"""
import os
import logging
from apiUtils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

API_SCOPE = "bx_flows_search"
FLOWS_API_URL = os.environ['FLOWS_API_URL']

"""
# Main Handler 
"""
def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    settings = event['flowsReadData']

    if 'status' in settings and settings['status'] == 'SKIPPED':
        event['status'] = 'SKIPPED'
        return event

    secret = get_secret()

    response = call_service(secret,API_SCOPE,FLOWS_API_URL)

    if response:
        header = ['status','name','ref']
        output_file = save_s3(response, header, filename='flows.csv')

        event['status'] = 'SUCCESS'
        event['flows_location'] = output_file
    else:
        event['status'] = 'SKIPPED'
        event['status_reason'] = 'No flows found for ' + CLIENT_KEY

    return event
