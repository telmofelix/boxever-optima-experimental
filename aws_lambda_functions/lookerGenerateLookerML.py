"""

Method that receives the JSON input with the configuration and:

- Reads the template files from S3
- Process all LookML templates and generates all source code automatically for optima project
- Save Them in S3

"""
import boto3
import logging
import os, tempfile
import json

# LOGGER
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#EXCEPTIONS
class ReadFileException(Exception): pass

class TemplateProcessingException(Exception): pass

class WriteFilesException(Exception): pass

class ProcessModelException(Exception): pass

# ERRORS
READ_FILE_ERROR = "OPTERR-LMPT-P1-0001"
TEMPLATE_ERROR  = "OPTERR-LMPT-P2-0002"
FILE_ERROR      = "OPTERR-LMPT-P1-0003"
MODEL_ERROR     = "OPTERR-LMPT-P1-0004"

s3 = boto3.resource('s3')
BUCKET = os.environ['BUCKET']

# Template values
TABLE = "[[TABLE]]"
TABLE_NAME = "[[TABLE_NAME]]"
FIELD = "[[FIELD]]"
TYPE = "[[TYPE]]"
VIEW_TEMPLATE = "[[VIEW_TEMPLATE]]"
DATA_EXTENSIONS = "[[DATA_EXTENSIONS]]"

DIMENSION = "dimension: [[FIELD]] { type: [[TYPE]] sql: ${TABLE}.[[FIELD]] ;;}"
DIMENSION_PK = "dimension: [[FIELD]] { primary_key: yes type: string sql: ${TABLE}.[[FIELD]] ;;}"
COUNT_DISTINCT = "measure: count_distinct { type: count_distinct sql:  ${ref} ;; drill_fields: [ref] }"
COUNT = "measure: count { type: number sql:  COUNT(1) ;; drill_fields: [ref] }"
SUM = "measure: total_[[FIELD]] { type: sum sql: ${TABLE}.[[FIELD]] ;; drill_fields: [] }"
AVG = "measure: average_[[FIELD]] { type: average_distinct sql: ${TABLE}.[[FIELD]] ;; drill_fields: [] }"
MAX = "measure: max_[[FIELD]] { type: max sql: ${TABLE}.[[FIELD]] ;; drill_fields: [] }"
MIN = "measure: min_[[FIELD]] { type: min sql: ${TABLE}.[[FIELD]] ;; drill_fields: [] }"

DATA_EXTENSION = "join: [[TABLE]] { sql_on: ${guests.ref}=${[[TABLE]].guest_ref} ;; relationship: one_to_one type: left_outer }"

def handler(event, context):

    if event['status'] != 'FAILED':
        event['status'] = 'RUNNING'
        logger.info("Starting handler")
        logger.debug(str(event))

        conf = event['looker_conf']

        #Remove S3 temp folder
        delete_temp_folder(conf['looker_temp_folder_key'])

        view_template = read_file(conf['looker_view_template_key'])
        logger.debug("view_template: " + str(view_template))

        date_template = read_file(conf['looker_date_template_key'])
        logger.debug("date_template: " + str(date_template))

        date_time_template = read_file(conf['looker_date_time_template_key'])
        logger.debug("date_time_template: " + str(date_time_template))

        schema = read_file(conf['looker_schema_path'])
        logger.debug("schema: " + str(schema))

        #Process views
        view_templates = process_template(schema, view_template,  ".view.lkml", date_template, date_time_template, conf['looker_pii_fields'])
        save_files(view_templates, conf['looker_temp_folder_key'])

        copy_stats(conf)

        push_model = event['lookerPipeline']['pushModel'] or False
        logger.info("push model : " + str(push_model))
        #MODEL
        if push_model and push_model != 'False':
            model = read_file(conf['looker_model_template_key'])
            model = process_model(model, schema)
            logger.info("model: " + str(model))
            file_name = "optima.model.lkml"
            save_files({file_name: model}, conf['looker_temp_folder_key'])

        event['status'] = 'SUCCESS'
        event['prefix'] = 'looker_' # Set for code commit next step
        logger.info("Process Completed!")

    return event

""" 

Copy stats stats files to the temp folder.

TODO: Make Stats Dinamic

"""
def copy_stats(conf):
    try:
        logger.info("copy_stats")
        bucket = s3.Bucket(BUCKET)
        keys =  bucket.objects.filter(Prefix=conf['looker_stats_folder_key'])

        for obj in keys:
            copy_source = {
                'Bucket': BUCKET,
                'Key': obj.key
            }
            file_name = os.path.basename(obj.key)
            logger.info("file_name: " + file_name)
            new_path = conf['looker_temp_folder_key'] + file_name
            logger.info("new_path: " + new_path)
            s3.meta.client.copy(copy_source, BUCKET, new_path)
    except BaseException as e:
        logger.error("Error Copying files S3")
        logger.error(str(e))
        raise ReadFileException(e, "Error Copying Stats files S3", str(READ_FILE_ERROR) +  "\n" + str(e))


def read_file(key):
    try:
        logger.info("Reading file " + key)
        obj = s3.Object(BUCKET, key)
        return obj.get()['Body'].read().decode('utf-8')
    except BaseException as e:
        logger.error("Error Getting File from S3")
        logger.error(str(e))
        raise ReadFileException(e, "Error Getting File from S3", str(READ_FILE_ERROR) +  "\n" + str(e))


""" Process the views LookML """
def process_template(schema, view_template, extension, date_template, date_time_template, pii):
    processed_files = {}
    try:
        logger.info("process_template")
        logger.debug("date_template: " + str(date_template))
        items = json.loads(schema)['items']
        logger.info("pii: " + str(pii))
        for item in items:
            logger.debug("item: " + str(item))
            if 'dbTableName' in item:
                table = item['dbTableName']
                table_name = item['uiLabel']
                dimensions = []
                for prop in item['properties']:
                    if 'dbColumnName' in prop and (not pii or prop['dbColumnName'] not in pii):
                        column_name = prop['dbColumnName']
                        column_type = prop['type']
                        column_format = prop['format'] if 'format' in prop else ""
                        logger.info("column_name: " + str(column_name) + " column_type: " + str(column_type) + " column_format: " + str(column_format))
                        dimension = ""
                        if column_type == 'string' and column_name == 'ref':
                            dimension = DIMENSION_PK.replace(FIELD, column_name).replace(TYPE, 'string')
                            dimensions.append(dimension + "\n\n\t")
                            dimensions.append(COUNT + "\n\n\t")
                            dimensions.append(COUNT_DISTINCT + "\n\n\t")
                        else:
                            if column_format == 'date-time':
                                date_temp = date_time_template
                                dimension = date_temp.replace(FIELD, column_name)
                            elif column_format == 'date':
                                date_temp = date_template
                                dimension = date_temp.replace(FIELD, column_name)
                            elif column_format == 'number' or map_type(column_type) == 'number':
                                dimension = DIMENSION.replace(FIELD, column_name).replace(TYPE, 'number')
                                sum = SUM.replace(FIELD, column_name)
                                dimensions.append(sum + "\n\n\t")
                                avg = AVG.replace(FIELD, column_name)
                                dimensions.append(avg + "\n\n\t")
                                max = MAX.replace(FIELD, column_name)
                                dimensions.append(max + "\n\n\t")
                                min = MIN.replace(FIELD, column_name)
                                dimensions.append(min + "\n\n\t")
                            else:
                                dimension = DIMENSION.replace(FIELD, column_name).replace(TYPE, map_type(column_type))
                            logger.debug("Adding dimension: " + str(dimension))
                            dimensions.append(dimension + "\n\n\t")

                view = view_template
                view = view.replace(VIEW_TEMPLATE, ''.join(dimensions)).replace(TABLE_NAME, table_name).replace(TABLE, table).strip()

                file_name = table + extension
                processed_files[file_name] = view

        logger.debug("process_template ret: " + str(processed_files))
        return processed_files
    except BaseException as e:
        logger.error("Error Processing Templates")
        logger.error(str(e))
        raise TemplateProcessingException(e, "Error Processing Templates", str(TEMPLATE_ERROR) +  "\n" + str(e))

def map_type(column_type):
    if column_type == 'long' or column_type == 'integer' or column_type == 'double':
        return 'number'
    return column_type

def delete_temp_folder(folder_prefix):
    logger.info("Deleting folder: " + folder_prefix)
    s3.Bucket(BUCKET).objects.filter(Prefix=folder_prefix).delete()
    logger.info("Deleted folder: " + folder_prefix)


def save_files(files, prefix):
    try:
        logger.debug("save_files: " + str(files))
        for filename, data in files.items():
            full_name = prefix + filename
            s3.Object(BUCKET, full_name).put(Body=data)

    except BaseException as e:
        logger.error("Error Writing files to temp")
        logger.error(str(e))
        raise WriteFilesException(e, "Error writing files to temp", str(FILE_ERROR) +  "\n" + str(e))

""" Process the main model, add data extenstions """
def process_model(model, schema):
    try:
        logger.info("Processing model...")

        logger.debug("process_model, schema " + str(schema))
        items = json.loads(schema)['items']
        data_extensions = []
        for item in items:
            if item['entityType'] == 'GuestDataExtension':
                table = item['dbTableName']
                data_extension = DATA_EXTENSION.replace(TABLE, table)
                data_extensions.append(data_extension + "\n\t")

        ret_model = model.replace(DATA_EXTENSIONS,''.join(data_extensions))
        return ret_model

    except BaseException as e:
        logger.error("Error processing model")
        logger.error(str(e))
        raise ProcessModelException(e, "Error creating model file", str(MODEL_ERROR) +  "\n" + str(e))