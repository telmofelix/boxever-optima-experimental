import json
import os
from botocore.vendored import requests
from botocore.vendored.requests.utils import quote
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class ErrorSendingMessageToGChat(Exception): pass

# Error codes
ERROR_SENDING_MESSAGE_TO_GCHAT  = "OPTERR-NGCH-0010"

optimaBotUrl = os.environ['GCHAT_URL']
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

# Sample event:
# {
#    "msg": "The message to send to GChat",
#    "thread": "The thread. This field is optional"
# }
def notify_gchat(msg, thread=""):
    payload = { 'text': msg }
    try:
        requests.post(optimaBotUrl + thread, data=json.dumps(payload), headers=headers)
    except Exception as e:
        logger.error('Could not send message to GChat. Message: [{0}]"'.format(msg))
        logger.error('Error: {0}'.format(e))
        raise ErrorSendingMessageToGChat(e, "Error checking pipeline", ERROR_SENDING_MESSAGE_TO_GCHAT)

def handler(event, context):
    thread = "&threadKey=" + quote(event['thread']) if 'thread' in event else ""
    notify_gchat(event['msg'], thread)
    return event
