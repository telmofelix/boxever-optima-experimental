import boto3
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

athena = boto3.client('athena')
s3     = boto3.client('s3')

# Exceptions
class ErrorGettingS3DDL(Exception): pass
class ErrorRunningQuery(Exception): pass

ERROR_S3_DDL= "OPTERR-STATEX-0001"
ERROR_EXEC_QUERY= "OPTERR-STATEX-0002"

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/athena.html#Athena.Client.start_query_execution
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.get_object
def handler(event, context):
    if event['status'] == 'FAILED':
        return event

    database = event['database']
    output_location = event['outputLocation']

    script = event['script']

    s3_response = get_ddl_from_s3(script)

    executions=[]
    event['executions']=executions
    event['status']='SUBMITTED'

    sql_commands = s3_response['Body'].read().decode('utf-8').split(';')

    day_of_the_week = event['optima_vars']['day_of_the_week']
    bx_batch_id = event['optima_vars']['batchid']
    logger.info("day_of_the_week " + str(day_of_the_week))
    logger.info("bx_batch_id " + str(bx_batch_id))

    try:

        for j in range(len(sql_commands)):
            sql_command = sql_commands[j].strip()

            sql_command = sql_command.replace('${day_of_the_week}', day_of_the_week).replace('${bx_batch_id}', bx_batch_id)
            logger.info("sql_command: " + str(sql_command))

            if (len(sql_command) > 0):
                executions.append({'query': sql_command, 'status': 'PENDING'})

        for j in range(len(executions)):
            execution = executions[j]
            if execution['status'] == 'PENDING':
                sql_command = execution['query']
                logger.info("Executing: " + sql_command)

                response = athena.start_query_execution(
                    QueryString=sql_command,
                    QueryExecutionContext={
                        'Database': database
                    },
                    ResultConfiguration={
                        'OutputLocation': output_location,
                        'EncryptionConfiguration': {
                            'EncryptionOption': 'SSE_S3'
                        }
                    }
                )
                execution['status']='SUBMITTED'
                execution['id']=response['QueryExecutionId']
                execution['start_time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                logger.info("Execution started at " + str(execution['start_time']))
                break
    except Exception as e:
        logger.error("Error Starting DDL Query " + str(e))
        raise ErrorRunningQuery(e, "Error Starting DDL Query", ERROR_EXEC_QUERY)

    return event

def get_ddl_from_s3(script):
    logger.info("Getting S3 object: " + script)

    try:

        bucket = script[len('s3://'):script.index('/', len('s3://'))]
        key = script[script.index('/', len('s3://')) + 1:len(script)]

        logger.info("Bucket: " + bucket + ", Key: " + key)

        s3_response = s3.get_object(Bucket=bucket, Key=key)

        logger.debug(s3_response)

        return s3_response
    except Exception as e:
        logger.error("Error Getting DDL file from S3 " + str(e))
        raise ErrorGettingS3DDL(e, "Error Getting DDL file from S3", ERROR_S3_DDL)