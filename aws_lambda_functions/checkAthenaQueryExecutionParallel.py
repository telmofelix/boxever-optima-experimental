import boto3
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

athena = boto3.client('athena')

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/athena.html#Athena.Client.get_query_execution
def execute_task(event, task):
    database = event['database']
    output_location = event['outputLocation']
    executions = task['executions']

    if event['status'] == 'SKIPPED':
        logger.info("Skipping function...")
        return event

    for j in range(len(executions)):
        execution = executions[j]
        if execution['type'] == 'athena':

            if execution['status'] == 'SUBMITTED' or execution['status'] == 'QUEUED' or execution['status'] == 'RUNNING':
                try:
                    response = athena.get_query_execution(
                        QueryExecutionId=execution['id']
                    )
                    execution['status'] = response['QueryExecution']['Status']['State']

                    if execution['status'] == 'QUEUED' or execution['status'] == 'RUNNING':
                        event['status'] = execution['status']
                    if execution['status'] == 'FAILED' or execution['status'] == 'CANCELLED':
                        return handle_failure(event, execution['status'])

                    event = check_timeout(event, execution)
                    if event['status'] == 'TIMEOUT':
                        return event

                except Exception as e:
                    logger.error("ERROR: " + str(e))
                    return handle_failure(event, e)

    # Submit next pending
    for j in range(len(executions)):
        execution = executions[j]
        if execution['type'] == 'athena' and execution['status'] == 'PENDING':
            sql_command = execution['query']
            logger.info("Executing: " + sql_command)
            try:
                response = athena.start_query_execution(
                    QueryString=sql_command,
                    QueryExecutionContext={
                        'Database': database
                    },
                    ResultConfiguration={
                        'OutputLocation': output_location,
                        'EncryptionConfiguration': {
                            'EncryptionOption': 'SSE_S3'
                        }
                    }
                )
                execution['status'] = 'SUBMITTED'
                execution['id'] = response['QueryExecutionId']
                execution['start_time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                logger.info("Execution started at " + str(execution['start_time']))

                event['status'] = 'SUBMITTED'
                return event
            except Exception as e:
                logger.error("ERROR: " + str(e))
                return handle_failure(event, e)


def handler(event, context):

    try:
        if event['status'] == 'FAILED':
            return event

        event['status'] = 'SUBMITTED'
        parallel_tasks = event['parallelTasks']

        if len(parallel_tasks) == 0:
            event['status'] = 'SUCCEEDED'

        for task in parallel_tasks:
            execute_task(event, task)

            if event['status'] == 'FAILED':
                break

        all_succeeded = True
        for i in range(len(parallel_tasks)):
            task = parallel_tasks[i]
            for j in range(len(task['executions'])):
                execution = task['executions'][j]
                if execution['status'] != 'SUCCEEDED':
                    all_succeeded = False

        if all_succeeded:
            event['status'] = 'SUCCEEDED'

    except Exception as e:
        logger.error("ERROR: " + str(e))
        return handle_failure(event, e)

    return event

def handle_failure(event, error):
    num_retries = event['num_retries'] if 'num_retries' in event else 3
    current_retries = event['current_retries'] if 'current_retries' in event else 0
    logger.error("handle_failure: " + str(error))
    logger.error("num_retries: " + str(num_retries))
    logger.error("current_retries: " + str(current_retries))

    if current_retries >= num_retries:
        event['status'] = 'FAILED'
        event['current_retries'] = 0
    else:
        event['status'] = 'FAILED_RETRY'
        event['current_retries'] = current_retries + 1
    return event

def check_timeout(event, execution):
    now = datetime.datetime.now()
    start = datetime.datetime.strptime(execution['start_time'], "%Y-%m-%d %H:%M:%S")
    max_time = int(event['timeout'])
    minutes_diff = (now - start).total_seconds() / 60.0
    logger.debug("now " + str(now) + "start " + str(start) + "max_time " + str(max_time) + " minutes_diff" + str(minutes_diff))
    if minutes_diff > max_time: # timeout
        logger.error("Execution " + execution["id"] + " has timeout after " + str(minutes_diff) + " seconds")
        event['status'] = "TIMEOUT"
    return event
