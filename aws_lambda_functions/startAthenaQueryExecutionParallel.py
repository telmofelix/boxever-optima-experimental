import boto3
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

athena    = boto3.client('athena')
s3        = boto3.resource('s3')
s3_client = boto3.client('s3')

# Exceptions
class ErrorS3Delete(Exception): pass
class ErrorGettingS3DDL(Exception): pass
class ErrorRunningQuery(Exception): pass
class AthenaGenericError(Exception): pass

ERROR_S3_DELETE= "OPTERR-STATEXP-0001"
ERROR_S3_DDL= "OPTERR-STATEX-0002"
ERROR_EXEC_QUERY= "OPTERR-STATEXP-0003"
ERROR_GENERIC= "OPTERR-STATEXP-0003"

def delete_recursive(bucket_name, prefix):
    '''
    Recusively delete all keys with given prefix from the named bucket
    Stolen from http://stackoverflow.com/a/10055320/141084
    Document at https://gist.github.com/rosenhouse/8126048
    '''
    bucket = s3.Bucket(bucket_name)
    bucket.objects.filter(Prefix=prefix).delete()

def delete_s3_folder(task, executions, day_of_the_week, event):
    logger.info("Deleting " + task['s3'] + " recursively")
    bucket = get_s3_bucket_name(task['s3'])
    key = get_s3_key(task['s3'])

    key = key.replace('${day_of_the_week}', day_of_the_week)

    logger.info("Deleting S3 Bucket: " + bucket + ", Key: " + key + " recursively")
    try:
        execution = {'type': "s3", 'status': "SUBMITTED"}
        executions.append(execution)
        execution['command'] = "Delete: " + bucket + ", Key: " + key + " recursively"
        delete_recursive(bucket, key)
        execution['status'] = "SUCCEEDED"
    except Exception as e:
        logger.error("Error Deleting folder from S3: " + str(e))
        raise ErrorS3Delete(e, "Error Deleting folder from S3", ERROR_S3_DELETE)

def get_ddl_from_s3(task):

    try:
        logger.info("Getting S3 object: " + task['script'])

        bucket = get_s3_bucket_name(task['script'])
        key = get_s3_key(task['script'])

        logger.info("Bucket: " + bucket + ", Key: " + key)

        s3_response = s3_client.get_object(Bucket=bucket, Key=key)

        logger.debug(s3_response)

        return s3_response
    except Exception as e:
        logger.error("Error Getting DDL file from S3 " + str(e))
        raise ErrorGettingS3DDL(e, "Error Getting DDL file from S3", ERROR_S3_DDL)

def get_s3_bucket_name(url):
    return url[len('s3://'):url.index('/', len('s3://'))]


def get_s3_key(url):
    return url[url.index('/', len('s3://')) + 1:len(url)]


def execute_task(event, task):
    database = event['database']
    output_location = event['outputLocation']

    day_of_the_week = event['optima_vars']['day_of_the_week']
    bx_batch_id = event['optima_vars']['batchid']
    logger.info("day_of_the_week " + str(day_of_the_week))
    logger.info("bx_batch_id " + str(bx_batch_id))

    executions = []
    task['executions'] = executions

    if 's3' in task:
        delete_s3_folder(task, executions, day_of_the_week, event)

    s3_response = get_ddl_from_s3(task)

    sql_commands = s3_response['Body'].read().decode('utf-8').split(';')

    for j in range(len(sql_commands)):
        sql_command = sql_commands[j].strip()

        sql_command = sql_command.replace('${day_of_the_week}', day_of_the_week).replace('${bx_batch_id}', bx_batch_id)
        logger.info("sql_command: " + str(sql_command))

        if (len(sql_command) > 0):
            executions.append({'type': 'athena', 'query': sql_command, 'status': 'PENDING'})

    for j in range(len(executions)):
        execution = executions[j]
        if execution['status'] == 'PENDING':
            sql_command = execution['query']
            logger.info("Executing: " + sql_command)
            try:
                response = athena.start_query_execution(
                    QueryString=sql_command,
                    QueryExecutionContext={
                        'Database': database
                    },
                    ResultConfiguration={
                        'OutputLocation': output_location,
                        'EncryptionConfiguration': {
                            'EncryptionOption': 'SSE_S3'
                        }
                    }
                )
                execution['status'] = 'SUBMITTED'
                execution['id'] = response['QueryExecutionId']
                execution['start_time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                logger.info("Execution started at " + str(execution['start_time']))
                break
            except Exception as e:
                logger.error("Error Starting DDL Query " + str(e))
                raise ErrorRunningQuery(e, "Error Starting DDL Query", ERROR_EXEC_QUERY)


# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/athena.html#Athena.Client.start_query_execution
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.get_object
def handler(event, context):
    try:
        if event['status'] == 'FAILED':
            return event

        event['status'] = 'SUBMITTED'
        parallel_tasks = event['parallelTasks']

        if len(parallel_tasks) == 0:
            event['status'] = 'SUCCEEDED'
            return event

        for task in parallel_tasks:
            execute_task(event, task)

            if event['status'] == 'FAILED':
                return event
    except Exception as e:
        logger.error("Error Running Athena Tasks In Parallel " + str(e))
        raise AthenaGenericError(e, "Error Running Athena Tasks In Parallel", ERROR_GENERIC)
    return event
