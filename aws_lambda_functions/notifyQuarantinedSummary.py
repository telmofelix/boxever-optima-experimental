import json
import os.path
import gzip
from io import BytesIO
import calendar
from datetime import datetime
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class ErrorQuarantine(Exception): pass

ERROR_QUARANTINE= "OPTERR-QSUM-0001"

REPORT_TITLE = '*Quarantine Summary: '
REPORT_LINE = '\n  {0}. Count: *{1}*'

s3      = boto3.client('s3')
lambdas = boto3.client('lambda')

GCHAT_ARN = os.environ['GCHAT_ARN']

day_of_the_week        = calendar.day_name[datetime.today().weekday()].lower()
bucket                 = os.environ['BUCKET']
quarantineSummaryPaths = [ os.environ['GUEST_QUARANTINE_SUMMARY_PATH'],
                           os.environ['ORDERS_QUARANTINE_SUMMARY_PATH'],
                           os.environ['SESSIONS_QUARANTINE_SUMMARY_PATH'],
                           os.environ['EVENTS_QUARANTINE_SUMMARY_PATH'] ]

def get_quarantine_report_from_s3(bucket, prefix):
    response = s3.list_objects(Bucket=bucket, Prefix=prefix)
    if 'Contents' not in response:
        return None
    keys = response['Contents']
    if (len(keys) < 1):
        return None
    key = keys[0]['Key']
    logger.info("KEY: " + str(key))
    response = s3.get_object(Bucket=bucket, Key=key)
    logger.debug(response)
    if 'Body' in response:
        gzipfile = BytesIO(response['Body'].read())
        gzipfile = gzip.GzipFile(fileobj=gzipfile)
        return gzipfile.read()

def create_quarantine_report(report_content=None):
    report = None
    if report_content is not None:
        report = REPORT_TITLE + bucket + "\n"
        json_lines = report_content.splitlines()
        for json_line in json_lines:
            error = json.loads(json_line)
            report += REPORT_LINE.format(error["validationErrors"], error["count"])
    return report

def notify_gchat(message=None):
    payload = {
        "msg": message
    }
    lambdas.invoke(
        FunctionName   = GCHAT_ARN,
        InvocationType = 'Event',
        Payload        = json.dumps(payload)
    )

def handler(event, context):
    event['notify_quarantine_report'] = False
    quarantine_report = ""
    try:
        for path in quarantineSummaryPaths:
            path = path.replace('${day_of_the_week}', day_of_the_week)
            json_report = get_quarantine_report_from_s3(bucket=bucket, prefix=path)
            if json_report:
                report = create_quarantine_report(json_report)
                if report is not None:
                    quarantine_report += create_quarantine_report(json_report)
        if len(quarantine_report) > 1:
            quarantine_report = REPORT_TITLE + quarantine_report
            notify_gchat(quarantine_report)
            event['quarantine_report_generated'] = True
    except Exception as e:
        logger.error("Error Quarantine Report: " + str(e))
        raise ErrorQuarantine(e, "Error Quarantine Report ", ERROR_QUARANTINE)
    return event
