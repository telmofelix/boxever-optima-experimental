import boto3
import uuid
import calendar
import logging
from datetime import datetime, timedelta
import re
import os
import json
from random import randrange
import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class RunOptimaException(Exception): pass

# ERRORS
RUN_ERROR = "OPTERR-ROP-P1-0001"

BATCH_ID = 'bx_batch_id='

s3_client = boto3.client('s3')
client = boto3.client('stepfunctions')

def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    logger.debug("Run Optima Event: " + str(event))
    state_machine_arn = event['stateMachineArn']
    state_machine_execution_name = str(uuid.uuid4())
    state_machine_input = event['stateMachineInput']

    today = datetime.today()
    day_of_the_week = calendar.day_name[today.weekday()].lower()

    batchid = datetime.now().strftime('%Y-%m-%d')
    process_events_from = get_process_events_from(state_machine_input)
    logger.debug("process_events_from: " + str(process_events_from))
    logger.debug("batchid: " + str(batchid))
    modified_at_date_since = process_events_from.strftime('%Y-%m-%d')
    logger.info("modified_at_date_since: " + str(modified_at_date_since))
    date = datetime.now().strftime('%Y/%m/%d')

    optima_vars = { 'day_of_the_week' : day_of_the_week , 'batchid' : batchid, 'modified_at_date_since' : modified_at_date_since, 'current_date' : date}
    for k, v in state_machine_input.items():
        if isinstance(v,dict):
            v['optima_vars']    = optima_vars
    logger.debug("optima_vars: " + str(optima_vars))
    try:

        # Extra step in case multiple calls
        sleep_time =randrange(0,9)/randrange(1,4)
        logger.info("waiting for: " + str(sleep_time))
        time.sleep(sleep_time)

        # Check if it is already running
        response = client.list_executions(
            stateMachineArn=state_machine_arn,
            statusFilter='RUNNING'
        )

        if response and 'executions' in response and len(response['executions']) > 0:
            logger.error("ERROR: Pipeline already running:" + str(response['executions'][0]) )
            event['status']='ALREADY_RUNNING'
            raise RunOptimaException(None, "ERROR: Pipeline already running:" + str(response['executions'][0]), str(RUN_ERROR))

        response = client.start_execution(
            stateMachineArn=state_machine_arn,
            name=state_machine_execution_name,
            input=json.dumps(state_machine_input)
        )
        event['executionArn'] = response['executionArn']
        event['startDate']    = str(response['startDate'])
        event['status']    = 'SUBMITTED'
    except Exception as e:
        logger.error("ERROR: " + str(e))
        event['status']='FAILED'
        raise RunOptimaException(e, "Error Running Optima", str(RUN_ERROR) + str(e))

    logger.debug("EVENT: " + str(event))
    return event

def get_process_events_from(state_machine_input):
    input_events = state_machine_input['processEventsFromInDays']
    logger.info("get_process_events_from: " + str(input_events))
    if not input_events:
        input_events = datetime.now() - timedelta(days=int(1))
    elif input_events.isdigit():
        input_events = datetime.now() - timedelta(days=int(input_events))
    else:
        diff = get_difference_last_run(state_machine_input['optimaBucket'], state_machine_input['optimaEvents'])
        input_events = datetime.now() - timedelta(days=int(diff))
    logger.info("get_process_events_from RESULT: " + str(input_events))
    return input_events

def get_difference_last_run(bucket_name, events_folder):
    if not events_folder:
        raise RunOptimaException(None, "Error Running Optima", "No Event Folder Defined, check input")
    logger.info("get_difference_last_run: Bucket: " + str(bucket_name) + " events_folder: " + str(events_folder))
    # Read bucket contents
    key = os.path.basename(os.path.normpath(events_folder)) + "/"
    logger.info("key: " + str(key))
    folders = s3_client.list_objects(Bucket=bucket_name, Delimiter='/', Prefix=key)
    latest_run = datetime(1990, 10, 5, 00, 00) # Init in the past
    days = 1
    if folders and folders.get('CommonPrefixes'):
        for obj in folders.get('CommonPrefixes'):
            logger.debug(obj)
            folder = obj['Prefix']
            if BATCH_ID in folder:
                match = re.search(r'\d{4}-\d{2}-\d{2}', folder)
                date = datetime.strptime(match.group(), '%Y-%m-%d')
                logger.debug("date " + str(date))
                if date > latest_run:
                    latest_run = date
        days = abs((datetime.today() - latest_run).days)
        if days <= 0:
            days = 1
    logger.info("days " + str(days))
    return days
