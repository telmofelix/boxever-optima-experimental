import boto3
import os
import json
import logging
from random import randrange
import time

s3      = boto3.client('s3')
lambdas = boto3.client('lambda')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

config_file_form_env = os.environ.get('CONFIG_FILE')

# Exceptions
class ErrorRunningOptima(Exception): pass

# Error codes
ERROR_RUNNING_OPTIMA  = "OPTERR-RUNO-0001"

def handler(event, context):
    try:
        event['status'] = 'PENDING'
        config_file = event['stateMachineConfigFile'] if 'stateMachineConfigFile' in event else config_file_form_env
        logger.info("Getting S3 object: " + config_file)

        bucket = config_file[len('s3://'):config_file.index('/', len('s3://'))]
        key = config_file[config_file.index('/', len('s3://')) + 1:len(config_file)]

        logger.info("Bucket: " + bucket + ", Key: " + key)

        s3_response = s3.get_object(Bucket=bucket, Key=key)
        state_machine_data = s3_response['Body'].read().decode('utf-8')
        event = json.loads(state_machine_data)

        function_name = os.environ['FUNCTION_NAME']

        # Extra step in case multiple calls
        sleep_time =randrange(0,29)/randrange(1,3)
        logger.info("waiting for: " + str(sleep_time))
        time.sleep(sleep_time)

        response = lambdas.invoke(
            FunctionName=function_name,
            Payload=bytearray(json.dumps(event), 'utf8'))

        if not response or response['StatusCode'] != 200:
            event['status']    = 'FAILED'
            logger.error("Error running optima. " + str(e))
            raise ErrorRunningOptima(e, "Error checking pipeline", ERROR_RUNNING_OPTIMA)
        else:
            event['status']    = 'SUBMITTED'
        logger.info("EVENT: " + str(event))
    except Exception as e:
        event['status']    = 'FAILED'
        logger.error("Error running optima. " + str(e))
        raise ErrorRunningOptima(e, "Error checking pipeline", ERROR_RUNNING_OPTIMA)

    return event
