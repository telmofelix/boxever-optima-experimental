"""
Lambda Function which initializes the Optima Data Export

- It reads the bucket and looks for folders containing data
- It filters out any folder in the EXCLUDED_TABLES
- If Full Copy it is not enabled it only select the folders for the current day
- It split the number of folders evenly across the number of lambda functions available
- It generates the required JSON input for each function downstream

"""

import boto3
import os
import logging
import re
from random import shuffle
from datetime import datetime , timedelta
import calendar

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.client('s3')

# ENVIRONMENT VARS
BUCKET_NAME = os.environ['SOURCE_BUCKET_NAME']  # Source Bucker
DEST_BUCKET_NAME = os.environ['DEST_BUCKET_NAME']
# Number of threads to do the key copy
PARALLEL_FUNCTIONS = int(os.environ['PARALLEL_FUNCTIONS'])
# List of tables/folders to skip from export according to customer spec
EXCLUDED_TABLES = os.environ['EXCLUDED_TABLES']

WEEK_DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

# Exceptions
class InitExportError(Exception): pass

# Error codes
ERROR_INIT_EXPORT                 = "OPTERR-INTEXP-0001"
INITIAL_STATUS_FAILED             = "OPTERR-INTEXP-0002"

"""
# Exclude from export folders that are from the older DBs from previous days to increase performance of the pipeline
"""
def exclude_not_day_of_week(folder, day_of_week, full_copy):
    if full_copy or 'events' in folder: # events do not have day of the week
        return False

    for day in WEEK_DAYS:
        if day in folder:
            if day == day_of_week:
                return False
            else:
                return True
    return False

"""
# Exclude customer selected folders from export
"""
def exclude_folder(folder):

    if "trusted_" in folder: # Exclude trusted data set
        return True
    elif not EXCLUDED_TABLES or len(EXCLUDED_TABLES) <= 0:
        return False

    for folder_to_exclude in EXCLUDED_TABLES.split(","):
        if folder_to_exclude in folder:
            return True

    return False

"""
# Split elements into n sublists with equal number of elements. Distribute the items evenly for downstream functions
"""
def split_sublist(input, n):
    if len(input) <= 0:
        return []
    chunks = [{} for _ in range(n)]
    for i, k in enumerate(input):
        chunks[i % n][k] = input[k]
    return chunks


"""
# Generate destination folders for each source folder
"""
def generate_destination_folders(folders,day_of_the_week, date):
    #day_of_the_week corresponds to date
    #there is a policy to delete data older than 6 days, so oldest data is current date -6
    #we can generate the remaining dates from the current date
    date_reference = {}
    for i in range(7):
        date_reference[calendar.day_name[(datetime.today()- timedelta(days=i)).weekday()].lower()]=(datetime.today()- timedelta(days=i)).strftime("%Y-%m-%d")
    date_reference[day_of_the_week]=date
    destination_folders={}
    for folder in folders:
        if folder.split('_')[1] in WEEK_DAYS:
            destination_folders[folder]="_".join(folder.split('_')[2:])+'date='+date_reference[folder.split('_')[1]]
        else:
            destination_folders[folder]=folder.split("/")[0][9:]
    return destination_folders


"""
# Main Lambda Handler
"""
def handler(event, context):

    logger.info("Init Export")
    if event and 'status' in event and event['status'] == 'FAILED':
        logger.error(INITIAL_STATUS_FAILED + ". Initial status is FAILED")
        return event

    export_enabled = True if event['exportEnabled'] == "True" else False
    full_copy =  True if event['exportFullCopy'] == "True" else False # If true we copy current day plus all DBs
    logger.info("export_enabled: " + str(export_enabled) + " full_copy: " + str(full_copy))

    try:

        status = "SUCCESS"
        split_folders = []
        if export_enabled:

            day_of_the_week = event['guestsPipeline']['optima_vars']['day_of_the_week']
            batch_id = event['guestsPipeline']['optima_vars']['batchid']
            logger.info("day_of_the_week: " + day_of_the_week + " batch_id " + batch_id)
            # Get All contents for the buccket
            folders = s3.list_objects(Bucket=BUCKET_NAME, Delimiter='/')
            logger.debug("folders: " + str(folders))

            folder_list = []
            total = 0
            for obj in folders.get('CommonPrefixes'): # Select root level folders only
                logger.debug("obj: " + str(obj))
                folder = obj['Prefix']
                logger.debug("folder: " + folder)
                # Filter excluded or old folders
                if not exclude_folder(folder) and not exclude_not_day_of_week(folder, day_of_the_week, full_copy):
                    is_data_folder = re.search("^[a-f0-9]{1,8}_", folder) # matches random prefix i.e f47a023_
                    if is_data_folder:
                        if not full_copy and 'events' in folder:
                            folder = folder + "bx_batch_id=" + batch_id + "/"

                        folder_list.append(folder)
                        total = total + 1

            logger.info("total: " + str(total))
            #Generate destination folders
            folder_list=generate_destination_folders(folder_list,day_of_the_week,batch_id)
            # Split elements across the lists
            split_folders = split_sublist(folder_list, int(PARALLEL_FUNCTIONS))
            logger.info("split_folder len: " + str(len(split_folders)))

            # Shuffle the list to avoid similar items with similar size to be sent to the same function
            shuffle(split_folders)
            logger.debug("split_folders: " + str(split_folders))
        else:
            logger.info("Skipping export since it is not enabled")
            status = "SKIPPED"

        ret_val = {"status": status}

        # Create a JSON structure for each sub function containing the buckets, the folders to process and status
        for index in range(1, PARALLEL_FUNCTIONS + 1):
            folders = {}
            if index <= len(split_folders): # in case we have more functions than folders
                folders = split_folders[index - 1]

            json_folder = {
                "source_bucket": BUCKET_NAME,
                "dest_bucket": DEST_BUCKET_NAME,
                "folders": folders,
                "status": status
            }
            index_str = "folder" + str(index)
            ret_val[index_str] = json_folder
        logger.info("ret_val: " + str(ret_val))
        return ret_val
    except Exception as e:
        logger.error("Error Initializing Export. Exception: " + str(e))
        raise InitExportError(e, "Error Initializing Export " + str(e), ERROR_INIT_EXPORT)