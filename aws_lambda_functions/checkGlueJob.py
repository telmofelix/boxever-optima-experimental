import boto3
import logging
import datetime
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class ErrorCheckingGlueJob(Exception): pass

# Error codes
CHECK_GLUE_JOB_ERROR  = "OPTERR-CHKG-0001"


def handler(event, context):

    if event['status'] != 'FAILED':
        try:
            glue = boto3.client('glue')
            response = glue.get_job_run(
                JobName = event['jobName'],
                RunId   = event['jobId']
            )

            event['status'] = response['JobRun']['JobRunState']
            logger.info(str(event['jobId']) + " STATUS: " + str(event['status']))

            now = datetime.datetime.now()
            start = datetime.datetime.strptime(event['start_time'], "%Y-%m-%d %H:%M:%S")
            max_time = int(event['timeout'])
            minutes_diff = (now - start).total_seconds() / 60.0
            logger.debug("now " + str(now) + "start " + str(start) + "max_time " + str(max_time) + " minutes_diff" + str(minutes_diff))
            if minutes_diff > max_time: # timeout
                logger.error("Execution " + event['jobId'] + " has timeout after " + str(minutes_diff) + " seconds")
                event['status'] = "TIMEOUT"
                return event

        except Exception as e:
            logger.error("Error checking glue job. Job: " + event['jobName'] + ", instance id: " + event['jobId'])
            logger.error(str(e))
            raise ErrorCheckingGlueJob(e, "Error starting glue job", CHECK_GLUE_JOB_ERROR)
    logger.info("Check glue job " + event['jobId'] + ". Status: " + event['status'])
    return event
