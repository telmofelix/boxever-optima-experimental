"""
Functions that retrieve offers to be used in Optima.
- It queries the Offers Service and performs a search to get all the offers
- It retrieves the offers in JSON and generates a CSV file

"""
import os
import logging
from apiUtils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

API_SCOPE = "bx_offers_search"
OFFERS_API_URL = os.environ['OFFERS_API_URL']

"""
# Main Handler 
"""
def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    settings = event['offersReadData']

    if 'status' in settings and settings['status'] == 'SKIPPED':
        event['status'] = 'SKIPPED'
        return event

    secret = get_secret()

    response = call_service(secret,API_SCOPE,OFFERS_API_URL)

    if response:
        header = ['ref','name','description','createdAt','createdBy','modifiedAt','modifiedBy','archived']
        output_file = save_s3(response, header, filename='offers.csv')

        event['status'] = 'SUCCESS'
        event['offers_location'] = output_file
    else:
        event['status'] = 'SKIPPED'
        event['status_reason'] = 'No offers found for ' + CLIENT_KEY

    return event
