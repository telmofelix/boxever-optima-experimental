"""

Method that receives the JSON input with the configuration and:

- Commits the code to Code Commit Repo in a new branch
- Creates a PR
- Merge PR to master

"""
import boto3
import logging
import os, tempfile
import uuid
import time

# LOGGER
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#EXCEPTIONS
class CommitFilesException(Exception): pass

class MergeFilesException(Exception): pass

# ERRORS
COMMIT_ERROR = "OPTERR-LMCC-P1-0001"
MERGE_ERROR  = "OPTERR-LMCC--P1-0002"

s3 = boto3.resource('s3')
client = boto3.client('s3')
code_commit_client = boto3.client('codecommit')
BUCKET = os.environ['BUCKET']
bucket = s3.Bucket(BUCKET)


def handler(event, context):

    if event['status'] != 'FAILED':
        logger.info("Starting handler")
        logger.debug(str(event))
        conf = event['looker_conf']
        prefix = event['prefix']
        logger.info("prefix: " + prefix)
        temp_folder = prefix + 'temp_folder_key'
        files = get_commit_files(conf[temp_folder])

        commit_files(files, conf, prefix)
        event['status'] = 'SUCCESS'
        logger.info("Process Completed!")

    return event

def get_commit_files(temp_prefix):
    logger.info("Get files from: " + str(temp_prefix))
    files = []
    folders =  bucket.objects.filter(Prefix=temp_prefix)
    for obj in folders:
        path, filename = os.path.split(obj.key)
        body = obj.get()['Body'].read()
        files.append({'name': filename, 'data' : body })
    logger.debug("FILES: " + str(files))
    return files

""" Commit files to the Repo creating a new branch """
def commit_files(filelist, conf, prefix):

    logger.info("commit_files" )

    commitId = None
    try:
        # Get master last commit ID
        master_branch = code_commit_client.get_branch(repositoryName=conf[prefix + 'repo_name'],
                                                      branchName='master')
        commitId = master_branch['branch']['commitId']
    except code_commit_client.exceptions.BranchDoesNotExistException as e:
        response = code_commit_client.put_file(repositoryName=conf[prefix + 'repo_name'],
                                               branchName='master',
                                               fileContent=b' ',
                                               filePath='init',
                                               commitMessage='Lambda Auto Commit Looker',
                                               name='Lambda')
        commitId = code_commit_client.delete_file(repositoryName=conf[prefix + 'repo_name'],
                                   branchName='master',
                                   parentCommitId=response['commitId'],
                                   filePath='init',
                                   name='Lambda')
    try:
        # Create new branch

        logger.info("commitId master: " + str(commitId))
        new_branch_name = prefix + str(uuid.uuid1())

        branch = code_commit_client.create_branch(
            repositoryName=conf[prefix + 'repo_name'],
            branchName=new_branch_name,
            commitId=commitId)
        logger.info("Branch: " + str(branch))

        # Put files into the new branch
        for file in filelist: # BUG: create_commit does not currently work, we need to put files one by one which is very slow
            logger.info("Got file: " + str(file))
            data = file['data']
            logger.debug("Pushing data: " + str(data))

            retries = 0
            try:
                commit(conf, new_branch_name, data, file['name'], commitId, prefix)
            except Exception as e:
                if e.__class__ == code_commit_client.exceptions.SameFileContentException: # If the file has not been changed we continue
                    pass
                else:
                    logger.error("ERROR COMMIT: " + str(e))
                    time.sleep(1)
                    if retries < 2:
                        commit(conf, new_branch_name, data, file['name'], commitId, prefix)
                        retries = retries + 1

            # get new commit
            master_branch = code_commit_client.get_branch(repositoryName=conf[prefix + 'repo_name'],
                                                          branchName=new_branch_name)

            commitId = master_branch['branch']['commitId']

    except BaseException as e:
        logger.error("Error pushing files to code commit")
        logger.error(str(e))
        if new_branch_name:
            code_commit_client.delete_branch(repositoryName=conf[prefix + 'repo_name'], branchName=new_branch_name)
        raise CommitFilesException(e, "Error pushing files to code commit", str(COMMIT_ERROR) +  "\n" + str(e))

    merge_code(new_branch_name, conf, prefix)

    logger.info("File commit/push/merge completed.")


def merge_code(new_branch_name, conf, prefix):
    try:
        logger.info("commit completed, creating PR...")
        pr_req = code_commit_client.create_pull_request(title='Lambda Auto Merge ' + new_branch_name
                                                        ,description='Merging Pipeline branch ' + new_branch_name
                                                        ,targets=[{'repositoryName': conf[prefix + 'repo_name'],
                                                                   'sourceReference': new_branch_name, 'destinationReference': 'master'}])

        logger.info("PR " + str(pr_req) + " created, merging...")

        code_commit_client.merge_pull_request_by_fast_forward(
            pullRequestId=pr_req['pullRequest']['pullRequestId'],
            repositoryName=conf[prefix + 'repo_name'])

        logger.info("Merge completed")
        code_commit_client.delete_branch(repositoryName=conf[prefix + 'repo_name'], branchName=new_branch_name)

    except BaseException as e:
        logger.error("Error merging PR")
        logger.error(str(e))
        if e.__class__ != code_commit_client.exceptions.ManualMergeRequiredException and new_branch_name:
            code_commit_client.delete_branch(repositoryName=conf[prefix + 'repo_name'], branchName=new_branch_name)
        raise MergeFilesException(e, "Error pushing files to code commit", str(MERGE_ERROR) +  "\n" + str(e))


def commit(conf, new_branch_name, data, file, commitId, prefix):
    code_commit_client.put_file(repositoryName=conf[prefix + 'repo_name'],
                                branchName=new_branch_name,
                                fileContent=data,
                                filePath=file,
                                parentCommitId=commitId,
                                commitMessage='Lambda Auto Commit Looker',
                                name='Lambda')
