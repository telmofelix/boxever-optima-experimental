"""
Functions that retrieve decision variants to be used in Optima.
- It queries the Decision Variants Service and performs a search to get all the decision variants
- It retrieves the decision variants in JSON and generates a CSV file

"""
import os
import logging
from apiUtils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

API_SCOPE = "bx_decision_variants_search"
DECISION_VARIANTS_API_URL = os.environ['DECISION_VARIANTS_API_URL']

"""
# Main Handler 
"""
def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    settings = event['decisionVariantsReadData']

    if 'status' in settings and settings['status'] == 'SKIPPED':
        event['status'] = 'SKIPPED'
        return event

    secret = get_secret()

    response = call_service(secret,API_SCOPE,DECISION_VARIANTS_API_URL)

    if response:
        header = ['archived','decisionModelDefinitionRef','inProduction','name','ref']
        output_file = save_s3(response, header, filename='decision_variants.csv')

        event['status'] = 'SUCCESS'
        event['decision_variants_location'] = output_file
    else:
        event['status'] = 'SKIPPED'
        event['status_reason'] = 'No decision variants found for ' + CLIENT_KEY

    return event
