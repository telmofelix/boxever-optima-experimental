"""

Method that receives the JSON input with the configuration and:

- Reads the template files from S3
- Process all LookML templates and generates all source code automatically for the data quality project
- Save Them in S3

"""
import boto3
import logging
import os, tempfile
import datetime

# LOGGER
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#EXCEPTIONS
class ReadFileException(Exception): pass

class TemplateProcessingException(Exception): pass

class WriteFilesException(Exception): pass

class ProcessModelException(Exception): pass


# ERRORS
READ_FILE_ERROR = "OPTERR-DQPT-P1-0001"
TEMPLATE_ERROR = "OPTERR-DQPT-P2-0002"
FILE_ERROR = "OPTERR-DQPT-P1-0003"
MODEL_ERROR = "OPTERR-DQPT-P1-0005"

s3 = boto3.resource('s3')
BUCKET = os.environ['BUCKET']

# Template values
TABLE = "[[TABLE]]"
TABLE_NAME = "[[TABLE_NAME]]"
FIELD = "[[FIELD]]"
FILTER = "[[FILTER]]"
FILTER_NAME = "[[FILTER_NAME]]"
VALUE = "[[VALUE]]"
YEAR = "[[YEAR]]"
YEAR_NAME = "[[YEAR_NAME]]"
MONTH = "[[MONTH]]"
MONTH_NAME = "[[MONTH_NAME]]"
SUB_TEMPLATE = "[[SUB_TEMPLATE]]"

ROW = "[[ROW]]"
ROW_2 = "[[ROW_2]]"
ROW_6 = "[[ROW_6]]"

now = datetime.datetime.now()
ALL_TIME = now.year-100
CURRENT_YEAR = now.year

YEARS = list(dict.fromkeys([ALL_TIME, CURRENT_YEAR])) # Remove dups
MONTHS = list(dict.fromkeys([1]))


EXPLORE_TEMPLATE = "explore: stats_[[TABLE]]_[[FIELD]] {hidden: yes}"

def handler(event, context):

    if event['status'] != 'FAILED':
        event['status'] = 'RUNNING'
        logger.info("Starting handler")
        logger.debug(str(event))

        logger.debug("data quality generate looker, event: " + str(event))
        conf = event['looker_conf']

        read_years_months_from_spec(conf)

        #Remove S3 temp folder
        delete_temp_folder(conf['data_quality_temp_folder_key'])

        #VIEWs
        view_template = read_file(conf['data_quality_template_view_key'])
        logger.debug("view_template: " + str(view_template))

        filter_template = read_file(conf['data_quality_filter_template'])
        logger.debug("sub_template: " + str(filter_template))

        dashboard_template = read_file(conf['data_quality_dashboard_template'])
        logger.debug("dashboard_template: " + str(dashboard_template))

        #Process views
        view_templates = process_template(view_template, event['data_quality'], "stats_views", ".view.lkml", filter_template, conf['data_quality_omit_fields'], conf['data_quality_split_by'])
        save_files(view_templates, conf['data_quality_temp_folder_key'])

        #DASHBOARDs
        dash_template = read_file(conf['data_quality_template_dashboard_key'])
        logger.debug("dash_template: " + str(dash_template))

        #Process dashboards
        dash_templates = process_template(dash_template, event['data_quality'], "data_quality", ".dashboard.lookml", dashboard_template, conf['data_quality_omit_fields'], conf['data_quality_split_by'])
        save_files(dash_templates, conf['data_quality_temp_folder_key'])

        # Summary dashboard
        summary = read_file(conf['data_quality_summary_dashboard_key'])
        logger.debug("summary: " + str(summary))
        save_files({'data_quality__summary.dashboard.lookml': summary}, conf['data_quality_temp_folder_key'])

        #MODEL
        model = read_file(conf['data_quality_template_model_key'])
        model = process_model(model, event['data_quality'], conf['data_quality_omit_fields'])
        logger.debug("model: " + str(model))
        file_name = "data_quality.model.lkml"
        save_files({file_name: model}, conf['data_quality_temp_folder_key'])

        event['status'] = 'SUCCESS'
        event['prefix'] = 'data_quality_' # Set for code commit next step
        logger.info("Process Completed!")

    return event

def read_years_months_from_spec(conf):
    years = conf['data_quality_report_years']
    months = conf['data_quality_report_months']
    logger.info("years: " + str(years))
    logger.info("months: " + str(months))

    if years and months:
        if "," in years:
            years = years.split(",")
        else:
            years = [years]
        if "," in months:
            months = months.split(",")
        else:
            months = [months]

        for year in years:
            year = int(year)
            val = now.year - year;
            if val not in YEARS:
                YEARS.append(val)
        for month in months:
            month = int(month)
            val = now.month - month;
            if val not in MONTHS:
                MONTHS.append(val)

def read_file(key):
    try:
        logger.info("Reading file " + key)
        obj = s3.Object(BUCKET, key)
        return obj.get()['Body'].read().decode('utf-8')
    except BaseException as e:
        logger.error("Error Getting File from S3")
        logger.error(str(e))
        raise ReadFileException(e, "Error Getting File from S3", str(READ_FILE_ERROR))

""" Process the views and the dashboards LookML """
def process_template(template, entities, type, extension, sub_template, omit_spec, split_by):
    processed_files = {}
    try:
        logger.info("process_template, entities " + str(entities))
        logger.debug("sub_template: " + str(sub_template))
        omit_fields = get_omit_fields(omit_spec)

        for entity, subfield in entities.items():
            logger.info("subfield: " + str(subfield))
            columns = subfield['columns']

            logger.info(" columns: " + str(columns))
            for field in columns:
                process_view_field_template(field, entity, template, sub_template, subfield, type,
                                            omit_fields, processed_files, extension, split_by)

        logger.debug("process_template ret: " + str(processed_files))
        return processed_files
    except BaseException as e:
        logger.error("Error Processing Templates")
        logger.error(str(e))
        raise TemplateProcessingException(e, "Error Processing Templates", str(TEMPLATE_ERROR) +  "\n" + str(e))

def process_view_field_template(field, entity, template, sub_template, subfield, type, omit_fields, processed_files, extension, split_by):
    # skip dates since there are derived fields, TODO: get type from schema service but timestamp are currently string
    if "date" not in field and "_at" not in field and (not omit_fields or field not in omit_fields):
        logger.info("processing entity: " + str(entity) + " Field: " + str(field) + " type: " + str(type) + " split_by: " + str(split_by))
        temp = template
        row = 0
        row_2 = 2
        row_6 = 6
        temp = temp.replace(TABLE, entity)
        temp = temp.replace(FIELD, field)
        temp = temp.replace(TABLE_NAME,''.join(x for x in entity.title() if not x.isspace()) )
        temp = temp.replace(ROW, str(row))
        temp = temp.replace(ROW_2, str(row_2))
        temp = temp.replace(ROW_6, str(row_6))


        if sub_template and 'filter' in subfield and 'filter_fields' in subfield:
            filter = subfield['filter']
            filter_fields = subfield['filter_fields']

            if filter and filter_fields:
                logger.debug("filter_fields: " + str(filter_fields))
                process_filter_temp = sub_template
                filter_fields_templates = []
                titles = []
                for filter_field in filter_fields:
                    for year in YEARS:
                        year_name = str(year) if year > 2000 else ""
                        for month in MONTHS:
                            month_name = datetime.date(year, month, 1).strftime('%B') if month > 1 else ""
                            if year == ALL_TIME: # Do not split by month for all the time
                                month = 1
                            process_filter_temp = sub_template
                            process_filter_temp = process_filter_temp.replace(TABLE,entity)
                            process_filter_temp = process_filter_temp.replace(TABLE_NAME,''.join(x for x in entity.title() if not x.isspace()) )
                            process_filter_temp = process_filter_temp.replace(FIELD, field)
                            process_filter_temp = process_filter_temp.replace(FILTER, filter)
                            process_filter_temp = process_filter_temp.replace(FILTER_NAME, ''.join(x for x in filter.title() if not x.isspace()))
                            process_filter_temp = process_filter_temp.replace(VALUE, filter_field)
                            process_filter_temp = process_filter_temp.replace(YEAR, str(year))
                            process_filter_temp = process_filter_temp.replace(YEAR_NAME, year_name)
                            process_filter_temp = process_filter_temp.replace(MONTH_NAME, month_name)
                            process_filter_temp = process_filter_temp.replace(MONTH, str(month))

                            title = filter + filter_field + str(year) + str(month)
                            if type == "data_quality":
                                row = row + 11
                                row_2 = row + 2
                                row_6 = row + 6
                                process_filter_temp = process_filter_temp.replace(ROW, str(row))
                                process_filter_temp = process_filter_temp.replace(ROW_2, str(row_2))
                                process_filter_temp = process_filter_temp.replace(ROW_6, str(row_6))
                            logger.debug("process_filter_temp: " + str(process_filter_temp))
                            if title not in titles:
                                filter_fields_templates.append(process_filter_temp  + "\n")
                                titles.append(title)
                            if year == ALL_TIME:
                                break
                        if split_by == 'month':
                            generate_file(split_by, temp, entity, filter_fields_templates, field, year_name, month_name, extension, processed_files, filter_field, type)
                    if split_by == 'year':
                        generate_file(split_by, temp, entity, filter_fields_templates, field, year_name, None, extension, processed_files, filter_field, type)
                if split_by == 'filter':
                    generate_file(split_by, temp, entity, filter_fields_templates, field, None, None, extension, processed_files, None, type)
            if split_by == 'field':
                generate_file(split_by, temp, entity, None, field, None, None, extension, processed_files, None, type)
        else:
            generate_file(split_by, temp, entity, None, field, None, None, extension, processed_files, None, type)

def generate_file(split_by, template, entity, filter_fields_templates, field, year, month, extension, processed_files, filter_field, type):
    if filter_fields_templates:
        temp = template.replace(SUB_TEMPLATE, ''.join(filter_fields_templates)).strip()
    else:
        split_by = 'field'
        temp = template.replace(SUB_TEMPLATE, "")

    if split_by == 'filter':
        file_name = type + "_" + entity + "_" + field + "_" + filter_field + extension
    elif split_by == 'year':
        file_name = type + "_" + entity + "_" + field + "_" + filter_field + "_" + year + extension
    elif split_by == 'month':
        file_name = type + "_" + entity + "_" + field + "_" + filter_field + "_" + year + "_" + month + extension
    else:
        file_name = type + "_" + entity + "_" + field + extension

    logger.info("Adding template: " + str(file_name) + " for field: " + str(field))
    logger.debug(temp)
    processed_files[file_name] = temp

def get_omit_fields(omit_spec):
    fields = omit_spec
    if fields:
        ret_val = fields.split(",")
        return  ret_val

def delete_temp_folder(folder_prefix):
    logger.info("Deleting folder: " + folder_prefix)
    s3.Bucket(BUCKET).objects.filter(Prefix=folder_prefix).delete()
    logger.info("Deleted folder: " + folder_prefix)

def save_files(files, prefix):
    try:
        logger.debug("save_files: " + str(files))
        for filename, data in files.items():
            full_name = prefix + filename
            s3.Object(BUCKET, full_name).put(Body=data)

    except BaseException as e:
        logger.error("Error Writing files to temp")
        logger.error(str(e))
        raise WriteFilesException(e, "Error writing files to temp", str(FILE_ERROR))

""" Process the main model file by adding references to all the files previously created """
def process_model(model, entities, omit_spec):
    try:
        logger.info("Processing model...")
        omit_fields = get_omit_fields(omit_spec)
        logger.debug("process_model, entities " + str(entities))
        for entity, subfield in entities.items():
            columns = subfield['columns']
            logger.debug(" columns: " + str(columns) + " filter: " + str(filter))
            for field in columns:
                if "date" not in field and "_at" not in field and (not omit_fields or field not in omit_fields): # skip dates since they are derived fields
                    logger.info("processing entity: " + str(entity) + " Field: " + str(field))
                    new_explore = EXPLORE_TEMPLATE.replace(FIELD, field).replace(TABLE, entity)
                    logger.debug("new_explore: " + str(new_explore))
                    model = model + "\n" + str(new_explore)

        return model

    except BaseException as e:
        logger.error("Error processing model")
        logger.error(str(e))
        raise ProcessModelException(e, "Error creating model file", str(MODEL_ERROR) +  "\n" + str(e))