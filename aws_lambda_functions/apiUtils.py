"""
Auxiliary Functions for api querying
- It retrieves the secret from Secret Manager
- It obtains the bearer token for oauth
- It queries the specified api
- It stores in s3 the results

"""
from botocore.vendored import requests
import csv
import json
import logging
import boto3
import os
from string import ascii_lowercase
from random import choice

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class GetSecretException(Exception): pass
class APICallException(Exception): pass
class SaveFileException(Exception): pass

# ERRORS
SECRET_ERROR = "OPTERR-ACSOF-P1-0001"
API_ERROR = "OPTERR-ACSOF-P1-0002"
S3_ERROR = "OPTERR-ACSOF-P1-0003"

secret_name = os.environ['SECRET_NAME']
region_name = os.environ['REGION']
OUTPUT_FILE_LOCATION = os.environ['OUTPUT_FILE_LOCATION']
AUTH_URL = os.environ['AUTH_URL']
AUTH_HEADERS = {'Content-Type' : "application/x-www-form-urlencoded", "Accept" : "application/json"}
CLIENT_KEY = os.environ['CLIENT_KEY']

session = boto3.session.Session()
secret_client = session.client(
    service_name='secretsmanager',
    region_name=region_name)
s3_client = boto3.client('s3')

"""
# API Call to AWS Secret Manager to get the system credentials 
"""
def get_secret():

    logger.info("Getting Secret: " + str(secret_name))
    try:

        get_secret_value_response = secret_client.get_secret_value(
            SecretId=secret_name
        )
        logger.debug(get_secret_value_response)
        # Decrypts secret using the associated KMS CMK.
        if 'SecretString' in get_secret_value_response:
            secret = json.loads(get_secret_value_response['SecretString'])
            return secret
    except Exception as e:
        logger.error("ERROR: " + str(e))
        raise GetSecretException(e, "Error Getting Secret ", str(SECRET_ERROR) + str(e))


"""
# Get Access Token by calling OAuth Service. Sometimes the key id is the client ID and sometimes is passed separately
"""
def get_access_token(clientKey, secret, api_scope):

    logger.info("get_access_token, KeyID: " + clientKey)
    # POST Request Payload
    data={"grant_type" : "client_credentials", "scope" : api_scope, "expires_in" : "600"}

    if CLIENT_KEY: # If client Key is passed we add it to the payload
        data['clientKey'] = CLIENT_KEY

    resp = requests.post(AUTH_URL, headers=AUTH_HEADERS, data=data, auth=(clientKey, secret))
    logger.debug(resp.text)
    body = json.loads(resp.text)
    if 'access_token' not in body:
        raise GetSecretException(None, "Couldn't retrive OAuth Token: " + str(body), str(SECRET_ERROR))
    access_token = body['access_token']
    return access_token

"""
# Function that makes the HTTP Call
"""
def call_service(secret, api_scope, api_url):
    try:
        # Get access token from OAuth service
        access_token = get_access_token(secret['clientKey'], secret['secret'], api_scope)

        logger.info("calling: " + api_url)

        headers = {"Authorization": "Bearer {0}".format(access_token)}

        resp = requests.get(api_url, headers=headers).json()
        logger.debug("resp: " + str(resp))

        if 'status'in resp and resp['status'] != 200:
            raise APICallException(None, "Error Calling API: " + str(resp), str(API_ERROR))

        return resp

    except Exception as e:
        logger.error("ERROR: " + str(e))
        raise APICallException(e, "Error Calling API ", str(API_ERROR) + str(e))

"""
# Save response to S3
"""
def save_s3(response, header, filename):
    logger.info("Saving file to S3...")
    try:

        # open a file for writing
        tmp_filename=generate_string()
        with open('/tmp/'+tmp_filename+'.csv', 'w') as data:
            # create the csv writer object
            csvwriter = csv.writer(data)
            csvwriter.writerow(header)
            for item in response['items']:
                logger.debug("ITEM: " + str(item))
                row= [item[key] for key in header]
                csvwriter.writerow(row)

        bucket = OUTPUT_FILE_LOCATION[len('s3://'):OUTPUT_FILE_LOCATION.index('/', len('s3://'))]
        key = OUTPUT_FILE_LOCATION[OUTPUT_FILE_LOCATION.index('/', len('s3://')) + 1:len(OUTPUT_FILE_LOCATION)] + filename
        logger.info("Uploading bucket: " + str(bucket) + " key: " + str(key))

        with open('/tmp/'+tmp_filename+'.csv', 'rb') as f:
            s3_client.upload_fileobj(f, bucket, key)

        return OUTPUT_FILE_LOCATION

    except Exception as e:
        logger.error("ERROR: " + str(e))
        raise SaveFileException(e, "Error Saving File S3 ", str(S3_ERROR) + str(e))


"""
# Generates a random string of fixed length
"""
def generate_string(string_length=5):
    letters = ascii_lowercase
    return ''.join(choice(letters) for i in range(string_length))