import boto3
import logging

# Sample event:
# {
#    "outputLocation": "s3://path/to/delete"
# }
s3            = boto3.resource('s3')
locationField = "statsOutputLocation"

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class DeleteFileError(Exception): pass

# Error codes
ERROR_DELETING_FOLDER  = "OPTERR-DLTF-0001"

def handler(event, context):
    logger.info("Remove S3 folder: " + event[locationField])
    status = "SUCCESS"
    try:
        location   = event[locationField][5:]

        day_of_the_week = event['optima_vars']['day_of_the_week']

        location = location.replace('${day_of_the_week}', day_of_the_week)

        bucket_name = location[:location.index('/')]
        prefix     = location[location.index('/') + 1:]

        bucket = s3.Bucket(bucket_name)
        bucket.objects.filter(Prefix=prefix).delete()
        logger.info("folder " + str(prefix) + " deleted")
    except Exception as e:
        logger.error("Error deleting S3 folder " + event[locationField] + ". Exception: " + str(e))
        raise DeleteFileError(e, "Error deleting S3 folder " + event[locationField], ERROR_DELETING_FOLDER)
    logger.info("Folder " + event[locationField] + " deleted")
    return {
        'status' : status,
        'folder' : locationField
    }

