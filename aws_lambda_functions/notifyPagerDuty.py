import boto3
import os
from json import dumps
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

sns      = boto3.client('sns')
topicArn = os.environ['TOPIC_ARN']

# Exceptions
class ErrorSendingMessageSNS(Exception): pass

ERROR_SENDING_MESSAGE= "OPTERR-NGSNS-0001"

def handler(event, context):
    logger.debug("event: " + str(event))
    try:

        response = sns.publish(
            TopicArn = topicArn,
            Message  =  dumps(event),
        )

        logger.info("SNS Response" + str(response))

        task_name = "Undefined"

        if 'database' in event:
            task_name = event['database']
        elif 'jobName' in event:
            task_name = event['jobName']

        result = {'taskName' : task_name, 'msg': 'State Machine Failed', 'status': 'FAILED'}
        logger.info("SNS result" + str(result))
    except Exception as e:
        logger.error("Error publish SNS Error: " + str(e))
        raise ErrorSendingMessageSNS(e, "Error publish event to SNS", ERROR_SENDING_MESSAGE)

    return result
