"""
Functions that retrieve decisions to be used in Optima.
- It queries the Decisions Service and performs a search to get all the decisions
- It retrieves the decisions in JSON and generates a CSV file

"""
import os
import logging
from apiUtils import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

API_SCOPE = "bx_decisions_search"
DECISIONS_API_URL = os.environ['DECISIONS_API_URL']

"""
# Main Handler 
"""
def handler(event, context):

    if event and 'status' in event and event['status'] == 'FAILED':
        return event

    settings = event['decisionsReadData']

    if 'status' in settings and settings['status'] == 'SKIPPED':
        event['status'] = 'SKIPPED'
        return event

    secret = get_secret()

    response = call_service(secret,API_SCOPE,DECISIONS_API_URL)

    if response:
        header = ['archived','name','ref']
        output_file = save_s3(response, header, filename='decisions.csv')

        event['status'] = 'SUCCESS'
        event['decisions_location'] = output_file
    else:
        event['status'] = 'SKIPPED'
        event['status_reason'] = 'No decisions found for ' + CLIENT_KEY

    return event
