import boto3
import json
import logging

s3 = boto3.client('s3')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class InitialStatusFailed(Exception): pass
class ErrorGettingStateMachineInput(Exception): pass

# Error codes
INITIAL_STATUS_FAILED             = "OPTERR-SMI-0001"
ERROR_GETTING_STATE_MACHINE_INPUT = "OPTERR-SMI-0002"

def handler(event, context):
    if event and 'status' in event and event['status'] == 'FAILED':
        logger.error(INITIAL_STATUS_FAILED + ". Initial status is FAILED")
        return event

    try:
        input = event['InputSrc']
        logger.info("Input: " + str(input))
        config_file = input['stateMachineConfigFile']
        logger.info("Getting S3 object: " + config_file)

        bucket = config_file[len('s3://'):config_file.index('/', len('s3://'))]
        key = config_file[config_file.index('/', len('s3://')) + 1:len(config_file)]

        logger.info("Bucket: " + bucket + ", Key: " + key)

        s3_response = s3.get_object(Bucket=bucket, Key=key)
        state_machine_data = s3_response['Body'].read().decode('utf-8')

        state_machine_input = json.loads(state_machine_data)
        logger.info("state_machine_input: " + str(state_machine_input))

        # get pipeline name
        pipeline = input['pipeline']
        logger.info("pipeline: " + str(pipeline))

        # set previous status
        state_machine_input[pipeline]['status'] = event['status']

        new_input = state_machine_input[pipeline]['stateMachineInput']
        logger.info("new_input: " + str(new_input))

        vars = event['optima_vars']
        vars_sql = {'day_of_the_week': vars['day_of_the_week'], 'batchid': vars['batchid']}
        logger.info("vars_sql: " + str(vars_sql))
        # Set the optima vars to be used in the sub pipeline
        state_machine_input[pipeline]['optima_vars'] = vars

        # # Update the status and add vars from master pipeline
        for k, v in event.items():
            if isinstance(v, dict) and 'status' in v:
                new_input[k]['status'] = v['status']
                new_input[k]['timeout'] = v['timeout']
                if 'Etl' not in k or 'DataExtensions' in k:
                    new_input[k]['optima_vars'] = vars_sql
        logger.info("FINAL state_machine_input: " + str(state_machine_input))

    except Exception as e:
        state_machine_input = event
        state_machine_input['status'] = 'FAILED'
        logger.error("Error running optima. " + str(e))
        raise ErrorGettingStateMachineInput(e, "Error getting state machine input", ERROR_GETTING_STATE_MACHINE_INPUT)

    return state_machine_input
