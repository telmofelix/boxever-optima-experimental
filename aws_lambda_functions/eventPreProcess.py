import boto3
import logging

s3 = boto3.resource('s3')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Exceptions
class NoBatchId(Exception): pass
class ErrorPreprocessingEvent(Exception): pass

# Error codes
INITIAL_STATUS_FAILED      = "OPTERR-EPP-0001"
NO_BATCH_ID                = "OPTERR-EPP-0002"
ERROR_PRE_PROCESSING_EVENT = "OPTERR-EPP-0003"

def deleteBatchIdFolder(event):
    bx_batch_id = event['jobArgs']["--bx_batch_id"]
    path = event["destinationPath"].replace("s3://","") + "bx_batch_id=" + bx_batch_id + "/"
    bucket_name = path[:path.index('/')]
    prefix = path[path.index('/') + 1:]
    bucket = s3.Bucket(bucket_name)
    logger.info("delete: " + bucket_name + "/" + prefix)
    bucket.objects.filter(Prefix=prefix).delete()


def handler(event, context):
    if event['status'] == 'FAILED':
        logger.error(INITIAL_STATUS_FAILED + ". Initial status is FAILED")
        return event

    job_args = event['jobArgs']
    try:
        if "--bx_batch_id" in job_args and job_args["--bx_batch_id"] is not None:
            deleteBatchIdFolder(event)
        else:
            logger.error("ERROR: No batch Id provided for ETL job. " + str(job_args))
            raise NoBatchId("No batch Id provided for ETL job. job args: " + str(job_args), NO_BATCH_ID)
    except Exception as e:
        logger.error("Error pre processing event. Error: " + str(e))
        raise ErrorPreprocessingEvent(e, "Error pre processing event", ERROR_PRE_PROCESSING_EVENT)

    return event




