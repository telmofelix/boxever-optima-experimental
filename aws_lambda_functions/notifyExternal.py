"""

Function that sends a message to a SNS topic when the pipeline completes.

"""
import boto3
import os
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

sns      = boto3.client('sns')
topicArn = os.environ['SNS_ARN']

MSG = "Boxever pipeline completed successfully. {0}"

# Exceptions
class ErrorSendingMessageSNS(Exception): pass

ERROR_SENDING_MESSAGE= "OPTERR-EXSNS-0001"

def handler(event, context):
    logger.debug("event: " + str(event))
    try:

        should_notify = True if event['notifySNS'] == "True" else False

        if should_notify:

            msg = MSG.format(f"{datetime.datetime.now():%Y-%m-%d %H:%M:%S}")

            response = sns.publish(
                TopicArn = topicArn,
                Message  =  msg,
            )

            logger.info("SNS Response" + str(response))
        else:
            logger.info("External customer notification skipped")


    except Exception as e:
        logger.error("Error publish SNS Error: " + str(e))
        raise ErrorSendingMessageSNS(e, "Error publish event to SNS", ERROR_SENDING_MESSAGE)

    return event
