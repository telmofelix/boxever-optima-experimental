package com.boxever.labs.optima.examples

import com.boxever.labs.optima.examples.functions.milliToTimestamp
import org.apache.spark.sql.{SaveMode, SparkSession}

object  TimestampInt96FormatExample {

  def main(args: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .master("local[*]")
      .getOrCreate()

    val spark = createSession

    import spark.implicits._

    val projectName = this.getClass.getSimpleName.substring(0, this.getClass.getSimpleName.length - 1)
    val samplesDir = System.getProperty("user.dir") + "/boxever-optima-examples/src/main/resources/examples"
    val inputDir = samplesDir + "/" + projectName + "/"
    val outputDir = System.getProperty("user.dir") + "/build/etl/" + projectName + "/"

    val guestsDF = spark.read.json(inputDir)

    val trustedGuestsDF = guestsDF.withColumn("createdAt", milliToTimestamp($"createdAt"))

    trustedGuestsDF.printSchema
    trustedGuestsDF.show

    trustedGuestsDF.write.mode(SaveMode.Overwrite).parquet(outputDir)
  }

}
