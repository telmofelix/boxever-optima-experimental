package com.boxever.labs.optima.examples

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.types.{ArrayType, StringType, StructType, TimestampType}

import scala.collection.mutable

// TODO Better error handling of Null
object functions {

  def milliToFormattedDate(pattern: String): UserDefinedFunction = udf((millis: Long) => new SimpleDateFormat(pattern).format(new Date(millis)))

  def timestampToFormattedDate(pattern: String): UserDefinedFunction = udf((ts: Timestamp) => new SimpleDateFormat(pattern).format(ts))

  def milliToTimestamp: UserDefinedFunction = udf((millis: Long) => new Timestamp(millis))

  def timestampToMillis: UserDefinedFunction = udf((ts: Timestamp) => ts.getTime)

  def trimAry: UserDefinedFunction = udf((a: mutable.WrappedArray[String]) => a.map(d => d.trim))

  def lowerAry: UserDefinedFunction = udf((a: mutable.WrappedArray[String]) => a.map(d => d.toLowerCase))

  def upperAry: UserDefinedFunction = udf((a: mutable.WrappedArray[String]) => a.map(d => d.toUpperCase))

  // TODO Rename and move to a particular function class
  /**
    * Iterate over sequence of order item rows and sum up the price * quantity
    *
    * @return The total price as Long
    */
  def totalOrderPrice: UserDefinedFunction = udf((gr: mutable.Seq[Row]) => gr.map(r => r.getAs[Long]("price") * r.getAs[Long]("quantity")).sum)

  // TODO Rename and move to a particular transformation class
  /**
    * Iterate over sequence of identifier rows and perform data cleaning to columns provider, id and expiryDate
    *
    * @return a cleaned sequence of rows
    */
  def transformIdentifiers: UserDefinedFunction = udf((identifiers: mutable.Seq[Row]) => {
    identifiers.map(r => Row(r.getAs[String]("provider").toUpperCase, r.getAs[String]("id"), new Timestamp(r.getAs[Long]("expiryDate"))))
  }, ArrayType(new StructType()
    .add("provider", StringType)
    .add("id", StringType)
    .add("expiryDate", TimestampType)))

  /**
    * Iterate over a twitter row and perform data cleaning to columns handle and registeredAt
    *
    * @return a cleaned sequence of rows
    */
  def transformTwitter: UserDefinedFunction = udf((r: Row) => {
    Row(r.getAs[String]("handle").toLowerCase, new Timestamp(r.getAs[Long]("registeredAt")))
  }, new StructType()
    .add("handle", StringType)
    .add("registeredAt", TimestampType))
}
