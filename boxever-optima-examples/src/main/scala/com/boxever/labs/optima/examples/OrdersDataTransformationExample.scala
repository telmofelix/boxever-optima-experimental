package com.boxever.labs.optima.examples

import com.boxever.labs.optima.examples.functions._
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
  * The following is an example of a UDF that uses orderItem.price and orderItem.quantity to fix the quality of order.price.
  *
  * Converts
  * +-------------+--------------------+-----+--------------------+
  * |    createdAt|          orderItems|price|                 ref|
  * +-------------+--------------------+-----+--------------------+
  * |1546032815914|[[50, 2, 38603c62...|    0|0940b634-0055-4af...|
  * |1546032815914|[[50, 1, f9dee5b3...|    0|a4e04065-4a60-4d3...|
  * +-------------+--------------------+-----+--------------------+
  *
  *
  * To
  * +--------------------+-----+--------------------+
  * |           createdAt|price|                 ref|
  * +--------------------+-----+--------------------+
  * |2018-12-28 21:33:...|  100|0940b634-0055-4af...|
  * |2018-12-28 21:33:...|   79|a4e04065-4a60-4d3...|
  * +--------------------+-----+--------------------+
  */
object OrdersDataTransformationExample {

  def toLong(x: Any): Option[Long] = x match {
    case i: Int => Some(i)
    case _ => None
  }

  def main(args: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .master("local[*]")
      .getOrCreate()

    val spark = createSession
    import spark.implicits._

    val samplesDir = System.getProperty("user.dir") + "/boxever-optima-examples/src/main/resources/examples"
    val outputDir = System.getProperty("user.dir") + "/build/etl"

    val ordersDF = spark.read.json(samplesDir + "/orders/")
    ordersDF.printSchema
    ordersDF.show

    val trustedOrdersDF = ordersDF
      .withColumn("createdAt", milliToTimestamp($"createdAt"))
      .withColumn("price", totalOrderPrice(col("orderItems")))
      .withColumn("ref", trim(col("ref")))
      .drop("orderItems")

    trustedOrdersDF.printSchema
    trustedOrdersDF.show

    trustedOrdersDF.write.mode(SaveMode.Overwrite).parquet(outputDir + "/v2Trusted/orders/")
  }

}
