package com.boxever.labs.optima.examples

import com.boxever.labs.optima.examples.functions._
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.{SaveMode, SparkSession}

object GuestTwitterDataTransformationExample {

  def main(args: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .master("local[*]")
      .getOrCreate()

    val spark = createSession
    import spark.implicits._

    val samplesDir = System.getProperty("user.dir") + "/boxever-optima-examples/src/main/resources/examples"
    val outputDir = System.getProperty("user.dir") + "/build/etl"

    val guestsDF = spark.read.json(samplesDir + "/guest_twitter/")
    guestsDF.printSchema
    guestsDF.show

    // https://stackoverflow.com/questions/39255973/split-1-column-into-3-columns-in-spark-scala
    val trustedGuestsDF = guestsDF
      .withColumn("createdAt", milliToTimestamp($"createdAt"))
      .withColumn("emails", trimAry(lowerAry(col("emails"))))
      .withColumn("firstName", trim(col("firstName")))
      .withColumn("gender", trim(lower(col("gender"))))
      .withColumn("guestType", trim(lower(col("guestType"))))
      .withColumn("twitter", transformTwitter(col("twitter")))
      .withColumn("lastName", trim(col("lastName")))
      .withColumn("lastSeen", milliToTimestamp($"lastSeen"))
      .withColumn("ref", trim(col("ref")))

    trustedGuestsDF.printSchema
    trustedGuestsDF.show

    trustedGuestsDF.drop("twitter").write.mode(SaveMode.Overwrite).parquet(outputDir + "/v2Trusted/guests/")

    val exploreGuestsDF = trustedGuestsDF
      .withColumnRenamed("createdAt", "created_at")
      .withColumnRenamed("firstName", "first_name")
      .withColumnRenamed("gender", "gender")
      .withColumnRenamed("guestType", "guest_type")
      .withColumnRenamed("lastName", "last_name")
      .withColumnRenamed("lastSeen", "last_seen")
      .withColumnRenamed("ref", "ref")
      .drop("twitter")

    exploreGuestsDF.printSchema
    exploreGuestsDF.show

    exploreGuestsDF.write.mode(SaveMode.Overwrite).parquet(outputDir + "/explore/guests/")

    val exploreGuestIdentifiersDF = trustedGuestsDF
      .select(
        col("ref").as("guest_ref"),
        col("twitter.handle").as("handle"),
        col("twitter.registeredAt").as("registered_at")
    )

    exploreGuestIdentifiersDF.printSchema
    exploreGuestIdentifiersDF.show

    exploreGuestIdentifiersDF.write.mode(SaveMode.Overwrite).parquet(outputDir + "/explore/guest_twitter/")

  }

}
