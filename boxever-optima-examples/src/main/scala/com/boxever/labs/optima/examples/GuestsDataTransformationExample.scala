package com.boxever.labs.optima.examples

import com.boxever.labs.optima.examples.functions._
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.{SaveMode, SparkSession}

object GuestsDataTransformationExample {

  def main(args: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .master("local[*]")
      .getOrCreate()

    val spark = createSession
    import spark.implicits._

    val samplesDir = System.getProperty("user.dir") + "/boxever-optima-examples/src/main/resources/examples"
    val outputDir = System.getProperty("user.dir") + "/build/etl"

    val guestsDF = spark.read.json(samplesDir + "/guests/")
    guestsDF.printSchema
    guestsDF.show

    // https://stackoverflow.com/questions/39255973/split-1-column-into-3-columns-in-spark-scala
    val trustedGuestsDF = guestsDF
      .withColumn("createdAt", milliToTimestamp($"createdAt"))
      .withColumn("emails", trimAry(lowerAry(col("emails"))))
      .withColumn("firstName", trim(col("firstName")))
      .withColumn("gender", trim(lower(col("gender"))))
      .withColumn("guestType", trim(lower(col("guestType"))))
      .withColumn("lastName", trim(col("lastName")))
      .withColumn("lastSeen", milliToTimestamp($"lastSeen"))
      .withColumn("ref", trim(col("ref")))
      .withColumn("createdAtYearMonth", timestampToFormattedDate("yyyy-MM")($"createdAt"))

    trustedGuestsDF.printSchema
    trustedGuestsDF.show

    // Note for partitions to work in Hive/Athena the partition columns must be lowercase
    // Partition columns are not part of the spark_schema using "parquet-tools schema build/etl/v2Trusted/guests/guesttype\=customer/createdatyearmonth\=2018-12/". Also note you cannot specify "parquet-tools schema build/etl/v2Trusted/guests/" as you will get a "java.io.FileNotFoundException: ..../build/etl/v2Trusted/guests/guesttype=customer (Is a directory)"
    // Partition columns are not part of the meta using "parquet-tools meta build/etl/v2Trusted/guests/
    trustedGuestsDF.write
      .mode(SaveMode.Overwrite)
      .partitionBy("guesttype", "createdatyearmonth") // Needs to be lower cased for Hive compatibility. See Hive_And_Partitions.md
      .parquet(outputDir + "/v2Trusted/guests/")


    val exploreGuestsDF = trustedGuestsDF
      .withColumnRenamed("createdAt", "created_at")
      .withColumnRenamed("firstName", "first_name")
      .withColumnRenamed("gender", "gender")
      .withColumnRenamed("guestType", "guest_type")
      .withColumnRenamed("lastName", "last_name")
      .withColumnRenamed("lastSeen", "last_seen")
      .withColumnRenamed("ref", "ref")
      .drop("emails")

    exploreGuestsDF.printSchema
    exploreGuestsDF.show

    exploreGuestsDF.write.mode(SaveMode.Overwrite).parquet(outputDir + "/explore/guests/")

    val exploreGuestEmailsDF = trustedGuestsDF.
      select(col("ref").as("guest_ref"), explode(col("emails")).as("email"))


    exploreGuestEmailsDF.printSchema
    exploreGuestEmailsDF.show // Note when this is displayed in the console it looks like jack@boxever.com contains spaces at the start. This is just the console representation.

    exploreGuestEmailsDF.write.mode(SaveMode.Overwrite).parquet(outputDir + "/explore/guest_emails/")

//    val df1 = spark.read.parquet(outputDir + "/explore/guests/").as("g")
//    val df2 = spark.read.parquet(outputDir + "/explore/guest_emails/").as("ge")
//    df1
//      .join(df2, col("g.ref") === col("ge.guest_ref"), "inner")
//      .drop("guest_ref")
//      .show

    val df1 = spark.read.parquet(outputDir + "/explore/guests/")
    val df2 = spark.read.parquet(outputDir + "/explore/guest_emails/")

    df1
      .join(df2, df1("ref") === df2("guest_ref"))
      .drop("guest_ref")
      .show

    // Verify the time has millisecond precision still
    val df = spark.read.parquet(outputDir + "/explore/guests/")
    df
      .select(col("created_at"), col("last_seen"))
      .withColumn("created_at", timestampToMillis($"created_at"))
      .withColumn("last_seen", timestampToMillis($"last_seen"))
      .show


    val tempDF = spark.read.parquet(outputDir + "/v2Trusted/guests/")
    tempDF.printSchema
    tempDF.show
  }

}
