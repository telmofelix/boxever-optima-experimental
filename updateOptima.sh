#!/bin/bash

usage() {
    echo "./updateOptima.sh {OPTIMA_ENVIRONMENT} {CUSTOMER}";
    echo "Example: ./updateOptima.sh labs emirates"
    echo "Example: ./updateOptima.sh labs spinair"
    echo "Example: ./updateOptima.sh labs viva"

    echo "Example: ./updateOptima.sh explore emirates"
    echo "Example: ./updateOptima.sh explore norwegian"
    echo "Example: ./updateOptima.sh explore qatar"
    echo "Example: ./updateOptima.sh explore ryanair"
    echo "Example: ./updateOptima.sh explore ryanair-uat"
    echo "Example: ./updateOptima.sh explore volaris"

    echo "Example: ./updateOptima.sh dev spinach"

    exit 1;
}

if [ $# -ne 2 ]; then
    usage
fi

OPTIMA_ENVIRONMENT=$1
CUSTOMER=$2

mkdir -p ~/.aws
if [[ ! -f ~/.aws/credentials ]]; then
    touch mkdir -p ~/.aws/credentials
fi

if [[ ! -f ~/.onelogin/onelogin-aws-cli.jar ]]; then
    echo "Downloading one login jar to ~/.onelogin/onelogin-aws-cli.jar"
    mkdir -p ~/.onelogin/
    curl https://s3-eu-west-1.amazonaws.com/boxever-labs-binaries/services/one-login/onelogin-aws-cli.jar -o ~/.onelogin/onelogin-aws-cli.jar
fi

if [[ ! -f ~/.onelogin/boxever.onelogin.properties ]]; then
    echo "Please enter you one login username. e.g. your Boxever email address"
    read ONE_LOGIN_USERNAME
    echo "ONE_LOGIN_USERNAME=${ONE_LOGIN_USERNAME}" > ~/.onelogin/boxever.onelogin.properties
fi
source ~/.onelogin/boxever.onelogin.properties

# https://boxever.atlassian.net/wiki/spaces/OPS/pages/528482305/Accessing+AWS+through+OneLogin
if [[ "${OPTIMA_ENVIRONMENT}" == "labs" ]]; then
    OPTIMA_ENVIRONMENT="labs"
    AWS_ACCOUNT_ID="228626570070"
    AWS_IAM_ROLE="LabsAdministratorAccess"
    export AWS_PROFILE="labs"
elif [[ "${OPTIMA_ENVIRONMENT}" == "explore" ]]; then
    OPTIMA_ENVIRONMENT="production"
    AWS_ACCOUNT_ID="507754017716"
    AWS_IAM_ROLE="ExploreAdministratorAccess"
    export AWS_PROFILE="explore"
elif [[ "${OPTIMA_ENVIRONMENT}" == "dev" ]]; then
    OPTIMA_ENVIRONMENT="dev"
    AWS_ACCOUNT_ID="238091120948"
    AWS_IAM_ROLE="OperationsAccess"
    export AWS_PROFILE="dev"
else
    echo "Invalid optima environment ${OPTIMA_ENVIRONMENT}. Expected labs or explore"
    exit
fi

read -p "Do you want to build the jar? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # Build Gradle
    ./gradlew clean build

fi

mkdir -p ~/.aws
if [[ ! -f ~/.aws/credentials ]]; then
    touch mkdir -p ~/.aws/credentials
fi

read -p "Do you need to login? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    if [[ "${BOXEVER_MOBILE_AUTHENTICATOR_ID}" != "" ]]; then
        java -jar ~/.onelogin/onelogin-aws-cli.jar -u ${ONE_LOGIN_USERNAME} --appid 358863 --region eu-west-1 --subdomain boxever --profile ${AWS_PROFILE} --aws-account-id ${AWS_ACCOUNT_ID} --aws-role-name ${AWS_IAM_ROLE} --boxever-mobile-authenticator-id ${BOXEVER_MOBILE_AUTHENTICATOR_ID}
    else
        java -jar ~/.onelogin/onelogin-aws-cli.jar -u ${ONE_LOGIN_USERNAME} --appid 358863 --region eu-west-1 --subdomain boxever --profile ${AWS_PROFILE} --aws-account-id ${AWS_ACCOUNT_ID} --aws-role-name ${AWS_IAM_ROLE}
    fi
fi

NUM=$(grep -n \\[${AWS_PROFILE}] ~/.aws/credentials | cut -d : -f 1)
LINE_FROM=$((1 + NUM))
LINE_TO=$((4 + NUM))
sed -n ${LINE_FROM}','${LINE_TO}' p' ~/.aws/credentials > ~/.aws/tmp_credentials
source ~/.aws/tmp_credentials
rm ~/.aws/tmp_credentials

export AWS_ACCESS_KEY_ID=${aws_access_key_id}
export AWS_SECURITY_TOKEN=${aws_session_token}
export AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}

CURRENT_DIR=$(pwd)
cd ansible

ansible-playbook playbook.yml -e 'host_key_checking=False' -i inventory -e @boxever-optima-${CUSTOMER}-${OPTIMA_ENVIRONMENT}-vars.yml

cd ${CURRENT_DIR}