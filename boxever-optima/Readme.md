[TOC]

# Overview

The following [OptimaETL.scala](https://bitbucket.org/boxever/boxever-labs-optima/src/default/boxever-optima/src/main/scala/com/boxever/labs/spark/optima/OptimaETL.scala) script takes a nested json object and performs 

* Transformations of nested structure to a flat structure
* Column renaming e.g guestRef to guest_ref
* Column type conversion e.g. Long to TimeStamp
* Column data cleansing e.g. trim, toLowercase, toUppercase
* Validation & quarantine and reporting at a row level

#### Limitations

* Does not support creating columns if the source column does not exist in the source data set
* Does not support transformation of nested structure to nested structure such as a array of primitives or character strings, complex object or an array of complex objects
* Does not support functions such as *sum* of orderItems.price * orderItems.quantity to order.price
* Does not support joining two desperate datasets together
* Does not support stats such as stats on Guest or in Guest Stats table e.g. guest_stats.liveTimeValue, guest_stats.totalOrderValue, guest_stats.most_frequently_search_product
* Does not support enrichment such as Journey Elements on Orders or Journey table

#### Sample Data & Specifications
 
* [Sample Data](https://bitbucket.org/boxever/boxever-labs-optima/src/default/boxever-optima/src/main/resources/optima/sample-data/)
* [Optima Specifications](https://bitbucket.org/boxever/boxever-labs-optima/src/default/boxever-optima/src/main/resources/optima/spec/)

# Guests

## Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "createdAt": 1546032815914, "guestType": "CUSTOMER", "firstName":  "Jack", "lastName": "Smith", "gender":  "male", "lastSeen": 1546032815914}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "guestType": "custdomer", "firstName":  "Mary", "lastName": "Smith", "gender": "female", "lastSeen": 1546032815914}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "firstName":  "Peter", "lastName": "Smith", "gender":  "MALE", "lastSeen": 1546032815914}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "createdAt": "1546032815914", "firstName":  "Paul", "lastName": "Smith", "gender":  "unknown", "lastSeen": 1546032815914, "emails": [{"paul.smith@example.com"}]}

## Trusted Output

    +--------------------+--------------------+---------+-------+---------+--------+--------------------+--------------------+----------+--------------------+------------------------+----------------+
    |                 ref|           createdAt|firstName| gender|guestType|lastName|            lastSeen|          modifiedAt|guest_type|last_seen_year_month|containsValidationErrors|validationErrors|
    +--------------------+--------------------+---------+-------+---------+--------+--------------------+--------------------+----------+--------------------+------------------------+----------------+
    |68ae46d0-3916-4d9...|2018-12-28 21:33:...|     Jack|   male| customer|   Smith|2018-12-28 21:33:...|2018-12-28 21:33:...|  customer|             2018-12|                   false|                |
    |02169f57-2dcb-4bb...| 1970-01-01 00:00:00|    Peter|   male|  unknown|   Smith|2018-12-28 21:33:...| 1970-01-01 00:00:00|   unknown|             2018-12|                   false|                |
    |2e182b0f-ac3c-4ac...|2018-12-28 21:33:...|     Paul|unknown|  unknown|   Smith|2018-12-28 21:33:...| 1970-01-01 00:00:00|   unknown|             2018-12|                   false|                |
    +--------------------+--------------------+---------+-------+---------+--------+--------------------+--------------------+----------+--------------------+------------------------+----------------+

## Quarantine Output

    +--------------------+-------------------+---------+------+---------+--------+--------------------+-------------------+----------+--------------------+------------------------+--------------------+
    |                 ref|          createdAt|firstName|gender|guestType|lastName|            lastSeen|         modifiedAt|guest_type|last_seen_year_month|containsValidationErrors|    validationErrors|
    +--------------------+-------------------+---------+------+---------+--------+--------------------+-------------------+----------+--------------------+------------------------+--------------------+
    |7e7b7531-21c5-427...|1970-01-01 00:00:00|     Mary|female|custdomer|   Smith|2018-12-28 21:33:...|1970-01-01 00:00:00| custdomer|             2018-12|                    true|Failed to match o...|
    +--------------------+-------------------+---------+------+---------+--------+--------------------+-------------------+----------+--------------------+------------------------+--------------------+

# Orders

## Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "68ddfa14-0928-4e40-bde4-9e0be974b580", "price":  99.00}]}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "3ffe78bb-6147-4058-adde-56f2f5869318", "price":  99.00}]}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "832aa5bc-d75d-4f33-b59d-b8c7f684e329", "price":  99.00}]}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "be032cfc-c8b5-4281-988b-c3d515459eff", "price":  99.00},{"ref": "eaf5fe9c-ea85-47df-9062-af4766a655e9", "price":  99.00}]}
    
## Trusted Output

    +--------------------+--------------------+-----+--------------------+------------------------+----------------+
    |            guestRef|                 ref|price|           clientKey|containsValidationErrors|validationErrors|
    +--------------------+--------------------+-----+--------------------+------------------------+----------------+
    |68ae46d0-3916-4d9...|68ddfa14-0928-4e4...| 99.0|vmjlbwpibqpeyeflt...|                   false|                |
    |7e7b7531-21c5-427...|3ffe78bb-6147-405...| 99.0|vmjlbwpibqpeyeflt...|                   false|                |
    |02169f57-2dcb-4bb...|832aa5bc-d75d-4f3...| 99.0|vmjlbwpibqpeyeflt...|                   false|                |
    |2e182b0f-ac3c-4ac...|be032cfc-c8b5-428...| 99.0|vmjlbwpibqpeyeflt...|                   false|                |
    |2e182b0f-ac3c-4ac...|eaf5fe9c-ea85-47d...| 99.0|vmjlbwpibqpeyeflt...|                   false|                |
    +--------------------+--------------------+-----+--------------------+------------------------+----------------+

## Quarantine Output

    +--------+---+-----+---------+------------------------+----------------+
    |guestRef|ref|price|clientKey|containsValidationErrors|validationErrors|
    +--------+---+-----+---------+------------------------+----------------+
    +--------+---+-----+---------+------------------------+----------------+
    
# Order Items

## Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "68ddfa14-0928-4e40-bde4-9e0be974b580", "price": 99.00, "orderItems": [{"ref": "37b5bc25-766b-42c3-bd8e-012008850633", "price": 99.00}]}]}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "3ffe78bb-6147-4058-adde-56f2f5869318", "price": 99.00, "orderItems": [{"ref": "d57519b7-fc50-4e0c-b84f-8dc6a6e45e97", "price": 99.00}]}]}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "832aa5bc-d75d-4f33-b59d-b8c7f684e329", "price": 99.00, "orderItems": [{"ref": "7ed4794d-e09f-4b2b-afcc-cbc8a17ad79f", "price": 99.00}]}]}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "clientKey": "vmjlbwpibqpeyefltbrozxvwykxpacut", "orders": [{"ref": "be032cfc-c8b5-4281-988b-c3d515459eff", "price": 99.00, "orderItems": [{"ref": "6c10bc87-56a5-470e-9848-0929c13e8208", "price": 99.00}]},{"ref": "eaf5fe9c-ea85-47df-9062-af4766a655e9", "price": 99.00,"orderItems": [{"ref": "c37855b5-e2a6-4763-82b7-98af23cfb595", "price": 99.00}]}]}
    
## Trusted Output

    +--------------------+--------------------+--------------------+--------------------+------------------------+----------------+
    |            guestRef|            orderRef|                 ref|           clientKey|containsValidationErrors|validationErrors|
    +--------------------+--------------------+--------------------+--------------------+------------------------+----------------+
    |68ae46d0-3916-4d9...|68ddfa14-0928-4e4...|37b5bc25-766b-42c...|vmjlbwpibqpeyeflt...|                   false|                |
    |7e7b7531-21c5-427...|3ffe78bb-6147-405...|d57519b7-fc50-4e0...|vmjlbwpibqpeyeflt...|                   false|                |
    |02169f57-2dcb-4bb...|832aa5bc-d75d-4f3...|7ed4794d-e09f-4b2...|vmjlbwpibqpeyeflt...|                   false|                |
    |2e182b0f-ac3c-4ac...|be032cfc-c8b5-428...|6c10bc87-56a5-470...|vmjlbwpibqpeyeflt...|                   false|                |
    |2e182b0f-ac3c-4ac...|eaf5fe9c-ea85-47d...|c37855b5-e2a6-476...|vmjlbwpibqpeyeflt...|                   false|                |
    +--------------------+--------------------+--------------------+--------------------+------------------------+----------------+

## Quarantine Output

    +--------+--------+---+---------+------------------------+----------------+
    |guestRef|orderRef|ref|clientKey|containsValidationErrors|validationErrors|
    +--------+--------+---+---------+------------------------+----------------+
    +--------+--------+---+---------+------------------------+----------------+
    
    
# Future Work / Examples

## Guests - Array of primitives or character strings
Below is an example of converting emails which is an array of strings

### Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "createdAt": 1546032815914, "guestType": "CUSTOMER", "firstName":  "Jack", "lastName": "Smith", "gender":  "male", "lastSeen": 1546032815914}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "guestType": "custdomer", "firstName":  "Mary", "lastName": "Smith", "gender": "female", "lastSeen": 1546032815914}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "firstName":  "Peter", "lastName": "Smith", "gender":  "MALE", "lastSeen": 1546032815914}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "createdAt": "1546032815914", "firstName":  "Paul", "lastName": "Smith", "gender":  "unknown", "lastSeen": 1546032815914, "emails": ["paul.smith@example.com"]}

### Trusted Output

    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+-----------------+------------------------+----------------+
    |                 ref|guestType|           createdAt|firstName|lastName| gender|              emails|            lastSeen|lastSeenYearMonth|containsValidationErrors|validationErrors|
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+-----------------+------------------------+----------------+
    |68ae46d0-3916-4d9...| customer|2018-12-28 21:33:...|     Jack|   Smith|   male|                [[]]|2018-12-28 21:33:...|          2018-12|                   false|                |
    |02169f57-2dcb-4bb...|  unknown| 1970-01-01 00:00:00|    Peter|   Smith|   male|                [[]]|2018-12-28 21:33:...|          2018-12|                   false|                |
    |2e182b0f-ac3c-4ac...|  unknown|2018-12-28 21:33:...|     Paul|   Smith|unknown|[[paul.smith@boxe...|2018-12-28 21:33:...|          2018-12|                   false|                |
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+-----------------+------------------------+----------------+

## Guests - Complex object
Below is an example of converting twitter (fictitious in the Boxever eco system) which is an complex object

### Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "createdAt": 1546032815914, "guestType": "CUSTOMER", "firstName":  "Jack", "lastName": "Smith", "gender":  "male", "lastSeen": 1546032815914}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "guestType": "custdomer", "firstName":  "Mary", "lastName": "Smith", "gender": "female", "lastSeen": 1546032815914}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "firstName":  "Peter", "lastName": "Smith", "gender":  "MALE", "lastSeen": 1546032815914}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "createdAt": "1546032815914", "firstName":  "Paul", "lastName": "Smith", "gender":  "unknown", "lastSeen": 1546032815914, "emails": ["paul.smith@example.com"], "twitter": {"handle": "@paul", "registeredAt":1546032815914}}

### Trusted Output

    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+
    |                 ref|guestType|           createdAt|firstName|lastName| gender|              emails|             twitter|            lastSeen|lastSeenYearMonth|containsValidationErrors|validationErrors|
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+
    |68ae46d0-3916-4d9...| customer|2018-12-28 21:33:...|     Jack|   Smith|   male|                [[]]|                  []|2018-12-28 21:33:...|          2018-12|                   false|                |
    |02169f57-2dcb-4bb...|  unknown| 1970-01-01 00:00:00|    Peter|   Smith|   male|                [[]]|                  []|2018-12-28 21:33:...|          2018-12|                   false|                |
    |2e182b0f-ac3c-4ac...|  unknown|2018-12-28 21:33:...|     Paul|   Smith|unknown|[[paul.smith@boxe...|[@paul, 2018-12-2...|2018-12-28 21:33:...|          2018-12|                   false|                |
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+


## Guests - Array of complex objects
Below is an example of converting identifiers which is an array of complex objects

### Input

    {"ref": "68ae46d0-3916-4d93-8b91-21425a351d5a", "createdAt": 1546032815914, "guestType": "CUSTOMER", "firstName":  "Jack", "lastName": "Smith", "gender":  "male", "lastSeen": 1546032815914}
    {"ref": "7e7b7531-21c5-427b-bb60-e6489ac5464c", "guestType": "custdomer", "firstName":  "Mary", "lastName": "Smith", "gender": "female", "lastSeen": 1546032815914}
    {"ref": "02169f57-2dcb-4bb0-8d85-afba0c6ab1c0", "firstName":  "Peter", "lastName": "Smith", "gender":  "MALE", "lastSeen": 1546032815914}
    {"ref": "2e182b0f-ac3c-4ac5-8e92-61ba697c78e5", "createdAt": "1546032815914", "firstName":  "Paul", "lastName": "Smith", "gender":  "unknown", "lastSeen": 1546032815914, "emails": ["paul.smith@example.com"], "identifiers": [{"provider": "Google","id": "ff10158a-0b47-41f9-ab64-17113b0fdaa9", "expiryDate": 1547457315112}, {"provider": "Globe","id": "ad7c72cd-0e7e-4e65-b7a8-2f12cc5ffb3c", "expiryDate": 1547485436859}]}}

### Trusted Output

    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+
    |                 ref|guestType|           createdAt|firstName|lastName| gender|              emails|         identifiers|            lastSeen|lastSeenYearMonth|containsValidationErrors|validationErrors|
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+
    |68ae46d0-3916-4d9...| customer|2018-12-28 21:33:...|     Jack|   Smith|   male|                [[]]|                [[]]|2018-12-28 21:33:...|          2018-12|                   false|                |
    |02169f57-2dcb-4bb...|  unknown| 1970-01-01 00:00:00|    Peter|   Smith|   male|                [[]]|                [[]]|2018-12-28 21:33:...|          2018-12|                   false|                |
    |2e182b0f-ac3c-4ac...|  unknown|2018-12-28 21:33:...|     Paul|   Smith|unknown|[[paul.smith@boxe...|[[GOOGLE, ff821d4...|2018-12-28 21:33:...|          2018-12|                   false|                |
    +--------------------+---------+--------------------+---------+--------+-------+--------------------+--------------------+--------------------+-----------------+------------------------+----------------+



# Submitting on EMR
To submit a Optima ETL job on EMR the following needs to be followed

## Build the project
Build the project using gradle

    ./gradlew clean build

## Copy Binary to EMR Master
Copy the binary boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar to the directoy /home/hadoop on the EMR master.

    scp boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar emr:/home/hadoop/
    
## Login to EMR master

    ssh emr


## Submit Spark Job - Optima v3
Execute one of the following commands on the EMR master

### Guests

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v3.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests-optima-etl.yml --date 2019/01/30
        
### Orders

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v3.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/orders-optima-etl.yml --date 2019/01/30
       
### Sessions

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v3.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/sessions-optima-etl.yml --date 2019/01/30
        
### Events

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v3.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/events-optima-etl.yml --date 2019/01/30
    
                
## Submit Spark Job - Optima v2
Execute one of the following commands on the EMR master
 
### Spinair

#### Guests

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v2.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/guests-optima-etl.yml --date 2019/01/30
    
#### Orders

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v2.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/orders-optima-etl.yml --date 2019/01/30
 
#### Sessions

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v2.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/sessions-optima-etl.yml --date 2019/01/30
        
#### Events

    spark-submit --master yarn --deploy-mode cluster --executor-memory 1g --class com.boxever.labs.spark.optima.v2.OptimaETL /home/hadoop/boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://boxever-optima-spinair-labs-eu-west-1/config/optima-trusted-specs/events-optima-etl.yml --date 2019/01/30
 
 
# Spark Shell

## Login to EMR master

    ssh emr
    
## Launch Spark Shell

    spark-shell
 
### Spinair

Rplace with the relevant client key

* Spinair: wjtc2eog1lvueo72kts3mn1ean0nentz
* Emirates: ekb7q5q7htudvxjat3zmeuv2qjus0z6w

#### Guests

##### Snapshots

    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/2019/01/30/v2/guests/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    df.count

##### Trusted

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/trusted/snapshots/2019/01/30/v2/guests/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
##### Explore

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/explore/snapshots/2019/01/30/v2/guests/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
    
#### Orders

##### Snapshots

    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/2019/01/30/v2/orders/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    df.count
 
##### Trusted

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/trusted/snapshots/2019/01/30/v2/guests/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
##### Explore

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/explore/snapshots/2019/01/30/v2/guests/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
    
#### Sessions

##### Snapshots

    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/2019/01/30/v2/sessions/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    
    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/trusted/snapshots/2019/01/30/v2/sessions/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    df.count
        
##### Trusted

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/trusted/snapshots/2019/01/30/v2/sessions/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
##### Explore

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/explore/snapshots/2019/01/30/v2/sessions/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
        
        
#### Events

##### Snapshots

    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/2019/01/30/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)


##### Immutable
    
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2016/Q3/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2016/Q4/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2017/Q1/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2017/Q2/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2017/Q3/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2017/Q4/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q1/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q2/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q3/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q4/")
    val df = spark.read.json("s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2019/Q1/")
    
    val df = spark.read.json(
        "s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q2/", 
        "s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q2/"
    )
    df.printSchema
    df.show(10)
   
##### Immutable In Parquet

    object SafeSimpleDateFormat extends ThreadLocal[util.HashMap[String, SimpleDateFormat]] {
      override def initialValue = {
        new util.HashMap[String, SimpleDateFormat]()
      }
    
      def get(pattern: String): SimpleDateFormat = {
        val map: util.HashMap[String, SimpleDateFormat] = get()
        if (!map.containsKey(pattern)) {
            map.put(pattern, new SimpleDateFormat(pattern));
        }
        return map.get(pattern)
      }
    }

    def milliToFormattedDate(pattern: String): UserDefinedFunction = udf((millis: Long) => SafeSimpleDateFormat.get(pattern).format(new Date(millis)))

    val sourcePath = "s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/2018/Q4/"
    val destinationPath = "s3://boxever-optima-spinair-labs-eu-west-1/data/raw/evants/"

    val sourceDF = spark.read.json(sourcePath)
    val destinationDF = sourceDF.
      withColumn("created_at_year_month_day", milliToFormattedDate("yyyy-mm-dd")($"createdAt")).
      withColumn("created_at_year", milliToFormattedDate("yyyy")($"createdAt")).
      withColumn("created_at_month", milliToFormattedDate("MM")($"createdAt")).
      withColumn("created_at_day", milliToFormattedDate("dd")($"createdAt")).
      withColumn("created_at_week", milliToFormattedDate("ww")($"createdAt"))

    destinationDF.
      repartition(destinationDF("created_at_year_month_day")).
      write.
      mode(SaveMode.Overwrite).
      partitionBy("created_at_year_month_day").
      parquet(destinationPath)
      
    df.write
        .repartition(destinationDF("created_at_year_month_day"))
        .mode(SaveMode.Overwrite)
        .partitionBy("created_at_year_month_day")
        .parquet(de)
    
    df.repartition()
    
##### Trusted

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/trusted/snapshots/2019/01/30/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
    
##### Explore

    val df = spark.read.parquet("s3://boxever-optima-spinair-labs-eu-west-1/data/explore/snapshots/2019/01/30/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/")
    df.printSchema
    df.show(10)
        

            
# References
https://aws.amazon.com/premiumsupport/knowledge-center/emr-submit-spark-job-remote-cluster/