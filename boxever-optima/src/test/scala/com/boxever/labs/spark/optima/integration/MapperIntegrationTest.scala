package com.boxever.labs.spark.optima.integration

import java.io.File
import java.util

import com.boxever.labs.spark.optima.complexstructure.{ComplexStructureDestinationSchema, ComplexStructureMapper}
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.segments.Segments
import com.boxever.labs.spark.optima.simplestructure.{SimpleStructureDestinationSchema, SimpleStructureMapper}
import com.boxever.labs.spark.optima.spec.{OptimaSpec, OptimaSpecReader}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.types._
import org.scalatest.FunSuite

import scala.collection.JavaConverters._
import scala.collection.mutable

class MapperIntegrationTest extends FunSuite with SharedSparkSession {

  def getConfig(mappingType: String, testSuiteName: String): OptimaSpec = {
    val testDir = "/com/boxever/labs/spark/optima/" + mappingType

    val sysArgs = Array(mappingType, "--config_file", "classpath://" + testDir + "/" + testSuiteName + ".yml")

    val args = OptimaArgParser.getResolvedOptions(sysArgs)

    var userDir = new File(System.getProperty("user.dir"))
    if (userDir.getName == "boxever-labs-optima") {
      userDir = new File(userDir, "boxever-optima")
    }

    val configFile = args("config_file")

    //JSON Data
    //For complex/simple mapping, point to dir containing yml mapping and json data with same name as mapping
    //Parquet Data
    //For simple mapping, point to dir containing yml mapping and directory with same name as mapping
    // (which contains partitioned/un-partitioned data)
    //Default is simple mapping
    val sourcePath = if (mappingType.equalsIgnoreCase("complex")) {
      userDir.toString + "/src/test/resources/" + testDir + "/" + testSuiteName + ".json"
    }
    else if (mappingType.equalsIgnoreCase("simple")){
      userDir.toString + "/src/test/resources/" + testDir + "/" + testSuiteName + ".json"
    }
    else if (mappingType.equalsIgnoreCase("segments")){
      userDir.toString + "/src/test/resources/" + testDir + "/" + testSuiteName + ".json"
    }
    else{
      //use simple by default
      userDir.toString + "/src/test/resources/" + testDir + "/" + testSuiteName + '/'
    }

    val destPath = userDir.toString + "/build/etl-tests/" + testSuiteName + "/output/"
    val quarantinePath = userDir.toString + "/build/etl-tests/" + testSuiteName + "/quarantine/"

    val variables = new util.HashMap[String, String]()
    variables.put("sourcePath", sourcePath)
    variables.put("destPath", destPath)
    variables.put("quarantinePath", quarantinePath)

    OptimaSpecReader.read(configFile, variables.asScala.toMap)
  }

  test("assert events_add_product/events.json"){
    val config = getConfig("complex","events_add_product/events")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)
    val product = result.filter("ref == '6897d4d5-43df-41f8-95f1-b581e934d303'").collect()(0)
      .getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("arbitraryData")
      .asInstanceOf[GenericRowWithSchema].getAs[GenericRowWithSchema](1)
    assert(product.getAs[String]("currency") == "AED")
    assert(product.getAs[String]("origin") == "DXB")
    assert(product.getAs[String]("destination") == "LHR")
    assert(product.getAs[Double]("price") == 1512.5)
    assert(product.getAs[Long]("quantity") == 1)
  }

  test("assert events_date_transformation/events.json"){
    val config = getConfig("complex","events_date_transformation/events")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)
    val quarantine = spark.read.json(config.quarantine.path)
    assert(quarantine.count() == 1)
  }

  test("assert orders_long_to_double_mismatch_fix/orders.json"){
    val config = getConfig("complex","orders_long_to_double_mismatch_fix/orders")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)
    assert(result.filter("ref = '972115ef-1348-4c09-b2ba-b3fa2d2a39ee'").collect()(0).getAs[Double]("price") == 0)
  }

  test("assert session_utm/session.json"){
    val config = getConfig("complex","session_utm/session")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)
  }

  test("assert sessions/sessions0001.json"){
    val config = getConfig("complex","sessions/sessions0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    result.show()
    assert(result.count() == 1)
  }

  test("assert session_utm_col_nested_in_yaml_not_json/session.json"){
    val config = getConfig("complex","session_utm_col_nested_in_yaml_not_json/session")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.parquet(config.destination.path)
    assert(result.count() == 1)
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(0).getAs[GenericRowWithSchema]("values")(1)==null)
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(0).getAs[GenericRowWithSchema]("values")(4)==null)
  }

  test("assert session_quarantine/session.json"){
    val config = getConfig("complex","session_quarantine/session")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 5)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 3)
    assert(result.filter("ref = '68ae46d0-3916-4d93-8b91-21425a351d5a'").collect()(0).getAs[String]("channel") == "WEB")
    assert(result.filter("ref = '68ae46d0-3916-4d93-8b91-21425a351d5b'").collect()(0).getAs[String]("channel") == "EMAIL")
    assert(result.filter("ref = '68ae46d0-3916-4d93-8b91-21425a351d5d'").collect()(0).getAs[String]("channel") == "OTHER")
    val quarantine = spark.read.json(config.quarantine.path)
    assert(quarantine.count() == 2)
    assert(quarantine.filter("originalData.ref = '68ae46d0-3916-4d93-8b91-21425a351d5c'").collect()(0).getAs[GenericRowWithSchema]("originalData").getAs[String]("channel") == "SNAILMAIL")
    assert(quarantine.filter("originalData.ref = '68ae46d0-3916-4d93-8b91-21425a351d5e'").collect()(0).getAs[GenericRowWithSchema]("originalData").getAs[String]("channel") == "GARBAGE")
    val quarantineSummary = spark.read.json(config.quarantineSummary.path)
    assert(quarantineSummary.filter("validationErrors like '%GARBAGE%'").collect()(0).getAs[Long]("count") == 1)
    assert(quarantineSummary.filter("validationErrors like '%SNAILMAIL%'").collect()(0).getAs[Long]("count") == 1)
  }

  test("assert session_where/sessions0001.json"){
    val config = getConfig("complex","session_where/sessions0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 5)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    result.show()
    assert(result.count() == 3)
    assert(result.where("to_date(modifiedAt) < to_date('2018-04-17')").count() == 0)
    assert(result.where("to_date(modifiedAt) >= to_date('2018-04-17')").count() == 3)
  }

  test("assert guests/guests0001.json"){
    val config = getConfig("complex", "guests/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(2)(0).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="jack.smith@boxever.com")
    assert(result.take(2)(0).getAs[mutable.WrappedArray.ofRef[String]]("emails")(1)=="jack@boxever.com")
    assert(result.take(2)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
    assert(result.take(2)(1).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="paul.smith@boxever.com")
    assert(result.take(2)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    //result.filter("ref = '68ae46d0-3916-4d93-8b91-21425a351d5a'").collect()
  }

  test("assert guests_folder_does_not_exist/guests0000.json"){
    val config = getConfig("complex", "guests_folder_does_not_exist/guests0001")

    System.out.println(config.getSource.getPaths)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val expected = ComplexStructureDestinationSchema.getDestinationSchema(new StructType, config.mappings)
    val result = spark.read.schema(expected).parquet(config.destination.path)
    assert(result.count() == 0)
    assert(expected.equals(result.schema))
  }

  test("assert guest_twitter/guests0001.json"){
    val config = getConfig("complex","guest_twitter/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(2)(0).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="jack.smith@boxever.com")
    assert(result.take(2)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
    assert(result.take(2)(0).getAs[GenericRowWithSchema]("twitter").getAs[String]("handle")=="@jack")
    assert(result.take(2)(0).getAs[GenericRowWithSchema]("twitter").getAs[Long]("registeredAt")==1546032815914L)
    assert(result.take(2)(1).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="paul.smith@boxever.com")
    assert(result.take(2)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    assert(result.take(2)(1).getAs[GenericRowWithSchema]("twitter").getAs[String]("handle")=="@paul")
    assert(result.take(2)(1).getAs[GenericRowWithSchema]("twitter").getAs[Long]("registeredAt")==1546032815914L)
  }

  test("assert guest_identifiers/guests0001.json"){
    val config = getConfig("complex","guest_identifiers/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 4)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 4)
    assert(result.take(4)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")==null)
    assert(result.take(4)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
    assert(result.take(4)(1).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(0).getAs[String]("provider")=="Google")
    assert(result.take(4)(1).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(1).getAs[String]("provider")=="Globe")
    assert(result.take(4)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    assert(result.take(4)(2).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(1).getAs[String]("expiryDate")==null)
    assert(result.take(4)(2).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(0).getAs[String]("provider")=="Google")
    assert(result.take(4)(2).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(1).getAs[String]("provider")=="Globe")
    assert(result.take(4)(3).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(1).getAs[String]("expiryDate")==null)
    assert(result.take(4)(3).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("identifiers")(0).getAs[String]("provider")=="Google")
  }

  test("assert guest_col_in_yaml_not_json/guests0001.json"){
    val config = getConfig("complex","guest_col_in_yaml_not_json/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.parquet(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(1)(0).getAs[Long]("createdAt")==0)
    assert(result.take(1)(0).getAs[StructType]("emails")==null)
  }

  test("assert guest_col_identifiers_in_yaml_not_json/guests0001.json"){
    val config = getConfig("complex","guest_col_identifiers_in_yaml_not_json/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.parquet(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(1)(0).getAs[Long]("createdAt")==0)
    assert(result.take(1)(0).getAs[StructType]("emails")==null)
    assert(result.take(1)(0).getAs[ArrayType]("identifiers")==null)
  }

  test("assert guest_duplicate_mapping/guests0001.json"){
    val config = getConfig("complex","guest_duplicate_mapping/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(2)(0).getAs[String]("guestType")=="CUSTOMER")
    assert(result.take(2)(0).getAs[String]("guest_type")=="customer")
    assert(result.take(2)(1).getAs[String]("guestType")=="CUSTOMER")
    assert(result.take(2)(1).getAs[String]("guest_type")=="customer")
  }

  test("assert guest_data_extensions/guests0001.json"){
    val config = getConfig("complex","guest_data_extensions/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)
    assert(result.take(1)(0).getAs[String]("ref")=="cbe910a8-bedc-4b2c-a219-95509253261f")
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(0).getAs[String]("name")=="Weather")
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(0).getAs[GenericRowWithSchema]("values").getAs[String]("city-name")==null)
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(0).getAs[GenericRowWithSchema]("values").getAs[String]("forecast")=="Clouds")
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(1).getAs[String]("name")=="Locations")
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(1).getAs[GenericRowWithSchema]("values").getAs[String]("city-name")=="Dublin")
    assert(result.take(1)(0).getAs[mutable.WrappedArray.ofRef[GenericRowWithSchema]]("dataExtensions")(1).getAs[GenericRowWithSchema]("values").getAs[String]("forecast")==null)
    //TODO - add in checks for long values in values of data extensions, validations, skipvalidation
    //result.select("dataExtensions.values.forecast").where("forecast == Clouds")
  }

  //  test("assert guest_data_extensions_when/guests0001.json"){
  //    val config = getConfig("complex", "guest_data_extensions_when/guests0001")
  //
  //    System.out.println(config.getSource.getPaths)
  //
  //    val df = spark.read.json(config.getSource.getPaths: _*)
  //
  //    System.out.println(df.count())
  //    assert(df.count() == 1)
  //
  //    OptimaETL.run(spark: SparkSession, config)
  //  }

  test("assert guest_quarantine/guests0001.json"){
    val config = getConfig("complex","guest_quarantine/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    val quarantine = spark.read.json(config.quarantine.path)
    assert(result.count() == 1)
    assert(quarantine.count() == 1)
  }

  test("assert guests_partitioning/guests0001.json"){
    val config = getConfig("complex", "guests_partitioning/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    ComplexStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.parquet(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(2)(0).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="jack.smith@boxever.com")
    assert(result.take(2)(0).getAs[mutable.WrappedArray.ofRef[String]]("emails")(1)=="jack@boxever.com")
    assert(result.take(2)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
    assert(result.take(2)(0).getAs[Integer]("last_seen_year")==2018)
    assert(result.take(2)(0).getAs[Integer]("last_seen_month")==12)
    assert(result.take(2)(0).getAs[Integer]("last_seen_day")==28)
    assert(result.take(2)(1).getAs[mutable.WrappedArray.ofRef[String]]("emails")(0)=="paul.smith@boxever.com")
    assert(result.take(2)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    assert(result.take(2)(1).getAs[Integer]("last_seen_year")==2018)
    assert(result.take(2)(1).getAs[Integer]("last_seen_month")==12)
    assert(result.take(2)(1).getAs[Integer]("last_seen_day")==28)
    assert(new File(config.destination.path + "/last_seen_year=2018/last_seen_month=12/last_seen_day=28").exists() == true)
    //result.filter("ref = '68ae46d0-3916-4d93-8b91-21425a351d5a'").collect()
  }

  test("assert optima_guests/guests0000.json"){
    val config = getConfig("simple", "optima_guests/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 2)

    SimpleStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 2)
    assert(result.take(2)(0).getAs[String]("emails")=="jack.smith@boxever.com, jack@boxever.com")
    assert(result.take(2)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
    assert(result.take(2)(1).getAs[String]("emails")=="paul.smith@boxever.com")
    assert(result.take(2)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
  }

  test("assert optima_guests_folder_does_not_exist/guests0000.json"){
    val config = getConfig("simple", "optima_guests_folder_does_not_exist/guests0001")

    System.out.println(config.getSource.getPaths)

    SimpleStructureMapper.run(spark: SparkSession, config)

    val expected = SimpleStructureDestinationSchema.getDestinationSchema(new StructType, config.mappings)
    val result = spark.read.schema(expected).parquet(config.destination.path)
    assert(result.count() == 0)
    assert(expected.equals(result.schema))
  }

  //  test("assert optima_guest_twitter/guests0000.json"){
  //    val config = getConfig("simple", "optima_guest_twitter/guests0001")
  //
  //    System.out.println(config.getSource.getPaths)
  //
  //    val df = spark.read.json(config.getSource.getPaths: _*)
  //
  //    System.out.println(df.count())
  //    assert(df.count() == 2)
  //
  //    SimpleStructureMapper.run(spark: SparkSession, config)
  //
  //    val result = spark.read.json(config.destination.path)
  //    assert(result.count() == 2)

  ////  assert(result.take(2)(0).getAs[String]("emails")=="jack.smith@boxever.com")
  ////  assert(result.take(2)(0).getAs[String]("ref")=="68ae46d0-3916-4d93-8b91-21425a351d5a")
  ////  assert(result.take(2)(0).getAs[String]("handle")=="@jack")
  ////  assert(result.take(2)(0).getAs[Long]("registeredAt")==1546032815914L)
  ////  assert(result.take(2)(1).getAs[String]("emails")=="paul.smith@boxever.com")
  ////  assert(result.take(2)(1).getAs[String]("ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
  ////  assert(result.take(2)(1).getAs[String]("handle")=="@paul")
  ////  assert(result.take(2)(1).getAs[Long]("registeredAt")==1546032815914L)
  //  }

  test("assert optima_guest_identifiers/guests0000.json"){
    val config = getConfig("simple", "optima_guest_identifiers/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 4)

    SimpleStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 6)
    assert(result.filter("guest_ref = '68ae46d0-3916-4d93-8b91-21425a351d5a'").count() == 0)
    assert(result.take(6)(0).getAs[String]("provider")=="Google")
    assert(result.take(6)(0).getAs[String]("expiry_date")=="2019-01-14T09:15:15.112Z")
    assert(result.take(6)(0).getAs[String]("guest_ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    assert(result.take(6)(1).getAs[String]("provider")=="Globe")
    assert(result.take(6)(1).getAs[String]("guest_ref")=="ce29f8bf-6c60-421d-8035-29cadecee40c")
    assert(result.take(6)(2).getAs[String]("expiry_date")=="2019-01-14T09:15:15.112Z")
    assert(result.take(6)(2).getAs[String]("provider")=="Google")
//    assert(result.take(6)(3).getAs[String]("expiry_date")=="1970-01-01T00:00:00.000Z")
    assert(result.take(6)(3).getAs[String]("provider")=="Globe")
    assert(result.take(6)(4).getAs[String]("provider")=="Google")
  }

  test("assert optima_guest_data_extensions/guests0000.json"){
    val config = getConfig("simple", "optima_guest_data_extensions/guests0001")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 1)

    SimpleStructureMapper.run(spark: SparkSession, config)

    val result = spark.read.json(config.destination.path)
    assert(result.count() == 1)

    assert(result.take(1)(0).getAs[String]("ref")=="cbe910a8-bedc-4b2c-a219-95509253261f")
    assert(result.take(1)(0).getAs[String]("name")=="Weather")
    assert(result.take(1)(0).getAs[String]("forecast")=="Clouds")
  }


  //Placeholder for testing with parquet data
  //Data should reside in src/test/resources/com/boxever/labs/spark/optima/simplestructure/sessions/sessions/
  //  test("assert sessions/sessions.parquet"){
  //    val config = getConfig("simple","sessions/sessions")
  //
  //    System.out.println(config.getSource.getPaths)
  //
  //    SimpleStructureMapper.run(spark: SparkSession, config)
  //  }

  test("segments"){
    val config = getConfig("segments", "segments/segments")

    System.out.println(config.getSource.getPaths)

    val df = spark.read.json(config.getSource.getPaths: _*)

    System.out.println(df.count())
    assert(df.count() == 6)

    Segments.run(spark: SparkSession, config)
    val result = spark.read.json(config.destination.path)
    assert(result.count() == 12)
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a3'").count() == 4)
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a3'").collect()(0).getAs[String]("segment_name") == "Guests with offline orders")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a3'").collect()(0).getAs[String]("segment_ref") == "2d12675c-6b51-455b-a584-4d74e7820590")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a3'").collect()(2).getAs[String]("segment_name") == "Retain At Risk - Non Fallow")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a3'").collect()(2).getAs[String]("segment_ref") == "b2397172-1086-4fb5-b9d3-cab90968f221")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a6'").count() == 3)
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a6'").collect()(0).getAs[String]("segment_name") == "All Uploaded Customers")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a6'").collect()(0).getAs[String]("segment_ref") == "37d70b8f-30e7-47b9-b3c2-ceed6a6e6b4f")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a6'").collect()(2).getAs[String]("segment_name") == "Retain At Risk - All")
    assert(result.filter("guest_ref = 'bc853b2f-12ae-45ce-8a4c-336a6652c3a6'").collect()(2).getAs[String]("segment_ref") == "f951969d-1fc3-45af-9629-655dcd59cdf6")
  }


  protected override def beforeAll(): Unit = {
    super.beforeAll()
  }

  protected override def afterAll(): Unit = {
    super.afterAll()
  }
}