package com.boxever.labs.spark.optima.hive

import org.apache.spark.sql.types._
import org.scalatest.FunSuite

class HiveSchemaSuite extends FunSuite {
  test("assert can build schema with simple types") {
    val schemaName = "optima_trusted"
    val tableName = "guests"
    val partitions = List[String]("guest_type")
    val dataPath = "s3://boxever-test-bucket/optima_trusted/guests"

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("guest_type", StringType)
      .add("age", IntegerType)

    val ddl = new HiveSchema(sourceSchema).getHiveSchema(schemaName, tableName, true, partitions, dataPath)
    val expected = "DROP TABLE IF EXISTS optima_trusted.guests;\n\nCREATE EXTERNAL TABLE optima_trusted.guests (\n\t`ref` STRING,\n\t`age` INT\n)\nPARTITIONED BY (\n\t`guest_type` STRING\n)\nSTORED AS PARQUET\nLOCATION 's3://boxever-test-bucket/optima_trusted/guests';"
    System.out.println(ddl)
    System.out.println(expected)
    assert(ddl == expected)
  }

  test("assert can build schema with collection of simple types") {
    val schemaName = "optima_trusted"
    val tableName = "guests"
    val partitions = List[String]("guest_type")
    val dataPath = "s3://boxever-test-bucket/optima_trusted/guests"

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("guest_type", StringType)
      .add("emails", ArrayType(StringType))
      .add("age", IntegerType)

    val ddl = new HiveSchema(sourceSchema).getHiveSchema(schemaName, tableName, true, partitions, dataPath)
    val expected = "DROP TABLE IF EXISTS optima_trusted.guests;\n\nCREATE EXTERNAL TABLE optima_trusted.guests (\n\t`ref` STRING,\n\t`emails` ARRAY<STRING>,\n\t`age` INT\n)\nPARTITIONED BY (\n\t`guest_type` STRING\n)\nSTORED AS PARQUET\nLOCATION 's3://boxever-test-bucket/optima_trusted/guests';"
    System.out.println(ddl)
    System.out.println(expected)
    assert(ddl == expected)
  }

  test("assert can build schema with complex type") {
    val schemaName = "optima_trusted"
    val tableName = "guests"
    val partitions = List[String]("guest_type")
    val dataPath = "s3://boxever-test-bucket/optima_trusted/guests"

    val twitterSchema = new StructType()
      .add("handle", StringType)
      .add("registeredAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("guest_type", StringType)
      .add("twitter", twitterSchema)

    val ddl = new HiveSchema(sourceSchema).getHiveSchema(schemaName, tableName, true, partitions, dataPath)
    val expected = "DROP TABLE IF EXISTS optima_trusted.guests;\n\nCREATE EXTERNAL TABLE optima_trusted.guests (\n\t`ref` STRING,\n\t`twitter` STRUCT<`handle`: STRING, `registeredAt`: BIGINT>\n)\nPARTITIONED BY (\n\t`guest_type` STRING\n)\nSTORED AS PARQUET\nLOCATION 's3://boxever-test-bucket/optima_trusted/guests';"
    System.out.println(ddl)
    System.out.println(expected)
    assert(ddl == expected)
  }

  test("assert can build schema with collection of complex types") {
    val schemaName = "optima_trusted"
    val tableName = "guests"
    val partitions = List[String]("guest_type")
    val dataPath = "s3://boxever-test-bucket/optima_trusted/guests"

    val identifiersSchema = new StructType()
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("guest_type", StringType)
      .add("identifiers", ArrayType(identifiersSchema))

    val ddl = new HiveSchema(sourceSchema).getHiveSchema(schemaName, tableName, true, partitions, dataPath)
    val expected = "DROP TABLE IF EXISTS optima_trusted.guests;\n\nCREATE EXTERNAL TABLE optima_trusted.guests (\n\t`ref` STRING,\n\t`identifiers` ARRAY<STRUCT<`id`: STRING, `provider`: STRING, `expiresAt`: BIGINT>>\n)\nPARTITIONED BY (\n\t`guest_type` STRING\n)\nSTORED AS PARQUET\nLOCATION 's3://boxever-test-bucket/optima_trusted/guests';"
    System.out.println(ddl)
    System.out.println(expected)
    assert(ddl == expected)
  }
}
