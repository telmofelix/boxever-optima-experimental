package com.boxever.labs.spark.optima.complexstructure

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureRowTransformations.transformRow
import com.boxever.labs.spark.optima.spec.{Mapping, MappingDestination, MappingSource}
import org.apache.spark.sql.catalyst.expressions.{GenericRow, GenericRowWithSchema}
import org.apache.spark.sql.types._
import org.scalatest.FunSuite

import scala.collection.mutable

class ComplexStructureRowTransformationsSuite extends FunSuite {

  def getSimpleMapping(name: String, fieldType: String): Mapping = {
    new Mapping(
      source = new MappingSource(
        name = name,
        fieldType = fieldType),
      destination = new MappingDestination(
        name = name,
        fieldType = fieldType,
        mappings = List()),
      transformations = List(), validations = List()
    )
  }

  def getSimpleMapping(sourceName: String, sourceFieldType: String, destName: String, destFieldType: String): Mapping = {
    new Mapping(
      source = new MappingSource(
        name = sourceName,
        fieldType = sourceFieldType),
      destination = new MappingDestination(
        name = destName,
        fieldType = destFieldType,
        mappings = List()),
      transformations = List(), validations = List()
    )
  }

  test("assert simple type mapping") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("createdAt", LongType)
      .add("guestType", StringType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", 1546032815914L, "customer"), sourceSchema)

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      getSimpleMapping("createdAt", "long"),
      getSimpleMapping("guestType", "string")
    )

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == 1546032815914L)
    assert(transformedRow.get(2) == "customer")
  }

  test("assert simple type mapping with different order of field names") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("createdAt", LongType)
      .add("guestType", StringType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", 1546032815914L, "customer"), sourceSchema)

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      getSimpleMapping("createdAt", "long"),
      getSimpleMapping("guestType", "string")
    )

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == 1546032815914L)
    assert(transformedRow.get(2) == "customer")
  }

  test("assert simple type mapping with string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array("jack@boxever.com", "peter@boxever.com"))), sourceSchema)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      emailsMapping
    )

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1)(0) == "jack@boxever.com")
    assert(transformedRow.getAs[mutable.WrappedArray[String]](1)(1) == "peter@boxever.com")
  }

  test("assert simple type mapping with empty string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array())), sourceSchema)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      emailsMapping
    )

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1).isEmpty)
  }

  test("assert simple type mapping with null string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", null), sourceSchema)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      emailsMapping
    )

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1) == null)
  }

  test("assert complex type mapping") {

    val twitterSchema = new StructType()
      .add("handle", StringType)
      .add("registeredAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("twitter", twitterSchema)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", new GenericRowWithSchema(Array("@boxever", 1546032815914L), twitterSchema)), sourceSchema)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "twitter",
        fieldType = "complex",
        collection = false),
      destination = new MappingDestination(
        name = "twitter",
        fieldType = null,
        mappings = List(
          getSimpleMapping("handle", "string"),
          getSimpleMapping("registeredAt", "long")
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(
      getSimpleMapping("ref", "string"),
      twitterMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    val twitter = transformedRow.getStruct(1)

    assert(twitter.get(0) == "@boxever")
    assert(twitter.get(1) == 1546032815914L)
  }

  test("assert collection of complex type mapping") {

    val identifiersSchema = new StructType()
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("identifiers", ArrayType(identifiersSchema))

    System.out.println("Source Schema")
    sourceSchema.printTreeString()

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array[GenericRowWithSchema](new GenericRowWithSchema(Array("1234", "Google", 1546032815914L), identifiersSchema)))), sourceSchema)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "identifiers",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "identifiers",
        fieldType = null,
        mappings = List(
          getSimpleMapping("id", "string"),
          getSimpleMapping("provider", "string"),
          getSimpleMapping("expiresAt", "long")
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), twitterMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")

    val identifiers = transformedRow.getAs[mutable.WrappedArray[GenericRowWithSchema]](1)

    assert(identifiers(0).get(0) == "1234")
    assert(identifiers(0).get(1) == "Google")
    assert(identifiers(0).get(2) == 1546032815914L)
  }

  test("assert collection of empty complex type mapping") {

    val identifiersSchema = new StructType()
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("identifiers", ArrayType(identifiersSchema))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array[GenericRowWithSchema]())), sourceSchema)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "identifiers",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "identifiers",
        fieldType = null,
        mappings = List(
          getSimpleMapping("id", "string"),
          getSimpleMapping("provider", "string"),
          getSimpleMapping("expiresAt", "long")
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), twitterMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")

    val identifiers = transformedRow.getAs[mutable.WrappedArray[GenericRowWithSchema]](1)

    assert(identifiers.isEmpty)
  }

  test("assert collection of null complex type mapping") {

    val identifiersSchema = new StructType()
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("identifiers", ArrayType(identifiersSchema))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array("2b74425e-99c4-45e2-97e5-8e52cff87e05", null), sourceSchema)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "identifiers",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "identifiers",
        fieldType = null,
        mappings = List(
          getSimpleMapping("id", "string"),
          getSimpleMapping("provider", "string"),
          getSimpleMapping("expiresAt", "long")
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), twitterMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")

    val identifiers = transformedRow.getAs[mutable.WrappedArray[GenericRowWithSchema]](1)

    assert(identifiers == null)
  }

  // Complex Type with nested Complex Type
  test("assert Complex Type with nested Complex Type") {

    val dataExtensionValuesSchema = new StructType()
      .add("utm_campaign", StringType)
      .add("utm_medium", StringType)

    val dataExtensionSchema = new StructType()
      .add("name", StringType)
      .add("key", StringType)
      .add("createdAt", LongType)
      .add("modifiedAt", LongType)
      .add("values", dataExtensionValuesSchema)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("dataExtensions", dataExtensionSchema)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array(
      "2b74425e-99c4-45e2-97e5-8e52cff87e05",
      (new GenericRowWithSchema(Array(
        "ArbitraryData",
        "default",
        1546032815914L,
        1546032815914L
        ,
//        Array(
          new GenericRowWithSchema(
          Array("MyCampaign", "EMAIL"), dataExtensionValuesSchema)
//        )
      ), dataExtensionSchema))), sourceSchema)

    val dataExtensionMapping = new Mapping(
      source = new MappingSource(
        name = "dataExtensions",
        fieldType = "complex"),
      destination = new MappingDestination(
        name = "dataExtensions",
        fieldType = "complex",
        mappings = List(
          getSimpleMapping("name", "string"),
          getSimpleMapping("key", "string"),
          getSimpleMapping("createdAt", "long"),
          getSimpleMapping("modifiedAt", "long")
          ,
          new Mapping(
            source = new MappingSource(
              name = "values",
              fieldType = "complex"),
            destination = new MappingDestination(
              name = "values",
              fieldType = "complex",
              mappings = List(
                getSimpleMapping("utm_campaign", "string"),
                getSimpleMapping("utm_medium", "string")
              )),
            transformations = List(), validations = List()
          )
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), dataExtensionMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.getAs[GenericRow](1)(0) == "ArbitraryData")
    assert(transformedRow.getAs[GenericRow](1)(2) == 1546032815914L)
    assert(transformedRow.getAs[GenericRow](1)(4).asInstanceOf[GenericRow].getString(0) == "MyCampaign")
    assert(transformedRow.getAs[GenericRow](1)(4).asInstanceOf[GenericRow].getString(1) == "EMAIL")
  }


  //Complex Type with nested Collection Complex Type
  test("assert Complex Type with nested Collection Complex Type") {

    val dataExtensionValuesSchema = new StructType()
      .add("utm_campaign", StringType)
      .add("utm_medium", StringType)

    val dataExtensionSchema = new StructType()
      .add("name", StringType)
      .add("key", StringType)
      .add("createdAt", LongType)
      .add("modifiedAt", LongType)
      .add("values", ArrayType(dataExtensionValuesSchema))

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("dataExtensions", dataExtensionSchema)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array(
      "2b74425e-99c4-45e2-97e5-8e52cff87e05",
      new GenericRowWithSchema(Array(
        "ArbitraryData",
        "default",
        1546032815914L,
        1546032815914L
        ,
        mutable.WrappedArray.make(Array[GenericRowWithSchema](new GenericRowWithSchema(Array(
          "MyCampaign",
          "EMAIL"), dataExtensionValuesSchema))
        )
      )
        , dataExtensionSchema)),
       sourceSchema)

    val dataExtensionMapping = new Mapping(
      source = new MappingSource(
        name = "dataExtensions",
        fieldType = "complex"),
      destination = new MappingDestination(
        name = "dataExtensions",
        fieldType = "complex",
        mappings = List(
          getSimpleMapping("name", "string"),
          getSimpleMapping("key", "string"),
          getSimpleMapping("createdAt", "long"),
          getSimpleMapping("modifiedAt", "long")
                    ,
                    new Mapping(
                      source = new MappingSource(
                        name = "values",
                        fieldType = "complex",
                        collection = true),
                      destination = new MappingDestination(
                        name = "values",
                        fieldType = "complex",
                        collection = true,
                        mappings = List(
                          getSimpleMapping("utm_campaign", "string"),
                          getSimpleMapping("utm_medium", "string")
                        )),
                      transformations = List(), validations = List()
                    )
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), dataExtensionMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.getAs[GenericRow](1)(0) == "ArbitraryData")
    assert(transformedRow.getAs[GenericRow](1)(2) == 1546032815914L)
    assert(transformedRow.getAs[GenericRow](1)(4).asInstanceOf[mutable.WrappedArray.ofRef[GenericRow]](0).getString(0) == "MyCampaign")
    assert(transformedRow.getAs[GenericRow](1)(4).asInstanceOf[mutable.WrappedArray.ofRef[GenericRow]](0).getString(1) == "EMAIL")
  }


  //  Collection of Complex Type with nested Complex Type
  test("assert Complex Type with nested collection of Complex Type with nested Complex Type") {

    val dataExtensionValuesSchema = new StructType()
      .add("utm_campaign", StringType)
      .add("utm_medium", StringType)

    val dataExtensionSchema = new StructType()
      .add("name", StringType)
      .add("key", StringType)
      .add("createdAt", LongType)
      .add("modifiedAt", LongType)
      .add("values", dataExtensionValuesSchema)

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("dataExtensions", ArrayType(dataExtensionSchema))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array(
      "2b74425e-99c4-45e2-97e5-8e52cff87e05",
      mutable.WrappedArray.make(Array[GenericRowWithSchema](new GenericRowWithSchema(Array(
        "ArbitraryData",
        "default",
        1546032815914L,
        1546032815914L
        ,
        //        Array(
        new GenericRowWithSchema(
          Array("MyCampaign", "EMAIL"), dataExtensionValuesSchema)
        //        )
      ), dataExtensionSchema))),
      dataExtensionSchema), sourceSchema)

    val dataExtensionMapping = new Mapping(
      source = new MappingSource(
        name = "dataExtensions",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "dataExtensions",
        fieldType = "complex",
        collection = true,
        mappings = List(
          getSimpleMapping("name", "string"),
          getSimpleMapping("key", "string"),
          getSimpleMapping("createdAt", "long"),
          getSimpleMapping("modifiedAt", "long")
          ,
          new Mapping(
            source = new MappingSource(
              name = "values",
              fieldType = "complex"),
            destination = new MappingDestination(
              name = "values",
              fieldType = "complex",
              mappings = List(
                getSimpleMapping("utm_campaign", "string"),
                getSimpleMapping("utm_medium", "string")
              )),
            transformations = List(), validations = List()
          )
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), dataExtensionMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(0) == "ArbitraryData")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(2) == 1546032815914L)
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(4).asInstanceOf[GenericRow].getString(0) == "MyCampaign")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(4).asInstanceOf[GenericRow].getString(1) == "EMAIL")
  }

  //  Collection of Complex Type with nested Collection Complex Type
  test("assert Collection of Complex Type with nested Collection Complex Type") {

    val dataExtensionValuesSchema = new StructType()
      .add("utm_campaign", StringType)
      .add("utm_medium", StringType)

    val dataExtensionSchema = new StructType()
      .add("name", StringType)
      .add("key", StringType)
      .add("createdAt", LongType)
      .add("modifiedAt", LongType)
      .add("values", ArrayType(dataExtensionValuesSchema))

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("dataExtensions", ArrayType(dataExtensionSchema))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema(Array(
      "2b74425e-99c4-45e2-97e5-8e52cff87e05",
      mutable.WrappedArray.make(Array[GenericRowWithSchema](new GenericRowWithSchema(Array(
        "ArbitraryData",
        "default",
        1546032815914L,
        1546032815914L
        ,
        mutable.WrappedArray.make(Array[GenericRowWithSchema](new GenericRowWithSchema(Array(
          "MyCampaign",
          "EMAIL"), dataExtensionValuesSchema))
        )
      )
        , dataExtensionSchema))),
      dataExtensionSchema), sourceSchema)

    val dataExtensionMapping = new Mapping(
      source = new MappingSource(
        name = "dataExtensions",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "dataExtensions",
        fieldType = "complex",
        collection = true,
        mappings = List(
          getSimpleMapping("name", "string"),
          getSimpleMapping("key", "string"),
          getSimpleMapping("createdAt", "long"),
          getSimpleMapping("modifiedAt", "long")
          ,
          new Mapping(
            source = new MappingSource(
              name = "values",
              fieldType = "complex",
              collection = true),
            destination = new MappingDestination(
              name = "values",
              fieldType = "complex",
              collection = true,
              mappings = List(
                getSimpleMapping("utm_campaign", "string"),
                getSimpleMapping("utm_medium", "string")
              )),
            transformations = List(), validations = List()
          )
        )
      ),
      transformations = List(), validations = List())

    val mappings: List[Mapping] = List(getSimpleMapping("ref", "string"), dataExtensionMapping)

    val transformedRow = transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(0) == "ArbitraryData")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(2) == 1546032815914L)
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(4).asInstanceOf[mutable.WrappedArray.ofRef[GenericRow]](0).getString(0) == "MyCampaign")
    assert(transformedRow.get(1).asInstanceOf[mutable.WrappedArray.ofRef[GenericRowWithSchema]](0)(4).asInstanceOf[mutable.WrappedArray.ofRef[GenericRow]](0).getString(1) == "EMAIL")
  }

  //TODO - add more tests for values in data-extensions with long types, validations, skipValidationIfNull, etc

}
