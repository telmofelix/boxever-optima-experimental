package com.boxever.labs.spark.optima.spec

import java.io.File

import com.boxever.labs.spark.optima.spec.OptimaSpecReader.read
import org.scalatest.FunSuite

class OptimaSpecReaderSuite  extends FunSuite {
  test("assert we can read all the files and they are structurally valid") {
    var userDir = new File(System.getProperty("user.dir"))
    if (userDir.getName == "boxever-optima") {
      userDir = userDir.getParentFile
    }

    // Ansible
    // Airline
    // Trusted Specs (Prototype)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/prototype/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/prototype/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/prototype/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/prototype/events.yml", Map())

    // Airline
    // Optima Specs (Prototype)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/guest_emails.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/guest_identifiers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/guest_subscriptions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_consumers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_consumer_order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_contacts.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_item_flights.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/order_item_flight_segments.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/sessions.yml", Map())

    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_add.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_campaign_tracking.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_checkout.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_identity.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_search.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_tracking_execution.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_tracking_interaction.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/prototype/events_view.yml", Map())

    // Airline
    // Trusted Specs (Phase1)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1/events.yml", Map())

    // Airline
    // Optima Specs (Phase1)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/guest_emails.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/guest_identifiers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/guest_subscriptions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_consumers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_consumer_order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_contacts.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_item_flights.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/order_item_flight_segments.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_add.yml", Map())
//    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_campaign_tracking.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_checkout.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_identity.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_search.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_tracking_execution.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_tracking_interaction.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_view.yml", Map())

    // Airline
    // Trusted Specs (Phase1 PII)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1_pii/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1_pii/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1_pii/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/airline/phase1_pii/events.yml", Map())

    // Airline
    // Optima Specs (Phase1 PII)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/guest_emails.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/guest_identifiers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/guest_subscriptions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_consumers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_consumer_order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_contacts.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_item_flights.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/order_item_flight_segments.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_add.yml", Map())
    //    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_campaign_tracking.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_checkout.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_identity.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_search.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_tracking_execution.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_tracking_interaction.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1_pii/events_view.yml", Map())

//    // Banking
//    // Trusted Specs (Phase1)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/banking/phase1/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/banking/phase1/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/banking/phase1/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-trusted-specs/banking/phase1/events.yml", Map())
//
//    // Banking
//    // Optima Specs (Phase1)
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/guest_emails.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/guest_identifiers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/guest_subscriptions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/guests.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/orders.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/order_consumers.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/order_consumer_order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/order_contacts.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/order_items.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/sessions.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_calculator.yml", Map())
    //    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/airline/phase1/events_campaign_tracking.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_checkout.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_contact_request.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_identity.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_search.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_tracking_execution.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_tracking_interaction.yml", Map())
    read(userDir + "/ansible/roles/boxever-optima/templates/optima-specs/banking/phase1/events_view.yml", Map())


    // EMR Emirates
    // Trusted Specs
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/trusted/emirates/guests.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/trusted/emirates/orders.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/trusted/emirates/sessions.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/trusted/emirates/events.yml", Map())

    // Explore Specs
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/guests.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/guest_identifiers.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/guest_subscriptions.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/orders.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_consumers.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_consumer_order_items.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_contacts.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_items.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_item_flights.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/order_item_flight_segments.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/sessions.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/events.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/events_search.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/events_tracking_execution.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/events_tracking_interaction.yml", Map())
    read(userDir + "/boxever-optima/src/main/resources/optima/specs/emr/explore/emirates/events_view.yml", Map())

    // Tests (src/test/resources)
    read(userDir + "/boxever-optima/src/test/resources/com/boxever/labs/spark/optima/complex/yaml_validation/guests0001.yml", Map())

  }
}
