package com.boxever.labs.spark.optima.simplestructure

import com.boxever.labs.spark.optima.spec.{Mapping, MappingDestination, MappingSource}
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types._
import org.scalatest.FunSuite

import scala.collection.mutable

class TransformationsSuite extends FunSuite {

  test("assert simple type mapping") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("createdAt", LongType)
      .add("guestType", StringType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", 1546032815914L, "customer").toArray, sourceSchema)

    val refMapping = new Mapping(
        source = new MappingSource(
          name = "ref",
          fieldType = "string",
          collection = false),
        destination = new MappingDestination(
          name = "ref",
          fieldType = "string",
          mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val createdAtMapping = new Mapping(
      source = new MappingSource(
        name = "createdAt",
        fieldType = "long",
        collection = false),
      destination = new MappingDestination(
        name = "createdAt",
        fieldType = "long",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val guestTypeMapping = new Mapping(
      source = new MappingSource(
        name = "guestType",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "guestType",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, createdAtMapping, guestTypeMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == 1546032815914L)
    assert(transformedRow.get(2) == "customer")
  }

  test("assert simple type mapping with string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array("jack@boxever.com", "peter@boxever.com")) ).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, emailsMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1)(0) == "jack@boxever.com")
    assert(transformedRow.getAs[mutable.WrappedArray[String]](1)(1) == "peter@boxever.com")
  }

  test("assert simple type mapping with empty string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", mutable.WrappedArray.make(Array()) ).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, emailsMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1).isEmpty)
  }

  test("assert simple type mapping with null string array") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("emails", ArrayType(StringType))

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", null ).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val emailsMapping = new Mapping(
      source = new MappingSource(
        name = "emails",
        fieldType = "string",
        collection = true),
      destination = new MappingDestination(
        name = "emails",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, emailsMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.getAs[mutable.WrappedArray[String]](1) == null)
  }

  test("assert complex type mapping") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("handle", StringType)
      .add("registeredAt", LongType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", "@boxever", 1546032815914L).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "twitter",
        fieldType = "complex",
        collection = false),
      destination = new MappingDestination(
        name = "twitter",
        fieldType = null,
        mappings = Seq(
          new Mapping(
            source = new MappingSource(
              name = "handle",
              fieldType = "string",
              collection = false),
            destination = new MappingDestination(
              name = "handle",
              fieldType = "string",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          ),
          new Mapping(
            source = new MappingSource(
              name = "registeredAt",
              fieldType = "long",
              collection = false),
            destination = new MappingDestination(
              name = "registeredAt",
              fieldType = "long",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          )
        ).toList
      ),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, twitterMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == "@boxever")
    assert(transformedRow.get(2) == 1546032815914L)
  }

  test("assert collection of complex type mapping") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", "1234", "Google", 1546032815914L).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "identifiers",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "identifiers",
        fieldType = null,
        mappings = Seq(
          new Mapping(
            source = new MappingSource(
              name = "id",
              fieldType = "string",
              collection = false),
            destination = new MappingDestination(
              name = "id",
              fieldType = "string",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          ),
          new Mapping(
            source = new MappingSource(
              name = "provider",
              fieldType = "string",
              collection = false),
            destination = new MappingDestination(
              name = "provider",
              fieldType = "string",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          ),
          new Mapping(
            source = new MappingSource(
              name = "expiresAt",
              fieldType = "long",
              collection = false),
            destination = new MappingDestination(
              name = "expiresAt",
              fieldType = "long",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          )
        ).toList
      ),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, twitterMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == "1234")
    assert(transformedRow.get(2) == "Google")
    assert(transformedRow.get(3) == 1546032815914L)
  }

  test("assert collection of empty complex type mapping") {

    val sourceSchema = new StructType()
      .add("ref", StringType)
      .add("id", StringType)
      .add("provider", StringType)
      .add("expiresAt", LongType)

    System.out.println("Source Schema");
    sourceSchema.printTreeString();

    val row = new GenericRowWithSchema( Seq("2b74425e-99c4-45e2-97e5-8e52cff87e05", null, null, null ).toArray, sourceSchema)

    val refMapping = new Mapping(
      source = new MappingSource(
        name = "ref",
        fieldType = "string",
        collection = false),
      destination = new MappingDestination(
        name = "ref",
        fieldType = "string",
        mappings = null),
      transformations = Seq().toList, validations = Seq().toList)

    val twitterMapping = new Mapping(
      source = new MappingSource(
        name = "identifiers",
        fieldType = "complex",
        collection = true),
      destination = new MappingDestination(
        name = "identifiers",
        fieldType = null,
        mappings = Seq(
          new Mapping(
            source = new MappingSource(
              name = "id",
              fieldType = "string",
              collection = false),
            destination = new MappingDestination(
              name = "id",
              fieldType = "string",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          ),
          new Mapping(
            source = new MappingSource(
              name = "provider",
              fieldType = "string",
              collection = false),
            destination = new MappingDestination(
              name = "provider",
              fieldType = "string",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          ),
          new Mapping(
            source = new MappingSource(
              name = "expiresAt",
              fieldType = "long",
              collection = false),
            destination = new MappingDestination(
              name = "expiresAt",
              fieldType = "long",
              mappings = null),
            transformations = Seq().toList,
            validations = Seq().toList
          )
        ).toList
      ),
      transformations = Seq().toList, validations = Seq().toList)

    val mappings: List[Mapping] = Seq(refMapping, twitterMapping).toList

    val transformedRow = SimpleStructureMapper.transformRow(row, mappings)

    assert(transformedRow.get(0) == "2b74425e-99c4-45e2-97e5-8e52cff87e05")
    assert(transformedRow.get(1) == null)
  }
}