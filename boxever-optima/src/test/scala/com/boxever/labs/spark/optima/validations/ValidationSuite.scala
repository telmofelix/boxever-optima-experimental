package com.boxever.labs.spark.optima.validations

import javax.validation.ValidationException
import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer

class ValidationSuite extends FunSuite {

  /*
   * UUID and matchesPattern validation tests
   */

  val uuids = List("0afe2191-32fd-480c-ad27-7e314244396c",
                   "b205859d-253f-4e43-b773-6008b0363862")

  val patterns = List("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")

  val min : String = "0"
  val max : String = "100"

  test(s"UUIDs are isUUID") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.isUUID("test_column", uuid)
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"UUIDs match ${patterns}") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.matchesPattern("test_column", uuid, patterns)
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"UUIDs are isUUID and match ${patterns}") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.matchesPattern("test_column", uuid, patterns)
        ColumnValidations.isUUID("test_column", uuid)
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"UUIDs min/max length validation ${patterns}") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.minLength("test_column", uuid, List("0"))
        ColumnValidations.maxLength("test_column", uuid, List("64"))
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"UUIDs min/max length validation ${patterns} fail minimum") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.minLength("test_column", uuid, List("100"))
        ColumnValidations.maxLength("test_column", uuid, List("512"))
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors(0)=="Failed min length check: List(100), for value: \"0afe2191-32fd-480c-ad27-7e314244396c\", for dest column: test_column")
    assert(errors(1)=="Failed min length check: List(100), for value: \"b205859d-253f-4e43-b773-6008b0363862\", for dest column: test_column")
  }

  test(s"UUIDs min/max length validation ${patterns} fail maximum") {
    var errors = new ListBuffer[String]()
    uuids.foreach(uuid =>
      try {
        ColumnValidations.minLength("test_column", uuid, List("0"))
        ColumnValidations.maxLength("test_column", uuid, List("16"))
      } catch {
        case e : ValidationException => errors += e.getMessage
      }
    )
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors(0)=="Failed max length check: List(16), for value: \"0afe2191-32fd-480c-ad27-7e314244396c\", for dest column: test_column")
    assert(errors(1)=="Failed max length check: List(16), for value: \"b205859d-253f-4e43-b773-6008b0363862\", for dest column: test_column")
  }

  test(s"min/max validation on int ${min} - ${max}") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 10, List(min))
      ColumnValidations.maximum("test_column", 10, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"min/max validation on int ${min} - ${max} fail minimum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", -1000, List(min))
      ColumnValidations.maximum("test_column", -1000, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed minimum check: List(0), for value: \"-1000\", for dest column: test_column")
  }

  test(s"min/max validation on int ${min} - ${max} fail maximum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 1000, List(min))
      ColumnValidations.maximum("test_column", 1000, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed maximum check: List(100), for value: \"1000\", for dest column: test_column")
  }

  test(s"min/max validation on long ${min} - ${max}") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 10.toLong, List(min))
      ColumnValidations.maximum("test_column", 10.toLong, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"min/max validation on long ${min} - ${max} fail minimum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", -1000.toLong, List(min))
      ColumnValidations.maximum("test_column", -1000.toLong, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed minimum check: List(0), for value: \"-1000\", for dest column: test_column")
  }

  test(s"min/max validation on long ${min} - ${max} fail maximum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 1000.toLong, List(min))
      ColumnValidations.maximum("test_column", 1000.toLong, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed maximum check: List(100), for value: \"1000\", for dest column: test_column")
  }

  test(s"min/max validation on float ${min} - ${max}") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 10.toFloat, List(min))
      ColumnValidations.maximum("test_column", 10.toFloat, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"min/max validation on float ${min} - ${max} fail minimum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", -1000.toFloat, List(min))
      ColumnValidations.maximum("test_column", -1000.toFloat, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed minimum check: List(0), for value: \"-1000.0\", for dest column: test_column")
  }

  test(s"min/max validation on float ${min} - ${max} fail maximum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 1000.toFloat, List(min))
      ColumnValidations.maximum("test_column", 1000.toFloat, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed maximum check: List(100), for value: \"1000.0\", for dest column: test_column")
  }

  test(s"min/max validation on double ${min} - ${max}") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 10.toDouble, List(min))
      ColumnValidations.maximum("test_column", 10.toDouble, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.nonEmpty) { fail(errors.mkString("\n")) }
  }

  test(s"min/max validation on double ${min} - ${max} fail minimum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", -1000.toDouble, List(min))
      ColumnValidations.maximum("test_column", -1000.toDouble, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed minimum check: List(0), for value: \"-1000.0\", for dest column: test_column")
  }

  test(s"min/max validation on double ${min} - ${max} fail maximum") {
    var errors = new ListBuffer[String]()
    try {
      ColumnValidations.minimum("test_column", 1000.toDouble, List(min))
      ColumnValidations.maximum("test_column", 1000.toDouble, List(max))
    } catch {
      case e : ValidationException => errors += e.getMessage
    }
    if (errors.isEmpty) { fail(errors.mkString("\n")) }
    assert(errors.head=="Failed maximum check: List(100), for value: \"1000.0\", for dest column: test_column")
  }
}
