package com.boxever.labs.spark.optima.parsers

import org.scalatest.FunSuite

class OptimaArgParserSuite extends FunSuite {
  test("assert can parse basic arguments") {
    val args = Array("--config_file", "/tmp/myfile.txt", "--date", "2018/02/01")

    val options = OptimaArgParser.getResolvedOptions(args)

    assert(options.size == 2)
    assert(options("config_file") == "/tmp/myfile.txt")
    assert(options("date") == "2018/02/01")
  }

  test("assert can ignore non arguments") {
    val args = Array("--config_file", "/tmp/myfile.txt", "test", "--date", "2018/02/01", "--noOptionSpecified")

    val options = OptimaArgParser.getResolvedOptions(args)

    assert(options.size == 2)
    assert(options("config_file") == "/tmp/myfile.txt")
    assert(options("date") == "2018/02/01")
  }
}
