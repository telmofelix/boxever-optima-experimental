package com.boxever.labs.spark.optima.complexstructure

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureDestinationSchema._
import com.boxever.labs.spark.optima.spec.{Mapping, MappingDestination, MappingSource}
import org.apache.spark.sql.types._
import org.scalatest.FunSuite

class ComplexStructureDestinationSchemaSuite extends FunSuite {

  def assertStandardFields(targetStruct: StructType): Any = {
    // containsValidationErrors
    assert(targetStruct.fields(targetStruct.fields.length - 2).name == "containsValidationErrors")
    assert(targetStruct.fields(targetStruct.fields.length - 2).dataType.isInstanceOf[BooleanType])

    // validationErrors
    assert(targetStruct.fields(targetStruct.fields.length - 1).name == "validationErrors")
    assert(targetStruct.fields(targetStruct.fields.length - 1).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(targetStruct.fields.length - 1).dataType.asInstanceOf[ArrayType].elementType.isInstanceOf[StringType])
  }

  test("assert can build destination schema with a simple type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    //targetStruct.printTreeString()

    assert(targetStruct.fields.length == 3)
    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of simple type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string", collection = true), MappingDestination("field-1-target", "string", null), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    //targetStruct.printTreeString()

    assert(targetStruct.fields.length == 3)
    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(0).dataType.asInstanceOf[ArrayType].elementType.isInstanceOf[StringType])

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a complex type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex"),  MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"),  MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"),  MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-3", "string"),  MappingDestination("complex-field-3-target", "string", null), null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()

    assert(targetStruct.fields.length == 4)
    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[StructType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[StringType])

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of complex type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex", collection = true), MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-3", "string"), MappingDestination("complex-field-3-target", "string", null), null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[StringType])

    assertStandardFields(targetStruct)
  }

  //Complex Type with nested Complex Type
  test("assert can build destination schema with a Complex Type with nested Complex Type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex"),  MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"),  MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"),  MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("nested-complex", "complex"),  MappingDestination("nested-complex-target", fieldType = "complex",
            List(
              Mapping(MappingSource("complex-field-1", "string"),  MappingDestination("complex-field-1-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-2", "string"),  MappingDestination("complex-field-2-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-3", "string"),  MappingDestination("complex-field-3-target", "string", null), null, null)
            )),
            null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[StructType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).name === "nested-complex-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[StructType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")


    assertStandardFields(targetStruct)
  }

  //Complex Type with nested Collection Complex Type
  test("assert can build destination schema with a Complex Type with nested Collection Complex Type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex"), MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("nested-collection-complex", "complex", collection = true), MappingDestination("nested-collection-complex-target", fieldType = "complex",
            List(
              Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-3", "string"), MappingDestination("complex-field-3-target", "string", null), null, null)
            )),
            null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[StructType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).name === "nested-collection-complex-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[ArrayType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")

    assertStandardFields(targetStruct)
  }

  //Collection of Complex Type with nested Complex Type
  test("assert can build destination schema with a Collection of Complex Type with nested Complex Type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex", collection = true), MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("nested-complex", "complex"), MappingDestination("nested-complex-target", fieldType = "complex",
            List(
              Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-3", "string"), MappingDestination("complex-field-3-target", "string", null), null, null)
            )),
            null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).name === "nested-complex-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[StructType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")

    assertStandardFields(targetStruct)
  }

  //Collection of Complex Type with nested Collection Complex Type
  test("assert can build destination schema with a Collection of Complex Type with nested Collection Complex Type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string"),  MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "complex", collection = true), MappingDestination("field-2-target", fieldType = "complex",
        List(
          Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
          Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
          Mapping(MappingSource("nested-collection-complex", "complex", collection = true), MappingDestination("nested-collection-complex-target", fieldType = "complex",
            List(
              Mapping(MappingSource("complex-field-1", "string"), MappingDestination("complex-field-1-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-2", "string"), MappingDestination("complex-field-2-target", "string", null), null, null),
              Mapping(MappingSource("complex-field-3", "string"), MappingDestination("complex-field-3-target", "string", null), null, null)
            )),
            null, null)
        )),
        null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields.length == 3)

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).dataType.isInstanceOf[StringType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).name === "nested-collection-complex-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.isInstanceOf[ArrayType])

    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(0).name === "complex-field-1-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(1).name === "complex-field-2-target")
    assert(targetStruct.fields(1).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType].fields(2).name === "complex-field-3-target")

    assertStandardFields(targetStruct)
  }
}
