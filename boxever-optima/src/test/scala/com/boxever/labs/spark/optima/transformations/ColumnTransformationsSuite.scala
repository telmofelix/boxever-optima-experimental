package com.boxever.labs.spark.optima.transformations

import java.sql.{Date, Timestamp}

import com.boxever.labs.spark.optima.transformations.ColumnTransformations._
import org.scalatest.FunSuite

class ColumnTransformationsSuite extends FunSuite {

  test("assert can lowercase") {
    assert(toLowerCase("Test").get == "test")
  }

  test("assert can uppercase") {
    assert(toUpperCase("Test").get == "TEST")
  }

  test("assert can cast string (long) to timestamp") {
    val input = "1559299403553"
    val expectedResult = new Timestamp(1559299403553L)

    val result = castStringToTimestampInt64(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string to timestamp") {
    val input = "2019-05-31T10:43:23.553Z"
    val expectedResult = new Timestamp(1559299403553L)

    val result = castStringToTimestampInt64(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string (no time) to timestamp") {
    val input = "2019-05-31"
    val expectedResult = new Timestamp(1559260800000L)

    val result = castStringToTimestampInt64(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast string (long) to date") {
    val input = "1559299403553"
    val expectedResult = new Date(1559299403553L)

    val result = castStringToDate(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string to date") {
    val input = "2019-05-31T10:43:23.553Z"
    val expectedResult = new Date(1559299403553L)

    val result = castStringToDate(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string (no time) to date") {
    val input = "2019-05-31"
    val expectedResult = new Date(1559260800000L)

    val result = castStringToDate(input)

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string (no time) to date with pattern") {
    val input = "2019-05-31"
    val expectedResult = new Date(1559260800000L)

    val result = castStringToDateWithPattern(input, "yyyy-MM-dd")

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string (with hour and minute time) to date with pattern") {
    val input = "2019-05-31T10:43:23"
    val expectedResult = new Date(1559299380000L)

    val result = castStringToDateWithPattern(input, "yyyy-MM-dd'T'HH:mm")

    assert(result.get == expectedResult)
  }

  test("assert can cast iso string (date with hh:mm time) to timestamp") {
    val timeAsLongString = "2018-01-03T00:00"
    val expectedResult = new Timestamp(1514937600000L)

    val result = castStringToTimestampInt64(timeAsLongString)

    assert(result.get == expectedResult)
  }
}
