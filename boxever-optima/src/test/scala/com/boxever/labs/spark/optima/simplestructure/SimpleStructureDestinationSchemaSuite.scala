package com.boxever.labs.spark.optima.simplestructure

import com.boxever.labs.spark.optima.simplestructure.SimpleStructureDestinationSchema._
import com.boxever.labs.spark.optima.spec.{Mapping, MappingDestination, MappingSource}
import org.apache.spark.sql.types._
import org.scalatest.FunSuite

class SimpleStructureDestinationSchemaSuite extends FunSuite {
  def assertStandardFields(targetStruct: StructType): Any = {
    // containsValidationErrors
    assert(targetStruct.fields(targetStruct.fields.length - 2).name == "containsValidationErrors")
    assert(targetStruct.fields(targetStruct.fields.length - 2).dataType.isInstanceOf[BooleanType])

    // validationErrors
    assert(targetStruct.fields(targetStruct.fields.length - 1).name == "validationErrors")
    assert(targetStruct.fields(targetStruct.fields.length - 1).dataType.isInstanceOf[ArrayType])
    assert(targetStruct.fields(targetStruct.fields.length - 1).dataType.asInstanceOf[ArrayType].elementType.isInstanceOf[StringType])
  }

  test("assert can build destination schema with a simple type") {
    val mappings = List(
      Mapping(MappingSource("field-1", "string", collection = false), MappingDestination("field-1-target", "string", null), null, null),
      Mapping(MappingSource("field-2", "boolean", collection = false), MappingDestination("field-2-target", "boolean", null), null, null),
      Mapping(MappingSource("field-3", "string", collection = false), MappingDestination("field-3-target", "long", null), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 5)

    assert(targetStruct.fields(0).name == "field-1-target")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "field-2-target")
    assert(targetStruct.fields(1).dataType == BooleanType)
    assert(targetStruct.fields(2).name == "field-3-target")
    assert(targetStruct.fields(2).dataType == LongType)

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of simple type") {
    val mappings = List(
      Mapping(MappingSource("ref", "string", collection = false), MappingDestination("guest_ref", "string", null), null, null),
      Mapping(MappingSource("emails", "string", collection = true), MappingDestination("email", "string", null), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 4)

    assert(targetStruct.fields(0).name == "guest_ref")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "email")
    assert(targetStruct.fields(1).dataType == StringType)

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a complex type") {
    val mappings = List(
      Mapping(MappingSource("ref", "string", collection = false), MappingDestination("guest_ref", "string", null), null, null),
      Mapping(MappingSource("twitterAccount", "complex", collection = false), MappingDestination("twitterAccount", "complex", List(
        Mapping(MappingSource("handle", "string", collection = false), MappingDestination("handle", "string", null), null, null),
        Mapping(MappingSource("registeredAt", "long", collection = false), MappingDestination("registered_at", "timestampInt64", null), null, null)
      )), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 5)

    assert(targetStruct.fields(0).name == "guest_ref")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "handle")
    assert(targetStruct.fields(1).dataType == StringType)
    assert(targetStruct.fields(2).name == "registered_at")
    assert(targetStruct.fields(2).dataType == TimestampType)

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of complex type") {
    val mappings = List(
      Mapping(MappingSource("ref", "string", collection = false), MappingDestination("guest_ref", "string", null), null, null),
      Mapping(MappingSource("identifiers", "complex", collection = true), MappingDestination("identifiers", "complex", List(
        Mapping(MappingSource("id", "string", collection = false), MappingDestination("id", "string", null), null, null),
        Mapping(MappingSource("provider", "string", collection = false), MappingDestination("provider", "string", null), null, null)
      )), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 5)

    assert(targetStruct.fields(0).name == "guest_ref")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "id")
    assert(targetStruct.fields(1).dataType == StringType)
    assert(targetStruct.fields(2).name == "provider")
    assert(targetStruct.fields(2).dataType == StringType)

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of complex type with a nested complex type") {
    val mappings = List(
      Mapping(MappingSource("ref", "string", collection = false), MappingDestination("guest_ref", "string", null), null, null),
      Mapping(MappingSource("dataExtensions", "complex", collection = true), MappingDestination("data_extensions", "complex", List(
        Mapping(MappingSource("key", "string", collection = false), MappingDestination("key", "string", null), null, null),
        Mapping(MappingSource("name", "string", collection = false), MappingDestination("name", "string", null), null, null),
        Mapping(MappingSource("createdAt", "timestampInt64", collection = false), MappingDestination("created_at", "timestampInt64", null), null, null),
        Mapping(MappingSource("values", "complex", collection = false), MappingDestination("values", "complex", List(
          Mapping(MappingSource("utm_campaign", "string", collection = false), MappingDestination("utm_campaign", "string", null), null, null)
        )), null, null)
      )), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 7)

    assert(targetStruct.fields(0).name == "guest_ref")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "key")
    assert(targetStruct.fields(1).dataType == StringType)
    assert(targetStruct.fields(2).name == "name")
    assert(targetStruct.fields(2).dataType == StringType)
    assert(targetStruct.fields(3).name == "created_at")
    assert(targetStruct.fields(3).dataType == TimestampType)
    assert(targetStruct.fields(4).name == "utm_campaign")
    assert(targetStruct.fields(4).dataType == StringType)

    assertStandardFields(targetStruct)
  }

  test("assert can build destination schema with a collection of complex type with a nested collection of complex type") {
    val mappings = List(
      Mapping(MappingSource("ref", "string", collection = false), MappingDestination("order_ref", "string", null), null, null),
      Mapping(MappingSource("ordersItems", "complex", collection = true), MappingDestination("order_items", "complex", List(
        Mapping(MappingSource("ref", "string", collection = false), MappingDestination("order_item_ref", "string", null), null, null),
        Mapping(MappingSource("createdAt", "timestampInt64", collection = false), MappingDestination("created_at", "timestampInt64", null), null, null),
        Mapping(MappingSource("modifiedAt", "timestampInt64", collection = false), MappingDestination("modified_at", "timestampInt64", null), null, null),
        Mapping(MappingSource("orderItemFlightSegments", "complex", collection = true), MappingDestination("order_item_flight_segments", "complex", List(
          Mapping(MappingSource("id", "string", collection = false), MappingDestination("id", "string", null), null, null)
        )), null, null)
      )), null, null)
    )
    val targetStruct = getDestinationSchema(mappings)
    targetStruct.printTreeString()
    assert(targetStruct.fields.length == 7)

    assert(targetStruct.fields(0).name == "order_ref")
    assert(targetStruct.fields(0).dataType == StringType)
    assert(targetStruct.fields(1).name == "order_item_ref")
    assert(targetStruct.fields(1).dataType == StringType)
    assert(targetStruct.fields(2).name == "created_at")
    assert(targetStruct.fields(2).dataType == TimestampType)
    assert(targetStruct.fields(3).name == "modified_at")
    assert(targetStruct.fields(3).dataType == TimestampType)
    assert(targetStruct.fields(4).name == "id")
    assert(targetStruct.fields(4).dataType == StringType)

    assertStandardFields(targetStruct)
  }
}
