
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession

val conf = new SparkConf()
val spark = SparkSession.builder()
  .appName("worksheet")
  .master("local[*]")
  .config(conf)
  .getOrCreate()

val df = spark.read.json(spark.sparkContext.makeRDD(
    """{ "aComplexType": { "fieldOne": "  field one value  ", "fieldTwo": 2 }, "simpleType": "  simple  "}""" ::
    """{ "aComplexType": { "fieldOne": "  field one value  ", "fieldTwo": 2 }, "simpleType": "  another row  "}""" ::
  Nil))

println("____ df.printSchema ________________________")
df.printSchema()

println("____ df.show _______________________________")
df.show( false)

println("____ df.select _____________________________")
df.select("simpleType").show(false)


/*
List(
  ("my_client_key", "o1", "s1", now(13), List(Item("o1i1", now(13), 10.0, 1), Item("o1i2", now(13), 20.0, 2))),
  ("my_client_key", "o2", "s1", now(12), List(Item("o2i1", now(12), 10.0, 1), Item("o2i2", now(12), 20.0, 2))),
  ("my_client_key", "o3", "s1", now(11), List(Item("o3i1", now(11), 10.0, 1), Item("o3i2", now(11), 20.0, 2))),
  ("my_client_key", "o4", "s1", now(10), List(Item("o4i1", now(10), 10.0, 1), Item("o4i2", now(10), 20.0, 2)))
).toDF(
  "client_key",
  "order_ref",
  "session_ref",
  "updated",
  "items"
).coalesce(1)
*/