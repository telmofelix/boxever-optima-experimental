package com.boxever.labs.spark.optima.transformations

import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.util

import org.joda.time.format.ISODateTimeFormat

object ColumnTransformations {

  def setStringIfNull(value: Option[Any], params: List[String]): Option[Any] = {
    if (value.isDefined) value else Option.apply(params.head)
  }

  def setBooleanIfNull(value: Option[Any], params: List[String]): Option[Any] = {
    if (value.isDefined) value else Option.apply(params.head.toBoolean)
  }

  def setIntIfNull(value: Option[Any], params: List[String]): Option[Any] = {
    if (value.isDefined) value else Option.apply(params.head.toInt)
  }

  def setLongIfNull(value: Option[Any], params: List[String]): Option[Any] = {
    if (value.isDefined) value else Option.apply(params.head.toLong)
  }

  def setDoubleIfNull(value: Option[Any], params: List[String]): Option[Any] = {
    if (value.isDefined) value else Option.apply(params.head.toDouble)
  }

  def toLowerCase(value: String): Option[String] = {
    Option.apply(value.toLowerCase)
  }

  def toUpperCase(value: String): Option[String] = {
    Option.apply(value.toUpperCase)
  }

  def trimString(value: String): Option[String] = {
    Option.apply(value.trim)
  }

  def replaceOneOf(value: String, params: List[String]): Option[String] = {
    val replaceOneOf = params.slice(0, params.size - 1)
    val replaceWithValue = params(params.size - 1)
    if (value != null && replaceOneOf.contains(value)) Option.apply(replaceWithValue) else Option.apply(value)
  }

  def castLongToTimestampInt64(value: Long): Option[Timestamp] = {
    Option.apply(new Timestamp(value))
  }

  def castStringToTimestampInt64(value: String): Option[Timestamp] = {
    try {
      return Option.apply(new Timestamp(value.toLong))
    } catch {
      case _: NumberFormatException =>
        return Option.apply(new Timestamp(ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(value).getMillis))
    }
  }

  def castLongToDate(value: Long): Option[Date] = {
    Option.apply(new Date(value))
  }

  def castStringToDateWithPattern(value: String, pattern: String): Option[Date] = {
    Option.apply(new Date(SafeSimpleDateFormat.get(pattern).parse(value).getTime))
  }

  def castStringToDate(value: String): Option[Date] = {
    try {
      return Option.apply(new Date(value.toLong))
    } catch {
      case _: NumberFormatException =>
        return Option.apply(new Date(ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(value).getMillis))
    }
  }

  def castStringToInt(value: String): Option[Int] = {
    Option.apply(value.toInt)
  }

  def castLongToInt(value: Long): Option[Int] = {
    Option.apply(value.toInt)
  }

  object SafeSimpleDateFormat extends ThreadLocal[util.HashMap[String, SimpleDateFormat]] {
    override def initialValue = {
      new util.HashMap[String, SimpleDateFormat]()
    }

    def get(pattern: String): SimpleDateFormat = {
      val map: util.HashMap[String, SimpleDateFormat] = get()
      if (!map.containsKey(pattern)) {
        map.put(pattern, new SimpleDateFormat(pattern));
      }
      return map.get(pattern)
    }
  }

  def formatDate(value: Any, params: List[String]): Option[String] = {
    if (value != null && value.isInstanceOf[Date]) Option.apply(SafeSimpleDateFormat.get(params.head).format(value)) else Option.empty
  }
}
