package com.boxever.labs.spark.optima.segments

import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.spark.sql.SparkSession
/**
  * Submitting to EMR
  * ./gradlew clean build -x test
  *
  * Spinair
  * scp boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar emr:boxever-optima-0.0.1-SNAPSHOT-all.jar
  * ssh emr
  * spark-submit --master yarn --deploy-mode cluster --class com.boxever.labs.spark.optima.raw.SparkApp --conf spark.yarn.maxAppAttempts=1 boxever-optima-0.0.1-SNAPSHOT-all.jar --source_path s3://boxever-data-production-eu-west-1/snapshots/2019/02/14/v2/events/wjtc2eog1lvueo72kts3mn1ean0nentz/ --destination_path s3://boxever-explore-spinair-labs-eu-west-1/optima/tmp/events/
  *
  * Emirates
  * scp boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar emr-ek:boxever-optima-0.0.1-SNAPSHOT-all.jar
  * ssh emr-ek
  * spark-submit --master yarn --deploy-mode cluster --class com.boxever.labs.spark.optima.raw.SparkApp --conf spark.yarn.maxAppAttempts=1 boxever-optima-0.0.1-SNAPSHOT-all.jar --source_path s3://boxever-data-production-eu-west-1/snapshots/2019/02/14/v2/events/ekb7q5q7htudvxjat3zmeuv2qjus0z6w/ --destination_path s3://boxever-explore-emirates-production-eu-west-1/optima/tmp/events/
  *
  */
object SparkApp {
  def main(sysArgs: Array[String]) {
    val spark = SparkSession.builder().getOrCreate();
    spark.conf.set("spark.sql.parquet.fs.optimized.committer.optimization-enabled", "true")
    spark.conf.set("fs.s3.buckets.create.enabled", "false")
    spark.conf.set("spark.sql.parquet.mergeSchema", "true")    //to support schema evolution in parquet

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    Segments.run(spark, config)
  }
}
