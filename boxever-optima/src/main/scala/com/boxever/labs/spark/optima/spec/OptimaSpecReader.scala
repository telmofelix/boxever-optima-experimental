package com.boxever.labs.spark.optima.spec

import java.io.{FileInputStream, FileNotFoundException, IOException, InputStream}
import java.util.{HashMap, Objects}

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GetObjectRequest
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import javax.validation.ValidationException
import org.apache.commons.io.IOUtils
import org.apache.commons.text.StringSubstitutor
import org.apache.log4j.Logger

import scala.collection.JavaConverters._

object OptimaSpecReader {

  private[this] val logger = Logger.getLogger(getClass.getName)

  @throws[IOException]
  def read(src: String, variables: Map[String, String]): OptimaSpec = {
    logger.info("Loading Optima Spec : " + src);

    val spec = if (src.startsWith("s3://")) {
      val location = src.substring("s3://".length)
      val bucketName = location.substring(0, location.indexOf("/"))
      val key = location.substring(location.indexOf("/") + 1)
      val s3Client = AmazonS3ClientBuilder.standard.build
      val s3Object = s3Client.getObject(new GetObjectRequest(bucketName, key))
      load(s3Object.getObjectContent, objectMapper(src), variables)
    } else if (src.startsWith("classpath://")) {
      val location = src.substring("classpath://".length)
      val in = getClass.getResourceAsStream(location)
      if (Objects.isNull(in)) {
        throw new FileNotFoundException("Could not find classpath file: " + location)
      }
      load(in, objectMapper(src), variables)
    } else {
      load(new FileInputStream(src), objectMapper(src), variables)
    }

    if (spec.version == 2) {
      validateVersion2(spec)
    }

    spec
  }

  def load(in: InputStream, mapper: ObjectMapper, variables: Map[String, String]): OptimaSpec = {
    val finalVariables = new HashMap[String, String]

    finalVariables.putAll(variables.asJava)
    finalVariables.putAll(System.getenv)

    val sub = new StringSubstitutor(finalVariables)
    val config = sub.replace(IOUtils.toString(in))

    // Filter out Ansible Jinja2 template imports
    val newConfig = scala.io.Source.fromString(config).getLines().filter(l => !l.contains("{% include")).mkString("\n")

    logger.info("Optima Spec Loaded As : \n" + newConfig)

    mapper.readValue(newConfig, classOf[OptimaSpec])
  }

  def objectMapper(filename: String): ObjectMapper =
    if (filename.endsWith(".yaml") || filename.endsWith(".yml")) {
      val mapper = new ObjectMapper(new YAMLFactory)
      mapper.registerModule(DefaultScalaModule)
      mapper
    } else {
      new ObjectMapper()
    }

  def getDestinationFieldNames(mappings: List[Mapping]): List[String] = {
    val dd : List[List[String]] = mappings.map(mapping => {
      if (Objects.nonNull(mapping.getDestination.mappings)) {
        getDestinationFieldNames(mapping.getDestination.mappings) ++ Seq(mapping.destination.name)
      } else {
        List() ++ Seq(mapping.destination.name)
      }
    })
    dd.flatten
  }

  def validateVersion2(spec: OptimaSpec) = {
    // Validate Partitions are defined in mappings
    val partitions = spec.getDestination.partitions
    if (Objects.nonNull(partitions)) {
      val des = getDestinationFieldNames(spec.mappings)

      partitions.foreach(partition => {
        if (!des.contains(partition)) {
          throw new ValidationException("Partition field(s) " + partition + " must be defined in mappings")
        }
      })
    }
  }
}
