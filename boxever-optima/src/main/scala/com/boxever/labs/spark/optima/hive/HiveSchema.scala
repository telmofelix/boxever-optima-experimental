package com.boxever.labs.spark.optima.hive

import org.apache.spark.sql.types._

object HiveSchema {
  def main(sysArgs: Array[String]): Unit = {
    val schemaName = "optima_trusted"
    val entity = "guests"
    val partitions = List[String]("guest_type")
    val dataPath = "s3://boxever-test-bucket/optima_trusted/guests"
    val bx_batch_id = "bx_batch_id"

    val sourceSchema = new StructType()
      .add("age", IntegerType)
      .add("guest_type", StringType)
      .add("ref", StringType)

    val ddl = new HiveSchema(sourceSchema).getHiveSchema(schemaName, entity, true, partitions, dataPath)
    System.out.println(ddl)
  }
}

class HiveSchema(schema: StructType) {


  def printHiveSchema(schemaName: String, tableName: String, external: Boolean, partitions: List[String], location: String): Unit = {
    System.out.println(getHiveSchema(schemaName, tableName, external, partitions, location))
  }

  def getHiveSchema(schemaName: String, tableName: String, external: Boolean, partitions: List[String], location: String): String = {
    val stringBuilder = new StringBuilder
    stringBuilder.append("DROP TABLE IF EXISTS ")
    if (schemaName != null) stringBuilder.append(schemaName + ".")
    stringBuilder.append(tableName)
    stringBuilder.append(";")
    stringBuilder.append("\n")
    stringBuilder.append("\n")

    stringBuilder.append("CREATE ")
    if (external) stringBuilder.append("EXTERNAL ")
    stringBuilder.append("TABLE ")
    if (schemaName != null) stringBuilder.append(schemaName + ".")
    stringBuilder.append(tableName + " (")
    stringBuilder.append("\n")

    stringBuilder.append(getColumns(schema, partitions))

    stringBuilder.append("\n")
    stringBuilder.append(")")
    stringBuilder.append("\n")

    if (partitions.nonEmpty) {
      stringBuilder.append(getPartitionedBy(schema, partitions))
      stringBuilder.append("\n")
    }

    stringBuilder.append("STORED AS PARQUET")

    if (location != null) {
      stringBuilder.append("\n")
      stringBuilder.append("LOCATION '")
      stringBuilder.append(location)
      stringBuilder.append("'")
    }
    stringBuilder.append(";")

    stringBuilder.toString()
  }

  /**
    * Get the hive formatted columns
    *
    * @param structType The struct type to get the hive formatted columns
    * @param partitions the partition column names to partition by
    * @return the hive formatted columns
    */
  def getColumns(structType: StructType, partitions: List[String]): String = {
    val columns = structType.fields.filter(f => !partitions.contains(f.name)).map(f => {
      "\t`" + f.name + "` " + f.dataType.sql + ",\n"
    }).mkString
    columns.take(columns.length - 2)
  }

  /**
    * Get the hive formatted partitioned by
    *
    * @param structType The struct type to get the hive formatted partitions
    * @param partitions the partition column names to partition by
    * @return the hive formatted partitioned by or empty with partitions is empty
    */
  def getPartitionedBy(structType: StructType, partitions: List[String]): String = {
    val stringBuilder = new StringBuilder()
    if (partitions.nonEmpty) {
      stringBuilder.append("PARTITIONED BY (")
      stringBuilder.append("\n")

      val columns = partitions.map(p => structType.fields.filter(f => f.name == p)).flatten.map(f => {
        "\t`" + f.name + "` " + f.dataType.sql + ",\n"
      }).mkString
      stringBuilder.append(columns.take(columns.size - 2))

      stringBuilder.append("\n")

      stringBuilder.append(")")
    }
    stringBuilder.toString()
  }

}
