package com.boxever.labs.spark.optima.complexstructure

import com.boxever.labs.spark.optima.spec.Mapping
import org.apache.spark.sql.types._

object ComplexStructureDestinationSchema {
  val TypeString         = "string"
  val TypeBoolean        = "boolean"
  val TypeInteger        = "integer"
  val TypeLong           = "long"
  val TypeFloat          = "float"
  val TypeDouble         = "double"
  val TypeDate           = "date"
  val TypeTimestampInt64 = "timestampInt64"
  val TypeComplex        = "complex"
  val TypeCollection     = "collection"

  def wrapIfCollection(isCollection: Boolean, targetDataType: DataType): DataType = if (isCollection) ArrayType(targetDataType) else targetDataType

  def getSourceSchema(sourceSchema: StructType, mappings: List[Mapping]): StructType = {
    var schema = sourceSchema
    mappings.foreach(mapping => {
      val targetDataType = mapping.fieldSourceType match {
        case TypeString         => StringType
        case TypeBoolean        => BooleanType
        case TypeInteger        => IntegerType
        case TypeLong           => LongType
        case TypeFloat          => FloatType
        case TypeDouble         => DoubleType
        case TypeDate           => DateType
        case TypeTimestampInt64 => TimestampType
        //Complex mappings have mappings on destination so we need to iterate through this to get the nested source fields
        case TypeComplex        => getSourceSchema(new StructType, mapping.destination.mappings)
        case _                  => StringType
      }
      if (!schema.fields.map(_.name).contains(mapping.source.name))
        schema = schema.add(StructField(mapping.getSource.getName, wrapIfCollection(mapping.getSource.getCollection, targetDataType)))
    })
    schema
  }

  def getSourceSchema(mappings: List[Mapping]): StructType = {
    getSourceSchema(new StructType, mappings)
  }

  def getDestinationSchema(sourceSchema: StructType, mappings: List[Mapping]): StructType = {
    var schema = sourceSchema
    mappings.foreach(mapping => {
      val targetDataType = mapping.fieldType match {
        case TypeString         => StringType
        case TypeBoolean        => BooleanType
        case TypeInteger        => IntegerType
        case TypeLong           => LongType
        case TypeFloat          => FloatType
        case TypeDouble         => DoubleType
        case TypeDate           => DateType
        case TypeTimestampInt64 => TimestampType
        case TypeComplex        => getDestinationSchema(new StructType, mapping.destination.mappings)
        case _                  => StringType
      }
      schema = schema.add(StructField(mapping.getDestination.getName, wrapIfCollection(mapping.getSource.getCollection, targetDataType)))
    })
    schema
  }

  def getDestinationSchema(mappings: List[Mapping]): StructType = {
    getDestinationSchema(new StructType, mappings)
      .add(StructField("containsValidationErrors", BooleanType))
      .add(StructField("validationErrors", ArrayType(StringType)))
  }
}
