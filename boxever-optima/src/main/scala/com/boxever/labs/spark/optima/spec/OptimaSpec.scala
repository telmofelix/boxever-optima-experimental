package com.boxever.labs.spark.optima.spec

import scala.collection.JavaConverters._
import java.util.{Objects, List => JList}

import com.fasterxml.jackson.annotation.{JsonCreator, JsonIgnoreProperties, JsonProperty}

import scala.beans.BeanProperty

@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class OptimaSpec(@JsonProperty(value = "version", required = true) @BeanProperty     version: Integer,
                      @JsonProperty(value = "mapper", required = true) @BeanProperty      mapper: String,
                      @JsonProperty(value = "source", required = true) @BeanProperty      source: Source,
                      @JsonProperty(value = "destination", required = true) @BeanProperty destination: Destination,
                      @JsonProperty("quarantine")                           @BeanProperty  quarantine: Destination,
                      @JsonProperty("quarantine_summary")                   @BeanProperty  quarantineSummary: Destination,
                      @JsonProperty("mappings")                  mappings: List[Mapping]) {
  def getMappings(): JList[Mapping] = mappings.asJava
}

/**
  * Type class used for <code>source</code> section.
  *
  * <code>
  * paths:                              list of file paths to load
  * format:                             json
  * where:                              limit the source data frame based on the where expression
  * quarantineRowIdentifierColumnName:  the column name to use as the row identifier
  * </code>
  */
@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class Source(@JsonProperty("paths") @BeanProperty  paths: List[String],
                  @JsonProperty("format") @BeanProperty format: String,
                  @JsonProperty("where") @BeanProperty where: String,
                  //OptimaSpecReaderSuite does not handle Boolean type well if no default is given.  Using String instead.
                  @JsonProperty("allowRunWithoutInputData") @BeanProperty allowRunWithoutInputData: String = "false",
                  @JsonProperty("quarantineRowIdentifierColumnName") @BeanProperty quarantineRowIdentifierColumnName: String) {
  def determineQuarantineRowIdentifierColumnName(): String = if (Objects.isNull(quarantineRowIdentifierColumnName)) "ref" else quarantineRowIdentifierColumnName
}

/**
  * Type class used for <code>destination</code> and <code>quarantine</code> sections.
  *
  * <code>
  * path:   path to save to
  * format: parquet           # optional. Default based on src extension
  * compression:              # optional
  * partitions:               # the partition list
  *     - field_1
  *     - field_2
  *     - ...
  * repartition:
  *   number: 100             # repartition final data frame to 100
  *   fields:                 # list of partition fields
  *     - field_1
  *     - field_2
  *     - ...
  * save-mode: overwrite      # default: overwrite
  * </code>
  */
@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class Destination(@JsonProperty(value = "path", required = true) @BeanProperty                path: String,
                       @JsonProperty("format") @BeanProperty              format: String,
                       @JsonProperty("compression") @BeanProperty         compression: String,
                       @JsonProperty("partitions")                         partitions: List[String],
                       @JsonProperty("repartition") @BeanProperty         repartition: Repartition,
                       @JsonProperty("saveMode") @BeanProperty            saveMode: String,
                       @JsonProperty("maxRecordsPerFile") @BeanProperty   maxRecordsPerFile: Integer,
                       @JsonProperty("coalesce") @BeanProperty            coalesce: Integer,
                       @JsonProperty("bx_batch_id") @BeanProperty         bx_batch_id: String,
                       @JsonProperty("ddls") @BeanProperty                dataDefinitionLanguages: List[DataDefinitionLanguage]) {
  def getPartitions(): JList[String] = partitions.asJava
}
@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class Repartition(@BeanProperty @JsonProperty("size")                               size: Integer,
                       @JsonProperty("fields") @JsonIgnoreProperties(ignoreUnknown=true) fields: List[String]) {
  def getFields(): JList[String] = fields.asJava
}
@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class DataDefinitionLanguage(@JsonProperty("schemaName")  @BeanProperty         schemaName: String,
                                  @JsonProperty("tableName")   @BeanProperty         tableName: String,
                                  @JsonProperty("path")        @BeanProperty         path: String,
                                  @JsonProperty("tableSchemaName")        @BeanProperty         tableSchemaName: String,
                                  @JsonProperty("removeDuplicates")        @BeanProperty         removeDuplicates: String
                       ) {
}

/**
  * source:
  *   name: source-field
  *   type: field-type
  * target:
  *   name: target-field
  *   type: field-type
  * transformations:              # list of transformation functions
  * validations:                  # list of validation functions
  * throwValidationError: true    # optional. Default=true
  * quarantineOnError: true       # Optional. Default=true
  */
@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class Mapping(@JsonProperty(value = "source", required = true)       @BeanProperty           source: MappingSource,
                   @JsonProperty(value = "destination", required = true)  @BeanProperty           destination: MappingDestination,
                   @JsonProperty("transformations")                                               transformations: List[Function],
                   @JsonProperty("validations")                                                   validations: List[Function],
                   @JsonProperty("skipValidationOnNull")                  @BeanProperty           skipValidationOnNull: Boolean = false,
                   @JsonProperty("throwValidationError")                  @BeanProperty           throwValidationError: Boolean = true,
                   @JsonProperty("quarantineOnError")                     @BeanProperty           quarantineOnError: Boolean = true) {
  def getTransformations(): JList[Function] = transformations.asJava
  def getValidations(): JList[Function] = validations.asJava
  def fieldType: String = source.fieldType match {
      case "complex" => "complex"
      case  _        => destination.fieldType
  }
  def fieldSourceType: String = source.fieldType match {
    case "complex" => "complex"
    case  _        => source.fieldType
  }
  def name: String = if (destination.name != null) destination.name else source.name
}

@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class MappingSource(@JsonProperty(value = "name", required = true) @BeanProperty       name: String,
                         @JsonProperty(value = "type", required = true)                     fieldType: String,
                         @JsonProperty("collection")                    @BeanProperty       collection: Boolean    = false) {
  def getType(): String = fieldType
}

@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class MappingDestination(@JsonProperty(value = "name", required = true) @BeanProperty name: String,
                              @JsonProperty("type")                                        fieldType: String,
                              @JsonProperty("mappings")                                    mappings: List[Mapping],
                              @JsonProperty("collection")                    @BeanProperty collection: Boolean    = false){
  def getType(): String = fieldType
  def getMappings(): JList[Mapping] = mappings.asJava
}

@JsonIgnoreProperties(ignoreUnknown=false)
@JsonCreator
case class Function(@JsonProperty(value = "name", required = true) @BeanProperty           name: String,
                    @JsonProperty("params") @JsonIgnoreProperties(ignoreUnknown=false)     params: Option[List[String]]) {
  def getParams(): JList[String] = params.getOrElse(List[String]()).asJava
}
