package com.boxever.labs.spark.optima.compact

import com.amazonaws.services.glue.GlueContext
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions._

//TODO - compact Trusted AND Explore by last modifiedAt
object Compact {

  def run(spark: SparkSession, goldenPath: String, deltaPath: String, destinationPath: String, compactField: String, partitions: List[String]): Unit = {

    //Trusted
    //s3://{{ optima_trusted_bucket_name_and_path }}/sessions/
    //s3://{{ optima_trusted_bucket_name_and_path }}/sessions_delta/
    //Explore
    //s3://{{ optima_bucket_name_and_path }}/sessions/
    //s3://{{ optima_bucket_name_and_path }}/sessions_delta/
    //TODO - remove delta folder... no longer needed as we have backups?
    //TODO - write delta into main data folder in append mode
    val source = spark.read.parquet(goldenPath, deltaPath)

    //TODO - remove this hardcoding of table name
    source.createOrReplaceTempView("events")
    val compacted = spark.sql("SELECT * FROM (SELECT *, row_number() OVER(PARTITION BY ref ORDER BY modifiedAt DESC) row_n FROM events) WHERE row_n=1").drop("row_n")

    //TODO - destination path??? can't write to same place we read from (how to work in conjunction with backup?)
    compacted.repartition(partitions.map(c => col(c)): _*)
      .write
      //.option("compression", "gzip")
      .option("compression", "snappy")
      .mode(SaveMode.Overwrite)
      .partitionBy(partitions: _*)
      .parquet(destinationPath)
  }
}

object CompactGlueApp{

  def main(sysArgs: Array[String]) {
    print(sysArgs)

    val sparkContext: SparkContext = SparkContext.getOrCreate()
    val glueContext = new GlueContext(sparkContext);
    val spark = glueContext.getSparkSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val sourcePath = args("source_path")
    val deltaPath = args("delta_path")
    val destinationPath = args("destination_path")
    val compactField = args("compact_field")
    val configFile = args("config_file")
    val date = args("date")

    val variables = Map(("date", date))

    val config = OptimaSpecReader.read(configFile, variables)
    val partitions = config.destination.partitions
    //val tbl = config.destination.path

    Compact.run(spark, sourcePath, deltaPath, destinationPath, compactField, partitions)
  }
}

object CompactSparkApp{

  def main(sysArgs: Array[String]) {

    val spark = SparkSession
      .builder()
      .config("spark.sql.sources.bucketing.enabled", true)
      .getOrCreate();

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val sourcePath = args("source_path")
    val deltaPath = args("delta_path")
    val destinationPath = args("destination_path")
    val compactField = args("compact_field")
    val configFile = args("config_file")
    val date = args("date")

    val variables = Map(("date", date))

    val config = OptimaSpecReader.read(configFile, variables)
    val partitions = config.destination.partitions

    Compact.run(spark, sourcePath, deltaPath, destinationPath, compactField, partitions)
  }
}