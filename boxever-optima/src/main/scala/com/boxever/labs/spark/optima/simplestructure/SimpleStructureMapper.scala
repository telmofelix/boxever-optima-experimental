package com.boxever.labs.spark.optima.simplestructure

import java.net.URI
import java.util.Objects

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureRowTransformations._
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.simplestructure.SimpleStructureDestinationSchema._
import com.boxever.labs.spark.optima.spec._
import com.boxever.labs.spark.optima.validations.ColumnValidations._
import com.boxever.labs.spark.optima.writers.OptimaWriter._
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j._
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField}
import org.apache.spark.sql.{Column, DataFrame, Row, SparkSession}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Intelli J App Configuration
  * VM Options: -Dspark.master=local[*]
  * Program Arguments: --config_file classpath:///optima/specs/v2/guests.yml --date 2019/01/22
  * Classpath: Include dependencies with "Provided" scope (This is required when build.gradle contains the following)
  * compileOnly group: 'org.apache.spark', name: 'spark-core_2.11', version: '2.4.0'
  *
  * Submitting to Remote Server
  * ./gradle clean build
  * bin/spark-submit --master spark://192.168.0.31:7077 --deploy-mode cluster --class com.boxever.labs.spark.optima.v2.OptimaETL build/libs/boxever-labs-spark-job-1.0-SNAPSHOT-all.jar --config_file classpath:///optima/specs/v2/guests.yml --date 2019/01/22
  *
  * Threads: -Dspark.master=local vs -Dspark.master=local[*]
  * https://spark.apache.org/docs/latest/submitting-applications.html#master-urls
  *
  * https://forums.databricks.com/questions/2808/select-dataframe-mappings-from-a-sequence-of-string.html
  */
object SimpleStructureMapper {

  private[this] val logger = Logger.getLogger(getClass.getName)

  def main(sysArgs: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .appName("OptimaETL")
      .getOrCreate()
    val spark = createSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    run(spark, config)
  }

  def getFlattenedSourceMappingsNames(mappings: List[Mapping]): ArrayBuffer[Mapping] = {
    var list = new ArrayBuffer[Mapping]()
    mappings.foreach(c => {
      c.getSource.getType match {
        case "complex" => list = list ++ getFlattenedSourceMappingsNames(c.getDestination.mappings)
        case _ => list.append(c)
      }
    })
    list
  }

  def transformRow(rawRow: Row, mappings: List[Mapping]): Row = {
    val finalColumns = getFlattenedSourceMappingsNames(mappings)

    val transformedData = finalColumns.map(m => {
      m.getSource.getType match {
        case "string" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[String](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case "boolean" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Boolean](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case "integer" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Int](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case "long" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Long](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case "float" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Float](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case "double" => {
          val transformed = transformValue(if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Double](m.getDestination.getName)), m.transformations)
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
        case _ => {
          val transformed = if (rawRow.isNullAt(rawRow.fieldIndex(m.getDestination.getName))) Option.empty else Option.apply(rawRow.getAs[Object](m.getDestination.getName))
          val validationResult = if (transformed.isEmpty && m.skipValidationOnNull) {
            List[Exception]()
          } else {
            validateColumn(m.getDestination.getName, transformed, m.validations)
          }
          Tuple2.apply(transformed, validationResult)
        }
      }
    })

    val data = transformedData.map(td => if (td._1.isDefined) td._1.get else null)
    val errors = transformedData.map(_._2).flatten.map(e => e.getMessage).toArray
    // TODO Need to apply schema to row
    Row.fromSeq(data :+ !errors.isEmpty :+ errors)
  }

  class SelectedSource(val currentDF: DataFrame, val currentColumns: Array[Column], val alias: Option[String]) {
  }

  def createSelectedSourceDF(currentSelectedSource: SelectedSource, mappings: Array[Mapping]): SelectedSource = {
    val toBeAddedColumnNames = mappings.filter(!_.getSource.getCollection).map(mapping => {
      col(mapping.getDestination.getName)
    })

    val currentSourceColumnNames = mappings.map(mapping => {
      val sourceColumnName = currentSelectedSource.alias.isDefined match {
        case true => currentSelectedSource.alias.get + "." + mapping.getSource.getName
        case _ => mapping.getSource.getName
      }
      mapping.getSource.getCollection match {
        case true =>
          if (mapping.getSource.getType().equalsIgnoreCase("string"))
            concat_ws(", ", col(sourceColumnName)).alias(mapping.getDestination.getName)
          else
            explode(col(sourceColumnName)).alias(mapping.getDestination.getName)
        case _ =>
          col(sourceColumnName).alias(mapping.getDestination.getName)
      }
    })

    val tempSourceColumnNames = currentSelectedSource.currentColumns ++ currentSourceColumnNames
    val resultDF = currentSelectedSource.currentDF.select(tempSourceColumnNames: _*)

    val resultCurrentColumns = currentSelectedSource.currentColumns ++ toBeAddedColumnNames
    var resultSelectedSource = new SelectedSource(resultDF, resultCurrentColumns, currentSelectedSource.alias)

    //TODO - for double nested type like dataextensions values
    //in first pass, we get down to one level of nesting
    //in second pass, we are flat
    //however, we are left with both copies of the data
    mappings.filter(mapping => mapping.getSource.getType == "complex").foreach(mapping => {
      val tempSelectedSource = new SelectedSource(resultSelectedSource.currentDF, resultSelectedSource.currentColumns, Option.apply(mapping.getDestination.getName))
      resultSelectedSource = createSelectedSourceDF(tempSelectedSource, mapping.getDestination.mappings.toArray)
    })

    resultSelectedSource
  }

  def run(spark: SparkSession, config: OptimaSpec): Unit = {
    // TODO Send in via config
    val debug = false

    val valid = spark.sparkContext.longAccumulator("validcount")
    val invalid = spark.sparkContext.longAccumulator("invalidcount")

    val sourceSchema = getSourceSchema(config.mappings)

    //We have to check multiple paths.  We only use one source path currently, so doesn't matter if check for all or any
    val pathExists = config.getSource.getPaths.map(path => {
      val conf = spark.sparkContext.hadoopConfiguration
      val fs = FileSystem.get(URI.create(path), conf)
      fs.exists(new Path(path))
    }).contains(true)

    //Placeholder in spec which is populated via Ansible.  Default is disabled.
    val allowRunWithoutInputData = Objects.nonNull(config.source.getAllowRunWithoutInputData) && config.source.getAllowRunWithoutInputData.toLowerCase.equals("true")

    val rootDF = if(!pathExists && allowRunWithoutInputData) {
      logger.info("Creating Empty Data Frame with Schema : \n" + sourceSchema.treeString)
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], sourceSchema)
    }
    else
      config.getSource.getFormat match {
        case "parquet" => spark.read.parquet(config.getSource.getPaths: _*)
        case "json" => spark.read.json(config.getSource.getPaths: _*)
      }

    rootDF.printSchema()

    val selectedDF = if (Objects.nonNull(config.getSource.getWhere)) {
      logger.info("WHERE condition loaded as : " + config.getSource.getWhere)
      rootDF.where(config.getSource.getWhere).withColumn("_quarantine_id", col(config.source.determineQuarantineRowIdentifierColumnName))
    } else {
      rootDF.withColumn("_quarantine_id", col(config.source.determineQuarantineRowIdentifierColumnName))
    }


    selectedDF.printSchema()

    if (debug) {
      println("__ selectedDF.show _________________________")
      selectedDF.show(10)
    }

    // Only select the required sample-data based on the config source mappings
    val selectedSourceDF = createSelectedSourceDF(new SelectedSource(selectedDF, Array(col("_quarantine_id")), Option.empty[String]), config.mappings.toArray).currentDF

    selectedSourceDF.printSchema()

    if (debug) {
      println("__ selectedSourceDF.show _________________________")
      selectedSourceDF.show(10)
    }

    val destinationSchema = getDestinationSchema(config.mappings).add(StructField("_quarantine_id", StringType, true))
    destinationSchema.printTreeString()
    val destinationEncoder = RowEncoder.apply(destinationSchema)

    val transformedDF = selectedSourceDF.map(rawRow => {
      try {
        val row = transformRow(rawRow, config.mappings)
        val containsValidationErrors = row.getAs[Boolean](destinationSchema.fieldIndex("containsValidationErrors"))
        if (containsValidationErrors) {
          invalid.add(1)
          Row.fromSeq(row.toSeq ++ Seq(rawRow.getAs[String]("_quarantine_id")))
        } else {
          valid.add(1)
          Row.fromSeq(row.toSeq ++ Seq(null))
        }
      } catch {
        case t: Throwable => {
          invalid.add(1)
          val cols = Seq.fill(destinationSchema.size - 3)(null) ++ Seq(true, t.getMessage, rawRow.getAs[String]("_quarantine_id"))
          Row.fromSeq(cols)
        }
      }

    })(destinationEncoder)

    if (debug) {
      println("__ transformedDF.show _________________________")
      transformedDF.show(10)
    }

    // Get Valid Records
    val validTransformedDF = transformedDF
      .filter("containsValidationErrors = false")
      .drop("containsValidationErrors")
      .drop("validationErrors")
      .drop("_quarantine_id")

    if (debug) {
      println("__ validTransformedDF.show _________________________")
      validTransformedDF.show(10)
    }

    // Get In Valid Records
    val inValidTransformedDF = transformedDF
      .filter("containsValidationErrors = true")
      .select("validationErrors", "_quarantine_id")
      .join(selectedDF, "_quarantine_id")
    inValidTransformedDF.printSchema()

    if (debug) {
      println("__ inValidTransformedDF.show _________________________")
      inValidTransformedDF.show(10)
      inValidTransformedDF.select("validationErrors").limit(10).foreach(r => r.getAs[mutable.WrappedArray[String]]("validationErrors").foreach(e => println(e)))
    }


    // Write to destination
    if (Objects.nonNull(config.getDestination)) {
      saveToDestination(validTransformedDF, config.getDestination)
    }

    // Write to quarantine destination
    if (Objects.nonNull(config.getQuarantine) && invalid.count > 0) {
      saveToDestination(inValidTransformedDF, config.getQuarantine)
    }
  }
}
