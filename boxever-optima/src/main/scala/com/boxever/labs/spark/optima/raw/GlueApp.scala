package com.boxever.labs.spark.optima.raw

import com.amazonaws.services.glue.GlueContext
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import org.apache.spark.SparkContext

object GlueApp {
  def main(sysArgs: Array[String]) {
    print(sysArgs)
    val sparkContext: SparkContext = SparkContext.getOrCreate()
    val glueContext = new GlueContext(sparkContext);
    val spark = glueContext.getSparkSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val sourcePath = args("source_path")
    val destinationPath = args("destination_path")

    RawJsonToParquet.run(spark, sourcePath, destinationPath)
  }
}
