package com.boxever.labs.spark.optima.raw

import org.apache.spark.sql.{SaveMode, SparkSession}

/**
  *
  */
object RawJsonToParquet {

  def run(spark: SparkSession, sourcePath: String, destinationPath: String): Unit = {
    val sourceDF = spark.read.json(sourcePath)
    println("Rows to be processed: " + sourceDF.count)

    sourceDF
      .write
//      .option("compression", "gzip")
      .option("compression", "snappy")
      .mode(SaveMode.Overwrite)
      .parquet(destinationPath)
  }
}
