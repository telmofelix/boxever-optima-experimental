package com.boxever.labs.spark.optima.raw

import java.util.Date

import com.boxever.labs.spark.optima.transformations.ColumnTransformations.SafeSimpleDateFormat
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

/**
  * aws s3 ls --profile production --summarize --human-readable --recursive s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/ekb7q5q7htudvxjat3zmeuv2qjus0z6w/2018/Q4/
  * Compression: Snappy
  * Format: Json
  * Total Objects: 24878
  * Total Size: 206.7 GiB - avg 2GB/day
  *
  * Events: 622034500, Partitions: 2516
  * Premium (m4.2xlarge) 1 Master, 1 Task, 6 Slaves = 8 32GB, 8 VCPUs = 16 DPU
  *
  * DPU: 100
  * Execution time: 12 Min
  * Cost: 100 * (.44/60) * 12 = $8.8
  * aws s3 ls --profile labs --summarize --human-readable --recursive s3://boxever-optima-emirates-labs-eu-west-1/data/raw/events/
  * Compression: Snappy
  * Format: Parquet
  * Total Objects: 24878
  * Total Size: 104.7 GiB
  *
  *
  *
  * aws s3 ls --profile production --summarize --human-readable --recursive s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/ekb7q5q7htudvxjat3zmeuv2qjus0z6w/2018/
  * Compression: Snappy
  * Format: Json
  * Total Objects: 176518
  * Total Size: 774.8 GiB
  *
  * DPU: 100
  * Execution time: 12 Min
  * Cost: 100 * (.44/60) * 45 = $33
  * aws s3 ls --profile labs --summarize --human-readable --recursive s3://boxever-optima-emirates-labs-eu-west-1/data/raw/events/
  * Compression: Snappy
  * Format: Parquet
  * Total Objects: 3720
  * Total Size: 553.3 GiB
  *
  * References
  *
  * https://jobs.zalando.com/tech/blog/solving-many-small-files-avro/index.html?gh_src=4n3gxh1
  * number_of_partitions = input_size / (AVRO_COMPRESSION_RATIO * DEFAULT_HDFS_BLOCK_SIZE)
  * ? = 210944 MB / 64 MB = 3296
  *
  *
  * Spark Submit
  *
  * spark-submit --master yarn --deploy-mode cluster --executor-memory 1g \
  *   --class com.boxever.labs.spark.optima.v2.RawEventsJson2RawEventsParquetRemote boxever-optima-0.0.1-SNAPSHOT-all.jar \
  *   --source_path s3://boxever-data-production-eu-west-1/snapshots/immutable/v2/events/ekb7q5q7htudvxjat3zmeuv2qjus0z6w/2018/Q4/10-29-18-11-06T01-58-part-00127.snappy \
  *   --destination_path s3://boxever-optima-emirates-labs-eu-west-1/data/raw/events/
  */
object RawEventsJson2RawEvents {
  def milliToFormattedDate(pattern: String): UserDefinedFunction = udf((millis: Long) => SafeSimpleDateFormat.get(pattern).format(new Date(millis)))
  // def milliToFormattedDate(pattern: String): UserDefinedFunction = udf((millis: Long) => new SimpleDateFormat(pattern).format(new Date(millis)))

  def run(spark: SparkSession, sourcePath: String, destinationPath: String): Unit = {
    val sourceDF = spark.read.json(sourcePath)
    run(spark, sourceDF, destinationPath)
  }

  def run(spark: SparkSession, sourceDF: DataFrame, destinationPath: String): Unit = {
    import spark.implicits._
    println("Events to be processed: " + sourceDF.count)
    println("Current number of partitions: " + sourceDF.rdd.getNumPartitions)

    val destinationDF = sourceDF.
      withColumn("created_at_year_month_day", milliToFormattedDate("yyyy-MM-dd")($"createdAt")).
      withColumn("created_at_year", milliToFormattedDate("yyyy")($"createdAt")).
      withColumn("created_at_month", milliToFormattedDate("MM")($"createdAt")).
      withColumn("created_at_day", milliToFormattedDate("dd")($"createdAt")).
      withColumn("created_at_week", milliToFormattedDate("ww")($"createdAt"))

    // Each event ~65bytes therefore 1000000 = 65MB
    destinationDF
      .repartition(destinationDF("created_at_year_month_day"))
      .write
      // option("maxRecordsPerFile", 1000000).
      // option("compression", "gzip").
      // mode(SaveMode.Append).
      .mode(SaveMode.Overwrite)
      .partitionBy("created_at_year_month_day")
      .bucketBy(8, "ref")
      .sortBy("ref")
      .option("path", destinationPath)  // instead of .parquet(destinationPath)
      .saveAsTable("processed_events")
  }
}
