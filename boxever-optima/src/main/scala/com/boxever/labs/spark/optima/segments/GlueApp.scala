package com.boxever.labs.spark.optima.segments

import com.amazonaws.services.glue.GlueContext
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.spark.{SparkConf, SparkContext}

object GlueApp {

  def main(sysArgs: Array[String]) {
    // https://aws.amazon.com/blogs/big-data/improve-apache-spark-write-performance-on-apache-parquet-formats-with-the-emrfs-s3-optimized-committer/
    val sparkConf = new SparkConf()
      .set("spark.sql.parquet.fs.optimized.committer.optimization-enabled", "true")
      .set("fs.s3.buckets.create.enabled", "false")
      .set("spark.sql.parquet.mergeSchema", "true")    //to support schema evolution in parquet

    val sparkContext: SparkContext = new SparkContext(sparkConf)

    val glueContext = new GlueContext(sparkContext)
    val spark = glueContext.getSparkSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    Segments.run(spark, config)

  }
}
