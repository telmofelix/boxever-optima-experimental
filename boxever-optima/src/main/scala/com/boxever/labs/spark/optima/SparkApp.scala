package com.boxever.labs.spark.optima

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureMapper
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.simplestructure.SimpleStructureMapper
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.spark.sql.SparkSession

import scala.collection.JavaConverters._

/**
  * Submitting to EMR
  *
  * ./gradlew clean build -x test
  *
  * Spinair
  *
  * scp boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar emr:boxever-optima-0.0.1-SNAPSHOT-all.jar
  *
  * ssh emr
  *
  * spark-submit --master yarn \
  *              --deploy-mode cluster \
  *              --conf spark.yarn.maxAppAttempts=1 \
  *              \
  *              --class com.boxever.labs.spark.optima.SparkApp \
  *              boxever-optima-0.0.1-SNAPSHOT-all.jar \
  *              --config_file classpath:///optima/specs/simple/guests.yml \
  *              --date 2019/03/02
  *
  * Complex (Nested Data)
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/spinair/events.yml --date 2019/06/13
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/spinair/guests.yml --date 2019/03/02
  *
  * Simple (Flattened Data)
  * spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/spinair/events_tracking_interaction.yml --date 2019/06/13 --modified_at_date_since 2019-05-04
  *
  * Emirates
  *
  * scp boxever-optima/build/libs/boxever-optima-0.0.1-SNAPSHOT-all.jar emr-ek:boxever-optima-0.0.1-SNAPSHOT-all.jar
  *
  * ssh emr-ek
  *
  * Complex (Nested Data)
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/emirates/events.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/emirates/guests.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/emirates/orders.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/trusted/emirates/sessions.yml --date 2019/03/02
  *
  * Simple (Flattened Data)
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/events.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/events_search.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/events_tracking_execution.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/events_tracking_interaction.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/events_view.yml --date 2019/05/05 --modified_at_date_since 2019-05-04
  *
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/guests.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/guest_identifiers.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/guest_subscriptions.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/guest_emails.yml --date 2019/03/02
  *
  *
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/orders.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_consumers.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_consumer_order_items.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_contacts.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_items.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_item_flights.yml --date 2019/03/02
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/order_item_flight_segments.yml --date 2019/03/02
  *
  *
  *  spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file classpath:///optima/specs/emr/explore/emirates/sessions.yml --date 2019/03/02
  *
  * Alternatives
  * spark-submit --master yarn --deploy-mode cluster  --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.SparkApp boxever-optima-0.0.1-SNAPSHOT-all.jar --config_file s3://my-buckets/optima/specs/v3/guests.yml --date 2019/03/02
  */
object SparkApp {
  def main(sysArgs: Array[String]) {
    val spark = SparkSession.builder().getOrCreate();
    spark.conf.set("spark.sql.parquet.fs.optimized.committer.optimization-enabled", "true")
    spark.conf.set("fs.s3.buckets.create.enabled", "false")
    spark.conf.set("spark.sql.parquet.mergeSchema", "true")    //to support schema evolution in parquet

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    config.mapper match  {
      case "complex" => ComplexStructureMapper.run(spark, config)
      case "simple" => SimpleStructureMapper.run(spark, config)
      case _ => SimpleStructureMapper.run(spark, config)
    }
  }
}
