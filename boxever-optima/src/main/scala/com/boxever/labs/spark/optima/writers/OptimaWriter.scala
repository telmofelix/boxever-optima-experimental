package com.boxever.labs.spark.optima.writers

import java.io.ByteArrayInputStream
import java.util.Objects

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.{ObjectMetadata, PutObjectRequest}
import com.boxever.labs.spark.optima.hive.HiveSchema
import com.boxever.labs.spark.optima.spec.Destination
import org.apache.spark.sql.functions.{udf, _}
import org.apache.spark.sql.{Dataset, Row, SaveMode}

object OptimaWriter {
  val batch_partition = "bx_batch_id"     //batch partition used when using Delta ETL mode (append)

  def saveToDestination(df: Dataset[Row], destination: Destination): Unit = {

    val isAppendModeWithBatchId = Objects.nonNull(destination.saveMode) && destination.saveMode.equals("append") && Objects.nonNull(destination.bx_batch_id)

    println("SAVEMODE: " + destination.saveMode)
    println("bx_batch_id: " + destination.bx_batch_id)
    val newDF = if (Objects.nonNull(destination.getCoalesce)) {
      df.coalesce(destination.getCoalesce)
    } else {
      df
    }

    val partitions = if (isAppendModeWithBatchId) {
      batch_partition :: destination.partitions
    } else {
      destination.partitions
    }
    println("NEW PARTITIONS: " + partitions)

    // Add new column batch partition
    val writeDF = if (isAppendModeWithBatchId) {
      newDF.withColumn(batch_partition, lit(destination.bx_batch_id))
    } else {
      newDF
    }

    val writer = if (destination.partitions != null && destination.partitions.nonEmpty) {


      println("newDF: " + writeDF.printSchema())

      writeDF.repartition(partitions.map(c => col(c)): _*)
        .sortWithinPartitions(partitions.map(c => col(c)): _*)
        .write
        .partitionBy(partitions: _*)
    } else {
      newDF.write
    }

    if (Objects.nonNull(destination.getMaxRecordsPerFile)) {
      writer.option("maxRecordsPerFile", destination.getMaxRecordsPerFile.toString)
    }

    destination.saveMode match {
      case "overwrite" => writer.mode(SaveMode.Overwrite)
      case "append" => writer.mode(SaveMode.Append)
      case "ignore" => writer.mode(SaveMode.Ignore)
      case _ => writer.mode(SaveMode.ErrorIfExists)
    }

    destination.compression match {
      case "gzip" => writer.option("compression", "gzip")
      case "snappy" => writer.option("compression", "snappy")
      case _ => writer.option("compression", "gzip")
    }

    destination.format match {
      case "parquet" => writer.parquet(destination.path)
      case "json" => writer.json(destination.path)
      case "orc" => writer.orc(destination.path)
    }

    if (Objects.nonNull(destination.getDataDefinitionLanguages)) {
      val dataPath = destination.getPath

      destination.getDataDefinitionLanguages.foreach(dataDefinitionLanguage => {
        val schemaName = dataDefinitionLanguage.getSchemaName
        val tableName = dataDefinitionLanguage.getTableName

        val finalPartitions = if (Objects.nonNull(partitions)) {
          partitions
        } else {
          List[String]()
        }

        println("FINAL PARTITIONS: " + finalPartitions)

        val ddl = new HiveSchema(writeDF.schema).getHiveSchema(schemaName, tableName, true, finalPartitions, dataPath)

        writeDataDefinitionLanguage(dataDefinitionLanguage.getPath, ddl)
      })

    }
  }

  def writeDataDefinitionLanguage(destinationPath: String, ddl: String) = {
    if (destinationPath.startsWith("s3://")) {
      val location = destinationPath.substring("s3://".length)
      val bucketName = location.substring(0, location.indexOf("/"))
      val key = location.substring(location.indexOf("/") + 1)

      val metadata = new ObjectMetadata
      metadata.setContentLength(ddl.length)

      val inputStream = new ByteArrayInputStream(ddl.getBytes())

      val s3Client = AmazonS3ClientBuilder.standard.build
      val s3Object = s3Client.putObject(new PutObjectRequest(bucketName, key, inputStream, metadata))
    } else {
      // TODO Attempt to write to HDFS
    }
  }
}
