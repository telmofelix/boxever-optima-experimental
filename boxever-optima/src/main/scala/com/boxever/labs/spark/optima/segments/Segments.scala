package com.boxever.labs.spark.optima.segments
import com.boxever.labs.spark.optima.spec.OptimaSpec
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import com.boxever.labs.spark.optima.writers.OptimaWriter

object Segments {

  def run(spark: SparkSession, config: OptimaSpec): Unit ={

    val segmentsDF = config.getSource.getFormat match {
      case "parquet" => spark.read.parquet(config.getSource.getPaths: _*)
      case "json"    => spark.read.json(config.getSource.getPaths: _*)
    }

    //val columns = segmentsDF.select($"segments.*").columns
    val columns = segmentsDF.schema.filter(_.name.equals("segments")).head.dataType.asInstanceOf[StructType].map(_.name)

    val schema = new StructType()
      .add(StructField("guest_ref", StringType, true))
      .add(StructField("segment_ref", StringType, true))
      .add(StructField("segment_name", StringType, true))

    val rowsRdd = segmentsDF.rdd.flatMap{
      case Row(_,_,ref,segments: Row) =>
        segments.toSeq.zipWithIndex.filter{ case (name, index) => name != null }
        .map{case (name, index) => Row(ref,columns(index), name.toString)}}

    val flattenedSegmentsDF = spark.createDataFrame(rowsRdd, schema)
    OptimaWriter.saveToDestination(flattenedSegmentsDF, config.destination)

  }
}
