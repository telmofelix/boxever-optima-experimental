package com.boxever.labs.spark.optima.simplestructure

import com.boxever.labs.spark.optima.spec.Mapping
import org.apache.spark.sql.types._

object SimpleStructureDestinationSchema {
  val TypeString         = "string"
  val TypeBoolean        = "boolean"
  val TypeInteger        = "integer"
  val TypeLong           = "long"
  val TypeFloat          = "float"
  val TypeDouble         = "double"
  val TypeDate           = "date"
  val TypeTimestampInt64 = "timestampInt64"
  val TypeComplex        = "complex"

  def wrapIfCollection(isCollection: Boolean, targetDataType: DataType): DataType = if (isCollection) ArrayType(targetDataType) else targetDataType

  def getSourceSchema(sourceSchema: StructType, mappings: List[Mapping]): StructType = {
    var schema = sourceSchema
    mappings.foreach(mapping => {
      val targetDataType = mapping.fieldSourceType match {
        case TypeString         => StringType
        case TypeBoolean        => BooleanType
        case TypeInteger        => IntegerType
        case TypeLong           => LongType
        case TypeFloat          => FloatType
        case TypeDouble         => DoubleType
        case TypeDate           => DateType
        case TypeTimestampInt64 => TimestampType
        //Complex mappings have mappings on destination so we need to iterate through this to get the nested source fields
        case TypeComplex        => getSourceSchema(new StructType, mapping.destination.mappings)
        case _                  => StringType
      }
      if (!schema.fields.map(_.name).contains(mapping.source.name))
        schema = schema.add(StructField(mapping.getSource.getName, wrapIfCollection(mapping.getSource.getCollection, targetDataType)))
    })
    schema
  }

  def getSourceSchema(mappings: List[Mapping]): StructType = {
    getSourceSchema(new StructType, mappings)
  }

  def getDestinationSchema(sourceSchema: StructType, mappings: List[Mapping]): StructType = {
    var schema = sourceSchema
    mappings.foreach(c => {
      schema = c.getDestination.getType match {
        case TypeString => schema.add(StructField(c.getDestination.getName, StringType, true))
        case TypeBoolean => schema.add(StructField(c.getDestination.getName, BooleanType, true))
        case TypeInteger => schema.add(StructField(c.getDestination.getName, IntegerType, true))
        case TypeLong => schema.add(StructField(c.getDestination.getName, LongType, true))
        case TypeFloat => schema.add(StructField(c.getDestination.getName, FloatType, true))
        case TypeDouble => schema.add(StructField(c.getDestination.getName, DoubleType, true))
        case TypeDate => schema.add(StructField(c.getDestination.getName, DateType, true))
        case TypeTimestampInt64 => schema.add(StructField(c.getDestination.getName, TimestampType, true))
        case TypeComplex => getDestinationSchema(schema, c.getDestination.mappings)
        case _ => schema.add(StructField(c.getDestination.getName, StringType, true))
          //TODO - correct return type for dataextensions
          //c.getDestination.getType is null for destination of dataExtensions... default is returning StringType
          //if dest is null and source is complex, do recursive call as above for ComplexType?
      }
    })
    schema
  }

  def getDestinationSchema(mappings: List[Mapping]): StructType = {
    getDestinationSchema(new StructType, mappings)
      .add(StructField("containsValidationErrors", BooleanType, true))
      .add(StructField("validationErrors", ArrayType(StringType), true))
  }

}