package com.boxever.labs.spark.optima.hive

import java.util.Objects

import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import org.apache.spark.sql.SparkSession

/**
  * spark-submit --master yarn \
  *              --deploy-mode client \
  *              --executor-memory 1g \
  *              --conf spark.yarn.maxAppAttempts=1 \
  *              --class com.boxever.labs.spark.optima.hive.ShowHiveSchema \
  *              boxever-optima-0.0.1-SNAPSHOT-all.jar \
  *              --dataPath s3://boxever-explore-emirates-production-eu-west-1/oneview/data/guests \
  *              --schemaName oneview \
  *              --tableName guests \
  *              --partitions guest_type,last_seen_year_month
  *
  *
  * spark-submit --master yarn --deploy-mode client --executor-memory 1g --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.hive.ShowHiveSchema boxever-optima-0.0.1-SNAPSHOT-all.jar --dataPath s3://boxever-explore-emirates-production-eu-west-1/optima/trusted/guests/ --schemaName optima_trusted --tableName guests --partitions ""
  * spark-submit --master yarn --deploy-mode client --executor-memory 1g --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.hive.ShowHiveSchema boxever-optima-0.0.1-SNAPSHOT-all.jar --dataPath s3://boxever-explore-emirates-production-eu-west-1/optima/explore/guest/ --schemaName optima_rtp --tableName guests --partitions guest_type,last_seen_year_month
  * spark-submit --master yarn --deploy-mode client --executor-memory 1g --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.hive.ShowHiveSchema boxever-optima-0.0.1-SNAPSHOT-all.jar --dataPath s3://boxever-explore-emirates-production-eu-west-1/optima/explore/guest_identifiers/ --schemaName optima --tableName guest_identifiers --partitions guest_type,last_seen_year_month
  * spark-submit --master yarn --deploy-mode client --executor-memory 1g --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.hive.ShowHiveSchema boxever-optima-0.0.1-SNAPSHOT-all.jar --dataPath s3://boxever-explore-emirates-production-eu-west-1/optima/explore/guest_subscriptions/ --schemaName optima --tableName guest_subscriptions --partitions guest_type,last_seen_year_month
  * spark-submit --master yarn --deploy-mode client --executor-memory 1g --conf spark.yarn.maxAppAttempts=1 --class com.boxever.labs.spark.optima.hive.ShowHiveSchema boxever-optima-0.0.1-SNAPSHOT-all.jar --dataPath s3://boxever-explore-emirates-production-eu-west-1/optima/explore/guest_emails/ --schemaName optima --tableName guest_emails --partitions guest_type,last_seen_year_month
  *
  */

object ShowHiveSchema {

    def main(sysArgs: Array[String]) {
      val spark = SparkSession.builder().getOrCreate()

      val args = OptimaArgParser.getResolvedOptions(sysArgs)
      val dataPath = args("dataPath")
      val schemaName = args("schemaName")
      val tableName = args("tableName")
      val partitions = args("partitions").split(",").map(d => d.trim).filter(d => d.length > 0)

      val finalPartitions = if (Objects.nonNull(partitions)) {
        partitions.toList
      } else {
        List[String]()
      }

      val df = spark.read.parquet(dataPath)

      new HiveSchema(df.schema).printHiveSchema(schemaName, tableName, true, finalPartitions, dataPath)
    }

}
