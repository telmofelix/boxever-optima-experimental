package com.boxever.labs.spark.optima

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureMapper
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.simplestructure.SimpleStructureMapper
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.log4j._
import org.apache.spark.sql.SparkSession

/**
  * Intelli J App Configuration
  * VM Options: -Dspark.master=local[*]
  * Program Arguments:
  *     --config_file boxever-optima/src/main/resources/optima/specs/v3/guests.yml
  *     --date        2019/02/28
  * Classpath: Include dependencies with "Provided" scope (This is required when build.gradle contains the following)
  * compileOnly group: 'org.apache.spark', name: 'spark-core_2.11', version: '2.4.0'
  *
  */
object OptimaETL {

  private[this] val logger = Logger.getLogger(getClass.getName)

  def main(sysArgs: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      .appName("OptimaETL")
      .getOrCreate()
    val spark = createSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    config.mapper match  {
      case "complex" => ComplexStructureMapper.run(spark, config)
      case "simple" => SimpleStructureMapper.run(spark, config)
      case _ => SimpleStructureMapper.run(spark, config)
    }
  }

}
