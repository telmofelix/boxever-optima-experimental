package com.boxever.labs.spark.optima.parsers

object OptimaArgParser {
  def getResolvedOptions(sysArgs: Array[String]): Map[String, String] = {
    var result = scala.collection.mutable.Map[String, String]()

    val it = sysArgs.toList.iterator

    while (it.hasNext) {
      val arg = it.next()
      if (isDelimiter(arg) && it.hasNext) {
        val optionName = arg.substring(2)
        val optionValue = it.next()
        result += (optionName -> optionValue)
      }
    }

    result.toMap
  }

  private def isDelimiter(arg: String) = arg.startsWith("--")

  def main(args: Array[String]) {
    val newArgs = getResolvedOptions(args)
    System.out.println(newArgs)
  }
}
