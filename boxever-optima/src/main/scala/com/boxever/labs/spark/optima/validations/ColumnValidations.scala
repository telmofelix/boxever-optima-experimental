package com.boxever.labs.spark.optima.validations

import java.util.{Objects, UUID}

import com.boxever.labs.spark.optima.spec.Function
import javax.validation.ValidationException

import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex

object ColumnValidations {

  def validateColumn(destColumnName: String, value: Option[Any], validations: List[Function]): List[Exception] = {
    val errors = ListBuffer[Exception]()
    if (validations != null) {
      if (value.isEmpty && validations.nonEmpty) {
        errors += new ValidationException("Value is null for dest column: " + destColumnName)
      } else {
        validations.foreach(v => {
          try {
            v.name match {
              case "isUUID" => isUUID(destColumnName, value.get.toString)
              case "matchesPattern" => matchesPattern(destColumnName, value.get.toString, v.params.get)
              case "isOneOf" => isOneOf(destColumnName, value.get.toString, v.params.get)
              case "minMaxLength" => minMaxLength(destColumnName, value.get.toString, v.params.get)
              case "minLength" => minLength(destColumnName, value.get.toString, v.params.get)
              case "maxLength" => maxLength(destColumnName, value.get.toString, v.params.get)
              case "minimum" => minimum(destColumnName, value.get, v.params.get)
              case "maximum" => maximum(destColumnName, value.get, v.params.get)
              case _ => // Received an undefined function. Nothing to do. If we don't deal with defaults
              // it throws a scala.MatchError with null value.
            }
          } catch {
            case e: Exception => errors += e
          }
        })
      }
    }
    errors.toList
  }

  def isUUID(destColumnName: String, value: String): Unit = {
    try {
      UUID.fromString(value)
    } catch {
      case _: IllegalArgumentException => throw new ValidationException("Failed to validate UUID, for value: \"" + value + "\" for dest column: " + destColumnName)
    }
  }

  def matchesPattern(destColumnName: String, value: String, params: List[String]): Unit = {
    params.foreach(pattern => {
      if (new Regex(pattern).findAllMatchIn(value).length != 1) {
        throw new ValidationException("Failed to match pattern \"" + pattern + "\", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    })
  }

  def isOneOf(destColumnName: String, value: String, params: List[String]): Unit = {
    val result = params.find(param => param.equals(value))
    if (result.isEmpty) {
      throw new ValidationException("Failed to match one of: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
    }
  }

  def minMaxLength(destColumnName: String, value: String, params: List[String]): Unit = {
    if (!(value.length >= Integer.parseInt(params(0)) && value.length <= Integer.parseInt(params(1)))) {
      throw new ValidationException("Failed min max length check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
    }
  }

  def minLength(destColumnName: String, value: String, params: List[String]): Unit = {
    if (value.length < Integer.parseInt(params(0))) {
      throw new ValidationException("Failed min length check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
    }
  }

  def maxLength(destColumnName: String, value: String, params: List[String]): Unit = {
    if (value.length > Integer.parseInt(params(0))) {
      throw new ValidationException("Failed max length check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
    }
  }

  def minimum(destColumnName: String, value: Any, params: List[String]): Unit = {
    if (value.isInstanceOf[Int]) {
      if (value.asInstanceOf[Int] < params(0).toInt) {
        throw new ValidationException("Failed minimum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Long]){
      if (value.asInstanceOf[Long] < params(0).toLong) {
        throw new ValidationException("Failed minimum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Float]){
      if (value.asInstanceOf[Float] < params(0).toFloat) {
        throw new ValidationException("Failed minimum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Double]){
      if (value.asInstanceOf[Double] < params(0).toDouble) {
        throw new ValidationException("Failed minimum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
  }

  def maximum(destColumnName: String, value: Any, params: List[String]): Unit = {
    if (value.isInstanceOf[Int]) {
      if (value.asInstanceOf[Int] > params(0).toInt) {
        throw new ValidationException("Failed maximum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Long]){
      if (value.asInstanceOf[Long] > params(0).toLong) {
        throw new ValidationException("Failed maximum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Float]){
      if (value.asInstanceOf[Float] > params(0).toFloat) {
        throw new ValidationException("Failed maximum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
    else if (value.isInstanceOf[Double]){
      if (value.asInstanceOf[Double] > params(0).toDouble) {
        throw new ValidationException("Failed maximum check: " + params.toString + ", for value: \"" + value + "\", for dest column: " + destColumnName)
      }
    }
  }

}
