package com.boxever.labs.spark.optima.complexstructure

import java.net.URI
import java.util.Objects

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureDestinationSchema._
import com.boxever.labs.spark.optima.complexstructure.ComplexStructureRowTransformations._
import com.boxever.labs.spark.optima.parsers.OptimaArgParser
import com.boxever.labs.spark.optima.spec.{Mapping, OptimaSpec, OptimaSpecReader}
import com.boxever.labs.spark.optima.writers.OptimaWriter._
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j._
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, _}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.collection.mutable

/**
  * Intelli J App Configuration
  * VM Options: -Dspark.master=local[*]
  * Program Arguments: --config_file boxever-optima/src/main/resources/optima/specs/v3/guests.yml
  * Classpath: Include dependencies with "Provided" scope (This is required when build.gradle contains the following)
  * compileOnly group: 'org.apache.spark', name: 'spark-core_2.11', version: '2.4.0'
  *
  * Submitting to Remote Server
  * ./gradle clean build
  * bin/spark-submit --master spark://192.168.0.31:7077 --deploy-mode cluster --class com.boxever.labs.spark.optima.v3.OptimaETL build/libs/boxever-labs-spark-job-1.0-SNAPSHOT-all.jar --config_file classpath:///optima/specs/v3/guests.yml --date 2019/01/31
  *
  * Threads: -Dspark.master=local vs -Dspark.master=local[*]
  * https://spark.apache.org/docs/latest/submitting-applications.html#master-urls
  *
  * https://forums.databricks.com/questions/2808/select-dataframe-columns-from-a-sequence-of-string.html
  */
object ComplexStructureMapper {

  private[this] val logger = Logger.getLogger(getClass.getName)

  def main(sysArgs: Array[String]): Unit = {
    def createSession: SparkSession = SparkSession
      .builder
      //.config("spark.sql.parquet.fs.optimized.committer.optimization-enabled", true)
      .appName("OptimaETL")
      .getOrCreate()
    val spark = createSession

    val args = OptimaArgParser.getResolvedOptions(sysArgs)
    val configFile = args("config_file")

    val config = OptimaSpecReader.read(configFile, args)

    run(spark, config)
  }

  def run(spark: SparkSession, config: OptimaSpec): Unit = {
    // TODO Send in via config
    val debug = false

    val valid = spark.sparkContext.longAccumulator("validcount")
    val invalid = spark.sparkContext.longAccumulator("invalidcount")
    val sourceSchema = getSourceSchema(mappings = config.mappings)

    //We have to check multiple paths.  We only use one source path currently, so doesn't matter if check for all or any
    val pathExists = config.getSource.getPaths.map(path => {
      val conf = spark.sparkContext.hadoopConfiguration
      val fs = FileSystem.get(URI.create(path), conf)
      fs.exists(new Path(path))
    }).contains(true)

    //Placeholder in spec which is populated via Ansible.  Default is disabled.
    val allowRunWithoutInputData = Objects.nonNull(config.source.getAllowRunWithoutInputData) && config.source.getAllowRunWithoutInputData.toLowerCase.equals("true")

    val rootDF = if(!pathExists && allowRunWithoutInputData) {
      logger.info("Creating Empty Data Frame with Schema : \n" + sourceSchema.treeString)
      spark.createDataFrame(spark.sparkContext.emptyRDD[Row], sourceSchema)
    }
    else
      config.getSource.getFormat match {
        case "parquet" => spark.read.schema(sourceSchema).parquet(config.getSource.getPaths: _*)
        case "json"    => spark.read.schema(sourceSchema).json(config.getSource.getPaths: _*)
      }

    println("____ rootDF _____________________")
    rootDF.printSchema()
    println("____ sourceSchema _____________________")
    sourceSchema.printTreeString()

    val selectedDF = if (Objects.nonNull(config.getSource.getWhere)) {
      logger.info("WHERE condition loaded as : " + config.getSource.getWhere)
      rootDF.where(config.getSource.getWhere)
    } else {
      rootDF
    }

    val destinationSchema = getDestinationSchema(config.mappings).add("originalData", selectedDF.schema)
    println("____ destinationSchema _____________________")
    destinationSchema.printTreeString()
    val destinationEncoder = RowEncoder(destinationSchema)

    val selectedSourceDF = createSelectedSourceDF(selectedDF, config.mappings)
    println("__ selectedSourceDF _________________________")
    selectedSourceDF.printSchema()

    val transformedDF = selectedSourceDF.map(rawRow => {
      try {
        val row = transformRow(rawRow, config.mappings)
        val containsValidationErrors = row.getAs[Boolean](row.size - 2)
        if (containsValidationErrors) {
          invalid.add(1)
          val seq = row.toSeq ++ Seq(rawRow)
          Row.fromSeq(seq)
        } else {
          valid.add(1)
          val seq = row.toSeq ++ Seq(null)
          Row.fromSeq(seq)
        }
      } catch {
        case t: Throwable => {
          val cols = List.fill(destinationSchema.size - 3)(null) ++ Seq(true, t.getMessage, rawRow)
          Row.fromSeq(cols)
        }
      }

    })(destinationEncoder)

    if (debug) {
      println("__ transformedDF.show _________________________")
      transformedDF.show(10)
    }

    // Get Valid Records
    val validTransformedDF = transformedDF
      .filter("containsValidationErrors = false")
      .drop("containsValidationErrors")
      .drop("validationErrors")
      .drop("originalData")

    if (debug) {
      println("__ validTransformedDF.show _________________________")
      validTransformedDF.show(10)
    }

    // Get Invalid Records
    val quarantinedRecordsDf = transformedDF.filter("containsValidationErrors = true").select("validationErrors", "originalData")
    if (debug) {
      println("__ quarantinedRecordsDf.show _________________________")
      quarantinedRecordsDf.show(10)
      quarantinedRecordsDf.select("validationErrors").limit(10).foreach(r => r.getAs[mutable.WrappedArray[String]]("validationErrors").foreach(e => println(e)))
    }

    // Write to destination
    if (config.getDestination != null) {
      logger.info("VALID TRANSFORMED COUNT (Trusted) : " + valid.count)
      saveToDestination(validTransformedDF, config.getDestination)
    }

    // Write to quarantine destination
    if (config.getQuarantine != null && invalid.count > 0) {
      logger.info("INVALID TRANSFORMED COUNT (Quarantine) : " + invalid.count)
      saveToDestination(quarantinedRecordsDf, config.getQuarantine)

      //Faster to read in output of write quarantine than to rescan root data-frame with filter
      val quarantined = config.getQuarantine.getFormat match {
        case "parquet" => spark.read.parquet(config.getQuarantine.path)
        case "json"    => spark.read.json(config.getQuarantine.path)
      }
      //Generate report of validation errors ordered by count descending
      //Flatten array of string to string with concat_ws
      val quarantinedSummary = quarantined.groupBy("validationErrors").count().orderBy(desc("count"))
        .select(concat_ws("", col("validationErrors")).as("validationErrors"), col("count"))

      //Generate quarantine summary output path
      logger.info("Saving quarantine summary table")
      saveToDestination(quarantinedSummary, config.getQuarantineSummary)
    }
  }

  def createSelectedSourceDF(input: DataFrame, mappings: List[Mapping]): DataFrame = {
    var df = input
    mappings.foreach(m => {
      val currName = m.source.name
      val newName  = m.destination.name

      val sourceColumnNames: Seq[String] = mappings.map(_.source.name)
      val mappingsUsingSourceColumn = sourceColumnNames.filter(_.equals(m.source.name))

      if (m.fieldType != TypeComplex) {
        if(mappingsUsingSourceColumn.size == 1){
          df = df.withColumnRenamed(currName, newName) //rename original column
        }
        else {
          if(currName==newName)
            df.withColumnRenamed(currName, newName)
          else
            df = df.withColumn(newName, df.col(currName))   //create backup of column
        }
      } else {
        if(df.columns.contains(currName)){
          val currentColumn = df.schema.filter(_.name == currName).head
          val newStruct = renameStructNames(currentColumn, ComplexStructureRowTransformations.mappingsForName(mappings, currName))
          //handle case of either StructType or ArrayType[StructType]
          df = df.withColumn(newName, df(currName).cast(if (currentColumn.dataType.isInstanceOf[ArrayType]) new ArrayType(newStruct, true) else newStruct))
          if (newName != currName) {
            // TODO Issue with this is if another mapping wants to use this source field then its gone
            df = df.drop(currName)
          }
        }
      }
    })
    df
  }

  def renameStructNames(structField: StructField, mappings: List[Mapping]): StructType = {
    val structField2 : StructType = if (structField.dataType.isInstanceOf[ArrayType]){
      structField.dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType]
    }
    else{
      structField.dataType.asInstanceOf[StructType]
    }
    val temp = StructType(structField2
      .fields
      .map(f => {
        if (ComplexStructureRowTransformations.containsMappingName(mappings, f.name)) {
          StructField(ComplexStructureRowTransformations.findMappingName(mappings, f.name), f.dataType )
        } else {
          f
        }
      })
    )
    temp
  }
}

