package com.boxever.labs.spark.optima.complexstructure

import java.util.Objects.nonNull

import com.boxever.labs.spark.optima.spec.{Function, Mapping}
import com.boxever.labs.spark.optima.transformations.ColumnTransformations._
import com.boxever.labs.spark.optima.validations.ColumnValidations.validateColumn
import org.apache.log4j._
import org.apache.spark.sql.Row
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.types._

import scala.collection.mutable

object ComplexStructureRowTransformations {

  private[this] val logger = Logger.getLogger(getClass.getName)

  def transformRow(row: Row, mappings: List[Mapping]): Row = {
    // Do transformations in the order destination columns appear in YAML file
    // because of potential duplicate mappings from one source column
    val destinationCols: Seq[String] = mappings.map(_.destination.name)
    logger.debug("Destination Cols : " + destinationCols.toString())
    val transformedRow = destinationCols.map(field => {
      logger.debug("Field : " + field)
      //row.schema.fields.filter also fails if data is not in source dataframe - this try/catch needed here
      try {
        transform(row.schema.fields.filter(_.name.equalsIgnoreCase(field)).last, row, mappings)
      }
      catch {
        //This handles case where field present in YAML mapping is not present in JSON
        case t: Throwable => {
          val transformed = (transformValue(Option.empty, transformationsFor(field, mappings)), List())
          if (transformed._1.isDefined) Tuple2.apply(transformed._1.get, transformed._2) else Tuple2.apply(null, transformed._2)
        }
      }
    })

    val data = transformedRow.map(_._1)
    val errors = transformedRow.flatMap(x => x._2).toList.map(e => e.getMessage).toArray
    logger.debug("Transformed Data : " + data.toString())
    // TODO Need to apply schema to row
    Row.fromSeq(data :+ errors.nonEmpty :+ errors)
  }

  def transform(field: StructField, row: Row, mappings: List[Mapping]): (Any, List[Exception]) = {
    //try/catch handle cases where column is in mapping file but not in data - return null column
    val name = field.name
    val (value, errors) = field.dataType match {
      case value: StructType =>
        // Do transformations in the order src columns appear in YAML file
        // so that columns are ordered correctly to match schema in values element of data extensions
        // Column names in this struct match source mapping names so we iterate through this
        val srcCols: Seq[String] = mappingsFor(name, mappings).map(_.source.name)
        logger.debug("Source Cols [" + name + "] : " + srcCols.toString())
        val transformed = srcCols.map(currentCol => {
          try{
            transform(field.dataType.asInstanceOf[StructType].fields.filter(_.name == currentCol).last, row.getStruct(row.fieldIndex(name)), mappingsFor(name, mappings))
          }
          catch {
            //This handles case where field present in YAML mapping is not present in JSON
            case t: Throwable => {
              val transformed = (transformValue(Option.empty, transformationsFor(currentCol, mappings)), List())
              if (transformed._1.isDefined) Tuple2.apply(transformed._1.get, transformed._2) else Tuple2.apply(null, transformed._2)
            }
          }
        })
        //if/else need here for handling identifiers vs dataextensions, for example
        val values = if (mappings.filter(_.name == name).map(_.source.fieldType).head.equalsIgnoreCase("complex"))
          Row.fromSeq(transformed.map(x => x._1))
        else
          transformed.map(x => x._1)
        val errors = transformed.flatMap(x => x._2).toList
        (values, errors)
      case dataType: ArrayType =>
        try {
          dataType.elementType match {
            case subType: StructType =>
              // Iterate every value and call transformRow on every element
              val elements = row.getAs[mutable.WrappedArray[GenericRowWithSchema]](row.fieldIndex(name))
              val transformedElements = elements.map(row => {
                transformRow(row, mappingsFor(name, mappings))
              })
              (transformedElements, List())
            case _ =>
              // Iterate through Primitive Types here, call transformValue again recursively
              if (row.isNullAt(row.fieldIndex(name))) {
                (null, List())
              } else {
                val transformedElements = row.getAs[mutable.WrappedArray[Any]](row.fieldIndex(name)).map(arrayField => {
                  transformValue(Option.apply(arrayField), transformationsFor(name, mappings))
                })
                (transformedElements.map(_.get), List())
              }
          }
        }
        catch {
          // If column does not exist
          case _: IllegalArgumentException => (null, List())
        }
      case value: Any =>
        val raw = try {
          //logger.debug("Raw Value : " + row.getAs[String](row.fieldIndex(name)));
          field.dataType match {
            case StringType => if (row.isNullAt(row.fieldIndex(name))) { Option.empty } else { Option.apply(row.getAs[String](row.fieldIndex(name))) }
            case BooleanType => if (row.isNullAt(row.fieldIndex(name))) { Option.empty } else { Option.apply(row.getAs[Boolean](row.fieldIndex(name))) }
            case LongType => if (row.isNullAt(row.fieldIndex(name))) { Option.empty } else { Option.apply(row.getAs[Long](row.fieldIndex(name))) }
            case DoubleType => if (row.isNullAt(row.fieldIndex(name))) { Option.empty } else { Option.apply(row.getAs[Double](row.fieldIndex(name))) }
            case IntegerType => if (row.isNullAt(row.fieldIndex(name))) { Option.empty } else { Option.apply(row.getAs[Int](row.fieldIndex(name))) }
          }
        } catch {
          case _: Throwable => null
        }
        val transformed = transformValue(raw, transformationsFor(name, mappings))
        val errors =
          if (transformed.isEmpty && mappingFor(name, mappings).skipValidationOnNull) {
            List[Exception]()
        } else {
          validateColumn(name, transformed, validationsFor(name, mappings))
        }

        logger.debug("Name : " + name + ", Transformed : " + transformed)
        (if (transformed.isDefined) transformed.get else null, errors)
    }
    (value, errors)
  }

  def transformValue(value: Option[Any], transformations: List[Function]): Option[Any] = {
    var result = Tuple1.apply(value)
    if (transformations != null) {
      transformations.foreach(transformation => {
        result = transformation.name match {
          case "setStringIfNull" => Tuple1.apply(setStringIfNull(result._1, transformation.params.get))
          case "setBooleanIfNull" => Tuple1.apply(setBooleanIfNull(result._1, transformation.params.get))
          case "setIntIfNull" => Tuple1.apply(setIntIfNull(result._1, transformation.params.get))
          case "setLongIfNull" => Tuple1.apply(setLongIfNull(result._1, transformation.params.get))
          case "setDoubleIfNull" => Tuple1.apply(setDoubleIfNull(result._1, transformation.params.get))
          case "toLowerCase" => if (result._1.isDefined) Tuple1.apply(toLowerCase(result._1.get.toString)) else result
          case "toUpperCase" => if (result._1.isDefined) Tuple1.apply(toUpperCase(result._1.get.toString)) else result
          case "trim" => if (result._1.isDefined) Tuple1.apply(trimString(result._1.get.toString)) else result
          case "replaceOneOf" => if (result._1.isDefined) Tuple1.apply(replaceOneOf(result._1.get.toString, transformation.params.get)) else result
          case "castLongToTimestampInt64" => if (result._1.isDefined) Tuple1.apply(castLongToTimestampInt64(result._1.get.toString.toLong)) else result
          case "castStringToTimestampInt64" => if (result._1.isDefined) Tuple1.apply(castStringToTimestampInt64(result._1.get.toString)) else result
          case "castLongToDate" => if (result._1.isDefined) Tuple1.apply(castLongToDate(result._1.get.toString.toLong)) else result
          case "castStringToDate" => if (result._1.isDefined) Tuple1.apply(castStringToDate(result._1.get.toString)) else result
          case "castStringToDateWithPattern" => if (result._1.isDefined) Tuple1.apply(castStringToDateWithPattern(result._1.get.toString, transformation.params.get.head)) else result
          case "castStringToInt" => if (result._1.isDefined) Tuple1.apply(castStringToInt(result._1.get.toString)) else result
          case "castLongToInt" => if (result._1.isDefined) Tuple1.apply(castLongToInt(result._1.get.toString.toLong)) else result
          case "formatDate" => Tuple1.apply(formatDate(result._1.get, transformation.params.get))
        }
      })
    }
    result._1
  }

  def transformationsFor(name: String, mappings: List[Mapping]): List[Function] = {
    mappings
      .filter(nonNull)
      .filter(_.name.equalsIgnoreCase(name))
      .filter(_.transformations != null)
      .flatMap(_.transformations)
  }

  def validationsFor(name: String, mappings: List[Mapping]): List[Function] = {
    mappings
      .filter(nonNull)
      .filter(_.name.equalsIgnoreCase(name))
      .filter(_.validations != null)
      .flatMap(_.validations)
  }

  def mappingFor(name: String, mappings: List[Mapping]): Mapping =
    mappings
      .filter(nonNull)
      .filter(_.source.name.equalsIgnoreCase(name))
      .head

  def mappingsFor(name: String, mappings: List[Mapping]): List[Mapping] =
    mappings
      .filter(nonNull)
      .map(_.destination)
      .filter(_.name.equalsIgnoreCase(name))
      .filter(_.mappings != null)
      .map(_.mappings)
      .head

  def containsMappingName(mappings: List[Mapping], sourceName: String): Boolean =
    !mappings
      .filter(_.source.name.equalsIgnoreCase(sourceName)).isEmpty

  def findMappingName(mappings: List[Mapping], sourceName: String): String =
    mappings
      .filter(_.source.name == sourceName)
      .head
      .name

  def mappingsForName(mappings: List[Mapping], name: String): List[Mapping] =
    mappings
      .filter(_.source.name.equalsIgnoreCase(name))
      .head
      .destination
      .mappings
}