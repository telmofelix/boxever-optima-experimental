DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_item_flights_enriched;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_item_flights_enriched
WITH (
      external_location = 's3://{{ optima_order_item_flights_enriched_bucket_name_and_path }}/',
      format = 'PARQUET',
      parquet_compression = 'GZIP',
      partitioned_by = ARRAY['channel']
)
AS
SELECT oi.currency_code
,oi.modified_at
,oi.name
,oi.order_item_flight_ordered_at
,oi.price
,oi.product_id
,oi.quantity
,oi.ref
,oi.status
,oi.type
,oi.order_ordered_at
,oi.order_ref
,oi.ordered_at_date
,od.origin
,od.destination
,od.channel
FROM {{ optima_scratch_athena_schema_name }}.order_item_flights oi
JOIN {{ optima_scratch_athena_schema_name }}.order_item_flights_origin_destination od ON oi.ref = od.order_item_ref;
