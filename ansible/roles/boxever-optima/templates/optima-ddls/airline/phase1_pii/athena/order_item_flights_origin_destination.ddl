DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_item_flights_origin_destination;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_item_flights_origin_destination
WITH (
      external_location = 's3://{{ optima_order_item_flights_origin_destination_bucket_name_and_path }}/',
      format = 'PARQUET',
      parquet_compression = 'GZIP',
      partitioned_by = ARRAY['channel']
)
as

WITH cte1 AS (
SELECT order_item_flight_segments.order_item_ref
    , min(id) AS segment_id
FROM {{ optima_scratch_athena_schema_name }}.order_item_flight_segments
GROUP BY order_item_ref
),

origin AS (
SELECT order_item_flight_segments.order_item_ref
    , order_item_flight_segments.origin
    , order_item_flight_segments.ordered_at_date
    , order_item_flight_segments.channel
FROM {{ optima_scratch_athena_schema_name }}.order_item_flight_segments
JOIN cte1 ON order_item_flight_segments.order_item_ref = cte1.order_item_ref AND order_item_flight_segments.id = cte1.segment_id
),

cte2 AS (
SELECT order_item_flight_segments.order_item_ref
    , max(id) AS segment_id
FROM {{ optima_scratch_athena_schema_name }}.order_item_flight_segments
GROUP BY order_item_ref
),

destination AS (
SELECT order_item_flight_segments.order_item_ref
    , order_item_flight_segments.destination
FROM {{ optima_scratch_athena_schema_name }}.order_item_flight_segments
JOIN cte2 ON order_item_flight_segments.order_item_ref = cte2.order_item_ref AND order_item_flight_segments.id = cte2.segment_id
)

SELECT o.order_item_ref
    , o.ordered_at_date
    , o.origin
    , d.destination
    , o.channel
FROM origin o
JOIN destination d ON o.order_item_ref = d.order_item_ref;
