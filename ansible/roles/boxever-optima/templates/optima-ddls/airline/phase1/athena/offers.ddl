DROP TABLE {{ optima_trusted_scratch_athena_schema_name }}.offers;

CREATE EXTERNAL TABLE IF NOT EXISTS {{ optima_trusted_scratch_athena_schema_name }}.offers (
  `ref` string,
  `name` string,
  `description` string,
  `createdAt` date,
  `createdBy` string,
  `modifiedAt` string,
  `modifiedBy` string,
  `archived` boolean
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION '${offers_location}'
TBLPROPERTIES('skip.header.line.count'='1');