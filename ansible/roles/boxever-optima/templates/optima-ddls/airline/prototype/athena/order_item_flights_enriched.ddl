DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_item_flights_enriched;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_item_flights_enriched
WITH (
      external_location = 's3://{{ optima_order_item_flights_enriched_bucket_name_and_path }}/',
      format = 'PARQUET',
      parquet_compression = 'GZIP',
      partitioned_by = ARRAY['channel']
)
AS
SELECT oi.consumer_type_code
,oi.created_at
,oi.currency_code
,oi.description
,oi.item_id
,oi.language
,oi.modified_at
,oi.name
,oi.order_item_flight_ordered_at
,oi.original_currency_code
,oi.original_price
,oi.passenger_type_code
,oi.price
,oi.product_id
,oi.quantity
,oi.ref
,oi.reference_id
,oi.status
,oi.trip_type
,oi.type
,oi.vendor
,oi.order_ordered_at
,oi.order_ref
,oi.ordered_at_date
,od.origin
,od.destination
,od.channel
FROM {{ optima_scratch_athena_schema_name }}.order_item_flights oi
JOIN {{ optima_scratch_athena_schema_name }}.order_item_flights_origin_destination od ON oi.ref = od.order_item_ref;
