DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.session_utms;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.session_utms
  WITH(
    external_location = 's3://{{ optima_session_utms_bucket_name_and_path }}',
    format = 'PARQUET',
    parquet_compression = 'GZIP',
    partitioned_by = ARRAY['channel']
  )
AS
SELECT s.ref as session_ref,
       de."values".utm_campaign,
       de."values".utm_content,
       de."values".utm_medium,
       de."values".utm_source,
       de."values".utm_term,
       s.modifiedAt as modified_at,
       s.created_at_date,
       s.channel
  FROM {{ optima_trusted_scratch_athena_schema_name }}.sessions s
    CROSS JOIN UNNEST(s.dataExtensions) AS de(de)
 WHERE de.name = 'ArbitraryData'
   AND ( de."values".utm_campaign is NOT null
     AND length(de."values".utm_campaign) > 0);
