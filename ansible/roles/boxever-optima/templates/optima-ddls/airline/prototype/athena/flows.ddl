DROP TABLE {{ optima_trusted_scratch_athena_schema_name }}.flows;

CREATE EXTERNAL TABLE IF NOT EXISTS {{ optima_trusted_scratch_athena_schema_name }}.flows (
  `status` string,
  `name` string,
  `ref` string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION '${flows_location}'
TBLPROPERTIES('skip.header.line.count'='1');