DROP TABLE {{ optima_trusted_scratch_athena_schema_name }}.decision_variants;

CREATE EXTERNAL TABLE IF NOT EXISTS {{ optima_trusted_scratch_athena_schema_name }}.decision_variants (
  `archived` boolean,
  `decisionModelDefinitionRef` string,
  `inProduction` boolean,
  `name` string,
  `ref` string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION '${decision_variants_location}'
TBLPROPERTIES('skip.header.line.count'='1');