CREATE OR REPLACE VIEW {{ item.value.view_schema }}.{{ item.value.table }} AS SELECT DISTINCT t1.*
FROM {{ item.value.table_schema }}.{{ item.value.table }} t1 JOIN
  (select ref, max({{ item.value.dedup_field }}) as {{ item.value.dedup_field }} from {{ item.value.table_schema }}.{{ item.value.table }} group by ref) t2
   on t1.ref = t2.ref and t1.{{ item.value.dedup_field }} = t2.{{ item.value.dedup_field }}
WHERE CAST(t1.bx_batch_id AS DATE) <= CAST('${bx_batch_id}' AS DATE);