CREATE OR REPLACE VIEW {{ item.value.view_schema }}.{{ item.value.table }} AS SELECT t1.*
FROM {{ item.value.table_schema }}.{{ item.value.table }} t1
WHERE CAST(t1.bx_batch_id AS DATE) <= CAST('${bx_batch_id}' AS DATE);