DROP TABLE {{ optima_trusted_scratch_athena_schema_name }}.decisions;

CREATE EXTERNAL TABLE IF NOT EXISTS {{ optima_trusted_scratch_athena_schema_name }}.decisions (
  `archived` boolean,
  `name` string,
  `ref` string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ','
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION '${decisions_location}'
TBLPROPERTIES('skip.header.line.count'='1');