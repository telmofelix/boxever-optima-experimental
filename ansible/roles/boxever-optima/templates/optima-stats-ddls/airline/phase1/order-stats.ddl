DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_stats;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_stats
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_order_stats_bucket_name_and_path }}'
  )
AS
SELECT COALESCE(total_items.order_ref, total_price.order_ref, number_of_adults.order_ref) AS order_ref,
       total_items.total_items,
       total_price.total_price,
       number_of_adults.number_of_adults
  FROM {{ optima_scratch_athena_schema_name }}.order_stats_total_items            AS total_items
 FULL JOIN {{ optima_scratch_athena_schema_name }}.order_stats_total_price        AS total_price
   ON total_items.order_ref = total_price.order_ref
 FULL JOIN {{ optima_scratch_athena_schema_name }}.order_stats_number_of_adults   AS number_of_adults
   ON COALESCE(total_items.order_ref, total_price.order_ref) = number_of_adults.order_ref;