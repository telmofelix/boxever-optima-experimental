DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name_main }}.event_stats;

CREATE TABLE {{ optima_scratch_athena_schema_name_main }}.event_stats
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_event_stats_bucket_name_and_path }}'
  )
AS
SELECT count(*) as count,
       created_at_date AS created_at_date,
       channel AS channel,
       type AS type
       FROM {{ optima_scratch_athena_schema_name_main }}.events
       group by created_at_date, channel, type;