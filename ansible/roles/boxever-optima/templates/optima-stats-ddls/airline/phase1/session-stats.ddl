DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.session_stats;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.session_stats
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_session_stats_bucket_name_and_path }}'
  )
AS
SELECT
  *
FROM
  {{ optima_scratch_athena_schema_name }}.session_stats_duration;