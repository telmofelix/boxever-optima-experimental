DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_orders;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_orders
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_life_time_orders_bucket_name_and_path }}'
  )
AS
SELECT guest_ref        AS guest_ref,
       count(guest_ref) AS lifetime_orders
  FROM {{ optima_scratch_athena_schema_name }}.orders
 GROUP BY guest_ref;