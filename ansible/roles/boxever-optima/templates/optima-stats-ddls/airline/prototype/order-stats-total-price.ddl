DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_stats_total_price;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_stats_total_price
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_order_stats_total_price_bucket_name_and_path }}'
  )
AS
SELECT order_ref     AS order_ref,
       currency_code AS currency_code,
       SUM(price)    AS total_price
  FROM {{ optima_scratch_athena_schema_name }}.order_items
 GROUP BY order_ref, currency_code;
