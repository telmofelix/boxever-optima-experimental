DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats_months_since_last_order;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats_months_since_last_order
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_months_since_last_order_bucket_name_and_path }}'
  )
AS
SELECT guest_ref                                         AS guest_ref,
       DATE_DIFF('month', MAX(ordered_at), current_date) AS months_since_last_order
  FROM {{ optima_scratch_athena_schema_name }}.orders
 GROUP BY guest_ref;
