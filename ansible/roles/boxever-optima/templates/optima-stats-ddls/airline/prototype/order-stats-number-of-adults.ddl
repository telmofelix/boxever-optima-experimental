DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_stats_number_of_adults;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_stats_number_of_adults
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_order_stats_number_of_adults_bucket_name_and_path }}'
  )
AS
SELECT order_ref                                                                           AS order_ref,
       SUM(CASE WHEN type='FLIGHT' AND passenger_type_code='ADT' THEN quantity ELSE 0 END) AS number_of_adults
  FROM {{ optima_scratch_athena_schema_name }}.order_items
 GROUP BY order_ref;
