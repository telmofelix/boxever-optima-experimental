DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_stats_total_items;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_stats_total_items
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_order_stats_total_items_bucket_name_and_path }}'
  )
AS
SELECT order_ref        AS order_ref,
       COUNT(order_ref) AS total_items
  FROM {{ optima_scratch_athena_schema_name }}.order_items
 GROUP BY order_ref;
