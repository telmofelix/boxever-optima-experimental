DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_spend;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_spend
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_life_time_spend_bucket_name_and_path }}'
  )
AS
SELECT guest_ref     AS guest_ref,
       currency_code AS currency_code,
       SUM(price)    AS lifetime_spend
  FROM {{ optima_scratch_athena_schema_name }}.orders
 GROUP BY guest_ref, currency_code;
