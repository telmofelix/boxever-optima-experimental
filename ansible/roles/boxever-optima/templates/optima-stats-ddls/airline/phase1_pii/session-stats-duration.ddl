DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.session_stats_duration;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.session_stats_duration
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_session_stats_duration_bucket_name_and_path }}'
  )
AS
SELECT
  s.ref as session_ref,
  s.guestRef as guest_ref,
  s.modifiedAt as modified_at,
  s.created_at_date as created_at_date,
  s.duration as duration,
  de."values".searches as searches,
  de."values".views as views
FROM
  {{ optima_trusted_scratch_athena_schema_name }}.sessions s
CROSS JOIN
  UNNEST(s.dataExtensions) AS de(de)
WHERE
  de.name = 'SessionStatistics'
;