DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_bucket_name_and_path }}'
  )
AS
SELECT COALESCE(months_since_last_order.guest_ref, lifetime_spend.guest_ref, ninety_days_spend.guest_ref, lifetime_orders.guest_ref, ninety_days_orders.guest_ref) AS guest_ref,
       months_since_last_order.months_since_last_order,
       lifetime_orders.lifetime_orders,
       lifetime_spend.lifetime_spend,
       lifetime_spend.currency_code        AS lifetime_spend_currency_code,
       ninety_days_orders.ninety_day_orders,
       ninety_days_spend.ninety_days_spend,
       ninety_days_spend.currency_code     AS ninety_days_spend_currency_code
  FROM {{ optima_scratch_athena_schema_name }}.guest_stats_months_since_last_order AS months_since_last_order
 FULL JOIN {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_spend   AS lifetime_spend
   ON months_since_last_order.guest_ref = lifetime_spend.guest_ref
 FULL JOIN {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_spend AS ninety_days_spend
   ON COALESCE(months_since_last_order.guest_ref, lifetime_spend.guest_ref) = ninety_days_spend.guest_ref
 FULL JOIN {{ optima_scratch_athena_schema_name }}.guest_stats_lifetime_orders AS lifetime_orders
   ON COALESCE(months_since_last_order.guest_ref, lifetime_spend.guest_ref, ninety_days_spend.guest_ref) = lifetime_orders.guest_ref
 FULL JOIN {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_orders AS ninety_days_orders
   ON COALESCE(months_since_last_order.guest_ref, lifetime_spend.guest_ref, ninety_days_spend.guest_ref, lifetime_orders.guest_ref) = ninety_days_orders.guest_ref;
