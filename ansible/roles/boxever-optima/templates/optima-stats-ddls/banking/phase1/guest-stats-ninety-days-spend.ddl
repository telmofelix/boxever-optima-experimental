DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_spend;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_spend
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_ninety_days_spend_bucket_name_and_path }}'
  )
AS
SELECT guest_ref     AS guest_ref,
       currency_code AS currency_code,
       sum(price)    AS ninety_days_spend
  FROM {{ optima_scratch_athena_schema_name }}.orders
 WHERE ordered_at_date >= CAST(TO_ISO8601(current_date - interval '90' day) AS DATE)
 GROUP BY guest_ref, currency_code;
