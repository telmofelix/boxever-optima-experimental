DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.order_stats;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.order_stats
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_order_stats_bucket_name_and_path }}'
  )
AS
SELECT order_ref,
       total_items
  FROM {{ optima_scratch_athena_schema_name }}.order_stats_total_items
