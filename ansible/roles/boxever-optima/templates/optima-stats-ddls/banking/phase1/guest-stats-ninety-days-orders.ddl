DROP TABLE IF EXISTS {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_orders;

CREATE TABLE {{ optima_scratch_athena_schema_name }}.guest_stats_ninety_days_orders
  WITH (
    format = 'PARQUET',
    external_location = 's3://{{ optima_guest_stats_ninety_days_orders_bucket_name_and_path }}'
  )
AS
SELECT guest_ref        AS guest_ref,
       count(guest_ref) AS ninety_day_orders
  FROM {{ optima_scratch_athena_schema_name }}.orders
 WHERE ordered_at_date >= CAST(TO_ISO8601(current_date - interval '90' day) AS DATE)
 GROUP BY guest_ref;