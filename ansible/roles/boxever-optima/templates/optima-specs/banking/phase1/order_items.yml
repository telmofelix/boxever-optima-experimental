---
version: 3
mapper: simple
source:
  paths:
    - s3://{{ optima_trusted_orders_bucket_name_and_path }}/
  format: parquet
  allowRunWithoutInputData: "{{ allow_run_without_input_data }}"
destination:
  path: s3://{{ optima_order_items_bucket_name_and_path }}/
  format: parquet
  partitions:
#    - channel
#    - type
    - ordered_at_date
  saveMode: overwrite
  ddls:
    - schemaName: "{{ optima_scratch_hive_schema_name }}"
      tableName: order_items
      path: s3://{{ optima_scratch_hive_schema_bucket_name_and_path }}/order_items_table.ddl
    - schemaName: "{{ optima_scratch_athena_schema_name }}"
      tableName: order_items
      path: s3://{{ optima_scratch_athena_schema_bucket_name_and_path }}/order_items_table.ddl
quarantine:
  path: s3://{{ optima_quarantine_order_items_bucket_name_and_path }}/
  format: parquet
  partitions: []
  saveMode: overwrite
  ddls:
    - schemaName: "{{ optima_quarantine_hive_schema_name }}"
      tableName: order_items
      path: s3://{{ optima_quarantine_hive_schema_bucket_name_and_path }}/order_items.ddl
    - schemaName: "{{ optima_quarantine_athena_schema_name }}"
      tableName: order_items
      path: s3://{{ optima_quarantine_athena_schema_bucket_name_and_path }}/order_items.ddl
mappings:
#  - source:
#      name: channel
#      type: string
#    destination:
#      name: channel
#      type: string
  # Order Items
  - source:
      name: orderItems
      type: complex
      collection: true
    destination:
      name: orderItems
      type: complex
      mappings:
        - source:
            name: ref
            type: string
          destination:
            name: order_item_ref
            type: string
        - source:
            name: type
            type: string
          destination:
            name: type
            type: string
        - source:
            name: productId
            type: string
          destination:
            name: product_id
            type: string
        - source:
            name: referenceId
            type: string
          destination:
            name: reference_id
            type: string
        - source:
            name: channel
            type: string
          destination:
            name: channel
            type: string
        - source:
            name: orderedAt
            type: timestampInt64
          destination:
            name: order_item_ordered_at
            type: timestampInt64
        - source:
            name: modifiedAt
            type: timestampInt64
          destination:
            name: modified_at
            type: timestampInt64
        - source:
            name: applicationStatus
            type: string
          destination:
            name: application_status
            type: string
        - source:
            name: applicationStatusDate
            type: timestampInt64
          destination:
            name: application_status_date
            type: timestampInt64
        - source:
            name: drawdownStatus
            type: string
          destination:
            name: drawdown_status
            type: string
        - source:
            name: drawdownStatusDate
            type: timestampInt64
          destination:
            name: drawdown_status_date
            type: timestampInt64
        - source:
            name: openStatus
            type: string
          destination:
            name: open_status
            type: string
        - source:
            name: openDate
            type: timestampInt64
          destination:
            name: open_date
            type: timestampInt64
        - source:
            name: applicationId
            type: string
          destination:
            name: application_id
            type: string
        - source:
            name: applicationPreferredFulfilmentChannel
            type: string
          destination:
            name: application_preferred_fulfilment_channel
            type: string
        - source:
            name: applicationOriginatorChannel
            type: string
          destination:
            name: application_originator_channel
            type: string
        - source:
            name: applicationReferralChannel
            type: string
          destination:
            name: application_referral_channel
            type: string
        - source:
            name: inArrears
            type: boolean
          destination:
            name: in_arrears
            type: boolean
        - source:
            name: arrears
            type: complex
          destination:
            name: arrears
            type: complex
            mappings:
              - source:
                  name: daysPastDue
                  type: integer
                destination:
                  name: arrears_days_past_due
                  type: integer
              - source:
                  name: daysPastDueDate
                  type: timestampInt64
                destination:
                  name: arrears_days_past_due_date
                  type: timestampInt64
              - source:
                  name: daysInArrears
                  type: integer
                destination:
                  name: arrears_days_in_arrears
                  type: integer
              - source:
                  name: daysInArrearsDate
                  type: timestampInt64
                destination:
                  name: arrears_days_in_arrears_date
                  type: timestampInt64
              - source:
                  name: baselIIDaysPastDue
                  type: integer
                destination:
                  name: arrears_basel_ii_days_past_due
                  type: integer
              - source:
                  name: baselIIDaysPastDueDate
                  type: timestampInt64
                destination:
                  name: arrears_basel_ii_days_past_due_date
                  type: timestampInt64
        - source:
            name: balance
            type: double
          destination:
            name: balance
            type: double
        - source:
            name: contractMaturityDate
            type: timestampInt64
          destination:
            name: contract_maturity_date
            type: timestampInt64
        - source:
            name: feePeriod
            type: string
          destination:
            name: fee_period
            type: string
        - source:
            name: interestRate
            type: double
          destination:
            name: interest_rate
            type: double
        - source:
            name: interestRateType
            type: string
          destination:
            name: interest_rate_type
            type: string
        - source:
            name: lastFeeCharge
            type: double
          destination:
            name: last_fee_charge
            type: double
        - source:
            name: loanAmount
            type: double
          destination:
            name: loan_amount
            type: double
        - source:
            name: ltv
            type: double
          destination:
            name: ltv
            type: double
        - source:
            name: mainAccount
            type: boolean
          destination:
            name: main_account
            type: boolean
        - source:
            name: nextRepaymentDate
            type: timestampInt64
          destination:
            name: next_repayment_date
            type: timestampInt64
        - source:
            name: partyStatus
            type: string
          destination:
            name: party_status
            type: string
        - source:
            name: primaryApplicantOrHolder
            type: string
          destination:
            name: primary_applicant_or_holder
            type: string
        - source:
            name: category
            type: string
          destination:
            name: category
            type: string
        - source:
            name: purpose
            type: string
          destination:
            name: purpose
            type: string
        - source:
            name: remainingBalance
            type: double
          destination:
            name: remaining_balance
            type: double
        - source:
            name: repaymentFrequency
            type: string
          destination:
            name: repayment_frequency
            type: string
        - source:
            name: termMonths
            type: integer
          destination:
            name: term_months
            type: integer
        - source:
            name: topUpInd
            type: boolean
          destination:
            name: top_up_ind
            type: boolean
  - source:
      name: orderedAt
      type: timestampInt64
    destination:
      name: order_ordered_at
      type: timestampInt64
  - source:
      name: ordered_at_date
      type: date
    destination:
      name: ordered_at_date
      type: date
  - source:
      name: ref
      type: string
    destination:
      name: order_ref
      type: string