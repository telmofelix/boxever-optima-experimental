---
version: 3
mapper: complex
source:
  paths:
    - s3://{{ oneview_snapshots_bucket_name_and_path }}/${date}/v2/sessions/{{client_key}}/
  format: json
  allowRunWithoutInputData: "{{ allow_run_without_input_data }}"
destination:
  path: s3://{{ optima_trusted_sessions_bucket_name_and_path }}/
  format: parquet
  partitions:
    - channel
    - created_at_date
  saveMode: overwrite
  ddls:
    - schemaName: "{{ optima_trusted_scratch_hive_schema_name }}"
      tableName: sessions
      path: s3://{{ optima_trusted_scratch_hive_schema_bucket_name_and_path }}/sessions.ddl
    - schemaName: "{{ optima_trusted_scratch_athena_schema_name }}"
      tableName: sessions
      path: s3://{{ optima_trusted_scratch_athena_schema_bucket_name_and_path }}/sessions.ddl
quarantine:
  path: s3://{{ optima_trusted_quarantine_sessions_bucket_name_and_path }}/
  format: parquet
  partitions: []
  saveMode: overwrite
  ddls:
    - schemaName: "{{ optima_trusted_quarantine_hive_schema_name }}"
      tableName: sessions
      path: s3://{{ optima_trusted_quarantine_hive_schema_bucket_name_and_path }}/sessions.ddl
    - schemaName: "{{ optima_trusted_quarantine_athena_schema_name }}"
      tableName: sessions
      path: s3://{{ optima_trusted_quarantine_athena_schema_bucket_name_and_path }}/sessions.ddl
quarantine_summary:
  path: s3://{{ optima_trusted_quarantine_sessions_bucket_name_and_path }}_summary/
  format: json
  partitions: []
  coalesce: 1
  saveMode: overwrite
mappings:
  - source:
      name: browserRef
      type: string
    destination:
      name: browserRef
      type: string
    validations:
      - name: matchesPattern
        params:
          - "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
      - name: isUUID
    skipValidationOnNull: true
  - source:
      name: cartType
      type: string
    destination:
      name: cartType
      type: string
    transformations:
      - name: setStringIfNull
        params:
          - UKN
      - name: trim
      - name: toUpperCase
    validations:
      - name: isOneOf
        params:
          - BROWSED
          - CONVERTED
          - ABANDONED
          - UKN
  - source:
      name: channel
      type: string
    destination:
      name: channel
      type: string
    transformations:
      - name: setStringIfNull
        params:
          - UKN
      - name: trim
      - name: toUpperCase
      - name: replaceOneOf
        params:
          - MAIL
          - EMAIL
      - name: replaceOneOf
        params:
          - UNDEFINED
          - UKN
    validations:
      - name: isOneOf
        params:
          - AGENT
          - ATM
          - SERVICE_ISSUE
          - CALL_CENTER
          - EMAIL
          - WEB
          - MOBILE
          - MOBILE_WEB
          - MOBILE_APP
          - AIRPORT_KIOSK
          - DATA_SYNC
          - BRANCH
          - GDS
          - KIOSK
          - OFFLINE
          - OTA
          - SUPPORT
          - OTHER
          - UKN
  - source:
      name: createdAt
      type: long
    destination:
      name: createdAt
      type: timestampInt64
    transformations:
      - name: setLongIfNull
        params:
          - 0
      - name: castLongToTimestampInt64
  - source:
      name: createdAt
      type: long
    destination:
      name: created_at_date
      type: date
    transformations:
      - name: setLongIfNull
        params:
          - 0
      - name: castLongToDate
  - source:
      name: currency
      type: string
    destination:
      name: currency
      type: string
    transformations:
      - name: trim
      - name: toUpperCase
  - source:
      name: duration
      type: long
    destination:
      name: duration
      type: long
    transformations:
      - name: setLongIfNull
        params:
          - 0
  - source:
      name: endedAt
      type: long
    destination:
      name: endedAt
      type: timestampInt64
    transformations:
      - name: setLongIfNull
        params:
          - 0
      - name: castLongToTimestampInt64
  - source:
      name: firstPageUri
      type: string
    destination:
      name: firstPageUri
      type: string
    transformations:
      - name: trim
  - source:
      name: flowExecutionRef
      type: string
    destination:
      name: flowExecutionRef
      type: string
    validations:
      - name: matchesPattern
        params:
          - "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
      - name: isUUID
    skipValidationOnNull: true
  - source:
      name: guestRef
      type: string
    destination:
      name: guestRef
      type: string
    validations:
      - name: matchesPattern
        params:
          - "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
      - name: isUUID
    skipValidationOnNull: true
  - source:
      name: ip
      type: string
    destination:
      name: ip
      type: string
    transformations:
      - name: trim
  - source:
      name: language
      type: string
    destination:
      name: language
      type: string
    transformations:
      - name: trim
      - name: toUpperCase
  - source:
      name: lastPageUri
      type: string
    destination:
      name: lastPageUri
      type: string
    transformations:
      - name: trim
  - source:
      name: modifiedAt
      type: long
    destination:
      name: modifiedAt
      type: timestampInt64
    transformations:
      - name: setLongIfNull
        params:
          - 0
      - name: castLongToTimestampInt64
  - source:
      name: operatingSystem
      type: string
    destination:
      name: operatingSystem
      type: string
  - source:
      name: originalCurrencyCode
      type: string
    destination:
      name: originalCurrencyCode
      type: string
    transformations:
      - name: trim
      - name: toUpperCase
  - source:
      name: originalValue
      type: double
    destination:
      name: originalValue
      type: double
    transformations:
      - name: setDoubleIfNull
        params:
          - 0
  - source:
      name: pointOfSale
      type: string
    destination:
      name: pointOfSale
      type: string
  - source:
      name: ref
      type: string
    destination:
      name: ref
      type: string
    validations:
      - name: matchesPattern
        params:
          - "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"
      - name: isUUID
  - source:
      name: referer
      type: string
    destination:
      name: referer
      type: string
  - source:
      name: sessionType
      type: string
    destination:
      name: sessionType
      type: string
    transformations:
      - name: trim
      - name: toUpperCase
  - source:
      name: startedAt
      type: long
    destination:
      name: startedAt
      type: timestampInt64
    transformations:
      - name: setLongIfNull
        params:
          - 0
      - name: castLongToTimestampInt64
  - source:
      name: status
      type: string
    destination:
      name: status
      type: string
    transformations:
      - name: trim
      - name: toUpperCase
  - source:
      name: trafficSource
      type: string
    destination:
      name: trafficSource
      type: string
  - source:
      name: userAgent
      type: string
    destination:
      name: userAgent
      type: string
  - source:
      name: value
      type: double
    destination:
      name: value
      type: double
    transformations:
      - name: setDoubleIfNull
        params:
          - 0
  - source:
      name: dataExtensions
      type: complex
      collection: true
    destination:
      name: dataExtensions
      type: complex
      mappings:
        - source:
            name: createdAt
            type: long
          destination:
            name: createdAt
            type: timestampInt64
          transformations:
            - name: setLongIfNull
              params:
                - 0
            - name: castLongToTimestampInt64
        - source:
            name: key
            type: string
          destination:
            name: key
            type: string
        - source:
            name: modifiedAt
            type: long
          destination:
            name: modifiedAt
            type: timestampInt64
          transformations:
            - name: setLongIfNull
              params:
                - 0
            - name: castLongToTimestampInt64
        - source:
            name: name
            type: string
          destination:
            name: name
            type: string
        - source:
            name: values
            type: complex
          destination:
            name: values
            type: complex
            mappings:
              - source:
                  name: searches
                  type: long
                destination:
                  name: searches
                  type: long
              - source:
                  name: utm_campaign
                  type: string
                destination:
                  name: utm_campaign
                  type: string
              - source:
                  name: utm_content
                  type: string
                destination:
                  name: utm_content
                  type: string
              - source:
                  name: utm_medium
                  type: string
                destination:
                  name: utm_medium
                  type: string
              - source:
                  name: utm_source
                  type: string
                destination:
                  name: utm_source
                  type: string
              - source:
                  name: utm_term
                  type: string
                destination:
                  name: utm_term
                  type: string
              - source:
                  name: views
                  type: long
                destination:
                  name: views
                  type: long