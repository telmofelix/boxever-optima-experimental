connection: "explore"

# include all the views
include: "*.view"

datagroup: optima_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: optima_default_datagroup


##########
# guests #
##########

#explore: guest_identifiers {
#  join: guests {
#    sql_on: ${guest_identifiers.guest_ref}=${guests.ref} ;;
#    relationship: one_to_one
#    type: left_outer
#  }
#}
#
#explore: guest_stats {
#  join: guests {
#    sql_on: ${guest_stats.guest_ref}=${guests.ref} ;;
#    relationship: one_to_one
#    type: left_outer
#  }
#}
#
#explore: guest_subscriptions {
#  join: guests {
#    sql_on: ${guest_subscriptions.guest_ref}=${guests.ref} ;;
#    relationship: one_to_one
#    type: left_outer
#  }
#}


explore: guests {
  join: guest_stats {
    sql_on: ${guests.ref}=${guest_stats.guest_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: guest_identifiers {
    sql_on: ${guests.ref}=${guest_identifiers.guest_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: guest_subscriptions {
    sql_on: ${guests.ref}=${guest_subscriptions.guest_ref} ;;
    relationship: one_to_one
    type: left_outer
  }

### data extensions

  [[DATA_EXTENSIONS]]

### orders and order_items ###

  join: orders {
    sql_on: ${guests.ref}=${orders.guest_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_items {
    sql_on: ${orders.ref}=${order_items.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_item_flights {
    sql_on: ${orders.ref}=${order_item_flights.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_item_flight_segments {
    sql_on: ${orders.ref}=${order_item_flight_segments.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_contacts {
    sql_on: ${guests.ref}=${order_contacts.guest_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumers {
    sql_on: ${guests.ref}=${order_consumers.guest_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumer_order_items {
    sql_on: ${guests.ref}=${order_consumer_order_items.guest_ref} ;;
    relationship: one_to_many
    type: left_outer
  }

### sessions and events ###

  join: sessions {
    sql_on: ${guests.ref}=${sessions.guest_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: session_stats {
    sql_on: ${sessions.ref}=${session_stats.session_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: session_utms {
    sql_on: ${sessions.ref}=${session_utms.session_ref} ;;
    relationship: one_to_one
    type: left_outer
  }


  join: events {
    sql_on: ${sessions.ref}=${events.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_checkout {
    sql_on: ${sessions.ref}=${events_checkout.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_identity {
    sql_on: ${sessions.ref}=${events_identity.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_view {
    sql_on: ${sessions.ref}=${events_view.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_search {
    sql_on: ${sessions.ref}=${events_search.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_tracking_interaction {
    sql_on: ${sessions.ref}=${events_tracking_interaction.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_tracking_execution {
    sql_on: ${sessions.ref}=${events_tracking_execution.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
}

##########################
# orders and order_items #
##########################

#explore: order_consumer_order_items {}

#explore: order_consumers {}

#explore: order_contacts {}

#explore: order_item_flight_segments {}

#explore: order_item_flights {}

#explore: order_stats{
#  join: orders {
#    sql_on: ${order_stats.order_ref}=${orders.ref} ;;
#    relationship: one_to_one
#    type: left_outer
#  }
#}

explore: orders {
  join: guests {
    sql_on: ${orders.guest_ref}=${guests.ref} ;;
    relationship: many_to_one
    type: left_outer
  }
  join: order_stats {
    sql_on: ${orders.ref}=${order_stats.order_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: order_items {
    sql_on: ${orders.ref}=${order_items.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_item_flights {
    sql_on: ${orders.ref}=${order_item_flights.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_item_flight_segments {
    sql_on: ${orders.ref}=${order_item_flight_segments.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumers {
    sql_on: ${orders.ref}=${order_consumers.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_contacts {
    sql_on: ${orders.ref}=${order_contacts.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumer_order_items {
    sql_on: ${orders.ref}=${order_consumer_order_items.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
}

explore: order_items {
  join: orders {
    sql_on: ${order_items.order_ref}=${orders.ref} ;;
    relationship: many_to_one
    type: left_outer
  }
  join: order_item_flights {
    sql_on: ${orders.ref}=${order_item_flights.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_item_flight_segments {
    sql_on: ${orders.ref}=${order_item_flight_segments.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumers {
    sql_on: ${orders.ref}=${order_consumers.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_contacts {
    sql_on: ${orders.ref}=${order_contacts.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: order_consumer_order_items {
    sql_on: ${orders.ref}=${order_consumer_order_items.order_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
}

#######################
# sessions and events #
#######################

#explore: session_utms {}

explore: sessions {
  join: guests {
    sql_on: ${sessions.guest_ref}=${guests.ref} ;;
    relationship: many_to_one
    type: left_outer
  }
  join: session_utms {
    sql_on: ${sessions.ref}=${session_utms.session_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: session_stats {
    sql_on: ${sessions.ref}=${session_stats.session_ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_checkout {
    sql_on: ${sessions.ref}=${events_checkout.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_identity {
    sql_on: ${sessions.ref}=${events_identity.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_view {
    sql_on: ${sessions.ref}=${events_view.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_search {
    sql_on: ${sessions.ref}=${events_search.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_tracking_interaction {
    sql_on: ${sessions.ref}=${events_tracking_interaction.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
  join: events_tracking_execution {
    sql_on: ${sessions.ref}=${events_tracking_execution.session_ref} ;;
    relationship: one_to_many
    type: left_outer
  }
}

#explore: event_stats {}

explore: events {
  join: sessions {
    sql_on: ${events.session_ref}=${sessions.ref} ;;
    relationship: many_to_one
    type: left_outer
  }
  join: events_identity {
    sql_on: ${events_identity.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_checkout {
    sql_on: ${events_checkout.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_search {
    sql_on: ${events_search.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_tracking_interaction {
    sql_on: ${events_tracking_interaction.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_tracking_execution {
    sql_on: ${events_tracking_execution.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
  join: events_view {
    sql_on: ${events_view.ref}=${events.ref} ;;
    relationship: one_to_one
    type: left_outer
  }
}

# explore: events_campaign_tracking {
#  join: sessions {
#    sql_on: ${events_campaign_tracking.ref}=${sessions.ref} ;;
#    relationship: many_to_one
#    type: left_outer
#  }
# }
#
# explore: events_checkout {
#  join: sessions {
#    sql_on: ${events_checkout.session_ref}=${sessions.ref} ;;
#    relationship: many_to_one
#    type: left_outer
#  }
# }
#
# explore: events_search {
#   join: sessions {
#     sql_on: ${events_search.session_ref}=${sessions.ref} ;;
#     relationship: many_to_one
#     type: left_outer
#   }
# }
#
# explore: events_view {
#   join: sessions {
#     sql_on: ${events_view.session_ref}=${sessions.ref} ;;
#     relationship: many_to_one
#     type: left_outer
#   }
# }