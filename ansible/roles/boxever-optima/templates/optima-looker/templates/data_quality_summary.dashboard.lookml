- dashboard: data_quality_summary
  title: Data Quality Summary
  layout: newspaper
  elements:
  - name: Data Quality Summary
    type: text
    title_text: Data Quality Summary
    subtitle_text: Data Quality Report
    row: 0
    col: 0
    width: 24
    height: 2
  - title: Orders without order items
    name: Orders without order items
    model: optima
    explore: orders
    type: single_value
    fields: [orders.count]
    filters:
      order_items.ref: 'NULL'
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff1319"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    listen: {}
    row: 10
    col: 0
    width: 8
    height: 6
  - title: Session with no events
    name: Session with no events
    model: optima
    explore: sessions
    type: single_value
    fields: [sessions.count]
    filters:
      events.ref: 'NULL'
    sorts: [sessions.count desc]
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff2519"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    listen: {}
    row: 10
    col: 8
    width: 8
    height: 6
  - title: Customers without Guest Identifiers
    name: Customers without Guest Identifiers
    model: optima
    explore: guests
    type: single_value
    fields: [guest_identifiers.count]
    filters:
      guests.guest_type: customer
      guest_identifiers.count: 'NULL'
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff2326"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    listen: {}
    row: 16
    col: 0
    width: 8
    height: 6
  - title: Customers with  no PII
    name: Customers with  no PII
    model: optima
    explore: guests
    type: single_value
    fields: [guests.count]
    filters:
      guests.country: 'NULL'
      guests.guest_type: customer
      guests.state: 'NULL'
      guests.city: 'NULL'
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff1619"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    listen: {}
    row: 4
    col: 16
    width: 8
    height: 6
  - title: Visitors with PII
    name: Visitors with PII
    model: optima
    explore: guests
    type: single_value
    fields: [guests.count]
    filters:
      guests.guest_type: visitor
      guests.country: "-NULL"
      guests.city: "-NULL"
      guests.state: "-NULL"
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff4c6e"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    listen: {}
    row: 10
    col: 16
    width: 8
    height: 6
  - title: Visitor with orders
    name: Visitor with orders
    model: optima
    explore: guests
    type: single_value
    fields: [orders.count]
    filters:
      guests.guest_type: visitor
    sorts: [orders.count desc]
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff191d"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    series_types: {}
    listen: {}
    row: 4
    col: 8
    width: 8
    height: 6
  - title: Customers without orders
    name: Customers without orders
    model: optima
    explore: guests
    type: single_value
    fields: [guests.count]
    filters:
      guests.guest_type: customer
      orders.status: CANCELLED,DECLINED,NULL
    limit: 500
    query_timezone: Europe/Dublin
    custom_color_enabled: true
    custom_color: "#ff1613"
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: normal
    limit_displayed_rows: false
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hidden_series: [guests.count]
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: true
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen: {}
    row: 4
    col: 0
    width: 8
    height: 6
  - name: Integrity Report
    type: text
    title_text: Integrity Report
    row: 2
    col: 0
    width: 24
    height: 2
  - name: Completeness Report
    type: text
    title_text: Completeness Report
    row: 22
    col: 0
    width: 24
    height: 2
  - name: Validity Report
    type: text
    title_text: Validity Report
    row: 24
    col: 0
    width: 24
    height: 2
