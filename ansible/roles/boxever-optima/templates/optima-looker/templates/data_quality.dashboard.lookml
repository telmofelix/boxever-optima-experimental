- dashboard: data_quality__[[TABLE]]__[[FIELD]]
  title: Data Quality - [[TABLE_NAME]] - [[FIELD]]
  layout: newspaper
  elements:
  - name: [[TABLE_NAME]] Data Quality
    type: text
    title_text: [[TABLE_NAME]] Data Quality
    subtitle_text: All Data for [[TABLE_NAME]]
    row: [[ROW]]
    col: 0
    width: 24
    height: 2
  - title: Field Details
    name: Field Details
    model: data_quality
    explore: stats_[[TABLE]]_[[FIELD]]
    type: looker_single_record
    fields: [stats_[[TABLE]]_[[FIELD]].count_distinct, stats_[[TABLE]]_[[FIELD]].empty, stats_[[TABLE]]_[[FIELD]].non_empty,
      stats_[[TABLE]]_[[FIELD]].total]
    limit: 500
    dynamic_fields: [{table_calculation: completeness, label: Completeness, expression: "${stats_[[TABLE]]_[[FIELD]].non_empty}/${stats_[[TABLE]]_[[FIELD]].total}",
        value_format: !!null '', value_format_name: percent_2, _kind_hint: measure,
        _type_hint: number}, {table_calculation: uniqueness, label: Uniqueness, expression: "${stats_[[TABLE]]_[[FIELD]].count_distinct}/${stats_[[TABLE]]_[[FIELD]].non_empty}",
        value_format: !!null '', value_format_name: percent_4, _kind_hint: measure,
        _type_hint: number}]
    query_timezone: Europe/Dublin
    series_types: {}
    listen: {}
    row: [[ROW_2]]
    col: 4
    width: 5
    height: 4
  - title: Field Sample
    name: Field Sample
    model: optima
    explore: [[TABLE]]
    type: table
    fields: [[[TABLE]].[[FIELD]]]
    sorts: [[[TABLE]].[[FIELD]]]
    limit: 20
    show_view_names: 'true'
    series_types: {}
    listen: {}
    row: [[ROW_2]]
    col: 20
    width: 4
    height: 9
  - title: Populated vs Null
    name: Populated vs Null
    model: data_quality
    explore: stats_[[TABLE]]_[[FIELD]]
    type: looker_column
    fields: [stats_[[TABLE]]_[[FIELD]].non_empty, stats_[[TABLE]]_[[FIELD]].empty]
    limit: 500
    show_view_names: 'true'
    series_types: {}
    hidden_series: []
    listen: {}
    row: [[ROW_2]]
    col: 9
    width: 6
    height: 9
  - title: Top 5
    name: Top 5
    model: optima
    explore: [[TABLE]]
    type: looker_pie
    fields: [[[TABLE]].[[FIELD]], [[TABLE]].count]
    filters:
     [[TABLE]].[[FIELD]]: "-NULL"
    sorts: [[[TABLE]].count desc]
    limit: 5
    show_view_names: 'true'
    series_types: {}
    listen: {}
    row: [[ROW_2]]
    col: 15
    width: 5
    height: 9
  - title: Completeness
    name: Completeness
    model: data_quality
    explore: stats_[[TABLE]]_[[FIELD]]
    type: single_value
    fields: [stats_[[TABLE]]_[[FIELD]].non_empty, stats_[[TABLE]]_[[FIELD]].total]
    sorts: [completeness]
    limit: 500
    dynamic_fields: [{table_calculation: completeness, label: Completeness, expression: "${stats_[[TABLE]]_[[FIELD]].non_empty}/${stats_[[TABLE]]_[[FIELD]].total}",
        value_format: !!null '', value_format_name: percent_2, _kind_hint: measure,
        _type_hint: number}]
    show_view_names: 'true'
    series_types: {}
    hidden_fields: [stats_[[TABLE]]_[[FIELD]].non_empty, stats_[[TABLE]]_[[FIELD]].total]
    listen: {}
    row: [[ROW_2]]
    col: 0
    width: 4
    height: 4
  - title: Field Histogram
    name: Field Histogram
    model: optima
    explore: [[TABLE]]
    type: looker_bar
    fields: [[[TABLE]].count, [[TABLE]].[[FIELD]]]
    filters:
      [[TABLE]].[[FIELD]]: "-NULL"
    limit: 15
    dynamic_fields: [{table_calculation: percentage, label: Percentage, expression: "${[[TABLE]].count}/sum(${[[TABLE]].count})",
       value_format: !!null '', value_format_name: percent_2, _kind_hint: measure,
       _type_hint: number}]
    show_view_names: 'true'
    series_types: {}
    listen: {}
    row: [[ROW_6]]
    col: 0
    width: 9
    height: 5
[[SUB_TEMPLATE]]