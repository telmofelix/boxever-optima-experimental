view: order_stats {
  sql_table_name: order_stats ;;
  suggestions: no

  dimension: number_of_adults {
    type: number
    sql: ${TABLE}.number_of_adults ;;
  }

  dimension: order_ref {
    type: string
    sql: ${TABLE}.order_ref ;;
    primary_key: yes
  }

  dimension: total_items {
    type: number
    sql: ${TABLE}.total_items ;;
  }

  dimension: total_price {
    type: number
    sql: ${TABLE}.total_price ;;
  }

  measure: count {
    type: count_distinct
    sql: ${order_ref} ;;
    drill_fields: []
  }
}
