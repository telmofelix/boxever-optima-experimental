view: session_utms {
  sql_table_name: session_utms ;;
  suggestions: no

  dimension: channel {
    type: string
    sql: ${TABLE}.channel ;;
  }

  dimension_group: created_at_partitioned {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_at_date ;;
  }

  dimension: session_ref {
    type: string
    sql: ${TABLE}.session_ref ;;
    primary_key: yes
  }

  dimension: utm_campaign {
    type: string
    sql: ${TABLE}.utm_campaign ;;
  }

#   dimension: utm_content {
#     type: string
#     sql: ${TABLE}.utm_content ;;
#   }

  dimension: utm_medium {
    type: string
    sql: ${TABLE}.utm_medium ;;
  }

  dimension: utm_source {
    type: string
    sql: ${TABLE}.utm_source ;;
  }

#   dimension: utm_term {
#     type: string
#     sql: ${TABLE}.utm_term ;;
#   }

  measure: count {
    type: count_distinct
    sql: ${session_ref} ;;
    drill_fields: [channel, utm_campaign, utm_medium, utm_source]
  }
}
