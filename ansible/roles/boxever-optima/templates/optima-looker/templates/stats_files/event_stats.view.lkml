view: event_stats {
  sql_table_name: viva.event_stats ;;
  suggestions: no

  dimension: channel {
    type: string
    sql: ${TABLE}.channel ;;
  }

  dimension: count {
    type: number
    sql: ${TABLE}."count" ;;
  }

  dimension_group: created_at {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_at_date ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
}
