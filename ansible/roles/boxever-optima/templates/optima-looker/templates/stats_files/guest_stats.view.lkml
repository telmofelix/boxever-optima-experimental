view: guest_stats {
  sql_table_name: guest_stats ;;
  suggestions: no

  dimension: guest_ref {
    type: string
    sql: ${TABLE}.guest_ref ;;
    primary_key: yes
  }

  dimension: lifetime_orders {
    type: number
    sql: ${TABLE}.lifetime_orders ;;
  }

  dimension: lifetime_spend {
    type: number
    sql: ${TABLE}.lifetime_spend ;;
  }

  dimension: lifetime_spend_currency_code {
    type: string
    sql: ${TABLE}.lifetime_spend_currency_code ;;
  }

  dimension: months_since_last_order {
    type: number
    sql: ${TABLE}.months_since_last_order ;;
  }

  dimension: ninety_day_orders {
    type: number
    sql: ${TABLE}.ninety_day_orders ;;
  }

  dimension: ninety_days_spend {
    type: number
    sql: ${TABLE}.ninety_days_spend ;;
  }

  dimension: ninety_days_spend_currency_code {
    type: string
    sql: ${TABLE}.ninety_days_spend_currency_code ;;
   }

  measure: count {
    type: count_distinct
    sql: ${guest_ref} ;;
    drill_fields: []
  }
}
