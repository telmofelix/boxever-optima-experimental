view: session_stats {
  sql_table_name: session_stats ;;
  suggestions: no

  dimension_group: created_at_partitioned {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_at_date ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}."duration" ;;
  }

  dimension: searches {
    type: number
    sql: ${TABLE}."searches" ;;
  }

  dimension: session_ref {
    type: string
    sql: ${TABLE}.session_ref ;;
    primary_key: yes
  }

  dimension: views {
    type: number
    sql: ${TABLE}."views" ;;
  }

  measure: count {
    type: count_distinct
    sql: ${session_ref} ;;
    drill_fields: []
  }
}
