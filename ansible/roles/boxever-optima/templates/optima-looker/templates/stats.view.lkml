view: stats_[[TABLE]]_[[FIELD]] { 
label: "[[TABLE_NAME]] [[FIELD]]"
sql_table_name: [[TABLE]] ;; 
 
  measure: total {
    type: number
    sql: COUNT(1) ;;

  }

  measure: count_distinct {
    type: number
    sql: count(distinct [[FIELD]]) ;;

  }

  measure: empty {
    type: number
    sql: count( case when [[FIELD]] is null then 1 end ) ;;

  }

  measure: non_empty {
    type: number
    sql: count( case when [[FIELD]] is not null then 1 end ) ;;

  }

  measure: min {
      type: number
      sql: min([[FIELD]]) ;;

  }

   measure: max {
      type: number
      sql: max([[FIELD]]) ;;

   }

   measure: average {
      type: number
      sql: average([[FIELD]]) ;;

   }

   measure: median {
      type: number
      sql: median([[FIELD]]) ;;

   }

   [[SUB_TEMPLATE]]

} 