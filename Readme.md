[TOC]

# Overview

> ## This is a prototype project and is **NOT** intended for production use.

Boxever Optima is a prototype, powered by the Labs team, which aims to validate a new Data Pipeline and Data Cleaning 
approach using a new Serverless architecture with the overall goal of creating a new clean, optimized and trusted 
dataset for our Data Mart (Explore).

## Background

Currently the data lake is processed using Spark running on EMR. We are currently running on an old EMR Version of 5.1.0 
and Spark 2.0.1 and upgrading has been difficult and the maintenance overhead can be high. Our current cluster run 
using EMR and SPOT instances, this is quite lucrative as we can save up to 70% of the costs of running EMR. The challenge 
is it may not work as we scale as we need to be able to run parallel clusters and this could lead to challenges where 
both clusters are bidding against each other. THe solution to this is to go on-demand at an increased cost or to potentially
utilise a Serverless architecture. 

One such architecture is [AWS Glue](https://aws.amazon.com/glue/), which is a fully managed, pay as you go Spark 
infrastructure for running ETL Jobs. The cost of AWS Glue is above the on-demand cost of EMR but there is less management 
and maintenance overhead.

This project contains a prototype to test the Serverless infrastructure using a combination of AWS services

* AWS Glue - for running ETL Jobs
* AWS Step Functions and AWS Lambda - for Orchestration
* AWS Glue Data Crawlers & AWS Glue Catalog - to generate a Data Catalog from data in S3
* AWS S3 - for data storage
* AWS Athena (Based on Presto) - for running ad hoc SQL queries against the data defined in the AWS Glue Catalog
* AWS Cloudwatch - for monitoring
* AWS IAM - for access control to resources


## Simple Pipeline
Below is a simple pipeline using step functions. The actual latest pipeline is far more sophisticated so come ask the Labs team for a demo.

![Simple](docs/Optima-Data-Pipeline-Completed.jpg)



# Release Notes
The Release Notes can be viewed [here](ReleaseNotes.md).



# Quick Start Guides

## Optima

* [Optima Building & Provisioning Quickstart](docs/OptimaBuildingAndProvisionsingQuickstartGuide.md)
* [Optima Data Source Mapping Specification Quickstart](docs/OptimaDataSourceMappingSpecificationQuickstartGuide.md)
* [Optima Running Data Pipeline Quickstart](docs/OptimaRunningDataPipelineQuickstartGuide.md)

## Development

* [Hive, Presto and Spark Quickstart](docs/HivePrestoSparkQuickstart.md)
* [Parquet Tools Quickstart](docs/ParquetToolsQuickStart.md)



# Research Analysis
The following analysis was completed during prototype development

* [AWS Glue vs EMR Cost Analysis](https://docs.google.com/spreadsheets/d/149dqTSQxAXqReuX0EdiuAZAfonjl-WJRPzTxs8ubX9w/edit#gid=0).
* [Parquet Schema Updates](docs/ParquetSchemaUpdates.md)
* [Explore Partitions, Columnar Storage and Compressions](docs/ExplorePartitionsColumnarCompressions.md)
* [Hive and Partitions](docs/Hive_And_Partitions.md)
* [Timestamp int64 vs int96](docs/Timestamp_int64_vs_int96.md)
* [Understanding ETL Conversion, Storage Formats and Schemas](docs/Understanding_ETL_Conversion_Storage_Formats_And_Schemas.md)


# References

## Articles

* [Martin Fowler - Data Lake](https://martinfowler.com/bliki/DataLake.html)
* [The different type of Spark functions (custom transformations, column functions, UDFs)](https://medium.com/@mrpowers/the-different-type-of-spark-functions-custom-transformations-column-functions-udfs-bf556c9d0ce7)
* [Chaining Custom DataFrame Transformations in Spark](https://medium.com/@mrpowers/chaining-custom-dataframe-transformations-in-spark-a39e315f903c)
* [Spark User Defined Functions (UDFs)](https://medium.com/@mrpowers/spark-user-defined-functions-udfs-6c849e39443b)
* [Split 1 column into 3 columns in Spark Scala](https://stackoverflow.com/questions/39255973/split-1-column-into-3-columns-in-spark-scala)
* [Data Cleansing and Exploration for the New Era with Optimus](https://medium.com/@faviovazquez/data-cleansing-and-exploration-for-the-new-era-with-optimus-7a2ea2d996f8)
* [Spark + Parquet In Depth](https://databricks.com/session/spark-parquet-in-depth)
* [Resume AWS Step Functions from Any State](https://aws.amazon.com/blogs/compute/resume-aws-step-functions-from-any-state/)

## Case Studies

* [Delivering High Quality Analytics at Netflix](https://www.youtube.com/watch?v=nMyuCdqzpZc)

## Tooling

* [Optimus](https://hioptimus.com/) - Agile Data Science Workflows made easy with Python and Spark.
* [Quick Start With Apache Livy](https://dzone.com/articles/quick-start-with-apache-livy)

## Bugs/Problems

* [Hive - Implement table property to address Parquet int96 timestamp bug](https://issues.apache.org/jira/browse/HIVE-12767)
* [Spark - Support Parquet logical type TIMESTAMP_MILLIS](https://issues.apache.org/jira/browse/SPARK-10364)
* [INT96 should be marked as deprecated](https://issues.apache.org/jira/browse/PARQUET-323)